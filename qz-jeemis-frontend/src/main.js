// Vue
import Vue from 'vue'
import App from './App'
// store
import store from '@/store/index'
// 多国语
import i18n from './i18n'
// 图标
// import 'font-awesome/css/font-awesome.css'
// VXE表格
import 'xe-utils'
import VXETable from 'vxe-table'
import 'vxe-table/lib/index.css'
// 核心插件
import d2Admin from '@/plugin/d2admin'
// [ 可选插件组件 ] 右键菜单
import contentmenu from 'v-contextmenu'
import 'v-contextmenu/dist/index.css'
// [必须组件] ag-grid
import 'ag-grid-community' // ag-grid
// [可选组件]iview
import 'view-design/dist/styles/iview.css'
import { Split } from 'view-design'
//   [ 可选插件组件 ] 实现对话框拖拽的指令
import '@/libs/directive.js'
// 菜单和路由设置
import router from './router'
import 'element-ui/lib/theme-chalk/index.css'
// [ 可选插件组件 ] 图表
import VCharts from 'v-charts'
import setting from '@/setting.js'
import VueNativeSock from 'vue-native-websocket'
import XdhMap from 'xdh-map'
import 'xdh-map/lib/xdhmap.css'
import VueDragResize from 'vue-drag-resize'
import AMap from 'vue-amap' // 引入高德地图

const imghttp = 'http://localhost:80'
// const imghttp = 'http://10.2.98.122:80'
Vue.prototype.$imghttp = imghttp
Vue.component('vue-drag-resize', VueDragResize)
Vue.use(AMap)
AMap.initAMapApiLoader({
  key: '07b3418da963a47dfbdc03db605f0e81',
  plugin: ['AMap.Autocomplete', 'AMap.DistrictSearch', 'AMap.PlaceSearch', 'AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.Geolocation', 'AMap.Geocoder', 'AMap.AMapManager', 'AMap.Marker'],
  v: '1.4.14'
})
// import Antd from 'ant-design-vue'
// import 'ant-design-vue/dist/antd.css'
// Vue.use(Antd)

Vue.use(XdhMap)
Vue.use(VueNativeSock, `${window.SITE_CONFIG['imURL']}`, {
  connectManually: true, // 手动
  // reconnection: true, // (Boolean)是否自动重连，默认false?
  reconnectionAttempts: 5, // 重连次数
  reconnectionDelay: 3000 // 再次重连等待时常(1000)
})
//  日格式化过滤器
Vue.use(require('vue-moment'))
Vue.component('iv-Split', Split)
Vue.use(VXETable)
// 核心插件
Vue.use(d2Admin, { store })
// 可选插件组件
Vue.use(VCharts)
Vue.use(contentmenu)

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
  created () {
    //  迁移到 routes/index.js
  },
  data () {
    return {
      heartCheckInterval: undefined
    }
  },
  mounted () {
    this.$store.dispatch('d2admin/chat/connectSocket')
    this.$options.sockets.onmessage = async (data) => {
      await this.$store.dispatch('d2admin/chat/add', data)
    }
    this.heartCheck()
    // 展示系统信息
    this.$store.commit('d2admin/releases/versionShow')
    // 用户登录后从数据库加载一系列的设置
    this.$store.dispatch('d2admin/account/load')
    // 获取并记录用户 UA
    this.$store.commit('d2admin/ua/get')
    // 初始化全屏监听
    this.$store.dispatch('d2admin/fullscreen/listen')
  },
  methods: {
    heartCheck () { // 重置心跳
      this.heartCheckInterval && clearInterval(this.heartCheckInterval)
      if (!setting.websocket.enable) return
      this.heartCheckInterval = setInterval(() => {
        // console.info(this.$socket)
        if (this.$socket && this.$socket.readyState === 1) {
          this.$socket.send('1')
        } else {
          this.$store.dispatch('d2admin/chat/connectSocket')
        }
      }, 5 * 1000)
    }
  },
  watch: {
    // 检测路由变化切换侧边栏内容
    '$route.matched': {
      handler (matched) {
        if (this.layoutHeader && this.layoutHeader.headMenu && matched.length > 0) {
          let fullAside = this.$store.state.d2admin.menu.fullAside
          const _side = fullAside.filter(menu => menu.path === matched[0].path)
          this.$store.commit('d2admin/menu/asideSet', _side.length > 0 ? _side[0].children || [] : [])
        }
      },
      immediate: true
    }
  },
  computed: {
    layoutHeader () {
      return setting.layoutHeader
    }
  }
}).$mount('#app')
