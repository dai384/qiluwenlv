/**
 * 站内发信(SysMessageSend)表API
 *
 * @author sdmq
 * @since 2019-11-13 17:14:51
 */

import util from '@/libs/util'
import qs from 'qs'
import request from '@/plugin/axios'

export function page (data) {
  return request({
    url: 'platform/message/page',
    method: 'post',
    data
  })
}

export function list () {
  return request({
    url: 'message/sysmessagesend/list',
    method: 'get'
  })
}

export function info (id) {
  return request({
    url: `message/sysmessagesend/${id}`,
    method: 'get'
  })
}

export function save (data, method) {
  return request({
    url: 'message/sysmessagesend',
    method: method,
    data
  })
}

export function update (data) {
  return request({
    url: 'message/sysmessagesend',
    method: 'put',
    data
  })
}

export function remove (data) {
  return request({
    url: `message/sysmessagesend`,
    method: 'delete',
    data
  })
}

export function exportXls (data) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    ...data
  })
  let url = '/message/sysmessagesend/export'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}

export function download (id) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    'fileId': id
  })
  let url = '/file/download'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}
