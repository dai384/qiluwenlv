/**
 * 节目表(Wl_Progrem)表API
 *
 * @author sdmq
 * @since 2020-06-10 17:52:09
 */

import util from '@/libs/util'
import qs from 'qs'
import request from '@/plugin/axios'

export function page (data) {
  return request({
    url: 'programHistory/programHistory/page',
    method: 'post',
    data
  })
}


export function list () {
  return request({
    url: 'program/wlnewprogram/list',
    method: 'get'
  })
}

export function info (id) {
  return request({
    url: `program/wlnewprogram/${id}`,
    method: 'get'
  })
}

export function save (data, method) {
  return request({
    url: 'program/wlnewprogram',
    method: method,
    data
  })
}

export function update (data) {
  return request({
    url: 'program/wlnewprogram',
    method: 'put',
    data
  })
}



export function getCustomerlist () {
  return request({
    url: 'customer/wlcustomer/list',
    method: 'get'
  })
}

