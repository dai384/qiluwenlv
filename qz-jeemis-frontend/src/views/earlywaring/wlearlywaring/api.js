/**
 * 智能箱报警数据推送接收表(WlEarlyWaring)表API
 *
 * @author sdmq
 * @since 2020-08-14 09:44:39
 */

import util from '@/libs/util'
import qs from 'qs'
import request from '@/plugin/axios'

export function page (data) {
  return request({
    url: 'earlywaring/wlearlywaring/page',
    method: 'post',
    data
  })
}

export function list () {
  return request({
    url: 'earlywaring/wlearlywaring/list',
    method: 'get'
  })
}

export function info (id) {
  return request({
    url: `earlywaring/wlearlywaring/${id}`,
    method: 'get'
  })
}

export function save (data, method) {
  return request({
    url: 'earlywaring/wlearlywaring',
    method: method,
    data
  })
}

export function update (data) {
  return request({
    url: 'earlywaring/wlearlywaring',
    method: 'put',
    data
  })
}

export function remove (data) {
  return request({
    url: `earlywaring/wlearlywaring`,
    method: 'delete',
    data
  })
}

export function exportXls (data) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    ...data
  })
  let url = '/earlywaring/wlearlywaring/export'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}

export function download (id) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    'fileId': id
  })
  let url = '/file/download'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}