/**
 * (WlEquipmentinfo)表API
 *
 * @author sdmq
 * @since 2020-05-27 09:07:02
 */

import util from '@/libs/util'
import qs from 'qs'
import request from '@/plugin/axios'

export function page (data) {
  return request({
    url: 'terminal/equipmentInfo/page',
    method: 'post',
    data
  })
}

export function list () {
  return request({
    url: 'terminal/equipmentInfo/list',
    method: 'get'
  })
}
// 政区列表
export function regionList (data) {
  return request({
    url: 'unit/wlregion/list',
    method: 'post',
    data
  })
}

export function info (id) {
  return request({
    url: `terminal/equipmentInfo/${id}`,
    method: 'get'
  })
}

export function save (data, method) {
  return request({
    url: 'terminal/equipmentInfo',
    method: method,
    data
  })
}

export function update (data) {
  return request({
    url: 'terminal/equipmentInfo',
    method: 'put',
    data
  })
}

export function remove (data) {
  return request({
    url: `terminal/equipmentInfo`,
    method: 'delete',
    data
  })
}

export function exportXls (data) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    ...data
  })
  let url = '/terminal/equipmentInfo/export'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}

export function download (id) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    'fileId': id
  })
  let url = '/file/download'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}

export function saveAll (data) {
  return request({
    url: 'terminal/equipmentInfo/saveAll',
    method: 'post',
    headers: { 'Content-Type': 'multipart/form-data' },
    data
  })
}

export function start (data) {
  return request({
    url: 'program/wlnewprogram/start',
    method: 'post',
    data
  })
}

export function stop (data) {
  return request({
    url: 'program/wlnewprogram/stop',
    method: 'post',
    data
  })
}
