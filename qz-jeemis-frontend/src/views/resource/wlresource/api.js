/**
 * 资源表(WlResource)表API
 *
 * @author sdmq
 * @since 2020-06-05 17:01:24
 */

import util from '@/libs/util'
import qs from 'qs'
import request from '@/plugin/axios'

export function page (data) {
  return request({
    url: 'resource/wlresource/page',
    method: 'post',
    data
  })
}

export function list () {
  return request({
    url: 'resource/wlresource/list',
    method: 'get'
  })
}

export function info (id) {
  return request({
    url: `resource/wlresource/${id}`,
    method: 'get'
  })
}

export function save (data, method) {
  return request({
    url: 'resource/wlresource',
    method: method,
    data
  })
}

export function saveImg (data, method) {
  return request({
    url: 'resource/wlresource/saveImg',
    method: method,
    // headers: { 'content-type': 'multipart/form-data' },
    data
  })
}

export function saveAudio (data, method) {
  return request({
    url: 'resource/wlresource/saveAudio',
    method: method,
    data
  })
}

export function saveVideo (data, method) {
  return request({
    url: 'resource/wlresource/saveVideo',
    method: method,
    data
  })
}

export function update (data) {
  return request({
    url: 'resource/wlresource',
    method: 'put',
    data
  })
}

export function remove (data) {
  return request({
    url: `resource/wlresource`,
    method: 'delete',
    data
  })
}

export function exportXls (data) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    ...data
  })
  let url = '/resource/wlresource/export'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}

export function download (id) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    'fileId': id
  })
  let url = '/file/download'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}
