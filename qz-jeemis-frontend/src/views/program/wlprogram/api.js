/**
 * 节目表(Wl_Progrem)表API
 *
 * @author sdmq
 * @since 2020-06-10 17:52:09
 */

import util from '@/libs/util'
import qs from 'qs'
import request from '@/plugin/axios'

export function page (data) {
  return request({
    url: 'program/wlprogram/page',
    method: 'post',
    data
  })
}

export function templateList () {
  return request({
    url: 'template/wltemplate/list',
    method: 'get'
  })
}

export function selectReasourceByType (type) {
  return request({
    url: `resource/wlresource/selectReasourceByType/${type}`,
    method: 'get'
  })
}

export function list () {
  return request({
    url: 'program/wlprogram/list',
    method: 'get'
  })
}

export function info (id) {
  return request({
    url: `program/wlprogram/${id}`,
    method: 'get'
  })
}

export function templateInfo (id) {
  return request({
    url: `template/wltemplate/${id}`,
    method: 'get'
  })
}

export function save (data, method) {
  return request({
    url: 'program/wlprogram',
    method: method,
    data
  })
}

export function update (data) {
  return request({
    url: 'program/wlprogram',
    method: 'put',
    data
  })
}

export function remove (data) {
  return request({
    url: `program/wlprogram`,
    method: 'delete',
    data
  })
}

export function exportXls (data) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    ...data
  })
  let url = '/program/wlprogram/export'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}

export function download (id) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    'fileId': id
  })
  let url = '/file/download'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}
