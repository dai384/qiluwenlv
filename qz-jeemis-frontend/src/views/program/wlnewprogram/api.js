/**
 * 节目表(Wl_Progrem)表API
 *
 * @author sdmq
 * @since 2020-06-10 17:52:09
 */

import util from '@/libs/util'
import qs from 'qs'
import request from '@/plugin/axios'

export function page (data) {
  return request({
    url: 'program/wlnewprogram/page',
    method: 'post',
    data
  })
}

export function start (data) {
  return request({
    url: 'program/wlnewprogram/start',
    method: 'post',
    data
  })
}

export function termianlPage (data) {
  return request({
    url: 'terminal/equipmentInfo/page',
    method: 'post',
    data
  })
}

export function templateList () {
  return request({
    url: 'template/wltemplate/list',
    method: 'get'
  })
}

export function getCustomerlist () {
  return request({
    url: 'customer/wlcustomer/list',
    method: 'get'
  })
}

export function selectReasourceByType (type) {
  return request({
    url: `resource/wlresource/selectReasourceByType/${type}`,
    method: 'get'
  })
}

export function list () {
  return request({
    url: 'program/wlnewprogram/list',
    method: 'get'
  })
}

export function info (id) {
  return request({
    url: `program/wlnewprogram/${id}`,
    method: 'get'
  })
}

export function templateInfo (id) {
  return request({
    url: `template/wltemplate/${id}`,
    method: 'get'
  })
}

export function save (data, method) {
  return request({
    url: 'program/wlnewprogram',
    method: method,
    data
  })
}

export function update (data) {
  return request({
    url: 'program/wlnewprogram',
    method: 'put',
    data
  })
}

export function remove (data) {
  return request({
    url: `program/wlnewprogram`,
    method: 'delete',
    data
  })
}

export function exportXls (data) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    ...data
  })
  let url = '/program/wlnewprogram/export'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}

export function download (id) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    'fileId': id
  })
  let url = '/file/download'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}
