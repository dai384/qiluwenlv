/**
 * 客户信息表(WlCustomer)表API
 *
 * @author sdmq
 * @since 2020-07-14 13:50:09
 */

import util from '@/libs/util'
import qs from 'qs'
import request from '@/plugin/axios'

export function page (data) {
  return request({
    url: 'customer/wlcustomer/page',
    method: 'post',
    data
  })
}

export function list () {
  return request({
    url: 'customer/wlcustomer/list',
    method: 'get'
  })
}

export function info (id) {
  return request({
    url: `customer/wlcustomer/${id}`,
    method: 'get'
  })
}

export function save (data, method) {
  return request({
    url: 'customer/wlcustomer',
    method: method,
    data
  })
}

export function update (data) {
  return request({
    url: 'customer/wlcustomer',
    method: 'put',
    data
  })
}

export function remove (data) {
  return request({
    url: `customer/wlcustomer`,
    method: 'delete',
    data
  })
}

export function exportXls (data) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    ...data
  })
  let url = '/customer/wlcustomer/export'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}

export function download (id) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    'fileId': id
  })
  let url = '/file/download'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}
