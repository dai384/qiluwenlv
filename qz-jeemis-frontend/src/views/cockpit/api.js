/**
 * 表API
 *
 * @author sdmq
 * @since 2020-06-10 17:54:02
 */
import util from '@/libs/util'
import qs from 'qs'
import request from '@/plugin/axios'
export function page (data) {
  return request({
    url: 'cockpit/wlresource/page',
    method: 'post',
    data
  })
}
export function pageByResult (data) {
  return request({
    url: 'cockpit/wlresource/pageByResult',
    method: 'post',
    data
  })
}
// export function page (data) {
//   return request({
//     url: 'template/wl_template/page',
//     method: 'post',
//     data
//   })
// }
export function list () {
  return request({
    url: 'template/wl_template/list',
    method: 'get'
  })
}

export function info (id) {
  return request({
    url: `cockpit/wlresource/${id}`,
    method: 'get'
  })
}

export function queryCount (index) {
  return request({
    url: `/resource/wlresource/statistic`,
    method: 'get'
  })
}

export function queryNumber () {
  return request({
    url: `/cockpit/wlresource/queryNumber`,
    method: 'get'
  })
}

export function queryTasksCount () {
  return request({
    url: `/cockpit/wlresource/queryTasksCount`,
    method: 'get'
  })
}

export function save (data, method) {
  return request({
    url: 'template/wl_template',
    method: method,
    data
  })
}

export function update (data) {
  return request({
    url: '/cockpit/wlresource',
    method: 'put',
    data
  })
}

export function remove (data) {
  return request({
    url: `template/wl_template`,
    method: 'delete',
    data
  })
}

export function exportXls (data) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    ...data
  })
  let url = '/template/wl_template/export'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}

export function download (id) {
  let params = qs.stringify({
    'token': util.cookies.get('token'),
    'fileId': id
  })
  let url = '/file/download'
  window.location.href = `${window.SITE_CONFIG['apiURL']}${url}?${params}`
}
