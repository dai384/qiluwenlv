// 菜单 顶栏  以后改成动态从数据库中取回
import _ from 'lodash'

let headMenu = [
  {
    path: '/index',
    title: ' 首页',
    icon: 'home'
  }

// ,
// {
//   path: '/index5',
//   title: '菜单格式',
//   icon: 'align-justify'
//   // iconSvg: 'boot-2.png'
// }
]
// if (process.env.NODE_ENV === 'development' && process.env.VUE_APP_DEBUG === '1') {
//   let testMenu = [{
//     path: '/debug',
//     title: '开发工具',
//     icon: 'flask',
//     children: []
//   }]
//   loadModuleMenus(testMenu[0].children)
//   headMenu = [...headMenu, ...testMenu]
// }

/**
 * 装载子目录的菜单
 * @param menus
 */
function loadModuleMenus (menus) {
  const req = context => context.keys().map(context)
  return _.flatten(req(require.context('@/views', true, /menu\.js$/))
    .filter(e => e.default)
    .map(e => e.default))
    .forEach(e => menus.push(e))
}

export default headMenu
