package com.qzsoft.demomis.command.cleaner;

import cn.hutool.json.JSONUtil;
import com.qzsoft.jeemis.platform.im.JeeImMessageHelper;
import com.qzsoft.jeemis.task.task.ITask;
import lombok.extern.slf4j.Slf4j;
import novj.publ.api.NovaOpt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 定时关机
 */
@Slf4j
@Component("offTask")
public class offTask implements ITask {

	@Override
	public void run(String params) {
		System.out.println("进入定时关机定时器：");

	}
}
