package com.qzsoft.demomis.command.cleaner;

import com.qzsoft.jeemis.task.task.ITask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 定时开机
 */
@Slf4j
@Component("NovaTest")
public class onTask implements ITask {

    @Override
    public void run(String params) {
        System.out.println("进入定时开机定时器：");

    }
}
