package com.qzsoft.demomis.modules.cockpit.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qzsoft.demomis.modules.cockpit.entity.LdCockpitEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LdCockpitDao extends BaseMapper<LdCockpitEntity> {
        Integer selectPicCount(Integer index);//图片数量
        Integer selectAudCount(Integer index);//音频数量
        Integer selectVidCount(Integer index);//视频数量
        Integer selectTextCount(Integer index);//文本数量
        Integer selectLbCount(Integer index);//直播数量

        Integer selectEquCount();//终端数量
        Integer selectResCount();//资源数量
        Integer selectTemCount();//模板数量
        Integer selectProCount();//节目数量
        Integer selectTasCount();//待办数量
}
