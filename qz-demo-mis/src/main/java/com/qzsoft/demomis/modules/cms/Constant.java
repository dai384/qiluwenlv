package com.qzsoft.demomis.modules.cms;

public class Constant {

    /**
     * 情报板故障说明--天津光电比特
     */
    public final static String[] bugArr = new String[]{"控制器故障", "显示模组故障",
            "", "单像素管故障", "检测系统故障", "输入220V交流电故障", "防雷器故障", "光敏部件故障",
            "温度异常故障"};
    /**
     * 情报板故障说明--天津光电比特
     */
    public final static String[] bugArr2 = new String[]{"温度异常", "风扇异常",
            "箱体电压异常", "门开关异常", "亮度异常", "防雷器异常", "防雷器故障", "光敏部件故障",
            "温度异常故障"};

    /**
     * 是否验证情报板返回数据的正确性，做crc16校验
     */
    public static boolean checkResponse = false;

    /**
     * 执行命令的超时时间
     */
    public final static int TIMEOUT = 60000;

    /**
     * 默认播放列表文件名
     */
    public final static String DEFAULT_PLAYLIST = "PLAY.LST";

    /**
     * 成功
     */
    public final static int SUCCESS = 0;
}
