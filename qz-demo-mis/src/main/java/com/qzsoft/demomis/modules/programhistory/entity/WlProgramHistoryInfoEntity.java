package com.qzsoft.demomis.modules.programhistory.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;


/**
 * 节目时长(WlProgram)附属表实体类
 *
 * @author sdmq
 * @since 2020-06-30 10:16:58
 */
@ApiModel(value ="节目表")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("wl_program_history_info")
public class WlProgramHistoryInfoEntity extends Model<WlProgramHistoryInfoEntity> {
    private static final long serialVersionUID = -80698143780173771L;
    /**
     *id
     */
    @Excel(name = "id")
    @ApiModelProperty(value = "id")
    private String id;
    /**
     *节目ID
     */
    @Excel(name = "节目ID")
    @ApiModelProperty(value = "节目ID")
    private String programId;

    /**
     *节目名称
     */
    @Excel(name = "节目名称")
    @ApiModelProperty(value = "节目名称")
    private String name;

    /**
     *终端id
     */
    @Excel(name = "终端id")
    @ApiModelProperty(value = "终端id")
    private String equipmentId;
    /**
     *开播时间
     */
    @Excel(name = "开播时间" , format = "yyyy-MM-dd")
    @ApiModelProperty(value = "开播时间")
    private Date onDatetime;
    /**
     *关播时间
     */
    @Excel(name = "关播时间" , format = "yyyy-MM-dd")
    @ApiModelProperty(value = "关播时间")
    private Date offDatetime;

    /**
     *时长
     */
    @Excel(name = "时长" , format = "yyyy-MM-dd")
    @ApiModelProperty(value = "时长")
    private Integer timeCount;

    /**
     *创建人
     */
    @Excel(name = "创建人")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private String creator;
    /**
     *创建时间
     */
    @Excel(name = "创建时间" , format = "yyyy-MM-dd")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
    /**
     *编辑人
     */
    @Excel(name = "编辑人")
    @ApiModelProperty(value = "编辑人")
    private String updator;
    /**
     *编辑时间
     */
    @Excel(name = "编辑时间" , format = "yyyy-MM-dd")
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "编辑时间")
    private Date updateDate;

}