package com.qzsoft.demomis.modules.cms.sansi.request;

import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 重启控制板请求
 *
 * @author frank
 */
public class ResetDisplayRequest extends Request {

    public ResetDisplayRequest() throws CmsException {
        this.type = int2bytes(81, 2);
        this.address = int2bytes(0, 2);
    }

    public ResetDisplayRequest(int address) throws CmsException {
        this.type = int2bytes(81, 2);
        this.address = int2bytes(address, 2);
    }

    @Override
    protected void setData() throws CmsException, IOException {
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
