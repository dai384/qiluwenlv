/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.permission.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.permission.user.dto.PasswordDTO;
import com.qzsoft.demomis.modules.permission.user.dto.SysUserDTO;
import com.qzsoft.demomis.repository.sys.entity.SysUserEntity;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * 系统用户
 * 
 * @author Mark sunlightcs@gmail.com
 */
public interface SysUserService {

	/**
	 * page
	 * @param params
	 * @return
	 */
	IPage<SysUserEntity> page(Map<String, Object> params);

	/**
	 * 取列表
	 * @param
	 * @return
	 */
	List<SysUserEntity> list(Map<String, Object> params);

	SysUserDTO get(Long id);

	/**
	 *
	 * @param username
	 * @return
	 */
	SysUserDTO getByUsername(String username);

	void save(SysUserDTO dto);

	void update(SysUserDTO dto);

	void delete(Long[] ids);

	/**
	 * 修改密码
	 * @param passwordDTO
	 */
	void updatePassword(PasswordDTO passwordDTO);


	/**
	 * 批量删除用户
	 * @param idList
	 */
	void deleteBatchIds(Collection<? extends Serializable> idList);
	/**
	 *  根据部门ID，查询用户数
	 * @param deptId
	 * @return
	 */
	int getCountByDeptId(String deptId);
}
