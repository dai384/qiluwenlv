package com.qzsoft.demomis.modules.terminal.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.nova.NovaProgram;
import com.qzsoft.jeemis.common.utils.Result;
import com.qzsoft.jeemis.platform.security.user.SecurityUser;
import org.apache.ibatis.session.SqlSession;
import com.qzsoft.demomis.modules.terminal.dao.TerminalDao;
import com.qzsoft.demomis.modules.terminal.entity.TerminalEntity;
import com.qzsoft.demomis.modules.terminal.service.TerminalService;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ExcelUtils;
import com.qzsoft.jeemis.common.utils.ConvertDictUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * (WlEquipmentinfo)表服务实现类
 *
 * @author sdmq
 * @since 2020-05-27 09:07:00
 */
@Service("wlEquipmentinfoService")
public class TerminalServiceImpl extends BaseService implements TerminalService {
    private final SqlSession sqlSession;
    @Autowired
	private TerminalDao terminalDao;

	private NovaProgram novaProgram = new NovaProgram();

	@Value("${terminalUploadPath}")
	private String terminalUploadPath;

	public TerminalServiceImpl(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}
	
	/**
	 * WlEquipmentinfo数据分页
	 * @param params
	 * @return IPage
	 */
	@Override
	public IPage<TerminalEntity> page(Map<String, Object> params) {
		IPage<TerminalEntity> page =this.getPage(params,Constant.CREATE_DATE,false, TerminalEntity.class) ;
		QueryWrapper<TerminalEntity> queryWrapper =this.getQueryWrapper(params, TerminalEntity.class);
		IPage<TerminalEntity> pageData = terminalDao.selectPage(page, queryWrapper);
//		IPage<TerminalEntity> list = terminalDao.selectPageOwn(page, queryWrapper);
		pageData= ConvertDictUtils.formatDicDataPage(pageData);
		return pageData;
	}
    /**
	 * WlEquipmentinfo数据列表
	 * @param params
	 * @return
	 */
	@Override
	public List<TerminalEntity> list(Map<String, Object> params) {
		QueryWrapper<TerminalEntity> queryWrapper = this.getQueryWrapper(params, TerminalEntity.class);
		List<TerminalEntity> entityList = terminalDao.selectList(queryWrapper);
		entityList=ConvertDictUtils.formatDicDataList(entityList);
		return entityList;
	}

	@Override
	public TerminalEntity get(String id) {
		TerminalEntity entity = terminalDao.selectByIdBySelf(id);
		return entity;
	}
    /**
	 * WlEquipmentinfo保存
	 * createDate creator
	 * updateDate updater 
	 * 字段会自动注入值 如不需要注入请修改WlEquipmentinfoEntity
	 * @param entity
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(TerminalEntity entity) {
		entity.setCreator(SecurityUser.getUser().getId().toString());
		terminalDao.insert(entity);

		// ip地址
        String ip = entity.getIp();
		// 开关机
        String onOff = entity.getOnOff();
        // 亮度调节：0：自动，其他数值百分比
		int light = 0;
//        int light = entity.getLight();
		// 自动播放
//		String autoswitch = entity.getAutoswitch();
		String autoswitch = "1";
//		String programId = entity.getProgram();
		// 根据program查出资源类型
		if (onOff.equals("1")) {
			// 开机及相关操作
//			novaProgram.interfaceCall(ip, "1", light, autoswitch);
		} else {
			// 关机
//			novaProgram.interfaceCall(ip, "2", light, autoswitch);
		}
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(TerminalEntity entity) {
		entity.setUpdator(SecurityUser.getUser().getId().toString());
		terminalDao.updateById(entity);

		// ip地址
		String ip = entity.getIp();
		// 开关机
		String onOff = entity.getOnOff();
		// 亮度调节：0：自动，其他数值百分比
//		int light = entity.getLight();
		// 自动播放
//		String autoswitch = entity.getAutoswitch();
		String programId = entity.getProgram();
		if (onOff.equals("1")) {
			// 开机及相关操作
			novaProgram.interfaceCall(ip, "1", 1, "");
		} else {
			// 关机
			novaProgram.interfaceCall(ip, "2", 1, "");
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(String[] ids) {
		terminalDao.deleteBatchIds(Arrays.asList(ids));
	}
	@Override
	public void exportXls(Map<String, Object> params , HttpServletResponse response)  {
		List<TerminalEntity> list = this.list(params);
		ExcelUtils.exportExcelToTarget(response, "导出列表", list, TerminalEntity.class);
	}


	/**
	 * 文件导入
	 * @param file
	 * @return
	 */
	@Override
	public Result readExcelFile(MultipartFile file , HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
		// 返回值
		String res = "批量增加成功";
		//上传文件名
		String filename=file.getOriginalFilename();

		// 判空并保存本地某位置
		if(!file.isEmpty())
		{
			String  path = httpServletRequest.getServletContext().getRealPath("");
			System.out.println("上传excel获取的路径=================================================================" + path);
			//将时间设置为一层路径
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String date = sdf.format(d);
			path = path+date;
			File filepath=new File(path,filename);
			//判断路劲是否存在，如果不存在就创建一个
			if (!filepath.getParentFile().exists()){
				filepath.getParentFile().mkdirs();
			}
			//将上传文件保存到一个目标文件当中
			try {
				file.transferTo(new File(path+ File.separator+filename));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else {
			res = "文件为空，请重新上传";
		}

		// 读取excel值
		List<TerminalEntity> excelDatas = new ArrayList<>();
		// 获取文件流
		try{
			InputStream ins = file.getInputStream();
			// 文件类
			Workbook wb = null;
			// 文件流读取
			wb = new HSSFWorkbook(ins);
			// 读取文件流中sheet
			Sheet sheet = wb.getSheetAt(0);
			if (null != sheet) {
				// 读取sheet单元内容，line为行，从第几行开始读取，行编号从0开始
				for (int line = 1; line <= sheet.getLastRowNum(); line++) {
					TerminalEntity datas = new TerminalEntity();
					// 每行行内容
					Row row = sheet.getRow(line);
					// 读取行中列内容，列从0开始编号
					datas.setSn( row.getCell(0).getStringCellValue());
					datas.setControllerType( row.getCell(1).getStringCellValue() );
					datas.setStatus(  row.getCell(2).getStringCellValue() );
					datas.setType(  row.getCell(3).getStringCellValue() );
					datas.setLongitude( Double.parseDouble( row.getCell(4).getStringCellValue() ));
					datas.setLatitude( Double.parseDouble( row.getCell(5).getStringCellValue() ));
					datas.setProvince(  row.getCell(6).getStringCellValue() );
					datas.setCity(  row.getCell(7).getStringCellValue() );
					datas.setDistrice(  row.getCell(8).getStringCellValue() );
					datas.setStation(  row.getCell(9).getStringCellValue() );
					datas.setOnOff(  row.getCell(10).getStringCellValue() );
					datas.setVoice( Integer.parseInt(row.getCell(12).getStringCellValue() ));
					datas.setAutoswitch( row.getCell(13).getStringCellValue()  );
					datas.setOnDate( row.getCell(14).getStringCellValue() );
					datas.setOffDate( row.getCell(15).getStringCellValue() );
					datas.setRemark(  row.getCell(16).getStringCellValue() );
					excelDatas.add(datas);
				}
			}
			// 保存至数据库
			if( excelDatas.size() > 0 ){
				int result = 0;
				result = terminalDao.saveAll(excelDatas);
				if(result < sheet.getLastRowNum()){
					res = "数据入库有误，请联系管理员";
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			res = "文件读取失败";
		}

		return new Result().ok(res);
	}
}