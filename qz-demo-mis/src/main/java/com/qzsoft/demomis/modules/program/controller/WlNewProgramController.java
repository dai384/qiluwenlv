package com.qzsoft.demomis.modules.program.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.program.entity.WlProgramEntity;
import com.qzsoft.demomis.modules.program.service.WlProgramService;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.utils.Result;
import com.qzsoft.jeemis.common.validator.AssertUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 节目表(Wl_Program)表控制层
 *
 * @author sdmq
 * @since 2020-06-10 17:45:47
 */
@RestController
@RequestMapping("program/wlnewprogram")
@Api(tags="节目表")
public class WlNewProgramController {
    @Autowired
    private WlProgramService wlProgramService;

	@PostMapping("page")
	@ApiOperation("分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = "ge_createDate", value = "起始创建时间", paramType = "query", dataType="Date"),
			@ApiImplicitParam(name = "le_createDate", value = "截止创建时间", paramType = "query", dataType="Date")
	})
	@RequiresPermissions("program:wlprogram:page")
	public Result<IPage<WlProgramEntity>> page(@ApiIgnore @RequestBody Map<String, Object> params){
		IPage<WlProgramEntity> page = wlProgramService.page(params);

		return new Result<IPage<WlProgramEntity>>().ok(page);
	}

	@GetMapping("list")
	@ApiOperation("列表")
	@RequiresPermissions("program:wlprogram:list")
	public Result<List<WlProgramEntity>> list(@ApiIgnore @RequestParam Map<String, Object> params){
		List<WlProgramEntity> data = wlProgramService.list(params);

		return new Result<List<WlProgramEntity>>().ok(data);
	}


	@GetMapping("export")
	@ApiOperation("导出")
	@LogOperation("导出")
	@RequiresPermissions("program:wlprogram:export")
	public void export(@ApiIgnore @RequestParam Map<String, Object> params , HttpServletResponse response) throws Exception {
		wlProgramService.exportXls(params,response);
	}



	@GetMapping("{id}")
	@ApiOperation("信息")
	@RequiresPermissions("program:wlprogram:info")
	public Result<Map<String, Object>> newGet(@PathVariable("id") String id){

		Map<String, Object> map = wlProgramService.newGet(id);
		return new Result<Map<String, Object>>().ok(map);
	}

	@PostMapping
	@ApiOperation("保存")
	@LogOperation("保存")
	@RequiresPermissions("program:wlprogram:save")
	public Result newSave(@RequestBody Map<String, Object> map){

		wlProgramService.newSave(map);

		return new Result();
	}

	@PutMapping
	@ApiOperation("修改")
	@LogOperation("修改")
	@RequiresPermissions("program:wlprogram:update")
	public Result newUpdate(@RequestBody Map<String, Object> map){

		wlProgramService.newUpdate(map);

		return new Result();
	}

	@DeleteMapping
	@ApiOperation("删除")
	@LogOperation("删除")
	@RequiresPermissions("program:wlprogram:delete")
	public Result newDelete(@RequestBody String[] ids){
		//效验数据
		AssertUtils.isArrayEmpty(ids, "id");

		wlProgramService.newDelete(ids);

		return new Result();
	}

	@PostMapping("/start")
	@ApiOperation("节目发布")
	@LogOperation("节目发布")
	public Result programStart(@RequestBody Map<String, Object> map){
		String result = "";
		result = wlProgramService.programStart(map);

		return new Result().ok(result);
	}

}