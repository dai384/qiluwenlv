package com.qzsoft.demomis.modules.cockpit.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@ApiModel(value ="资源表")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("wl_resource")
public class LdCockpitEntity extends Model<LdCockpitEntity> {
    private static final long serialVersionUID = 766819277934455759L;
    /**
     *主键主键
     */
    @TableId(value ="id",type = IdType.ID_WORKER) //主键ID_WORKER策略
    @NotNull(message="{id.require}", groups = UpdateGroup.class)
    @ApiModelProperty(value = "主键主键")
    private Long id;
    /**
     *名称
     */
    @ApiModelProperty(value = "名称")
    private String name;
    /**
     *分组
     */
    @ApiModelProperty(value = "分组")
    private String grouping;
    /**
     *1:图片，2：音频，3：视频，4：直播路径，5：文本
     */
    @ApiModelProperty(value = "1:图片，2：音频，3：视频，4：直播路径，5：文本")
    private String type;
    /**
     *路径
     */
    @ApiModelProperty(value = "路径")
    private String path;
    /**
     *类型
     */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String suffix;
    /**
     *图片集合关联id
     */
    @ApiModelProperty(value = "图片集合关联id")
    private Long linkId;
    /**
     *直播路径
     */
    @Excel(name = "直播路径")
    @ApiModelProperty(value = "直播路径")
    private String urlPath;
    /**
     *文本内容，限2k字
     */
    @ApiModelProperty(value = "文本内容，限2k字")
    private String textContent;
    /**
     *资源时长（s）
     */
    @ApiModelProperty(value = "资源时长（s）")
    private String duration;
    /**
     *是否为关注资源（1为关注，0为不关注）
     */
    @ApiModelProperty(value = "是否为关注资源（1为关注，0为不关注）")
    private String marker;
    /**
     *备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
    /**
     *创建人
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private String creator;
    /**
     *创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
    /**
     *编辑人
     */
    @ApiModelProperty(value = "编辑人")
    private String updator;
    /**
     *编辑时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "编辑时间")
    private Date updateDate;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
