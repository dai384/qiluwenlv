package com.qzsoft.demomis.modules.programhistory.service.impl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.programhistory.dao.WlProgramHistoryDao;
import com.qzsoft.demomis.modules.programhistory.dao.WlProgramHistoryInfoDao;
import com.qzsoft.demomis.modules.programhistory.entity.WlProgramHistoryEntity;
import com.qzsoft.demomis.modules.programhistory.entity.WlProgramHistoryInfoEntity;
import com.qzsoft.demomis.modules.programhistory.service.WlProgramHistoryInfoService;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ConvertDictUtils;
import com.qzsoft.jeemis.platform.security.user.SecurityUser;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 节目时长(Wl_Program)表服务实现类
 *
 * @author sdmq
 * @since 2020-06-10 17:45:47
 */
@Service("wl_ProgramHistoryInfoService")
public class WlProgramHistoryInfoServiceImpl extends BaseService implements WlProgramHistoryInfoService {
    private final SqlSession sqlSession;
    @Autowired
	private WlProgramHistoryInfoDao wlProgramHistoryInfoDao;
	@Autowired
	private WlProgramHistoryDao wlProgramHistoryDao;


	public WlProgramHistoryInfoServiceImpl(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}
	
	/**
	 * Wl_Program数据分页
	 * @param params
	 * @return IPage
	 */
	@Override
	public IPage<WlProgramHistoryInfoEntity> page(Map<String, Object> params) {
		IPage<WlProgramHistoryInfoEntity> page =this.getPage(params,Constant.CREATE_DATE,false, WlProgramHistoryInfoEntity.class) ;
		QueryWrapper<WlProgramHistoryInfoEntity> queryWrapper =this.getQueryWrapper(params, WlProgramHistoryInfoEntity.class);
		IPage<WlProgramHistoryInfoEntity> pageData = wlProgramHistoryInfoDao.selectPage(page, queryWrapper);
		pageData= ConvertDictUtils.formatDicDataPage(pageData);
		return pageData;
	}
    /**
	 * Wl_Program数据列表
	 * @param params
	 * @return
	 */
	@Override
	public List<WlProgramHistoryInfoEntity> list(Map<String, Object> params) {
		QueryWrapper<WlProgramHistoryInfoEntity> queryWrapper = this.getQueryWrapper(params, WlProgramHistoryInfoEntity.class);
		List<WlProgramHistoryInfoEntity> entityList = wlProgramHistoryInfoDao.selectList(queryWrapper);
		entityList=ConvertDictUtils.formatDicDataList(entityList);
		return entityList;
	}

	@Override
	public Map<String, Object> get(String programId,String equipmentId) {
		// 节目信息
		WlProgramHistoryInfoEntity wlProgramHistoryEntity = wlProgramHistoryInfoDao.getHistoryById(programId,equipmentId);
		Map<String, Object> map = new HashMap<>();
		map.put("wlProgramHistoryEntity", wlProgramHistoryEntity);
		return map;
	}
    /**
	 * Wl_Program保存
	 * createDate creator
	 * updateDate updater 
	 * 字段会自动注入值 如不需要注入请修改Wl_ProgramEntity
	 * @param map
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(Map<String, Object> map) {
		String user = SecurityUser.getUser().getId().toString();
		WlProgramHistoryInfoEntity entity = new WlProgramHistoryInfoEntity();
		// 节目时长属性，按实体插入
		String programId = map.get("id").toString();
		//设备id集合
		List equipmentIdList = (List)map.get("equipmentIdList");
		entity.setProgramId(programId);
		entity.setOnDatetime(new Date());
		entity.setOffDatetime(null);
		entity.setTimeCount(0);
		entity.setCreateDate(new Date());
		entity.setCreator(user);
		if(equipmentIdList.size() > 0){
			for(int i = 0; i < equipmentIdList.size(); i++){
				entity.setEquipmentId(equipmentIdList.get(i).toString());
				wlProgramHistoryInfoDao.insert(entity);
			}
		}
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(Map<String, Object> map) {
		String user = SecurityUser.getUser().getId().toString();
		// 节目时长属性，按实体插入
		String programId = map.get("programId").toString();
		//设备id集合
		List equipmentIdList = (List)map.get("equipmentIdList");
		// 字符串转时间
		Date onDatetimes = null;
		Date offDatetimes = null;
		if(equipmentIdList.size() > 0){
			for(int i = 0; i < equipmentIdList.size(); i++){
				String   equipmentId  =  equipmentIdList.get(i).toString();
				WlProgramHistoryEntity wlProgramHistoryEntity = wlProgramHistoryDao.getHistoryById(programId,equipmentId);
				WlProgramHistoryInfoEntity wlProgramHistoryInfoEntity  =  wlProgramHistoryInfoDao.getHistoryById(programId,equipmentId);
				SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				try {
					onDatetimes = 	wlProgramHistoryInfoEntity.getOnDatetime();
					offDatetimes = new Date();
					//计算时差
					long   time  = DateUtil.between(offDatetimes,onDatetimes, DateUnit.MINUTE,false);
					//更新
					wlProgramHistoryInfoEntity.setTimeCount((int)time);
					wlProgramHistoryInfoEntity.setUpdateDate(new Date());
					wlProgramHistoryInfoEntity.setUpdator(user);
					//跟新主单时长
					wlProgramHistoryEntity.setTimeCount(wlProgramHistoryInfoEntity.getTimeCount() - (int)time);
					wlProgramHistoryEntity.setUpdateDate(new Date());
					wlProgramHistoryEntity.setUpdator(user);
				}catch (Exception e) {
					e.printStackTrace();
				}
				wlProgramHistoryInfoDao.updateById(wlProgramHistoryInfoEntity);
				wlProgramHistoryDao.updateById(wlProgramHistoryEntity);
			}
		}
	}
}