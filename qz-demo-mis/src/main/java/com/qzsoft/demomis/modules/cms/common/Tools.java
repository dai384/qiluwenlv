package com.qzsoft.demomis.modules.cms.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class Tools {
	/**
	 * 按指定长度分割字符串
	 * 
	 * @param msg
	 * @param num
	 * @return
	 */
	public static String[] split(String msg, int num) {
		int len = msg.length();
		if (len <= num)
			return new String[] { msg };

		int count = len / num ;
		count += len > num * count ? 1 : 0; // 这里应该值得注意

		String[] result = new String[count];

		int pos = 0;
		int splitLen = num;
		for (int i = 0; i < count; i++) {
			if (i == count - 1)
				splitLen = len - pos;

			result[i] = msg.substring(pos, pos + splitLen);
			pos += splitLen;

		}

		return result;
	}
	
	/**
	 * 导出
	 * @param workBook
	 * @param fileName
	 * @param response
	 */
	public static void writeDate(HSSFWorkbook workBook, String fileName, HttpServletResponse response){
		BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			workBook.write(os);
			
			byte[] content = os.toByteArray();
			InputStream is = new ByteArrayInputStream(content);


			response.setContentType("application/vnd.ms-excel;charset=utf-8");
			response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("UTF-8"),"ISO8859-1") + ".xls");// 组装附件名称和格式 
			
			ServletOutputStream out = response.getOutputStream();
			

			bis = new BufferedInputStream(is);
            bos = new BufferedOutputStream(out);

            byte[] buff = new byte[2048];
            int bytesRead;

            // Simple read/write loop.
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
            bis.close();
            bos.close();
            
            out.close();
            
           


		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * 获取表头
	 * @param dateFlag
	 * @param timeSVal
	 * @param timeEVal
	 * @return
	 */
	public static String getTitle(int dateFlag, String timeSVal, String timeEVal, String suffix) {
		if (dateFlag == 1) {
			String[] bStr = timeSVal.split("-");
			String[] eStr = timeEVal.split("-");
			int year = Integer.parseInt(bStr[0]);
			int month = Integer.parseInt(bStr[1]);
			int day = Integer.parseInt(bStr[2]);
			int eyear = Integer.parseInt(eStr[0]);
			int emonth = Integer.parseInt(eStr[1]);
			int eday = Integer.parseInt(eStr[2]);
			return year + "年" + month + "月" + day +"日"+ "至" + eyear + "年" + emonth + "月" + eday +"日"+ "期间" + suffix;
		} else if (dateFlag == 2) {
			String[] bStr = timeSVal.split("-");
			String[] eStr = timeEVal.split("-");
			int year = Integer.parseInt(bStr[0]);
			int month = Integer.parseInt(bStr[1]);
			int eyear = Integer.parseInt(eStr[0]);
			int emonth = Integer.parseInt(eStr[1]);
			return year + "-" + month + "至" + eyear + "-" + emonth + "期间" + suffix;
		} else if (dateFlag == 3) {
			return timeSVal + "至" + timeEVal + "期间" + suffix;
		}
		return "";
	}
	
	public static String getDateStr(String sDate, String eDate, SimpleDateFormat sdf){
		if(StringUtils.isEmpty(sDate) && StringUtils.isEmpty(eDate)){
			return "全部";
		}else if(!StringUtils.isEmpty(sDate) && StringUtils.isEmpty(eDate)){
			return sDate + "~" +  sdf.format(new Date());
		}else if(StringUtils.isEmpty(sDate) && !StringUtils.isEmpty(eDate)){
			return eDate + "之前";
		}else{
			return sDate + "~" + eDate;
		}
	}
}
