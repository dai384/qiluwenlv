package com.qzsoft.demomis.modules.cms.sansi.request;

import com.qzsoft.demomis.modules.cms.CmsException;

import java.io.IOException;

/**
 * 设置可变信息标志的当前显示亮度
 * 
 * @author frank
 * 
 */
public class SetLightRequest extends Request {
	private int light;

	public SetLightRequest() throws CmsException {
		this.type = int2bytes(5, 2);
		this.address = int2bytes(0, 2);
	}

	public SetLightRequest(int address) throws CmsException {
		this.type = int2bytes(3, 2);
		this.address = int2bytes(address, 2);
	}

	@Override
	protected void setData() throws CmsException, IOException {
		data = new byte[6];
		byte[] b = int2bytes(light, 2);
		data[0] = b[0];
		data[1] = b[1];
		data[2] = b[0];
		data[3] = b[1];
		data[4] = b[0];
		data[5] = b[1];
	}

	public int getLight() {
		return light;
	}

	public void setLight(int light) {
		this.light = light;
	}

}
