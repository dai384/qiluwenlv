package com.qzsoft.demomis.modules.program.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;


@ApiModel(value ="节目表")
@Data
public class PicEntity {
    private Long id;

    private String moudleId;

    private String resourceId;

    // 暂存资源路径
    private String remark;

    private String creator;
}
