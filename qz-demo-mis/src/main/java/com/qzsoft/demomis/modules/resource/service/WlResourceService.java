package com.qzsoft.demomis.modules.resource.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.resource.entity.ResourceStrtistic;
import com.qzsoft.demomis.modules.resource.entity.WlResourceEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;
/**
 * 资源表(WlResource)表服务接口
 *
 * @author sdmq
 * @since 2020-06-05 17:01:24
 */
public interface WlResourceService {
    /**
	 * 数据分页
	 * @param params
	 * @return IPage
	 */
	IPage<WlResourceEntity> page(Map<String, Object> params);
    
	/**
	 * 数据列表
	 * @param params
	 * @return
	 */
	List<WlResourceEntity> list(Map<String, Object> params);

	/*
	* 根据类型选择资源
	* */
	List<WlResourceEntity> selectReasourceByType(String type);

	/**
	 * 单个数据
	 * @param id
	 * @return
	 */
	WlResourceEntity get(String id);
	List<ResourceStrtistic>  getResource();
	ResourceStrtistic getResourceSum();

	/**
	 * 保存
	 * @param entity
	 */
	void save(WlResourceEntity entity);

	/**
	 * 更新
	 * @param
	 */
	void update(Map<String, Object> params);

	/**
	 * 更新
	 * @param
	 */
	void updateAll(WlResourceEntity entity);

	/**
	 * 批量删除
	 * @param ids
	 */
	Boolean delete(String[] ids);

	/**
	 * 导出Excel
	 * @param params
	 * @param response
	 */
	void exportXls(Map<String, Object> params , HttpServletResponse response);

	void saveSource( HttpServletRequest request );
}