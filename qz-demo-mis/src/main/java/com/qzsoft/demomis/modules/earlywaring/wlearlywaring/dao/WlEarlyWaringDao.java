package com.qzsoft.demomis.modules.earlywaring.wlearlywaring.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.qzsoft.demomis.modules.earlywaring.wlearlywaring.entity.WlEarlyWaringEntity;

/**
 * 智能箱报警数据推送接收表(WlEarlyWaring)表数据库访问层
 *
 * @author sdmq
 * @since 2020-08-14 09:44:37
 */
@Mapper
public interface WlEarlyWaringDao extends BaseMapper<WlEarlyWaringEntity> {

}