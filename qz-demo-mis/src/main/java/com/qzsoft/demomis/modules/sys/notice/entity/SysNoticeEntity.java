package com.qzsoft.demomis.modules.sys.notice.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.qzsoft.jeemis.common.annotation.Dict;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 通知公告(SysNotice)表实体类
 *
 * @author sdmq
 * @since 2019-09-17 21:05:18
 */
@ApiModel(value = "通知公告")
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_notice")
public class SysNoticeEntity extends Model<SysNoticeEntity> {
	private static final long serialVersionUID = -25723839900015523L;
	/**
	 *主键UUID主键
	 */
	@TableId
	@ApiModelProperty(value = "主键UUID")
	@NotNull(message="{id.require}", groups = UpdateGroup.class)
	private String id;
	/**
	 *通知标题
	 */
	@ApiModelProperty(value = "通知标题")
	@NotNull(message="标题不能为空", groups = DefaultGroup.class)
	@Excel(name = "标题")
	private String title;
	/**
	 *通知公告内容  (超文本编辑器)
	 */
	@ApiModelProperty(value = "通知公告内容(超文本编辑器)")
	private String content;
	/**
	 *codeid  (dx) 工作通知  政策法规   单位改革类
	 */
	@ApiModelProperty(value = "dy 一级栏目")
	private String noticeType1;

	@ApiModelProperty(value = "dx 二级栏目")
	private String noticeType2;

	/**
	 *发文机构
	 */
	@ApiModelProperty(value = "发文机构")
	private String deptName;
	/**
	 *通告范围 1 全员 2 所有下级单位  3直属单位
	 */
	@ApiModelProperty(value = "1 全员 2 所有下级单位  3直属单位")
	private String noticeScope;
	/**
	 *阅读期限,截止到某日期即 不显示了,默认2099年 即用不过期
	 */
	@ApiModelProperty(value = "阅读期限,为空默认2099年永不过期")
	private Date noticeTerm;
	/**
	 * 发布状态
	 *0 未发布  1 发布
	 */
	@ApiModelProperty(value = "0 未发布  1 发布")
	private Boolean hasPublish;
	/**
	 *发布时间
	 */
	@ApiModelProperty(value = "发布时间")
	private Date publishDate;
	/**
	 *创建者机构deptid
	 */
	@ApiModelProperty(value = "创建者机构deptid")
	@TableField(fill = FieldFill.INSERT)
	private String deptId;
	/**
	 *创建者机构deptid
	 */
	@ApiModelProperty(value = "创建者机构deptpkid")
	@TableField(fill = FieldFill.INSERT)
	private String deptPkid;
	/**
	 *排序,置顶通过排序号实现,如果排序号前10为置顶,置顶排序号和时间排序
	 */
	@ApiModelProperty(value = "排序,置顶通过排序号实现,如果排序号前10为置顶,置顶排序号和时间排序")
	private Integer orderid;
	/**
	 *创建者
	 */
	@ApiModelProperty(value = "创建者")
	@TableField(fill = FieldFill.INSERT)
	private Long creator;
	/**
	 *创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@TableField(fill = FieldFill.INSERT)
	private Date createDate;
	/**
	 *更新者
	 */
	@ApiModelProperty(value = "更新者")
	@TableField(fill = FieldFill.UPDATE)
	private Long updater;
	/**
	 *更新时间
	 */
	@ApiModelProperty(value = "更新时间")
	@TableField(fill = FieldFill.UPDATE)
	private Date updateDate;

	/**
	 * 表之外的字段
	 */
	@ApiModelProperty(value = "栏目1")
	@Dict(dicCodeField ="noticeType1",dictCodeId ="DW")
	@TableField(exist=false)
	@Excel(name = "一级栏目")
	private String noticeType1Desc;

	@ApiModelProperty(value = "栏目2")
	@Dict(dicCodeField ="noticeType2",dictCodeId ="DX")
	@TableField(exist=false)
	@Excel(name = "二级栏目")
	private String noticeType2Desc;

	/**
	 * 附件表
	 */
	@TableField(exist=false)
	private List<SysNoticeFileEntity> fileList;

	/**
	 * 获取主键值
	 *
	 * @return 主键值
	 */
	@Override
	protected Serializable pkVal() {
		return this.id;
	}
}