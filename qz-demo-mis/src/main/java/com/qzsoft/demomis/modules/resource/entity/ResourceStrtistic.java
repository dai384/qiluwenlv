package com.qzsoft.demomis.modules.resource.entity;

import lombok.Data;

@Data
public class ResourceStrtistic {
    private String name;
    private int freeNum;
    private int businessNum;
    private int freeSum;
    private int businessSum;
}
