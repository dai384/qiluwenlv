package com.qzsoft.demomis.modules.template.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qzsoft.demomis.modules.template.entity.WlTemplateMoudleEntity;
import com.qzsoft.jeemis.platform.security.user.SecurityUser;
import org.apache.ibatis.session.SqlSession;
import com.qzsoft.demomis.modules.template.dao.WlTemplateDao;
import com.qzsoft.demomis.modules.template.entity.WlTemplateEntity;
import com.qzsoft.demomis.modules.template.service.WlTemplateService;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ExcelUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.util.*;


/**
 * 模板表(Wl_Template)表服务实现类
 *
 * @author sdmq
 * @since 2020-06-10 17:46:07
 */
@Service("wl_TemplateService")
public class WlTemplateServiceImpl extends BaseService implements WlTemplateService {
    private final SqlSession sqlSession;
    @Autowired
	private WlTemplateDao wl_TemplateDao;
   
	public WlTemplateServiceImpl(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}
	
	/**
	 * Wl_Template数据分页
	 * @param params
	 * @return IPage
	 */
	public List<WlTemplateEntity> getPage(Map<String, Object> params) {
		int offset = Integer.valueOf(params.get("page").toString());
		int limit = Integer.valueOf(params.get("limit").toString());
		offset = ( offset - 1 ) * limit;
		params.put("offset", offset);
		return wl_TemplateDao.getPage(params);
	}
	public int total(Map<String, Object> params){
		return wl_TemplateDao.total(params);
	}
    /**
	 * Wl_Template数据列表
	 * @param params
	 * @return
	 */
	@Override
	public List<WlTemplateEntity> list(Map<String, Object> params) {
		return wl_TemplateDao.selectList(params);
	}

	@Override
	public WlTemplateEntity get(String id) {
		WlTemplateEntity entity = wl_TemplateDao.selectById(id);
		List<WlTemplateMoudleEntity> moudleEntityList = wl_TemplateDao.selectModuleList(id);
		if(moudleEntityList.size()>0){
		    entity.setDragList(moudleEntityList);
        }
		return entity;
	}
    /**
	 * Wl_Template保存
	 * createDate creator
	 * updateDate updater 
	 * 字段会自动注入值 如不需要注入请修改Wl_TemplateEntity
	 */
	@Override
	public void save(Map<String, Object> map) {
	    // 1、存模板主表
		String id = UUID.randomUUID().toString().replace("-", "");
		String creator = SecurityUser.getUser().getId().toString();
		map.put("id", id);
		map.put("creator", creator);
		int flag = wl_TemplateDao.insert(map);
		// 2、存模板组件附表
		List moudleList = (List)map.get("dragList");
		ObjectMapper objectMapper = new ObjectMapper();
		List<WlTemplateMoudleEntity> temMoudleList = new ArrayList<WlTemplateMoudleEntity>();
		for(int i = 0; i < moudleList.size(); i++){
			WlTemplateMoudleEntity entity = objectMapper.convertValue(moudleList.get(i), WlTemplateMoudleEntity.class);
//			System.out.println("=================================================================================="+entity.getShowVisible() == "true");
			if (entity.getShowVisible() == "true"){
				entity.setTemplateId(id);
				entity.setCreator(creator);
				temMoudleList.add(entity);
			}
		}
		if(temMoudleList.size()>0){
			int flagMoudle = wl_TemplateDao.insertMoudle(temMoudleList);
		}
	}


	@Transactional()
	/*
	* 确保节目列表中无使用此模板
	* */
	public String update(Map<String, Object> map) {
		String id = map.get("id").toString();
		// 确认
		int flag = wl_TemplateDao.selectProgramCount(id);

		if (flag>0) {
			return  "2";
		} else {
			// 1、更新主表
			wl_TemplateDao.updateById(map);

			// 2、删除附表
			wl_TemplateDao.deleteMoudle(id);
			// 3、插入附表
			List moudleList = (List)map.get("dragList");
			ObjectMapper objectMapper = new ObjectMapper();
			List<WlTemplateMoudleEntity> temMoudleList = new ArrayList<WlTemplateMoudleEntity>();
			for(int i = 0; i < moudleList.size(); i++){
				WlTemplateMoudleEntity entity = objectMapper.convertValue(moudleList.get(i), WlTemplateMoudleEntity.class);
				if ("true".equals(entity.getShowVisible())){
					entity.setTemplateId(id);
					entity.setCreator(SecurityUser.getUser().getId().toString());
					temMoudleList.add(entity);
				}
			}
			if(temMoudleList.size()>0){
				int flagMoudle = wl_TemplateDao.insertMoudle(temMoudleList);
			}

			return "1";
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	/*
	 * 确保节目列表中无使用此模板
	 * */
	public String delete(String[] ids) {
		// 确认
		String res = "1";
		for (String id : ids){
			int flag = wl_TemplateDao.selectProgramCount(id);
			if (flag > 0) {
				res = "2";
			} else {
				// 1、删主表
				wl_TemplateDao.deleteById(id);
				// 2、删附表
				wl_TemplateDao.deleteMoudle(id);
			}
		}
		return res;
	}
	@Override
	public void exportXls(Map<String, Object> params , HttpServletResponse response)  {
		List<WlTemplateEntity> list = this.list(params);
		ExcelUtils.exportExcelToTarget(response, "模板表导出列表", list, WlTemplateEntity.class);
	}
	
}