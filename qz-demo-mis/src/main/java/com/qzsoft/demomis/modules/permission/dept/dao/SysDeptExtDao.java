package com.qzsoft.demomis.modules.permission.dept.dao;

import com.qzsoft.demomis.repository.sys.entity.SysDeptEntity;
import com.qzsoft.jeemis.repository.sqlmapper.SqlMapper;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author sdmq
 * @date 2019/8/6 8:51
 */
@Repository
public class SysDeptExtDao {
	private SqlSession sqlSession;

	/**
	 * 构造方法
	 * @param sqlSession
	 */
	public SysDeptExtDao(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}

	/**
	 * 取回机构列表
	 * @param pId
	 * @return
	 */
	public List<SysDeptEntity> getList(String pId) {
		SqlMapper sqlMapper=new SqlMapper(sqlSession);
		String strSql = "";
		strSql = "select * from sys_dept where pid = #{0}  order by pid,sort,id";
		return sqlMapper.selectListArgs(strSql,SysDeptEntity.class,pId);
	}


	/**
	 * 取全部父节点
	 * @param
	 * @return
	 */
	public List<String> getDeptNodePptrs(String id) {
		// 仅mysql,不兼容其他数据库,主要为了快和简单,其实可以递归出所有父节点
		String strSql = "select id from sys_dept where #{0} like CONCAT(id,'%') order by id desc";
		SqlMapper sqlMapper=new SqlMapper(sqlSession);
		return sqlMapper.selectListArgs(strSql,String.class,id);
	}

}
