package com.qzsoft.demomis.modules.programhistory.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qzsoft.demomis.modules.programhistory.entity.WlProgramHistoryInfoEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 节目表(Wl_Program)表数据库访问层
 *
 * @author sdmq
 * @since 2020-06-10 17:45:46
 */
@Mapper
public interface WlProgramHistoryInfoDao extends BaseMapper<WlProgramHistoryInfoEntity> {
    WlProgramHistoryInfoEntity getHistoryById(String programId, String equipmentId);

}