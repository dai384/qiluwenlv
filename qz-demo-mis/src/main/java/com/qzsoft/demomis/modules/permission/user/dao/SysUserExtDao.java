package com.qzsoft.demomis.modules.permission.user.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

/**
 * @author sdmq
 * @date 2019/8/7 9:19
 */
@Repository
public class SysUserExtDao {
	private final SqlSession sqlSession;
	/**
	 * 构造方法
	 * @param sqlSession
	 */
	public SysUserExtDao(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}

}
