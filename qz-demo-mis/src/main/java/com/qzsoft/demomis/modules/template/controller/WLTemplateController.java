package com.qzsoft.demomis.modules.template.controller;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.template.entity.WlTemplateEntity;
import com.qzsoft.demomis.modules.template.service.WlTemplateService;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.utils.ExcelUtils;
import com.qzsoft.jeemis.common.utils.Result;
import com.qzsoft.jeemis.common.validator.AssertUtils;
import com.qzsoft.jeemis.common.validator.ValidatorUtils;
import com.qzsoft.jeemis.common.validator.group.AddGroup;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 模板表(wltemplate)表控制层
 *
 * @author sdmq
 * @since 2020-06-10 17:46:07
 */
@RestController
@RequestMapping("template/wltemplate")
@Api(tags="模板表")
public class WLTemplateController  {
    @Autowired
    private WlTemplateService wlTemplateService;

    @PostMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String") ,
            @ApiImplicitParam(name = "ge_createDate", value = "起始创建时间", paramType = "query", dataType="Date"),
            @ApiImplicitParam(name = "le_createDate", value = "截止创建时间", paramType = "query", dataType="Date")
    })
//    @RequiresPermissions("template:wltemplate:page")
    public Result page(@ApiIgnore @RequestBody Map<String, Object> params){
        List<WlTemplateEntity> page = wlTemplateService.getPage(params);
        int total = wlTemplateService.total(params);
        Map<String, Object> map = new HashMap<>();
        map.put("records", page);
        map.put("total", total);
        return new Result<>().ok(map);
    }

    @GetMapping("list")
    @ApiOperation("列表")
//    @RequiresPermissions("template:wltemplate:list")
    public Result<List<WlTemplateEntity>> list(@ApiIgnore @RequestParam Map<String, Object> params){
        List<WlTemplateEntity> data = wlTemplateService.list(params);

        return new Result<List<WlTemplateEntity>>().ok(data);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
//    @RequiresPermissions("template:wltemplate:info")
    public Result<WlTemplateEntity> get(@PathVariable("id") String id){
        WlTemplateEntity data = wlTemplateService.get(id);

        return new Result<WlTemplateEntity>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
//    @RequiresPermissions("template:wltemplate:save")
    public Result save(@RequestBody Map<String, Object> map){
        //效验数据
        wlTemplateService.save(map);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
//    @RequiresPermissions("template:wltemplate:update")
    public Result update(@RequestBody  Map<String, Object> map){
        //效验数据
//        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        String flag = wlTemplateService.update(map);

        return new Result().ok(flag);
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
//    @RequiresPermissions("template:wltemplate:delete")
    public Result delete(@RequestBody String[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        String flag = wlTemplateService.delete(ids);

        return new Result().ok(flag);
    }



    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
//    @RequiresPermissions("template:wltemplate:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params , HttpServletResponse response) throws Exception {
        wlTemplateService.exportXls(params,response);
    }

}