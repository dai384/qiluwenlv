package com.qzsoft.demomis.modules.sansi;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/*
* jdom2创建xml
* */
public class JdomXmlUtil {
    protected static final Logger LOGGER = LoggerFactory.getLogger(JdomXmlUtil.class);

     static private String programPath = "D:/qiluwenlv/program/sansi/xMedia1.xml";

    public static void main (String[] args) throws Exception{
        List<String> resourceList = new ArrayList<>();
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200723/6c43da9eab33458e9bd8a27c6b46cd1b.jpg");
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200723/597fdee4b7094b85a38a8b3a9cd8c3d8.jpg");

        Long start = System.currentTimeMillis();
        createXml(resourceList);
        LOGGER.info("sansi xml run time ："+ (System.currentTimeMillis() - start));
    }

    public static void createXml( List<String> resourceList) throws IOException {
        Element xMediaPlaylist = new Element("xMediaPlaylist");
        // 为xMediaPlaylist节点添加属性
        xMediaPlaylist.setAttribute(new Attribute("ProgrameName", "NewPrograme"));
        xMediaPlaylist.setAttribute(new Attribute("Name", "xMedia1"));
        xMediaPlaylist.setAttribute(new Attribute("Type", "0"));
        xMediaPlaylist.setAttribute(new Attribute("Background", ""));
        xMediaPlaylist.setAttribute(new Attribute("BgRed", "0"));
        xMediaPlaylist.setAttribute(new Attribute("BgGreen", "0"));
        xMediaPlaylist.setAttribute(new Attribute("BgBlue", "0"));
        xMediaPlaylist.setAttribute(new Attribute("BgSound", ""));
        xMediaPlaylist.setAttribute(new Attribute("NumScenes", "1"));
        xMediaPlaylist.setAttribute(new Attribute("NumScheduledZones", "0"));
        xMediaPlaylist.setAttribute(new Attribute("NumInteractiveZones", "0"));
        xMediaPlaylist.setAttribute(new Attribute("NumItems", resourceList.size() + "")); // item数
        xMediaPlaylist.setAttribute(new Attribute("NumSchedules", "0"));
        xMediaPlaylist.setAttribute(new Attribute("RepeatCount", "1"));
        xMediaPlaylist.setAttribute(new Attribute("RepeatDur", "3600"));

        Document doc = new Document(xMediaPlaylist);

        // 向xMediaPlaylist根节点中添加子节点scene
        Element scene = new Element("Scene");
        // 为scene节点添加属性
        scene.setAttribute(new Attribute("Id", "0"));
        scene.setAttribute(new Attribute("Name", "New Scene"));
        scene.setAttribute(new Attribute("MainZoneId", "255"));
        scene.setAttribute(new Attribute("NumZones", "1"));

        Element zone = new Element("Zone");
        // 为zone节点添加属性
        zone.setAttribute(new Attribute("Id", "0"));
        zone.setAttribute(new Attribute("Name", "New Zone"));
        zone.setAttribute(new Attribute("Left", "0"));
        zone.setAttribute(new Attribute("Top", "0"));
        zone.setAttribute(new Attribute("Left", "0"));
        zone.setAttribute(new Attribute("Top", "0"));
        zone.setAttribute(new Attribute("Width", "2625"));
        zone.setAttribute(new Attribute("Height", "315"));
        zone.setAttribute(new Attribute("RepeatCount", "1"));
        zone.setAttribute(new Attribute("RepeatDur", "600"));
        zone.setAttribute(new Attribute("Transparent", "0"));
        zone.setAttribute(new Attribute("Background", ""));
        zone.setAttribute(new Attribute("Red", "0"));
        zone.setAttribute(new Attribute("Green", "0"));
        zone.setAttribute(new Attribute("Blue", "0"));
        zone.setAttribute(new Attribute("Alpha", "255"));
        zone.setAttribute(new Attribute("NumItems", resourceList.size() + "")); // item 数目
        zone.setAttribute(new Attribute("AfterLastFrame", "0"));

        // 向zone根节点中添加子节点Item
        // 遍历添加
        for (int i = 0; i < resourceList.size(); i++) {
            String path = resourceList.get(i);
            String id = i+"";
            if (path.contains("image")){
                Element item = new Element("Item");
                // 为Item节点添加属性
                item.setAttribute(new Attribute("Id", id));
                item.setAttribute(new Attribute("Transition", "0"));
                item.setAttribute(new Attribute("Speed", "1"));
                item.setAttribute(new Attribute("RepeatCount", "0"));
                item.setAttribute(new Attribute("RepeatDur", "15"));
                item.setAttribute(new Attribute("Alignment", "1"));
                item.setAttribute(new Attribute("NumberOfParams", "0"));

                zone.addContent(item);
            } else if (path.contains("vedio")) {
                Element item = new Element("Item");
                // 为Item节点添加属性
                item.setAttribute(new Attribute("Id", id));
                item.setAttribute(new Attribute("Transition", "0"));
                item.setAttribute(new Attribute("Speed", "1"));
                item.setAttribute(new Attribute("RepeatCount", "1"));
                item.setAttribute(new Attribute("RepeatDur", "15"));
                item.setAttribute(new Attribute("Alignment", "1"));
                item.setAttribute(new Attribute("NumberOfParams", "0"));

                zone.addContent(item);
            }
        }

        scene.addContent(zone);

        // 将scene节点添加到xMediaPlaylist根节点中
        xMediaPlaylist.addContent(scene);

        // 向xMediaPlaylist根节点中添加子节点Item
        // 遍历添加
        for (int i = 0; i < resourceList.size(); i++) {
            String id = i + "";
            String fileName = resourceList.get(i).split("/")[5];

            Element item = new Element("Item");
            // 为Item节点添加属性
            item.setAttribute(new Attribute("Id", id));
            item.setAttribute(new Attribute("Name", ""));
            item.setAttribute(new Attribute("Type", "0"));
            item.setAttribute(new Attribute("DeviceNo", "0"));
            item.setAttribute(new Attribute("FileName", fileName));

            // 将item2节点添加到xMediaPlaylist根节点中
            xMediaPlaylist.addContent(item);
        }

//        doc.getRootElement().addContent(scene);

        //设置xml输出格式
        Format format = Format.getPrettyFormat();
        format.setEncoding("gb2312");// 设置编码
//        format.setIndent("    ");// 设置缩进

        // new XMLOutputter().output(doc, System.out);
        XMLOutputter xmlOutput = new XMLOutputter();

        // display nice nice
        xmlOutput.setFormat(format);
        xmlOutput.output(doc, new FileWriter(programPath));

        LOGGER.info("File Saved!");
    }

}
