package com.qzsoft.demomis.modules.cms.sansi.playlist;

import com.google.common.collect.Lists;
import com.qzsoft.demomis.modules.cms.common.Tools;
import com.qzsoft.demomis.modules.cms.sansi.playlist.esc.Esc;
import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class PlayListItem {
    private final String HEADER = "[playlist]";
    private final String RN = "\r\n";
    private final String ITEM_NO = "item_no=";
    private final String ITEM = "item";
    private List<String> items = Lists.newArrayList();

    public void addItem(Delay delay, Transition transition, Speed speed, Str str) {
        StringBuffer sb = new StringBuffer();
        sb.append(delay.getValue());
        sb.append(",");
        sb.append(transition.getValue());
        sb.append(",");
        sb.append(speed.getValue());
        sb.append(",");
        if (str != null) {
            sb.append(str.getControl());
            String param = sb.toString();
            if (StringUtils.isNotEmpty(str.getMsg())) {
                String[] arr = Tools.split(str.getMsg(), (1016 - param.length()));// 单行长度大于了1024了，要拆成多行
                for (String tmp : arr) {
                    items.add(param + tmp);
                }
            } else {
                items.add(param);
            }
        }else{
            items.add(sb.toString());
        }
    }

    public void addItem(Delay delay, Transition transition, Speed speed) {
        addItem(delay, transition, speed, null);
    }

    public void addItem(String msg, Esc... escs) {
        addItem(new Delay(), new Transition(), new Speed(), new Str(msg, escs));
    }

    public void addItem(Delay delay, String msg, Esc... escs) {
        addItem(delay, new Transition(), new Speed(), new Str(msg, escs));
    }

    public void addItem(Transition transition, String msg, Esc... escs) {
        addItem(new Delay(), transition, new Speed(), new Str(msg, escs));
    }

    public void addItem(Speed speed, String msg, Esc... escs) {
        addItem(new Delay(), new Transition(), speed, new Str(msg, escs));
    }

    public void addItem(Delay delay, Transition transition, String msg,
                        Esc... escs) {
        addItem(delay, transition, new Speed(), new Str(msg, escs));
    }

    public void addItem(Delay delay, Speed speed, String msg, Esc... escs) {
        addItem(delay, new Transition(), speed, new Str(msg, escs));
    }

    public void addItem(Transition transition, Speed speed, String msg,
                        Esc... escs) {
        addItem(new Delay(), transition, speed, new Str(msg, escs));
    }

    public void addItem(Delay delay, Transition transition, Speed speed,
                        String msg, Esc... escs) {
        addItem(delay, transition, speed, new Str(msg, escs));
    }

    public String getContent() {
        StringBuffer sb = new StringBuffer();
        sb.append(HEADER);
        sb.append(RN);
        sb.append(ITEM_NO);
//        sb.append(String.format("%1$03d", items.size()));
        sb.append(items.size());
        sb.append(RN);
        for (int i = 0; i < items.size(); i++) {
            sb.append(ITEM);
            //sb.append(String.format("%1$03d", i));
            sb.append(i);
            sb.append("=");
            sb.append(items.get(i));
            sb.append(RN);
        }
        return sb.toString();
    }

    public byte[] getBytes() throws UnsupportedEncodingException {
        String content = getContent();
        return content.getBytes("gbk");
    }
}
