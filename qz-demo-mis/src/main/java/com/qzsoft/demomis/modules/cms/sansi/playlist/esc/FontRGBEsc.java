package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * \cRRRGGGBBBYYY 字符背景颜色,\bt 为透明色,RRR、GGG、BBB、YYY 的含义同 上,缺省为 \bt。
 * 
 * @author frank
 * 
 */
public class FontRGBEsc extends Esc {
	private static Logger logger = LoggerFactory.getLogger(FontRGBEsc.class);
	private final String DEFAULT = "255255000000";// 黄色

	@Override
	protected String getCommand() {
		return "\\b";
	}

	private FontRGBEsc(int r, int g, int b, int y) {
		if (r < 0 || r > 255 || g < 0 || g > 255 || b < 0 || b > 255 || y < 0
				|| y > 255) {
			logger.warn("GRB颜色的取值范围为0~255，现在为 {}，将使用默认值 {}", value, "黄色");
			value = DEFAULT;
		} else {
			value = format3(r) + format3(g) + format3(b) + format3(y);
		}
	}

	private FontRGBEsc() {// 透明
		value = "t";
	}

	public static FontRGBEsc getInstance() {
		return new FontRGBEsc();
	}

	public static FontRGBEsc getInstance(int r, int g, int b, int y) {
		return new FontRGBEsc(r, g, b, y);
	}
}
