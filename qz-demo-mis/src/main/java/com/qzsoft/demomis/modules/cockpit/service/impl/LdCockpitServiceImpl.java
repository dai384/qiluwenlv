package com.qzsoft.demomis.modules.cockpit.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.cockpit.dao.LdCockpitDao;
import com.qzsoft.demomis.modules.cockpit.entity.LdCockpitEntity;
import com.qzsoft.demomis.modules.cockpit.service.LdCockpitService;
import com.qzsoft.demomis.modules.task.dao.WlTasksDao;
import com.qzsoft.demomis.modules.task.entity.WlTasksEntity;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ConvertDictUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("ldCockpitService")
public class LdCockpitServiceImpl extends BaseService implements LdCockpitService {
    @Autowired
    private LdCockpitDao ldCockpitDao;
    @Autowired
    private WlTasksDao wlTasksDao;

    @Override
    public Map<String,Integer> queryCount(Integer index) {
        Map<String,Integer> map = new HashMap<>();
        Integer picCount = ldCockpitDao.selectPicCount(index);
        Integer audCount = ldCockpitDao.selectAudCount(index);
        Integer vidCount = ldCockpitDao.selectVidCount(index);
        Integer textCount = ldCockpitDao.selectTextCount(index);
        Integer lbCount = ldCockpitDao.selectLbCount(index);
        map.put("pic",picCount);
        map.put("aud",audCount);
        map.put("vid",vidCount);
        map.put("text",textCount);
        map.put("lb",lbCount);
        return map;
    }

    @Override
    public Map<String, Integer> queryNumber() {
        Map<String,Integer> map = new HashMap<>();
        Integer equCount = ldCockpitDao.selectEquCount();
        Integer resCount = ldCockpitDao.selectResCount();
        Integer temCount = ldCockpitDao.selectTemCount();
        Integer proCount = ldCockpitDao.selectProCount();
        Integer tasCount = ldCockpitDao.selectTasCount();
        map.put("equ",equCount);
        map.put("res",resCount);
        map.put("tem",temCount);
        map.put("pro",proCount);
        map.put("tas",tasCount);
        return map;
    }

    @Override
    public IPage<WlTasksEntity> page(Map<String, Object> params) {
        IPage<WlTasksEntity> page =this.getPage(params, Constant.CREATE_DATE,false, WlTasksEntity.class) ;
        QueryWrapper<WlTasksEntity> queryWrapper =this.getQueryWrapper(params, WlTasksEntity.class);
        IPage<WlTasksEntity> pageData = wlTasksDao.selectPage(page, queryWrapper);
        pageData= ConvertDictUtils.formatDicDataPage(pageData);
        return pageData;
    }

    @Override
    public WlTasksEntity get(String id) {
        WlTasksEntity wlTasksEntity = wlTasksDao.selectById(id);
        return wlTasksEntity;
    }

    @Override
    public Map<String, Integer> queryTasksCount() {
        Map<String,Integer> map = new HashMap<>();
        Integer agree = wlTasksDao.selectAgreeCount();
        Integer reject = wlTasksDao.selectRejectCount();
        Integer wait = wlTasksDao.selectWaitCount();
        map.put("agree",agree);
        map.put("reject",reject);
        map.put("wait",wait);
        return map;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(WlTasksEntity entity) {
//        System.out.println("......update........"+entity);
        wlTasksDao.updateById(entity);
    }

    @Override
    public List<WlTasksEntity> getPageByResult(Map<String, Object> params) {
        int offset = Integer.valueOf(params.get("page").toString());
        int limit = Integer.valueOf(params.get("limit").toString());
        offset = ( offset - 1 ) * limit;
        params.put("offset", offset);
        return wlTasksDao.getPageByResult(params);
    }

    @Override
    public int total(Map<String, Object> params) {
        return wlTasksDao.total(params);
    }
}
