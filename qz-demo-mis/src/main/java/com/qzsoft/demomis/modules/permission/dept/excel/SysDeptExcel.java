package com.qzsoft.demomis.modules.permission.dept.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import com.qzsoft.jeemis.common.annotation.Dict;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 部门管理(SysDept)Excel导出
 * 利用框架带的字典翻译typeDesc
 *
 * @author sdmq
 * @since 2019-08-20 16:36:15
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class SysDeptExcel {
    @Excel(name = "主键",width = 20)
    private String pkid;
    @Excel(name = "id")
    private String id;
    @Excel(name = "上级ID")
    private String pid;
    @Excel(name = "机构名称",width = 30)
    private String name;
	@ExcelIgnore
    private String type;
	@Excel(name = "类型")
	@Dict(dicCodeField ="type",dictCodeId ="Z3",dicTextFormat ="{code}-{description}" )
	private String typeDesc;
    @Excel(name = "简拼")
    private String spell;
}