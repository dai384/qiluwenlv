package com.qzsoft.demomis.modules.customer.wlcustomer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.session.SqlSession;
import com.qzsoft.demomis.modules.customer.wlcustomer.dao.WlCustomerDao;
import com.qzsoft.demomis.modules.customer.wlcustomer.entity.WlCustomerEntity;
import com.qzsoft.demomis.modules.customer.wlcustomer.service.WlCustomerService;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ExcelUtils;
import com.qzsoft.jeemis.common.utils.ConvertDictUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 客户信息表(WlCustomer)表服务实现类
 *
 * @author sdmq
 * @since 2020-07-14 13:50:08
 */
@Service("wlCustomerService")
public class WlCustomerServiceImpl extends BaseService implements WlCustomerService {
    private final SqlSession sqlSession;
    @Autowired
	private WlCustomerDao wlCustomerDao;
   
	public WlCustomerServiceImpl(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}
	
	/**
	 * WlCustomer数据分页
	 * @param params
	 * @return IPage
	 */
	@Override
	public IPage<WlCustomerEntity> page(Map<String, Object> params) {
		IPage<WlCustomerEntity> page =this.getPage(params,Constant.CREATE_DATE,false,WlCustomerEntity.class) ;
		QueryWrapper<WlCustomerEntity> queryWrapper =this.getQueryWrapper(params,WlCustomerEntity.class);
		IPage<WlCustomerEntity> pageData = wlCustomerDao.selectPage(page, queryWrapper);
		pageData= ConvertDictUtils.formatDicDataPage(pageData);
		return pageData;
	}
    /**
	 * WlCustomer数据列表
	 * @param params
	 * @return
	 */
	@Override
	public List<WlCustomerEntity> list(Map<String, Object> params) {
		QueryWrapper<WlCustomerEntity> queryWrapper = this.getQueryWrapper(params,WlCustomerEntity.class);
		List<WlCustomerEntity> entityList = wlCustomerDao.selectList(queryWrapper);
		entityList=ConvertDictUtils.formatDicDataList(entityList);
		return entityList;
	}

	@Override
	public WlCustomerEntity get(String id) {
		WlCustomerEntity entity = wlCustomerDao.selectById(id);
		// 如无需代码翻译请屏蔽这句话 提高速度
		entity=ConvertDictUtils.formatDicData(entity);
		return entity;
	}
    /**
	 * WlCustomer保存
	 * createDate creator
	 * updateDate updater 
	 * 字段会自动注入值 如不需要注入请修改WlCustomerEntity
	 * @param entity
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(WlCustomerEntity entity) {
		wlCustomerDao.insert(entity);
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(WlCustomerEntity entity) {
		wlCustomerDao.updateById(entity);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(String[] ids) {
		List<Integer> idArray=new ArrayList<>(ids.length);
		for(String id : ids){
			idArray.add(Integer.valueOf(id));
		}
		wlCustomerDao.deleteBatchIds(idArray);
	}
	@Override
	public void exportXls(Map<String, Object> params , HttpServletResponse response)  {
		List<WlCustomerEntity> list = this.list(params);
		ExcelUtils.exportExcelToTarget(response, "客户信息表导出列表", list, WlCustomerEntity.class);
	}
	
}