/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.permission.dept.service;


import com.qzsoft.demomis.modules.permission.dept.dto.SysDeptDTO;

import java.util.List;

/**
 * 部门管理
 * 
 * @author
 */
public interface SysDeptTreeService {

	/**
	 * 全加载
	 * @param rootId
	 * @return
	 */
	public List<SysDeptDTO> getDeptTreeAll(String rootId) ;
	/**
	 * 取根节点
	 * @param rootId
	 * @return
	 */
	public List<SysDeptDTO> getDeptTreeRoot(String rootId);

	/**
	 * 传入父节点,返回孩子节点,取回根据机构树节点,返回子节点,实现机构树加载
	 * @param pid
	 * @return
	 */
	public List<SysDeptDTO> getDeptTreeChildren(String pid);

	/**
	 * 取所有父节点
	 * @param id
	 * @return
	 */
	public List<String> getDeptNodePids(String id);

	/**
	 * 取回机构树选择的条件
	 * @param b001TreeDTOList
	 * @return
	 */
	public String getDeptTreeCheckedCondition(List<SysDeptDTO> b001TreeDTOList);

	/**
	 * 取回选在树节点后的sql条件,例如选在 顶层含下级 id like '1%'
	 * @param b001TreeDTOList
	 * @param tableName
	 * @param strField
	 * @return
	 */
	public String getDeptTreeCheckedCondition(List<SysDeptDTO> b001TreeDTOList, String tableName,String strField);


}