package com.qzsoft.demomis.modules.customer.wlcustomer.entity;

import java.util.Date;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.*;
import java.io.Serializable;
import com.qzsoft.jeemis.common.annotation.Dict;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import java.util.Map;


/**
 * 客户信息表(WlCustomer)表实体类
 *
 * @author sdmq
 * @since 2020-07-14 13:50:05
 */
@ApiModel(value ="客户信息表")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("wl_customer")
public class WlCustomerEntity extends Model<WlCustomerEntity> {
    private static final long serialVersionUID = 641736384124929539L;

   /**
    *id主键
    */
    @TableId(type = IdType.ID_WORKER) //主键ID_WORKER策略
    @NotNull(message="{id.require}", groups = UpdateGroup.class)
    @ApiModelProperty(value = "id主键")
    private Long id;
   /**
    *公司名称
    */
    @ApiModelProperty(value = "公司名称")
    private String name;
   /**
    *客户联系人

    */
    @ApiModelProperty(value = "客户联系人 ")
    private String linkName;
   /**
    *客户联系人电话
    */
    @ApiModelProperty(value = "客户联系人电话")
    private String phone;
   /**
    *状态：1正常，2异常
    */
    @ApiModelProperty(value = "状态：1正常，2异常")
    private String status;
   /**
    *备注
    */
    @ApiModelProperty(value = "备注")
    private String remark;
   /**
    *创建人
    */
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private String creator;
   /**
    *创建时间
    */
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
   /**
    *编辑人
    */
    @ApiModelProperty(value = "编辑人")
    private String updator;
   /**
    *编辑时间
    */
   @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "编辑时间")
    private Date updateDate;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}