package com.qzsoft.demomis.modules.sansi;

/*
* 异或测试
* */
public class YiHuo {
    public  static  void main (String[] args) {
        int[] numarry = new int[]{1,2,3,2,3};
        int aim = 2;
        for(int i = 1; i < 5; i++)
        {
            aim = aim ^ numarry[i];
        }
        /*
        * 0  -->  1
        * 1  -->  2
        * 2  -->  3
        * 3  -->  2
        * 4  -->  3
         * */
        System.out.println("最后："+aim);
    }
}
