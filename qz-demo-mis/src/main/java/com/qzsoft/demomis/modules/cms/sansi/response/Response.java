package com.qzsoft.demomis.modules.cms.sansi.response;


import com.qzsoft.demomis.modules.cms.Basic;
import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.Constant;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

public abstract class Response extends Basic {
    protected abstract void decoder() throws IOException, CmsException;

    protected void basicDecoder() throws CmsException, IOException {
        if (bytes != null) {
            decoderAddress();
            decoderCheck();
            decoderData();
            checkResponse();
            bytes = unesc(bytes);
        }
    }

    protected void decoderAddress() throws CmsException {
        address = getArray(1, 2, CmsException.ADDRESS_EXCEPTION);
    }

    protected void decoderCheck() throws CmsException {
        check = getArray(bytes.length - 3, 2, CmsException.CHECK_EXCEPTION);
    }

    protected void decoderData() throws CmsException {
        data = getArray(1, -3, CmsException.DATA_EXCEPTION);
    }

    protected void checkResponse() throws IOException, CmsException {
        if (Constant.checkResponse) {
            System.out.println(CodecUtil.bytesToHexString(data));
            byte[] checked = CodecUtil.crc16Bytes(unesc(data));
            System.out.println(CodecUtil.bytesToHexString(unesc(data)));
            if (checked.length != 2 || checked[0] != check[0]
                    || checked[1] != check[1]) {
                throw new CmsException(
                        CmsException.CHECK_DATA_EXCEPTION);
            }

        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
