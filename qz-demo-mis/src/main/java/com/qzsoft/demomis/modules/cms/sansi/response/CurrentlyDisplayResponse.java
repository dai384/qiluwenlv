package com.qzsoft.demomis.modules.cms.sansi.response;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;


public class CurrentlyDisplayResponse extends Response {
    private int num;
    private int time;
    private int way;
    private int speek;
    private String msg;

    public CurrentlyDisplayResponse(byte[] bytes) throws IOException,
            CmsException {
        if (bytes != null) {
            this.bytes = bytes;
            decoder();
        }
    }

    @Override
    protected void decoder() throws CmsException, IOException {
        basicDecoder();
        System.out.println(getString(3, -3, ""));
        num = getInt(3, 3, CmsException.NUM_EXCEPTION);
        time = getInt(6, 5, CmsException.STOP_TIME_EXCEPTION);
        way = getInt(11, 2, CmsException.DISPLAY_WAY_EXCEPTION);
        speek = getInt(13, 5, CmsException.DISPLAY_SPEEK_EXCEPTION);
        msg = getString(18, -3, CmsException.CONTENT_EXCEPTION);
    }

    public int getNum() {
        return num;
    }

    public int getTime() {
        return time;
    }

    public int getWay() {
        return way;
    }

    public int getSpeek() {
        return speek;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
