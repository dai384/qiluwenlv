package com.qzsoft.demomis.modules.resource.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qzsoft.demomis.modules.resource.entity.ResourceStrtistic;
import org.apache.ibatis.annotations.Mapper;
import com.qzsoft.demomis.modules.resource.entity.WlResourceEntity;

import java.util.List;
import java.util.Map;

/**
 * 资源表(WlResource)表数据库访问层
 *
 * @author sdmq
 * @since 2020-06-05 17:01:23
 */
@Mapper
public interface WlResourceDao extends BaseMapper<WlResourceEntity> {

    List<WlResourceEntity> selectReasourceByType(String type);

    WlResourceEntity selectById(String id);


    List<ResourceStrtistic> getResource();
    ResourceStrtistic getResourceSum();

    int updateBySelfId(Map<String, Object> params);
}