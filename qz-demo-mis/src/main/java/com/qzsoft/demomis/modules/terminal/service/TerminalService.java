package com.qzsoft.demomis.modules.terminal.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.terminal.entity.TerminalEntity;
import com.qzsoft.jeemis.common.utils.Result;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;
/**
 * (WlEquipmentinfo)表服务接口
 *
 * @author sdmq
 * @since 2020-05-27 09:07:02
 */
public interface TerminalService {
    /**
	 * 数据分页
	 * @param params
	 * @return IPage
	 */
	IPage<TerminalEntity> page(Map<String, Object> params);
    
	/**
	 * 数据列表
	 * @param params
	 * @return
	 */
	List<TerminalEntity> list(Map<String, Object> params);

	/**
	 * 单个数据
	 * @param id
	 * @return
	 */
	TerminalEntity get(String id);

	/**
	 * 保存
	 * @param entity
	 */
	void save(TerminalEntity entity);

	/**
	 * 更新
	 * @param entity
	 */
	void update(TerminalEntity entity);

	/**
	 * 批量删除
	 * @param ids
	 */
	void delete(String[] ids);

	/**
	 * 导出Excel
	 * @param params
	 * @param response
	 */
	void exportXls(Map<String, Object> params , HttpServletResponse response);


	/**
	 * 读取excel文件
	 */
	Result readExcelFile(MultipartFile file, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse);

}