/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.permission.role.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.permission.role.dto.SysRoleDTO;
import com.qzsoft.demomis.modules.permission.role.service.SysRoleDataScopeService;
import com.qzsoft.demomis.modules.permission.role.service.SysRoleMenuService;
import com.qzsoft.demomis.modules.permission.role.service.SysRoleService;
import com.qzsoft.demomis.modules.permission.role.service.SysRoleUserService;
import com.qzsoft.demomis.repository.sys.dao.SysRoleDao;
import com.qzsoft.demomis.repository.sys.dao.SysRoleUserDao;
import com.qzsoft.demomis.repository.sys.entity.SysMenuEntity;
import com.qzsoft.demomis.repository.sys.entity.SysRoleEntity;
import com.qzsoft.demomis.repository.sys.entity.SysRoleUserEntity;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.exception.RenException;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ConvertUtils;
import com.qzsoft.jeemis.platform.security.user.UserDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 角色
 *
 * @author
 */
@Service
public class SysRoleServiceImpl extends BaseService implements SysRoleService {
	@Autowired
	private SysRoleMenuService sysRoleMenuService;
	@Autowired
	private SysRoleDataScopeService sysRoleDataScopeService;
	@Autowired
	private SysRoleUserService sysRoleUserService;
	@Autowired
	private SysRoleDao sysRoleDao;
	@Autowired
	private SysRoleUserDao sysRoleUserDao;


	@Override
	public IPage<SysRoleEntity> page(Map<String, Object> params) {

		IPage<SysRoleEntity> page =this.getPage(params,Constant.CREATE_DATE,false,SysRoleEntity.class) ;
		QueryWrapper<SysRoleEntity> queryWrapper =this.getQueryWrapper(params,SysRoleEntity.class,"name");
		IPage<SysRoleEntity> pageData = sysRoleDao.selectPage(page, queryWrapper);
		return pageData;
	}


	@Override
	public List<SysRoleEntity> list(Map<String, Object> params) {
		QueryWrapper<SysRoleEntity> queryWrapper = this.getQueryWrapper(params,SysRoleEntity.class,"name");;
		List<SysRoleEntity> entityList = sysRoleDao.selectList(queryWrapper);
		return entityList;
	}


	@Override
	public SysRoleDTO get(Long id) {
		SysRoleEntity entity = sysRoleDao.selectById(id);

		return ConvertUtils.sourceToTarget(entity, SysRoleDTO.class);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(SysRoleDTO dto) {
		SysRoleEntity entity = ConvertUtils.sourceToTarget(dto, SysRoleEntity.class);
		// 判断是否已经存在角色
		QueryWrapper<SysRoleEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq("name",dto.getName());
		if (0 < sysRoleDao.selectCount(queryWrapper)){
			throw(new RenException("用户名重复"));
		}

		//保存角色
		sysRoleDao.insert(entity);

		//保存角色菜单关系
		sysRoleMenuService.saveOrUpdate(entity.getId(), dto.getMenuIdList());

		//保存角色数据权限关系
		sysRoleDataScopeService.saveOrUpdate(entity.getId(), dto.getDeptIdList());

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(SysRoleDTO dto) {
		SysRoleEntity entity = ConvertUtils.sourceToTarget(dto, SysRoleEntity.class);

		//更新角色
		sysRoleDao.updateById(entity);

		//更新角色菜单关系
		sysRoleMenuService.saveOrUpdate(entity.getId(), dto.getMenuIdList());

		//更新角色数据权限关系
		sysRoleDataScopeService.saveOrUpdate(entity.getId(), dto.getDeptIdList());
	}




	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(Long[] ids) {
		// 判断角色是否使用
		QueryWrapper<SysRoleUserEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.in("role_id",ids);
		if  (sysRoleUserDao.selectCount(queryWrapper) > 0){
			throw(new RenException("角色正在使用不允许删除"));
		}

		// 判断角色是否系统内置角色
		QueryWrapper<SysRoleEntity> queryWrapper2=new QueryWrapper<>();
		queryWrapper2.in("id",ids).eq("has_system",1);
		if  (sysRoleDao.selectCount(queryWrapper2) > 0){
			throw(new RenException("系统角色不允许删除"));
		}

		//删除角色
		sysRoleDao.deleteBatchIds(Arrays.asList(ids));

		//删除角色用户关系
		sysRoleUserService.deleteByRoleIds(ids);

		//删除角色菜单关系
		sysRoleMenuService.deleteByRoleIds(ids);

		//删除角色数据权限关系
		sysRoleDataScopeService.deleteByRoleIds(ids);
	}

	/**
	 * 强制删除
	 * @param ids
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete2(Long[] ids) {

		//删除角色
		sysRoleDao.deleteBatchIds(Arrays.asList(ids));

		//删除角色用户关系
		sysRoleUserService.deleteByRoleIds(ids);

		//删除角色菜单关系
		sysRoleMenuService.deleteByRoleIds(ids);

		//删除角色数据权限关系
		sysRoleDataScopeService.deleteByRoleIds(ids);
	}

	@Override
	public List<SysMenuEntity> getUserMenuList(UserDetail user) {
		return null;
	}
}