package com.qzsoft.demomis.modules.cms.sansi;

import com.qzsoft.demomis.modules.cms.CmsConn;
import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.sansi.request.*;
import com.qzsoft.demomis.modules.cms.sansi.response.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;


/**
 * 情报板的通信函数
 */
public class SansiConn extends CmsConn {
    private static Logger logger = LoggerFactory.getLogger(CmsConn.class);

    static String ip = "127.0.0.1";
    static int port = 7211;

    public static void main (String args[] ){
        try {
            GetNetworkResponse getNetworkResponse = getNetwork(ip, port);

            System.out.println(getNetworkResponse.toString());
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 通过网口调用可变信息标志的当前故障
     *
     * @param ip
     * @param port
     * @return
     * @throws CmsException
     * @throws IOException
     * @throws InterruptedException
     */
    public static BugResponse getBug(String ip, int port)
            throws CmsException, IOException, InterruptedException {
        return new BugResponse(send(ip, port, new BugRequest().encoder()));
    }

    /**
     * 通过网口关闭情报板
     *
     * @param ip
     * @param port
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static CloseDisplayResponse closeDisplay(String ip, int port)
            throws IOException, CmsException, InterruptedException {
        return new CloseDisplayResponse(send(ip, port,
                new CloseDisplayRequest().encoder()));
    }

    /**
     * 通过网口取可变信息标志的当前显示内容
     *
     * @param ip
     * @param port
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static CurrentlyDisplayResponse getCurrently(String ip, int port)
            throws IOException, CmsException, InterruptedException {
        return new CurrentlyDisplayResponse(send(ip, port,
                new CurrentlyDisplayRequest().encoder()));
    }

    /**
     * 通过网口使可变信息标志显示预置的播放表
     *
     * @param ip
     * @param port
     * @param num
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static DisplayDefaultResponse setDefaultList(String ip, int port, int num) throws IOException, CmsException, InterruptedException {
        DisplayDefaultRequest request = new DisplayDefaultRequest();
        request.setNum(num);

        return new DisplayDefaultResponse(send(ip, port, request.encoder()));
    }

    /**
     * 通过网口从可变信息标志下载文件
     *
     * @param ip
     * @param port
     * @param fileName
     * @return
     * @throws CmsException
     * @throws IOException
     * @throws InterruptedException
     */
    public static DownloadFileResponse downloadFile(String ip, int port, String fileName)
            throws CmsException, IOException, InterruptedException {
        DownloadFileRequest request = new DownloadFileRequest();
        request.setName(fileName);

        byte[] bytes = send(ip, port, request.encoder());
        DownloadFileResponse response = new DownloadFileResponse();

        while (response.addData(bytes)) {
            request.addOffset();
            bytes = send(ip, port, request.encoder());
        }

        return response;
    }

    /**
     * 通过网口读取当前扫描板的日期和时间
     *
     * @param ip
     * @param port
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static GetDateTimeResponse getDateTime(String ip, int port)
            throws IOException, CmsException, InterruptedException {
        return new GetDateTimeResponse(send(ip, port,
                new GetDateTimeRequest().encoder()));
    }

    /**
     * 通过网口读取 LED 故障信息
     *
     * @param ip
     * @param port
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static GetLEDErrorResponse getLedError(String ip, int port)
            throws IOException, CmsException, InterruptedException {
        return new GetLEDErrorResponse(send(ip, port,
                new GetLEDErrorRequest().encoder()));
    }

    /**
     * 通过网口取可变信息标志的当前亮度调节方式和显示亮度
     *
     * @param ip
     * @param port
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static GetLightResponse getLight(String ip, int port)
            throws IOException, CmsException, InterruptedException {
        return new GetLightResponse(send(ip, port,
                new GetLightRequest().encoder()));
    }

    /**
     * 通过网口读取以太网配置信息
     *
     * @param ip
     * @param port
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static GetNetworkResponse getNetwork(String ip, int port)
            throws IOException, CmsException, InterruptedException {
        return new GetNetworkResponse(send(ip, port,
                new GetNetworkRequest().encoder()));
    }

    /**
     * 通过网口读取充放电控制器信息 (太阳能屏专用)
     *
     * @param ip
     * @param port
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static GetSolarControlResponse getSolarControl(String ip, int port)
            throws IOException, CmsException, InterruptedException {
        return new GetSolarControlResponse(send(ip, port,
                new GetSolarControlRequest().encoder()));
    }

    /**
     * 通过网口读取充放电控制器历史记录及时间(太阳能屏专用)
     *
     * @param ip
     * @param port
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static GetSolarDateTimeResponse getSolarDateTime(String ip, int port)
            throws IOException, CmsException, InterruptedException {
        return new GetSolarDateTimeResponse(send(ip, port,
                new GetSolarDateTimeRequest().encoder()));
    }

    /**
     * 通过网口开启情报板
     *
     * @param ip
     * @param port
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static OpenDisplayResponse open(String ip, int port)
            throws IOException, CmsException, InterruptedException {
        return new OpenDisplayResponse(send(ip, port,
                new OpenDisplayRequest().encoder()));
    }

    /**
     * 通过网口重启情报板
     *
     * @param ip
     * @param port
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static ResetDisplayResponse reset(String ip, int port)
            throws IOException, CmsException, InterruptedException {
        return new ResetDisplayResponse(send(ip, port,
                new ResetDisplayRequest().encoder()));
    }

    /**
     * 通过网口设置扫描控制板的日期和时间
     *
     * @param ip
     * @param port
     * @param date
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static SetDateTimeResponse setDateTime(String ip, int port, Date date)
            throws IOException, CmsException, InterruptedException {
        SetDateTimeRequest request = new SetDateTimeRequest();
        request.setDate(date);

        return new SetDateTimeResponse(send(ip, port, request.encoder()));
    }

    /**
     * 通过网口设置可变信息标志的亮度调节方式和显示亮度
     *
     * @param ip
     * @param port
     * @param light
     * @param way
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static SetLightResponse setLight(String ip, int port, int light, int way) throws IOException, CmsException, InterruptedException {
        if (way == 1) {//手动调节
            SetLightTypeRequest setLightTypeRequest = new SetLightTypeRequest();
            setLightTypeRequest.setWay(1);
            send(ip, port, setLightTypeRequest.encoder());
            SetLightRequest request = new SetLightRequest();
            request.setLight(light);
            return new SetLightResponse(send(ip, port, request.encoder()));
        } else {//自动调节
            SetLightTypeRequest setLightTypeRequest = new SetLightTypeRequest();
            setLightTypeRequest.setWay(0);
            return new SetLightResponse(send(ip, port, setLightTypeRequest.encoder()));
        }

    }

    /**
     * 通过网口设置以太网信息
     *
     * @param ip
     * @param port
     * @param dip     IP地址
     * @param dmac    Mac地址，无分隔符
     * @param dmask   子网掩码
     * @param dgetway 默认网关
     * @param dport   端口
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static SetNetworkResponse setNetwork(String ip, int port, String dip, String dmac, String dmask, String dgetway, int dport)
            throws IOException, CmsException, InterruptedException {
        SetNetworkRequest request = new SetNetworkRequest();
        request.setMac(dmac);
        request.setIp(dip);
        request.setMask(dmask);
        request.setGeteway(dgetway);
        request.setPort(dport);

        return new SetNetworkResponse(send(ip, port, request.encoder()));
    }

    /**
     * 通过网口设置充放电控制器的日期和时间(太阳能屏专用)
     *
     * @param ip
     * @param port
     * @param date
     * @return
     * @throws IOException
     * @throws CmsException
     * @throws InterruptedException
     */
    public static SetSolarDateTimeResponse setSolarDateTime(String ip, int port, Date date)
            throws IOException, CmsException, InterruptedException {
        SetSolarDateTimeRequest request = new SetSolarDateTimeRequest();
        request.setDate(date);

        return new SetSolarDateTimeResponse(send(ip, port, request.encoder()));
    }

    /**
     * 通过网口上传文件
     *
     * @param ip
     * @param port
     * @param fileName
     * @param bytes
     * @return
     * @throws IOException
     * @throws InterruptedException
     * @throws CmsException
     */
    public static UploadFileResponse uploadFile(String ip, int port, String fileName, byte[] bytes)
            throws IOException, InterruptedException, CmsException {
        UploadFileResponse response = null;
        UploadFileRequest request = new UploadFileRequest();
        request.setName(fileName);
        request.setContent(bytes);

        byte[][] data = request.encoder();

        if (data != null) {
            for (byte[] b : data) {
                response = new UploadFileResponse(send(ip, port, b));
            }
        }

        return response;
    }

    /**
     * 通过网口上传文件
     *
     * @param ip
     * @param port
     * @param fileName
     * @param file
     * @return
     * @throws IOException
     * @throws InterruptedException
     * @throws CmsException
     */
    public static UploadFileResponse uploadFile(String ip, int port, String fileName, File file)
            throws IOException, InterruptedException, CmsException {
        return uploadFile(ip, port, fileName, readFile(file));
    }

    /**
     * 通过网口上传文件
     *
     * @param ip
     * @param port
     * @param fileName
     * @param file
     * @return
     * @throws IOException
     * @throws InterruptedException
     * @throws CmsException
     */
    public static UploadFileResponse uploadFile(String ip, int port, String fileName, String file)
            throws IOException, InterruptedException, CmsException {
        return uploadFile(ip, port, fileName, readFile(new File(file)));
    }

    /**
     * 读取文件
     *
     * @param file
     * @return
     * @throws IOException
     */
    private static byte[] readFile(File file) throws IOException {
        FileInputStream in = new FileInputStream(file);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] bytes = new byte[1024];
        int len = 0;

        while ((len = in.read(bytes)) > 0) {
            out.write(bytes, 0, len);
        }

        byte[] data = out.toByteArray();
        in.close();
        out.close();

        return data;
    }
}
