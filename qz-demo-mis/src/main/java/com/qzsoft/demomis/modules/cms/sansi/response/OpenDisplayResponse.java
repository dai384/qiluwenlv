package com.qzsoft.demomis.modules.cms.sansi.response;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 开启情报板应答
 *
 * @author frank
 */
public class OpenDisplayResponse extends Response {
    private int code;

    public OpenDisplayResponse(byte[] bytes) throws IOException,
            CmsException {
        this.bytes = bytes;
        decoder();
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        code = getInt(3, 1, CmsException.CONTENT_EXCEPTION);
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
