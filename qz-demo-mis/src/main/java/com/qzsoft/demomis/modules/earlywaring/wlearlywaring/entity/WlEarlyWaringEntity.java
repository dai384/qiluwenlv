package com.qzsoft.demomis.modules.earlywaring.wlearlywaring.entity;

import java.util.Date;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.*;
import java.io.Serializable;
import com.qzsoft.jeemis.common.annotation.Dict;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import java.util.Map;


/**
 * 智能箱报警数据推送接收表(WlEarlyWaring)表实体类
 *
 * @author sdmq
 * @since 2020-08-14 09:44:36
 */
@ApiModel(value ="智能箱报警数据推送接收表")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("wl_early_waring")
public class WlEarlyWaringEntity extends Model<WlEarlyWaringEntity> {
    private static final long serialVersionUID = 337604397318883406L;

   /**
    *主键
    */
    @TableId
    @NotNull(message="{id.require}", groups = DefaultGroup.class)
    @ApiModelProperty(value = "主键")
    private Long id;
   /**
    *报警描述
    */
    @ApiModelProperty(value = "报警描述")
    private String description;
   /**
    *智能型名称
    */
    @ApiModelProperty(value = "智能型名称")
    private String name;
   /**
    *报警级别
    */
    @ApiModelProperty(value = "报警级别")
    private Integer level;
   /**
    *报警时间
    */
    @ApiModelProperty(value = "报警时间")
    private String time;
   /**
    *remark
    */
    @ApiModelProperty(value = "remark")
    private String remark;
   /**
    *createDate
    */
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "createDate")
    private Date createDate;
   /**
    *creator
    */
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "creator")
    private String creator;
   /**
    *updateDate
    */
   @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "updateDate")
    private Date updateDate;
   /**
    *updator
    */
    @ApiModelProperty(value = "updator")
    private String updator;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}