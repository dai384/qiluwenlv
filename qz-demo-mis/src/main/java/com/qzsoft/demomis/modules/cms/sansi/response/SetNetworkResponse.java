package com.qzsoft.demomis.modules.cms.sansi.response;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 设置以太网信息应答
 *
 * @author frank
 */
public class SetNetworkResponse extends Response {
    private int code1;
    private int code2;

    public SetNetworkResponse(byte[] bytes) throws IOException,
            CmsException {
        this.bytes = bytes;
        decoder();
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        basicDecoder();
        code1 = bytes[3];
        code2 = bytes[4];
    }

    public int getCode1() {
        return code1;
    }

    public int getCode2() {
        return code2;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
