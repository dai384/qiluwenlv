package com.qzsoft.demomis.modules.amapoffline;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class OffLine {
    /**
     * 远程文件下载
     * @param url 下载地址
     * @param file 保存文件地址
     */
    public static boolean download(URL url, File file) throws IOException {
        boolean flag = true;
        DataOutputStream dos = null;
        DataInputStream dis = null;
        try {
            if(!file.getParentFile().exists()) file.getParentFile().mkdirs();
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            dos = new DataOutputStream(new FileOutputStream(file));
            dis = new DataInputStream(conn.getInputStream());
            byte[] data = new byte[2048];
            int i = 0;
            while ((i = dis.read(data)) != -1) {
                dos.write(data, 0, i);
            }
            dos.flush();
            System.out.println("下载url===========================================================:"+url);
        } catch (IOException e) {
            flag = false;
            throw e;
        } finally {
            if(dis != null) dis.close();
            if(dos != null) dos.close();
        }
        return flag;
    }

    /**
     * 计算分辨率
     * @param maxLevel 最大级别
     */
    public static double[] getResolutions(int maxLevel){
        double max = 360.0/256.0;
        double[] resolutions = new double[maxLevel+1];
        for(int z=0;z<=maxLevel;z++) resolutions[z] = max/Math.pow(2, z);
        return resolutions;
    }

    /**
     * 根据经度获取切片规范下的行号
     *
     * @param lon
     * @param zoom
     * @return
     */
    public static int getOSMTileXFromLongitude(double lon, int zoom) {
        return (int) (Math.floor((lon + 180) / 360 * Math.pow(2, zoom)));
    }

    /**
     * 根据纬度获取切片规范下的列号
     *
     * @param lat
     * @param zoom
     * @return
     */
    public static int getOSMTileYFromLatitude(double lat, int zoom) {
        return (int) (Math.floor((1 - Math.log(Math.tan(lat * Math.PI / 180) + 1 / Math.cos(lat * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, zoom)));
    }

    public static void main(String[] arg) throws IOException{
//      String type = "ArcGIS";
        String type = "CUSTOM";
        double[] extent = {114.614143,34.133565,122.765999,38.349347};
        for(int z=0;z<14;z++){
            //计算行列号(使用瓦片的中心点经纬度计算)
            //起始结束行
            int minR = getOSMTileYFromLatitude(extent[3], z);
            int maxR = getOSMTileYFromLatitude(extent[1], z);
            //起始结束列
            int minC = getOSMTileXFromLongitude(extent[0], z);
            int maxC = getOSMTileXFromLongitude(extent[2], z);
            for(int y=minR;y<=maxR;y++){
                for(int x=minC;x<=maxC;x++){
//                    String urlstr = "http://t0.tianditu.com/DataServer?T=vec_w&x="+x+"&y="+y+"&l="+z; //天地图服务器t0-t8间选一个
//                    String urlstr = "http://mt2.google.cn/vt/lyrs=m&scale=1&hl=zh-CN&gl=cn&x="+x+"&y="+y+"&z="+z; //谷歌地图服务器t0-t2间选一个
//                    String urlstr = "http://online3.map.bdimg.com/onlinelabel/?qt=tile&x="+x+"&y="+y+"&z="+z+"&&styles=pl&udt=20170712&scaler=1&p=1"; //百度地图（加密过的）
//                    String urlstr = "http://c.tile.opencyclemap.org/cycle/"+z+"/"+x+"/"+y+".png"; //osm地图
                    String urlstr = "http://wprd04.is.autonavi.com/appmaptile?x="+x+"&y="+y+"&z="+z+"&lang=zh_cn&size=1&scl=1&style=8"; //高德地图(6：影像，7：矢量，8：影像路网)

                    String path = null;
                    if(type.equals("ArcGIS")) {
                        //ArcGIS格式瓦片下载
                        path = getTDTilesForArcGISPath(x,y,z);
                    }else {
                        //一般格式瓦片下载
                        path = getTDTilesForCustomPath(x,y,z);
                    }

                    File file = new File(path);
                    URL url = new URL(urlstr);
                    download(url,file);
                }
            }
        }
    }

    public static String getTDTilesForArcGISPath(int x,int y,int z) {
        String l = "L" + String.format("%02d",z);
        String r = "R" + makePath(y);
        String c = "C" + makePath(x);
        String path = "D:/山东地图amap打包/"+l+ File.separator+r+File.separator+c+".png";
        return path;
    }

    public static String getTDTilesForCustomPath(int x,int y,int z) {
        String path = "D:/山东地图amap打包/"+z+File.separator+y+File.separator+x+".png";
        return path;
    }

    private static String makePath(int num) {
        String str = Integer.toHexString(num);
        //ArcGIS行列都是8位长度
        while(str.length() < 8) {
            str = "0" + str;
        }
        return str;
    }
}