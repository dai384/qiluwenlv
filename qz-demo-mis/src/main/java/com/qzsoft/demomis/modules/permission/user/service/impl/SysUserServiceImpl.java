/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.permission.user.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.permission.dept.service.SysDeptService;
import com.qzsoft.demomis.modules.permission.role.service.SysRoleUserService;
import com.qzsoft.demomis.modules.permission.user.dto.PasswordDTO;
import com.qzsoft.demomis.modules.permission.user.dto.SysUserDTO;
import com.qzsoft.demomis.modules.permission.user.service.SysUserService;
import com.qzsoft.demomis.repository.sys.dao.SysUserDao;
import com.qzsoft.demomis.repository.sys.entity.SysUserEntity;
import com.qzsoft.jeemis.common.cacheutils.CacheGenUtils;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.enums.SuperAdminEnum;
import com.qzsoft.jeemis.common.exception.ErrorCode;
import com.qzsoft.jeemis.common.exception.RenException;
import com.qzsoft.jeemis.common.query.ConditionBuilder;
import com.qzsoft.jeemis.common.query.ConditionDTO;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ConvertUtils;
import com.qzsoft.jeemis.platform.security.password.PasswordUtils;
import com.qzsoft.jeemis.platform.security.user.SecurityUser;
import com.qzsoft.jeemis.platform.security.user.UserDetail;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 系统用户
 * 
 * @author
 */
@Service
public class SysUserServiceImpl extends BaseService implements SysUserService {
	@Autowired
	private SysRoleUserService sysRoleUserService;
	@Autowired
	private SysDeptService sysDeptService;
	@Autowired
	private SysUserDao sysUserDao;

	@Override
	public IPage<SysUserEntity> page(Map<String, Object> params) {
		UserDetail userDetail = this.basegetLoginUser();
		//分页
		IPage<SysUserEntity> page = getPage(params, Constant.CREATE_DATE, false,SysUserEntity.class);

		QueryWrapper<SysUserEntity> query= this.getQueryWrapper(params,SysUserEntity.class,"username");
		//普通管理员，只能查询所属部门及子部门的数据
		if(userDetail.getSuperAdmin() == SuperAdminEnum.NO.value()) {
			query.likeRight("dept_id",userDetail.getDeptId());
		}
		//查询
		IPage<SysUserEntity> list = sysUserDao.selectPage(page,query);
		return list;
	}

	@Override
	public List<SysUserEntity> list(Map<String, Object> params) {
		// 通用条件生成器
		QueryWrapper<SysUserEntity> queryUser=this.getQueryWrapper(params,SysUserEntity.class,"username");

		List<SysUserEntity> sysUserList =sysUserDao.selectList(queryUser) ;
		return sysUserList;
	}

	private  QueryWrapper<SysUserEntity> getQueryWrapperx(Map<String, Object> params){
		UserDetail userDetail = this.basegetLoginUser();
		QueryWrapper<SysUserEntity> queryUser= new QueryWrapper<>();
		//普通管理员，只能查询所属部门及子部门的数据
		queryUser.allEq(params);
		if(userDetail.getSuperAdmin() == SuperAdminEnum.NO.value()) {
			queryUser.likeRight("dept_id",userDetail.getDeptId());
		}
		return  queryUser;
	}
	private  QueryWrapper<SysUserEntity> getQueryWrapperx(ConditionDTO condition){
		ConditionBuilder conditionBuilder = new ConditionBuilder();
		conditionBuilder.addCondition(condition);
		UserDetail userDetail = this.basegetLoginUser();
		QueryWrapper<SysUserEntity> queryUser= conditionBuilder.toQueryWrapper();
		//普通管理员，只能查询所属部门及子部门的数据
		if(userDetail.getSuperAdmin() == SuperAdminEnum.NO.value()) {
			queryUser.likeRight("dept_id",userDetail.getDeptId());
		}
		return  queryUser;
	}



	@Override
	public SysUserDTO get(Long id) {
		SysUserEntity entity = sysUserDao.selectById(id);
		return ConvertUtils.sourceToTarget(entity, SysUserDTO.class);
	}

	@Override
	public SysUserDTO getByUsername(String username) {
		QueryWrapper<SysUserEntity> queryUser=new QueryWrapper<>();
		queryUser.eq("username",username);
		SysUserEntity entity = sysUserDao.selectOne(queryUser);
		return ConvertUtils.sourceToTarget(entity, SysUserDTO.class);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(SysUserDTO dto) {
		SysUserEntity entity = ConvertUtils.sourceToTarget(dto, SysUserEntity.class);

		//密码加密
		String password = PasswordUtils.encode(entity.getPassword());
		entity.setPassword(password);

		//保存用户
		// 处理用户的deppkid
		entity.setSuperAdmin(SuperAdminEnum.NO.value());
		entity.setDeptPkid(sysDeptService.getDeptPkIdById(entity.getDeptId()));
		sysUserDao.insert(entity);

		//保存角色用户关系
		sysRoleUserService.saveOrUpdate(entity.getId(), dto.getRoleIdList());
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(SysUserDTO dto) {
		SysUserEntity entity = ConvertUtils.sourceToTarget(dto, SysUserEntity.class);

		//密码加密
		if(StringUtils.isBlank(dto.getPassword())){
			entity.setPassword(null);
		}else{
			String password = PasswordUtils.encode(entity.getPassword());
			entity.setPassword(password);
		}

		//更新用户
		entity.setDeptPkid(sysDeptService.getDeptPkIdById(entity.getDeptId()));
		sysUserDao.updateById(entity);

		//更新角色用户关系
		sysRoleUserService.saveOrUpdate(entity.getId(), dto.getRoleIdList());
	}

	@Override
	public void delete(Long[] ids) {
		//删除用户
		sysUserDao.deleteBatchIds(Arrays.asList(ids));

		//删除角色用户关系
		sysRoleUserService.deleteByUserIds(ids);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updatePassword(PasswordDTO passwordDTO) {
		UserDetail user = SecurityUser.getUser();
		SysUserEntity userEntity = sysUserDao.selectById(user.getId());
		//原密码不正确
		if(!PasswordUtils.matches(passwordDTO.getPassword(), userEntity.getPassword())){
			throw(new RenException(ErrorCode.PASSWORD_ERROR));
		}
		String newPassword =passwordDTO.getNewPassword();
		newPassword = PasswordUtils.encode(newPassword);
		UpdateWrapper<SysUserEntity> updateWrapper=new UpdateWrapper<>();
		updateWrapper.eq("id",userEntity.getId()).set("password",newPassword);
		sysUserDao.update(new SysUserEntity(),updateWrapper);
		CacheGenUtils.delete(CacheGenUtils.SYS_USER_DETAIL + userEntity.getId());
	}

	@Override
	public void deleteBatchIds(Collection<? extends Serializable> idList) {
		sysUserDao.deleteBatchIds(idList);
	}

	@Override
	public int getCountByDeptId(String deptPkId) {
		QueryWrapper<SysUserEntity> queryUser=new QueryWrapper<>();
		queryUser.eq("dept_pkid",deptPkId);
		return sysUserDao.selectCount(queryUser);
	}


}
