/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.permission.dept.service;


import com.qzsoft.demomis.modules.permission.dept.excel.SysDeptExcel;
import com.qzsoft.demomis.repository.sys.entity.SysDeptEntity;

import java.util.List;

/**
 * 部门管理
 * 
 * @author
 */
public interface SysDeptService  {

	List<SysDeptEntity> list(String pid);

	List<SysDeptExcel> listAll();

	List<SysDeptEntity> listAll2();

	SysDeptEntity get(String id);

	SysDeptEntity saveDept(SysDeptEntity dto);

	void delete(String pkId);

	/**
	 *
	 * @param pkId1
	 * @param pkId2
	 */
	void saveDeptSort(String pkId1,String pkId2);


	String getDeptIdByPkId(String pkId);

	String getDeptPkIdById(String deptId);

}