package com.qzsoft.demomis.modules.sansi;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.Constant;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import com.qzsoft.demomis.modules.cms.sansi.request.Request;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.SerialPort;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SanSiSystem {
    protected static final Logger LOGGER = LoggerFactory.getLogger(SanSiSystem.class);

    private String programPath = "D:/qiluwenlv/program/sansi/xMedia1.xml";

    protected final byte blockSize = 0x52;
    protected final byte uploadFile = 0x53;
    protected final byte uploadPlayList = 0x01;
    protected final byte uploadPlayListEnd = 0x02;
    protected final byte startPlayList = 0x06;

    public int blockSizeInt;

    JdomXmlUtil xmlUtil = new JdomXmlUtil();
    Reassembly reassemb = new Reassembly();

    private static Map<String, String> map = Maps.newHashMap();

    // test
    public static void main(String[] args) throws Exception{
        String remoteIp = "127.0.0.1";
        int port = 7211;
//        int port = 1000;
        List<String> resourceList = new ArrayList<>();

        resourceList.add("/qiluwenlv/resource/imageSavePath/20200901/5c0171c7d0994e0fbb07164176e4a8cf.jpg");
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200901/b88a1c8e5ba94873acbf7a00e4009f22.jpg");

        SanSiSystem sanSiSystem = new SanSiSystem();
        sanSiSystem.sanSiOperation(remoteIp, port, resourceList);
    }


    public void sanSiOperation(String remoteIp, int port, List<String> resourceList) throws Exception{
        // 生成播放表
        xmlUtil.createXml(resourceList);

        LOGGER.info("==================create xml success===================");
        // 发送指令处理为二维数组
        // 1、查看blockSize
//            List<byte[]> list1 = Lists.newArrayList();
//            list1.add(reassembly(blockSize, "", 0));
//            byte[][] bytes1 = list1.toArray(new byte[list1.size()][]);
//            SocketConnet(remoteIp, port, bytes1);

       // 2、文件上传,

//        for (String resource: resourceList) {
//            List<byte[]> list2 = Lists.newArrayList();
//            // 查看resource长度，分包处理
//            int m = reassemb.queryLength(resource);
//            System.out.println("wenjian fenbao ，num："+m);
//            for (int i=0; i<m ;i++) {
//                System.out.println("wenjian，fenbao num ing："+m+", di n ci:   "+i);
//                list2.add(reassembly(uploadFile, resource, i));
//            }
//            byte[][] bytes2 = list2.toArray(new byte[list2.size()][]);
//            SocketConnet(remoteIp, port, bytes2);
//        }




        // 3、播放表上传
        List<byte[]> list3 = Lists.newArrayList();
        // 查看resource长度，分包处理
        int n = reassemb.queryCatalogueLength(programPath);
        System.out.println("wenjian fenbao ，num：："+n);
        for (int i=0; i<n ;i++) {
            System.out.println("wenjian，fenbao num ing："+n+", di n ci:   "+i);
            list3.add(reassembly(uploadPlayList, programPath, i));
        }
        byte[][] bytes3= list3.toArray(new byte[list3.size()][]);
        SocketConnet(remoteIp, port, bytes3);



        // 4、播放表发送完毕
//            SocketConnet(remoteIp, port, reassembly(uploadPlayListEnd));


            // 5、播放已上传的播放表
//            List<byte[]> list5 = Lists.newArrayList();
//            list5.add(reassembly(startPlayList, "", 0));
//            byte[][] bytes5 = list5.toArray(new byte[list5.size()][]);
//            SocketConnet(remoteIp, port, bytes5);

    }

    /*
    * 重新组装发送信息
    * */
    public byte[] reassembly(byte type, String  resource, int n) throws Exception{

        byte[] bytes = reassemb.reassemblyOperation(type, resource, n);

        return bytes;
    }


    /*
    * socket通信连接
    * */
    public byte[] SocketConnet(String remoteIp, int port, byte[][] bytes) throws Exception{

        if (StringUtils.isEmpty(remoteIp)) {
            throw new CmsException("IP 地址不能为空");
        }
        if (port < 0) {
            throw new CmsException("请使用正常的端口号，不能为" + port);
        }
//        if (StringUtils.startsWithIgnoreCase(remoteIp, "com")) {//串口通信
//            return serialCommand(remoteIp, port, bytes);
//        }
        byte[] data = null;
        Socket socket = new Socket();
        try {
            socket.connect(new InetSocketAddress(InetAddress.getByName(remoteIp), port),Constant.TIMEOUT);
            socket.setSoTimeout(Constant.TIMEOUT);
            InputStream in = socket.getInputStream();
            OutputStream out = socket.getOutputStream();

            if (bytes != null) {
                for (byte[] b : bytes) {
                    out.write(b);
                    out.flush();
                    // 十六进制指令
                    LOGGER.info("网络：" + remoteIp + ":" + port + "发送指令：" + CodecUtil.bytesToHexString(b));
                    int waitTime = 0;
                    while (true) {
                        int len = in.available();
                        if (len > 0) {
                            data = new byte[len];
                            in.read(data);
                            // 十六进制指令
                            String responseStr = CodecUtil.bytesToHexString(data);
                            String type = responseStr.substring(2, 4);
                            switch (type){
                                case "52":
                                    // 十进制int 转为十六进制
                                    int blockSizeInt = Integer.parseInt(responseStr.substring(6,14),16);
                                    LOGGER.info("网络：" + remoteIp + ":" + port + "收到应答： blockSize为" + blockSizeInt);
                                    break;
                                default: break;
                            }
                            LOGGER.info("网络：" + remoteIp + ":" + port + "收到应答：" + CodecUtil.bytesToHexString(data));
                            break;
                        }
                        waitTime += 500;
                        if (waitTime >Constant.TIMEOUT) {
                            throw new CmsException("等待指令返回超时");
                        }
                        Thread.sleep(500);
                    }
                }
            }
            out.close();
            in.close();
        } finally {
            socket.close();
            LOGGER.info("socket关闭！！！");
        }
        return data;
    }

    /**
     * 串口通信
     *
     * @param serial
     * @param request
     * @return
     * @throws CmsException
     * @throws IOException
     */
    protected static byte[] serialCommand(String serial, int speed, Request request) throws CmsException, IOException {
        if (StringUtils.isEmpty(serial)) {
            throw new CmsException("串口标识不能为空");
        }
        if (!map.keySet().contains(serial)) {
            LOGGER.info("第一次使用串口{}，建立同步锁", serial);
            map.put(serial, serial);
        } else {
            LOGGER.info("串口{}，已在同步锁中", serial);
        }
        LOGGER.info("串口{}，开始等待同步锁", serial);
        synchronized (map.get(serial)) {
            LOGGER.info("串口{}，打开同步锁，开始串口通信", serial);
            return serialCommand(serial, speed, request.encoder());
        }
    }


    protected static byte[] serialCommand(String serial, int speed, byte[] bytes) throws CmsException, IOException {
        long start = System.currentTimeMillis();
        SerialPort serialPort = openSerialPort(serial, speed);
        LOGGER.info("打开串口用时 {} 毫秒", (System.currentTimeMillis() - start));
        try {
            InputStream in = serialPort.getInputStream();
            OutputStream out = serialPort.getOutputStream();
            out.write(bytes);
            out.flush();
            LOGGER.info("串口：" + serial + "发送指令：" + CodecUtil.bytesToHexString(bytes));
            byte[] temp = new byte[524288];
            int len = 0;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] data = null;
            LOGGER.info("串口：" + serial + "等待返回数据");
            while ((len = in.read(temp)) > 0) {
                LOGGER.info("串口：" + serial + "有返回数据，开始读取");
                bout.write(temp, 0, len);
                LOGGER.info("串口：" + serial + "读取完毕，开始处理数据");
                for (byte b : temp) {
                    if (b == (byte) 0x03) {
                        data = bout.toByteArray();
                        LOGGER.info("串口：" + serial + "收到应答：" + CodecUtil.bytesToHexString(data));
                        out.close();
                        in.close();
                        LOGGER.info("串口：" + serial + "返回数据");
                        return data;
                    }
                }
            }
            out.close();
            in.close();
            LOGGER.info("串口：" + serial + "串口通信不畅通、无数据返回、通信超时或返回数据不合法");
            throw new CmsException("串口通信不畅通、无数据返回、通信超时或返回数据不合法");
        } finally {
            LOGGER.info("串口：" + serial + "开始关闭");
            serialPort.close();
            LOGGER.info("串口：" + serial + "关闭成功，释放同步锁");
        }
    }


    private static SerialPort openSerialPort(String serial, int speed) throws CmsException {
        CommPortIdentifier port = null;
        try {
            port = CommPortIdentifier.getPortIdentifier(serial);
        } catch (NoSuchPortException e) {
            throw new CmsException("没有该串口：" + serial);
        }
        try {
            SerialPort serialPort = (SerialPort) port.open("Frank" + serial, 5000);
            serialPort.setSerialPortParams(speed, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            serialPort.enableReceiveTimeout(Constant.TIMEOUT);
            return serialPort;
        } catch (Exception e) {
            e.printStackTrace();
            throw new CmsException("串口异常:" + serial);
        }
//        catch (PortInUseException e) {
//            throw new CmsException("串口已被占用：" + serial);
//        } catch (UnsupportedCommOperationException e) {
//            throw new CmsException("串口参数设置失败：" + serial);
//        }
    }
}
