package com.qzsoft.demomis.modules.customer.wlcustomer.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.qzsoft.demomis.modules.customer.wlcustomer.entity.WlCustomerEntity;

/**
 * 客户信息表(WlCustomer)表数据库访问层
 *
 * @author sdmq
 * @since 2020-07-14 13:50:08
 */
@Mapper
public interface WlCustomerDao extends BaseMapper<WlCustomerEntity> {

}