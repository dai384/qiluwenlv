package com.qzsoft.demomis.modules.programhistory.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.programhistory.entity.WlProgramHistoryInfoEntity;

import java.util.List;
import java.util.Map;

/**
 * 节目时长(Wl_Program)表服务接口
 *
 * @author sdmq
 * @since 2020-06-10 17:45:48
 */
public interface WlProgramHistoryInfoService {
    /**
	 * 数据分页
	 * @param params
	 * @return IPage
	 */
	IPage<WlProgramHistoryInfoEntity> page(Map<String, Object> params);

	/**
	 * 数据列表
	 * @param params
	 * @return
	 */
	List<WlProgramHistoryInfoEntity> list(Map<String, Object> params);

	/**
	 * 单个数据
	 * @param programId,equipmentId
	 * @return
	 */
	Map<String, Object> get(String programId, String equipmentId);

	/**
	 * 保存
	 * @param map
	 */
	void save(Map<String, Object> map);

	/**
	 * 更新
	 * @param map
	 */
	void update(Map<String, Object> map);

}