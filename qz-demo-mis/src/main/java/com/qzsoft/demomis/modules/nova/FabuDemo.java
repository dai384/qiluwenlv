package com.qzsoft.demomis.modules.nova;

import novj.platform.vxkit.common.bean.login.LoginResultBean;
import novj.platform.vxkit.common.bean.programinfo.Layout;
import novj.platform.vxkit.common.bean.programinfo.Widget;
import novj.platform.vxkit.common.bean.search.SearchResult;
import novj.platform.vxkit.common.result.DefaultOnResultListener;
import novj.platform.vxkit.common.result.OnResultListenerN;
import novj.platform.vxkit.handy.api.ProgramSendManager;
import novj.platform.vxkit.handy.api.SearchManager;
import novj.publ.api.NovaOpt;
import novj.publ.api.actions.ProgramManager;
import novj.publ.net.exception.ErrorDetail;
import novj.publ.net.svolley.Request.IRequestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 * 发布节目
 * */
@Service
public class FabuDemo {
    protected static final Logger LOGGER = LoggerFactory.getLogger(FabuDemo.class);

    private static String commenPath = "D:";
    private static String programPath = "D:/qiluwenlv/program/";

    static String remoteIp = "10.2.185.68";
    public static void main (String[] args){
        FabuDemo nn = new FabuDemo();
        nn.NovaOperation(remoteIp);
    }

    public void NovaOperation(String remoteIp) {
        List<String> resourceList = new ArrayList<>();

        // 栖霞北
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200804/1 (1).jpg");
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200804/1 (2).jpg");
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200804/1 (3).jpg");
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200804/1 (4).jpg");
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200804/1 (5).jpg");
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200804/1 (6).jpg");
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200804/1 (7).jpg");
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200804/1 (8).jpg");

        NovaOpt novaOpt = new NovaOpt();
        // 1、SDK 初始化: 1=Android 系统环境 2=Windows 或 Linux 系统环境
        novaOpt.GetInstance().initialize(2);
        // 2、搜索设备
        novaOpt.GetInstance().searchScreen(new SearchManager.OnScreenSearchListener(){
            @Override
            public void onSuccess(SearchResult searchResult){
                // 发现设备，自行添加处理逻辑
                novaOpt.GetInstance().connectDevice(searchResult, new DefaultOnResultListener() {
                    @Override
                    public void onSuccess(Integer response) {
                        LOGGER.info("Nova System connect success================================1===================================：terminal IP is："+remoteIp+"-----------------sn:"+searchResult.sn);
                        // connect success，自行添加处理逻辑
                        // 4、登录设备
                        String userName = "admin";
                        String passwords = "123456";
                        novaOpt.GetInstance().login(searchResult.sn, userName, passwords, new OnResultListenerN<LoginResultBean, ErrorDetail>() {
                            @Override
                            public void onSuccess(IRequestBase requestBase, LoginResultBean response) {
                                LOGGER.info("Nova System登录成功===================================================================：terminal IP is："+remoteIp);
                                //  1.  创建节目：返回正数为播放节目 ID
                                SimpleDateFormat formatter=new SimpleDateFormat("yyyyMMddHHmmss");
                                String date = formatter.format(new Date());
                                String programName = "新建节目"+date;
                                novaOpt.GetInstance().createProgram(programName, searchResult.width, searchResult.height);

                                for (String path: resourceList) {
                                    int pageId = novaOpt.GetInstance().addPage();
                                    if (path.contains("image")){
                                        novaOpt.GetInstance().addWidget(pageId, ProgramManager.WidgetMediaType.PICTURE, commenPath+path);
                                    } else if (path.contains("vedio")) {
                                        novaOpt.GetInstance().addWidget(pageId, ProgramManager.WidgetMediaType.VIDEO, commenPath+path);
                                    }
                                }

                                String programDir = programPath + date;

                                // 生成并保存节目
                                int result = novaOpt.GetInstance().makeProgram(programDir);
                                LOGGER.info("Nova System生成节目结果===================================================================：terminal IP is："+remoteIp+"; 路径："+programDir+"; 结果："+result);

                                if(result == 0){
                                    // 发布节目
                                    try {
                                        novaOpt.GetInstance().startTransfer(searchResult.sn, programDir, programName, "", true, new ProgramSendManager.OnProgramTransferListener() {
                                            @Override
                                            public void onStarted() {
                                                //开始发布
                                                LOGGER.info("Nova System开始发布===================================================================：terminal IP is："+remoteIp);
                                            }
                                            @Override
                                            public void onTransfer(long l, long l1) {
                                                //发布进度
                                                LOGGER.info("Nova System发布进度===================================================================：terminal IP is："+remoteIp+"； 已上传："+l+"； 共上传："+l1);
                                            }
                                            @Override
                                            public void onError(ErrorDetail errorDetail) {
                                                //发布失败
                                                LOGGER.info("Nova System发布失败===================================================================：搜索terminal IP is："+remoteIp+"; 错误码为："+errorDetail.errorCode+"; 错误描述为："+errorDetail.description);
                                            }
                                            @Override
                                            public void onCompleted() {
                                                // 发布完成
                                                LOGGER.info("Nova System发布完成===================================================================：terminal IP is："+remoteIp);
                                            }
                                            @Override
                                            public void onAborted() {
                                                //发布终止
                                                LOGGER.info("Nova System发布终止===================================================================：terminal IP is："+remoteIp);
                                            }
                                        });
                                    }catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    LOGGER.info("Nova System生成节目失败===================================================================：terminal IP is："+remoteIp);
                                }
                            }
                            @Override
                            public void onError(IRequestBase requestBase, ErrorDetail error) {
                                //执行失败，自行添加处理逻辑=
                                LOGGER.info("Nova System登录失败===================================================================：terminal IP is："+remoteIp+"; error"+error.errorCode+"; 错误描述为："+error.description);
                            }
                        });
                    }
                    @Override
                    public void onError(ErrorDetail error) {
                        //连接失败，自行添加处理逻辑
                        LOGGER.info("Nova System连接失败===================================================================：terminal IP is："+remoteIp);
                    }
                }, wrapper -> {
                    //连接断开，自行添加处理逻辑
                    LOGGER.info("Nova System连接断开===================================================================：terminal IP is："+remoteIp);
                });
            }
            @Override
            public void onError(ErrorDetail errorDetail){
                // 发生错误，自行添加处理逻辑=
                LOGGER.info("Nova System搜索设备失败================================fabudemo===================================：搜索terminal IP is："+remoteIp+"; 错误码为："+errorDetail.errorCode+"; 错误描述为："+errorDetail.description);
            }
        }, remoteIp);
    }
}
