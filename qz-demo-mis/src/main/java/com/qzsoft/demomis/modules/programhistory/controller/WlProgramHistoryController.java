package com.qzsoft.demomis.modules.programhistory.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.programhistory.entity.WlProgramHistoryEntity;
import com.qzsoft.demomis.modules.programhistory.service.WlProgramHistoryService;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;

/**
 * 节目时长表(Wl_Program)表控制层
 *
 * @author sdmq
 * @since 2020-06-10 17:45:47
 */
@RestController
@RequestMapping("programHistory/programHistory")
@Api(tags="节目表时长历史表")
public class WlProgramHistoryController {
    @Autowired
    private WlProgramHistoryService wlProgramHistoryService;

	@PostMapping("page")
	@ApiOperation("分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = "ge_createDate", value = "起始创建时间", paramType = "query", dataType="Date"),
			@ApiImplicitParam(name = "le_createDate", value = "截止创建时间", paramType = "query", dataType="Date")
	})
//	@RequiresPermissions("programHistory:programHistory:page")
	public Result<IPage<WlProgramHistoryEntity>> page(@ApiIgnore @RequestBody Map<String, Object> params){
		IPage<WlProgramHistoryEntity> page = wlProgramHistoryService.page(params);

		return new Result<IPage<WlProgramHistoryEntity>>().ok(page);
	}

	@GetMapping("list")
	@ApiOperation("列表")
//	@RequiresPermissions("programHistory:programHistory:list")
	public Result<List<WlProgramHistoryEntity>> list(@ApiIgnore @RequestParam Map<String, Object> params){
		List<WlProgramHistoryEntity> data = wlProgramHistoryService.list(params);

		return new Result<List<WlProgramHistoryEntity>>().ok(data);
	}


	@GetMapping("info")
	@ApiOperation("信息")
//	@RequiresPermissions("programHistory:programHistory:info")
	public Result<Map<String, Object>> newGet(@ApiIgnore @RequestParam("programId") String programId, @RequestParam("equipmentId") String equipmentId){

		Map<String, Object> map = wlProgramHistoryService.get(programId,equipmentId);
		return new Result<Map<String, Object>>().ok(map);
	}

	@PostMapping
	@ApiOperation("保存")
	@LogOperation("保存")
//	@RequiresPermissions("programHistory:programHistory:save")
	public Result save(@RequestBody Map<String, Object> map){
		wlProgramHistoryService.save(map);
		return new Result();
	}

	@PutMapping
	@ApiOperation("修改")
	@LogOperation("修改")
//	@RequiresPermissions("programHistory:programHistory:update")
	public Result update(@RequestBody Map<String, Object> map){
		wlProgramHistoryService.update(map);
		return new Result();
	}

}