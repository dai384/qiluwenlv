/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.permission.role.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qzsoft.demomis.modules.permission.role.service.SysRoleUserService;
import com.qzsoft.demomis.repository.sys.dao.SysRoleUserDao;
import com.qzsoft.demomis.repository.sys.entity.SysRoleUserEntity;
import com.qzsoft.jeemis.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色用户关系
 *
 * @author
 * @since 1.0.0
 */
@Service
public class SysRoleUserServiceImpl extends BaseService implements SysRoleUserService {

	@Autowired
	SysRoleUserDao sysRoleUserDao;
    @Override
    public void saveOrUpdate(Long userId, List<Long> roleIdList) {
        //先删除角色用户关系
        deleteByUserIds(new Long[]{userId});

        //用户没有一个角色权限的情况
        if(CollUtil.isEmpty(roleIdList)){
            return ;
        }

		List<Long> listAllDistinct = roleIdList.stream().distinct().collect(Collectors.toList());
        //保存角色用户关系
        for(Long roleId : listAllDistinct){
            SysRoleUserEntity sysRoleUserEntity = new SysRoleUserEntity();
            sysRoleUserEntity.setUserId(userId);
            sysRoleUserEntity.setRoleId(roleId);

            //保存
			sysRoleUserDao.insert(sysRoleUserEntity);
        }
    }

    @Override
    public void deleteByRoleIds(Long[] roleIds) {
		QueryWrapper<SysRoleUserEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.in("role_id",roleIds);
    	sysRoleUserDao.delete(queryWrapper);
    }

    @Override
    public void deleteByUserIds(Long[] userIds) {
		QueryWrapper<SysRoleUserEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.in("user_id",userIds);
		sysRoleUserDao.delete(queryWrapper);
    }

    @Override
    public List<Long> getRoleIdList(Long userId) {

		QueryWrapper<SysRoleUserEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq("user_id",userId).select("role_id");
		List<SysRoleUserEntity> roleList=sysRoleUserDao.selectList(queryWrapper);
		List<Long> roleIdList= roleList.stream().map(obj->{return obj.getRoleId();}).collect(Collectors.toList());
		return roleIdList;
    }
}