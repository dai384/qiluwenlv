package com.qzsoft.demomis.modules.permission.dept.controller;

import com.qzsoft.demomis.modules.permission.dept.dto.SysDeptDTO;
import com.qzsoft.demomis.modules.permission.dept.service.SysDeptTreeService;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.utils.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author sdmq
 * @date 2019/8/12 15:40
 */
@RestController
@RequestMapping("/api/sys/dept/tree")
public class SysDepTreeController {
	@Autowired
	private SysDeptTreeService sysDeptTreeService;
	@PostMapping("getroot")
	@ApiOperation("取树根节点")
	@LogOperation("取树根节点")
	public Result<List<SysDeptDTO>> getDeptTreeRoot(String rootId) {
		List<SysDeptDTO>  sysDeptDTOList= sysDeptTreeService.getDeptTreeRoot(rootId);
		return new Result<List<SysDeptDTO>>().ok(sysDeptDTOList);
	}

	@PostMapping("getchildren")
	@ApiOperation("取树子节点")
	@LogOperation("取树子节点")
	public Result<List<SysDeptDTO>> getDeptTreeChildren(String pid) {
		List<SysDeptDTO>  sysDeptDTOList= sysDeptTreeService.getDeptTreeChildren(pid);
		return new Result<List<SysDeptDTO>>().ok(sysDeptDTOList);
	}

	@PostMapping("getdeptpids")
	@ApiOperation("取节点所有父节点")
	@LogOperation("取节点所有父节点")
	public Result<List<String>> getDeptNodePids(String id) {
		List<String>  pidList= sysDeptTreeService.getDeptNodePids(id);
		return new Result<List<String>>().ok(pidList);
	}

	@PostMapping("gettreeall")
	@ApiOperation("取所有机构节点")
	public Result<List<SysDeptDTO>> getDeptTreeAll(String rootId) {
		List<SysDeptDTO>  sysDeptDTOList= sysDeptTreeService.getDeptTreeAll(rootId);
		return new Result<List<SysDeptDTO>>().ok(sysDeptDTOList);
	}
}
