/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.permission.dept.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.qzsoft.demomis.modules.permission.dept.dao.SysDeptExtDao;
import com.qzsoft.demomis.modules.permission.dept.excel.SysDeptExcel;
import com.qzsoft.demomis.modules.permission.dept.service.SysDeptService;
import com.qzsoft.demomis.repository.sys.dao.SysDeptDao;
import com.qzsoft.demomis.repository.sys.dao.SysUserDao;
import com.qzsoft.demomis.repository.sys.entity.SysDeptEntity;
import com.qzsoft.demomis.repository.sys.entity.SysUserEntity;
import com.qzsoft.jeemis.common.enums.SuperAdminEnum;
import com.qzsoft.jeemis.common.exception.ErrorCode;
import com.qzsoft.jeemis.common.exception.RenException;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ConvertDictUtils;
import com.qzsoft.jeemis.common.utils.ConvertUtils;
import com.qzsoft.jeemis.common.utils.Ten2ThirtyUtils;
import com.qzsoft.jeemis.platform.security.user.UserDetail;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author sdmq
 */
@Service
public class SysDeptServiceImpl extends BaseService implements SysDeptService {

	@Autowired
	SysDeptExtDao sysDeptExtDao;
	@Autowired
	SysDeptDao sysDeptDao;
	@Autowired
	SysUserDao sysUserDao;

	@Override
	public List<SysDeptEntity> list(String pId) {
		//普通管理员，只能查询所属部门及子部门的数据
		UserDetail user = this.basegetLoginUser();
		String userDeptId = user.getDeptId();
		if (StringUtils.isBlank(pId)){
			pId=userDeptId;
		}
		if (user.getSuperAdmin() == SuperAdminEnum.NO.value()) {
			if (pId.compareTo(userDeptId) > 0) {pId = userDeptId;}
		}
		//查询部门列表
		QueryWrapper<SysDeptEntity> query=new QueryWrapper<>();
		query.eq("pid",pId).orderByAsc("sort","id");
		List<SysDeptEntity> entityList = sysDeptDao.selectList(query);
		return entityList;
	}

	@Override
	public List<SysDeptExcel> listAll() {
		//普通管理员，只能查询所属部门及子部门的数据
		UserDetail user = this.basegetLoginUser();
		String userDeptId = user.getDeptId();
		logger.info(userDeptId);
		//查询部门列表
		QueryWrapper<SysDeptEntity> query=new QueryWrapper<>();
		query.likeRight("id",userDeptId).orderByAsc("sort","id");
		List<SysDeptEntity> entityList = sysDeptDao.selectList(query);
		List<SysDeptExcel> excelList =ConvertUtils.sourceToTarget(entityList, SysDeptExcel.class);
		excelList=ConvertDictUtils.formatDicDataList(excelList);
		return excelList;
	}

	@Override
	public List<SysDeptEntity> listAll2() {
		//普通管理员，只能查询所属部门及子部门的数据
		UserDetail user = this.basegetLoginUser();
		String userDeptId = user.getDeptId();
		//查询部门列表
		QueryWrapper<SysDeptEntity> query=new QueryWrapper<>();
		query.likeRight("id",userDeptId).orderByAsc("sort","id");
		List<SysDeptEntity> entityList = sysDeptDao.selectList(query);
		return entityList;
	}

	@Override
	public SysDeptEntity get(String pkId) {
		//部门ID为null
		if (StringUtils.isBlank(pkId)) {
			return null;
		}
		SysDeptEntity entity = sysDeptDao.selectById(pkId);
		return entity;
	}

	/**
	 *
	 * 加事务加同步锁 简单防止deptid重复 实际应该使用更好的方案
	 * @param sysDeptEntity
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public synchronized SysDeptEntity saveDept(SysDeptEntity sysDeptEntity) {
		// 分组任何时候都可以建立
		// 判断父节点,单位 type=1下可以建立机构和部门 分组
		// 判断父节点, 分组一直找,如果是根则什么都可以建,如果是单位什么都可以建,如果是部门只能建分组和部门
		UserDetail userDetail=this.basegetLoginUser();
		String pkId= sysDeptEntity.getPkid();
		if (StringUtils.isBlank(pkId)) {
			sysDeptEntity.setPkid(IdWorker.get32UUID());
			sysDeptEntity.setId(getDeptId(sysDeptEntity.getPid()));
			sysDeptEntity.setSort(sysDeptEntity.getId());
			sysDeptEntity.setCreator(userDetail.getId());
			sysDeptEntity.setCreateDate(new Date());
			sysDeptEntity.setUpdater(userDetail.getId());
			sysDeptEntity.setUpdateDate(new Date());
			sysDeptEntity.setHasLeaf(true);
			sysDeptEntity.setUpdater(userDetail.getId());
			sysDeptEntity.setUpdateDate(new Date());
			sysDeptDao.insert(sysDeptEntity);
		}
		else{
			QueryWrapper<SysDeptEntity> query=new QueryWrapper<>();
			query.eq("pid",sysDeptEntity.getId()).select("id");
			List<SysDeptEntity> objList = sysDeptDao.selectList(query);
			sysDeptEntity.setUpdater(userDetail.getId());
			sysDeptEntity.setUpdateDate(new Date());
			sysDeptEntity.setHasLeaf(objList.size()==0);
			sysDeptDao.updateById(sysDeptEntity);
		}
		return sysDeptEntity;
	}

	/**
	 * @return
	 */
	private  String getDeptId(String pId){
		QueryWrapper<SysDeptEntity> query=new QueryWrapper<>();
		query.eq("pid",pId).select("max(id) as id ").comment("取父节点下最大机构ID");
		List<Map<String,Object>> mapList= sysDeptDao.selectMaps(query);
		Integer id=0;
		String  strId="";
		if (mapList.size()>0){
			 Object objValue= mapList.get(0).get("id");
			strId=String.valueOf(objValue);
			strId=strId.substring(strId.length()-3);
			id=Ten2ThirtyUtils.thirtysixToTen(strId);
		}
		id++;
		strId=Ten2ThirtyUtils.tenTo36(id,"000").toUpperCase();
		return pId+strId;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(String pkId) {
		//判断是否有子部门
		SysDeptEntity sysDeptEntity=sysDeptDao.selectById(pkId);
		QueryWrapper<SysDeptEntity> queryDept=new QueryWrapper<>();
		queryDept.eq("pid",sysDeptEntity.getId()).select("id");
		List<SysDeptEntity> objList = sysDeptDao.selectList(queryDept);
		if (objList.size() > 0) {
			throw new RenException(ErrorCode.DEPT_SUB_DELETE_ERROR);
		}

		//判断部门下面是否有用户
		QueryWrapper<SysUserEntity> queryUser=new QueryWrapper<>();
		queryUser.eq("dept_pkid",pkId);
		int count = sysUserDao.selectCount(queryUser);
		if (count > 0) {
			throw new RenException(ErrorCode.DEPT_USER_DELETE_ERROR);
		}
		//删除
		sysDeptDao.deleteById(pkId);
		setDeptNodeLeaf(sysDeptEntity.getId());
	}


	/**
	 * 设置某个节点是否为叶子节点
	 * @param
	 * @param
	 */
	private void setDeptNodeLeaf(String id) {

		String strSql = "";
		QueryWrapper<SysDeptEntity> query=new QueryWrapper<>();
		query.eq("pid",id).select("id");
		List<SysDeptEntity> objList = sysDeptDao.selectList(query);
		query=new QueryWrapper<>();
		query.eq("id",id).select("pkid");
		SysDeptEntity sysDeptEntity =sysDeptDao.selectOne(query);
		sysDeptEntity.setHasLeaf(objList.size()==0);
		sysDeptDao.updateById(sysDeptEntity);
	}

	/**
	 * 保存排序 pkid1 和 pkid2 交换sort
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveDeptSort(String pkId1,String pkId2) {
		String sort1,sort2;

		QueryWrapper<SysDeptEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq("pkid",pkId1).select("sort");
		SysDeptEntity sysDeptEntity= sysDeptDao.selectOne(queryWrapper);
		sort1=sysDeptEntity.getSort();

		queryWrapper=new QueryWrapper<>();
		queryWrapper.eq("pkid",pkId2).select("sort");
		sysDeptEntity= sysDeptDao.selectOne(queryWrapper);
		sort2=sysDeptEntity.getSort();

		UpdateWrapper<SysDeptEntity> updateWrapper=new UpdateWrapper<>();
		updateWrapper.eq("pkid",pkId1).set("sort",sort2);
		sysDeptDao.update(new SysDeptEntity(),updateWrapper);

		updateWrapper=new UpdateWrapper<>();
		updateWrapper.eq("pkid",pkId2).set("sort",sort1);
		sysDeptDao.update(new SysDeptEntity(),updateWrapper);


	}

	@Override
	public String getDeptIdByPkId(String pkId) {
		QueryWrapper<SysDeptEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq("pkid",pkId).select("id");
		SysDeptEntity sysDeptEntity= sysDeptDao.selectOne(queryWrapper);
		return sysDeptEntity.getId();
	}

	@Override
	public String getDeptPkIdById(String deptId) {
		QueryWrapper<SysDeptEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq("id",deptId).select("pkid");
		SysDeptEntity sysDeptEntity= sysDeptDao.selectOne(queryWrapper);
		return sysDeptEntity.getPkid();
	}

}
