package com.qzsoft.demomis.modules.cms.sansi.response;

import com.google.common.collect.Lists;

import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * 从可变信息标志下载文件应答
 *
 * @author frank
 */
public class DownloadFileResponse {
    private String name;
    private List<DownloadFileResponseItem> items = Lists.newArrayList();

    public boolean addData(byte[] bytes) throws IOException, CmsException {
        if (bytes == null) {
            return false;
        }
        DownloadFileResponseItem item = new DownloadFileResponseItem(bytes);
        items.add(item);
        if (item.getB().length == 2048) {
            return true;
        } else {
            return false;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getContent() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        for (DownloadFileResponseItem item : items) {
            byte[] bb = item.getB();
            if (bb.length == 1 && bb[0] == 0x00) {
                break;
            }
            out.write(item.getB());
        }
        byte[] result = out.toByteArray();
        out.close();
        return result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    class DownloadFileResponseItem extends Response {
        private byte[] b;

        public DownloadFileResponseItem(byte[] bytes) throws IOException,
                CmsException {
            this.bytes = bytes;
            decoder();
        }

        @Override
        protected void decoder() throws IOException, CmsException {
            basicDecoder();
            b = getArray(3, -3, CmsException.DOWNLOAD_CONTENT_EXCEPTION);
        }

        public byte[] getB() {
            return b;
        }

    }
}
