package com.qzsoft.demomis.modules.sys.notice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qzsoft.demomis.modules.sys.notice.entity.SysNoticeFileEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * (SysNoticeFile)表数据库访问层
 *
 * @author sdmq
 * @since 2019-09-25 14:58:02
 */
@Mapper
public interface SysNoticeFileDao extends BaseMapper<SysNoticeFileEntity> {

}