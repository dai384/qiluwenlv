package com.qzsoft.demomis.modules.terminal.entity;

import java.io.Serializable;
import java.util.Date;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;

import javax.validation.constraints.NotNull;

/**
 * (WlEquipmentinfo)表实体类
 *
 * @author sdmq
 * @since 2020-05-27 14:49:25
 */
@ApiModel(value ="")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("wl_equipmentinfo")
public class TerminalEntity extends Model<TerminalEntity> {
    private static final long serialVersionUID = 578722087976547681L;
    /**
     *id主键
     */
    @TableId(value ="id",type = IdType.ID_WORKER) //主键ID_WORKER策略
    @NotNull(message="{id.require}", groups = UpdateGroup.class)
    @Excel(name = "id主键")
    @ApiModelProperty(value = "id主键")
    private Long id;
    /**
     *屏编号
     */
    @Excel(name = "屏编号")
    @ApiModelProperty(value = "屏编号")
    private String equipId;
    /**
     * 节目
     */
    @Excel(name = "节目")
    @ApiModelProperty(value = "节目")
    private String program;
    /**
     *类型
     */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;
    /**
     *分组
     */
    @Excel(name = "分组")
    @ApiModelProperty(value = "分组")
    private String groupName;
    /**
     *名称
     */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;
    /**
     *经度
     */
    @Excel(name = "经度")
    @ApiModelProperty(value = "经度")
    private Double longitude;
    /**
     *纬度
     */
    @Excel(name = "纬度")
    @ApiModelProperty(value = "纬度")
    private Double latitude;
    /**
     *ip（自动
     */
    @Excel(name = "ip（自动")
    @ApiModelProperty(value = "ip（自动")
    private String ip;
    /**
     *子网掩码（自动
     */
    @Excel(name = "子网掩码（自动")
    @ApiModelProperty(value = "子网掩码（自动")
    private String subnetMask;
    /**
     *dns服务器（自动
     */
    @Excel(name = "dns服务器（自动")
    @ApiModelProperty(value = "dns服务器（自动")
    private String dns;
    /**
     *终端屏幕像素点行数（自动
     */
    @Excel(name = "终端屏幕像素点行数（自动")
    @ApiModelProperty(value = "终端屏幕像素点行数（自动")
    private String rowPixel;
    /**
     *终端屏幕像素点列数（自动
     */
    @Excel(name = "终端屏幕像素点列数（自动")
    @ApiModelProperty(value = "终端屏幕像素点列数（自动")
    private String columnPixel;
    /**
     *模块型号（手动
     */
    @Excel(name = "模块型号（手动")
    @ApiModelProperty(value = "模块型号（手动")
    private String modulesType;
    /**
     *模块行
     */
    @Excel(name = "模块行")
    @ApiModelProperty(value = "模块行")
    private String moduleRow;
    /**
     *列像素点（自动
     */
    @Excel(name = "列像素点（自动")
    @ApiModelProperty(value = "列像素点（自动")
    private String columnPixels;
    /**
     *控制器型号
     */
    @Excel(name = "控制器型号")
    @ApiModelProperty(value = "控制器型号")
    private String controllerType;
    /**
     *省
     */
    @Excel(name = "省")
    @ApiModelProperty(value = "省")
    private String province;
    /**
     *市
     */
    @Excel(name = "市")
    @ApiModelProperty(value = "市")
    private String city;
    /**
     *区/县
     */
    @Excel(name = "区/县")
    @ApiModelProperty(value = "区/县")
    private String districe;
    /**
     *收费站
     */
    @Excel(name = "收费站")
    @ApiModelProperty(value = "收费站")
    private String station;
    /**
     *控制器SN号
     */
    @Excel(name = "控制器SN号")
    @ApiModelProperty(value = "控制器SN号")
    private String sn;
    /**
     *开关（1开；0关）
     */
    @Excel(name = "开关（1开；0关）")
    @ApiModelProperty(value = "开关（1开；0关）")
    private String onOff;
    /**
     *亮度
     */
    @Excel(name = "亮度")
    @ApiModelProperty(value = "亮度")
    private Integer light;
    /**
     *声音
     */
    @Excel(name = "声音")
    @ApiModelProperty(value = "声音")
    private Integer voice;
    /**
     *天气
     */
    @Excel(name = "天气")
    @ApiModelProperty(value = "天气")
    private String sweather;
    /**
     *风力
     */
    @Excel(name = "风力")
    @ApiModelProperty(value = "风力")
    private String wind;
    /**
     *温度
     */
    @Excel(name = "温度")
    @ApiModelProperty(value = "温度")
    private String temperatur;
    /**
     *湿度
     */
    @Excel(name = "湿度")
    @ApiModelProperty(value = "湿度")
    private String humidty;
    /**
     *自动开关机（1是；0否）
     */
    @Excel(name = "自动开关机（1是；0否）")
    @ApiModelProperty(value = "自动开关机（1是；0否）")
    private String autoswitch;
    /**
     *自动开机时间
     */
    @Excel(name = "自动开机时间")
    @ApiModelProperty(value = "自动开机时间")
    private String onDate;
    /**
     *自动关机时间
     */
    @Excel(name = "自动关机时间")
    @ApiModelProperty(value = "自动关机时间")
    private String offDate;
    /**
     *状态（1正常；0异常）
     */
    @Excel(name = "状态（1正常；0异常）")
    @ApiModelProperty(value = "状态（1正常；0异常）")
    private String status;
    /**
     *备注
     */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remark;
    /**
     *创建人
     */
    @Excel(name = "创建人")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private String creator;
    /**
     *创建时间
     */
    @Excel(name = "创建时间" , format = "yyyy-MM-dd")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
    /**
     *编辑人
     */
    @Excel(name = "编辑人")
    @ApiModelProperty(value = "编辑人")
    private String updator;
    /**
     *编辑时间
     */
    @Excel(name = "编辑时间" , format = "yyyy-MM-dd")
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "编辑时间")
    private Date updateDate;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
