package com.qzsoft.demomis.modules.sys.notice.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.sys.notice.entity.SysNoticeEntity;
import com.qzsoft.demomis.modules.sys.notice.service.SysNoticeService;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.utils.ExcelUtils;
import com.qzsoft.jeemis.common.utils.Result;
import com.qzsoft.jeemis.common.validator.AssertUtils;
import com.qzsoft.jeemis.common.validator.ValidatorUtils;
import com.qzsoft.jeemis.common.validator.group.AddGroup;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author sdmq
 * @date 2019/9/17 20:52
 */
@RestController
@RequestMapping("/sys/notice")
@Api(tags="通知公告")
public class SysNoticeController {
	@Autowired
	private SysNoticeService sysNoticeService;

	@GetMapping("page")
	@ApiOperation("分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = "title", value = "标题", paramType = "query", dataType="String")
	})
	@RequiresPermissions("sys:notice:page")
	public Result<IPage<SysNoticeEntity>> page(@ApiIgnore @RequestParam Map<String, Object> params){
		IPage<SysNoticeEntity> page = sysNoticeService.page(params);

		return new Result<IPage<SysNoticeEntity>>().ok(page);
	}

	@GetMapping("list")
	@ApiOperation("列表")
	@RequiresPermissions("sys:notice:list")
	public Result<List<SysNoticeEntity>> list(@ApiIgnore @RequestParam Map<String, Object> params){
		List<SysNoticeEntity> data = sysNoticeService.list(params);

		return new Result<List<SysNoticeEntity>>().ok(data);
	}

	@GetMapping("{id}")
	@ApiOperation("信息")
	@RequiresPermissions("sys:notice:info")
	public Result<SysNoticeEntity> get(@PathVariable("id") String id){
		SysNoticeEntity data = sysNoticeService.get(id);

		return new Result<SysNoticeEntity>().ok(data);
	}

	@PostMapping
	@ApiOperation("保存")
	@LogOperation("保存")
	@RequiresPermissions("sys:notice:save")
	public Result save(@RequestBody SysNoticeEntity dto){
		//效验数据
		ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);
		sysNoticeService.save(dto);
		return new Result();
	}

	@PutMapping
	@ApiOperation("修改")
	@LogOperation("修改")
	@RequiresPermissions("sys:notice:update")
	public Result update(@RequestBody SysNoticeEntity dto){
		//效验数据
		ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

		sysNoticeService.update(dto);

		return new Result();
	}

	@PostMapping("/publish")
	@ApiOperation("发布")
	@LogOperation("发布")
	@RequiresPermissions("sys:notice:update")
	public Result publishStart(@RequestBody String[] ids){
		//效验数据
		AssertUtils.isArrayEmpty(ids, "id");
		sysNoticeService.publishStart(ids);
		return new Result();
	}

	@PostMapping("/stop")
	@ApiOperation("停止发布")
	@LogOperation("停止发布")
	@RequiresPermissions("sys:notice:update")
	public Result publishStop(@RequestBody String[] ids){
		//效验数据
		AssertUtils.isArrayEmpty(ids, "id");
		sysNoticeService.publishStop(ids);
		return new Result();
	}

	@DeleteMapping
	@ApiOperation("删除")
	@LogOperation("删除")
	@RequiresPermissions("sys:notice:delete")
	public Result delete(@RequestBody String[] ids){
		//效验数据
		AssertUtils.isArrayEmpty(ids, "id");

		sysNoticeService.delete(ids);

		return new Result();
	}



	@GetMapping("export")
	@ApiOperation("导出")
	@LogOperation("导出")
	@RequiresPermissions("sys:notice:export")
	public void export(@ApiIgnore @RequestParam Map<String, Object> params , HttpServletResponse response) throws Exception {
		List<SysNoticeEntity> list = sysNoticeService.list(params);
		ExcelUtils.exportExcelToTarget(response, "导出通知通告列表", list, SysNoticeEntity.class);
	}
	@GetMapping("test")
	public Result test() {
		return new Result().ok("<div> <h3 @click='change'>测试2222222 {{data.msg}}</h3> <qz-dept-tree-gen></qz-dept-tree-gen></div>");
	}


}
