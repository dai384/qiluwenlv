package com.qzsoft.demomis.modules.terminal.controller;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.terminal.service.TerminalService;
import com.qzsoft.demomis.modules.terminal.entity.TerminalEntity;
import com.qzsoft.demomis.modules.unit.service.WlRegionService;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.utils.Result;
import com.qzsoft.jeemis.common.validator.AssertUtils;
import com.qzsoft.jeemis.common.validator.ValidatorUtils;
import com.qzsoft.jeemis.common.validator.group.AddGroup;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.tio.utils.time.Time;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * (equipmentInfo)表控制层
 *
 * @author sdmq
 * @since 2020-05-27 09:07:01
 */
@RestController
@RequestMapping("terminal/equipmentInfo")
@Api(tags="")
public class TerminalController {
    @Autowired
    private TerminalService terminalService;
	@Autowired
	private WlRegionService wlRegionService;

	@PostMapping("page")
	@ApiOperation("分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = "link_name", value = "名字", paramType = "query", dataType="String"),
			@ApiImplicitParam(name = "ge_createDate", value = "起始创建时间", paramType = "query", dataType="Date"),
			@ApiImplicitParam(name = "le_createDate", value = "截止创建时间", paramType = "query", dataType="Date")
	})
//	@RequiresPermissions("terminal:equipmentInfo:page")
	public Result<IPage<TerminalEntity>> page(@ApiIgnore @RequestBody Map<String, Object> params){
		IPage<TerminalEntity> page = terminalService.page(params);

		System.out.println(page);
		return new Result<IPage<TerminalEntity>>().ok(page);
	}

	@GetMapping("list")
	@ApiOperation("列表")
	@RequiresPermissions("terminal:equipmentInfo:list")
	public Result<List<TerminalEntity>> list(@ApiIgnore @RequestParam Map<String, Object> params){
		List<TerminalEntity> data = terminalService.list(params);

		return new Result<List<TerminalEntity>>().ok(data);
	}

	@GetMapping("{id}")
	@ApiOperation("信息")
	@RequiresPermissions("terminal:equipmentInfo:info")
	public Result<TerminalEntity> get(@PathVariable("id") String id){
		TerminalEntity data = terminalService.get(id);

		return new Result<TerminalEntity>().ok(data);
	}

	@PostMapping
	@ApiOperation("保存")
	@LogOperation("保存")
	@RequiresPermissions("terminal:equipmentInfo:save")
//	Map<String, Object> dto
//	TerminalEntity
	public Result save(@RequestBody TerminalEntity dto){
		//效验数据
		ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);
		terminalService.save(dto);
		return new Result();
	}

	@PutMapping
	@ApiOperation("修改")
	@LogOperation("修改")
	@RequiresPermissions("terminal:equipmentInfo:update")
	public Result update(@RequestBody TerminalEntity dto){
		//效验数据
		ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

		terminalService.update(dto);

		return new Result();
	}
	
	@DeleteMapping
	@ApiOperation("删除")
	@LogOperation("删除")
	@RequiresPermissions("terminal:equipmentInfo:delete")
	public Result delete(@RequestBody String[] ids){
		//效验数据
		AssertUtils.isArrayEmpty(ids, "id");

		terminalService.delete(ids);

		return new Result();
	}



	@GetMapping("export")
	@ApiOperation("导出")
	@LogOperation("导出")
	@RequiresPermissions("terminal:equipmentInfo:export")
	public void export(@ApiIgnore @RequestParam Map<String, Object> params , HttpServletResponse response) throws Exception {
		terminalService.exportXls(params,response);
	}


	@PostMapping("saveAll")
	@ApiOperation("批量上传")
	@LogOperation("批量上传")
	@RequiresPermissions("terminal:equipmentInfo:saveall")
	public Result saveAll(@RequestBody MultipartFile file, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
		return terminalService.readExcelFile(file, httpServletRequest, httpServletResponse);
	}

}