package com.qzsoft.demomis.modules.cms.sansi.response;

import com.google.common.collect.Lists;

import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.Constant;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;
import java.util.List;

/**
 * 可变信息标志的当前故障应答
 *
 * @author frank
 */
public class BugResponse extends Response {
    private List<String> errors = Lists.newArrayList();
    private String content;

    public BugResponse(byte[] bytes) throws IOException, CmsException {
        if (bytes != null) {
            this.bytes = bytes;
            decoder();
        }
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        basicDecoder();
        content = getString(3, 4, CmsException.CONTENT_EXCEPTION);
        long a = Long.parseLong(content, 16);
        for (int i = 1; i < 10; i++) {
            if ((a & (1 << i)) != 0 && i != 3) {
                errors.add(Constant.bugArr[i - 1]);
            }
        }
    }

    public List<String> getErrors() {
        return errors;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    // public static void main(String[] args) {
    // byte[] bytes = new byte[] { (byte) 0x02, (byte) 0x30, (byte) 0x30,
    // (byte) 0x30, (byte) 0x31, (byte) 0x31, (byte) 0x30,
    // (byte) 0xf9, (byte) 0xe8, 0x03 };
    // BugResponse br = new BugResponse();
    // try {
    // br.decoder(bytes);
    // } catch (DisplayException e) {
    // e.printStackTrace();
    // } catch (IOException e) {
    // e.printStackTrace();
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // for (String err : br.errors) {
    // System.out.println(err);
    // }
    // }
}
