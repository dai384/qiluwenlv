package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * \Cxxxyyy 图形或字符串显示的起始(左上角)坐标,xxx 和 yyy 的范围都是 [-99, 999],缺省值都是
 * 0。注意可变情报板的左上角为原点,坐标值为 (0, 0)
 * 
 * @author frank
 * 
 */
public class LocationEsc extends Esc {
	private static Logger logger = LoggerFactory.getLogger(LocationEsc.class);
	private final String DEFAULT = "000000";

	@Override
	protected String getCommand() {
		return "\\C";
	}

	private LocationEsc(int x, int y) {
		if (x < -99 || x > 999 || y < -99 || y > 999) {
			logger.warn("X坐标的取值范围为-99~999，现在为 {}，将使用默认值 {}", value, DEFAULT);
			value = DEFAULT;
		} else {
			value = format3(x) + format3(y);
		}
	}

	public static LocationEsc getInstance() {
		return new LocationEsc(0, 0);
	}

	public static LocationEsc getInstance(int x, int y) {
		return new LocationEsc(x, y);
	}

}
