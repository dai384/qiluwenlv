/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.permission.dept.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qzsoft.demomis.modules.permission.dept.dao.SysDeptExtDao;
import com.qzsoft.demomis.modules.permission.dept.dto.SysDeptDTO;
import com.qzsoft.demomis.modules.permission.dept.service.SysDeptTreeService;
import com.qzsoft.demomis.repository.sys.dao.SysDeptDao;
import com.qzsoft.demomis.repository.sys.dao.SysUserDao;
import com.qzsoft.demomis.repository.sys.entity.SysDeptEntity;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ConvertUtils;
import com.qzsoft.jeemis.common.utils.TreeUtils;
import com.qzsoft.jeemis.platform.security.user.UserDetail;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author sdmq
 */
@Service
public class SysDeptTreeServiceImpl extends BaseService implements SysDeptTreeService {

	@Autowired
	SysDeptExtDao sysDeptExtDao;
	@Autowired
	SysDeptDao sysDeptDao;
	@Autowired
	SysUserDao sysUserDao;

	private String getDeptRootId(String deptId){
		UserDetail userDetail = this.basegetLoginUser();
		String strDepId = userDetail.getDeptId();
		QueryWrapper<SysDeptEntity> query = new QueryWrapper<>();
		if (StringUtils.isBlank(deptId)) {
			deptId = strDepId;
		}
		// 控制权限查看范围
		if (StringUtils.isNotBlank(deptId) && deptId.compareTo(strDepId) < 0) {
			deptId = strDepId;
		}
		return deptId;
	}

	/**
	 * 取根节点,全加载
	 * @param rootId
	 * @return
	 */
	@Override
	public List<SysDeptDTO> getDeptTreeAll(String rootId) {

		rootId=this.getDeptRootId(rootId);
		// 如果pid为空 则按照用户的内容取回来, 另外取回无单位的内容
		QueryWrapper<SysDeptEntity> query = new QueryWrapper<>();
		query.likeRight("id", rootId).orderByAsc("pid,sort");
		List<SysDeptEntity> deptList = sysDeptDao.selectList(query);
		//转换DTO 主要为了减少传到前端的数据量
		List<SysDeptDTO> dtoList = ConvertUtils.sourceToTarget(deptList, SysDeptDTO.class);
		String pid="";
		if (dtoList.size()>0){
			pid=dtoList.get(0).getPid();
		}
		return TreeUtils.build(dtoList,pid );
	}
	/**
	 * 取根节点,懒加载
	 * @param rootId
	 * @return
	 */
	@Override
	public List<SysDeptDTO> getDeptTreeRoot(String rootId) {
		rootId=this.getDeptRootId(rootId);
		// 如果pid为空 则按照用户的内容取回来, 另外取回无单位的内容
		QueryWrapper<SysDeptEntity> query = new QueryWrapper<>();
		query.eq("id", rootId).orderByAsc("sort");
		List<SysDeptEntity> deptList = sysDeptDao.selectList(query);
		//转换DTO 主要为了减少传到前端的数据量
		List<SysDeptDTO> dtoList = ConvertUtils.sourceToTarget(deptList, SysDeptDTO.class);
		return dtoList;
	}

	@Override
	public List<SysDeptDTO> getDeptTreeChildren(String pid) {
		UserDetail userDetail = this.basegetLoginUser();
		String strDepId = userDetail.getB0110();
		QueryWrapper<SysDeptEntity> query = new QueryWrapper<>();
		query.eq("pid", pid).likeRight("id", strDepId).orderByAsc("sort");
		List<SysDeptEntity> deptList = sysDeptDao.selectList(query);
		//转换DTO 主要为了减少传到前端的数据量
		List<SysDeptDTO> dtoList = ConvertUtils.sourceToTarget(deptList, SysDeptDTO.class);
		return dtoList;
	}

	@Override
	public List<String> getDeptNodePids(String id) {

		List<String> listData = new ArrayList<>();
		listData.add(id);
		QueryWrapper<SysDeptEntity> queryWrapper=new QueryWrapper<>();
		String id2=id,pId="";
		while (true) {
			queryWrapper=new QueryWrapper<>();
			queryWrapper.select("pid").eq("id",id2);
			SysDeptEntity sysDeptEntity=sysDeptDao.selectOne(queryWrapper);
			if (sysDeptEntity==null){ break;}
			pId=sysDeptEntity.getPid();
			if (StringUtils.isBlank(pId)) {break;}
			listData.add(pId);
			id2=pId;
		}
		logger.info("所有机构父代码", listData);
		return listData;
	}


	@Override
	public String getDeptTreeCheckedCondition(List<SysDeptDTO> b001TreeDTOList) {
		return null;
	}

	@Override
	public String getDeptTreeCheckedCondition(List<SysDeptDTO> b001TreeDTOList, String tableName, String strField) {
		return null;
	}
}
