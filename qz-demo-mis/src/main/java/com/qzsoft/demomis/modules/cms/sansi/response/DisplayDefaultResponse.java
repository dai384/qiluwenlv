package com.qzsoft.demomis.modules.cms.sansi.response;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;


/**
 * 使可变信息标志显示预置的播放表应答
 *
 * @author frank
 */
public class DisplayDefaultResponse extends Response {
    private int code;
    private String msg;

    public DisplayDefaultResponse(byte[] bytes) throws IOException,
            CmsException {
        if (bytes != null) {
            this.bytes = bytes;
            decoder();
        }
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        basicDecoder();
        code = getInt(3, 1, CmsException.CONTENT_EXCEPTION);
        if (code == 0) {
            msg = "成功";
        } else {
            msg = "失败";
        }
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
