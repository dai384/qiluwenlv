package com.qzsoft.demomis.modules.unit.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.qzsoft.demomis.modules.unit.entity.WlRegionEntity;

/**
 * (WlRegion)表数据库访问层
 *
 * @author sdmq
 * @since 2020-05-28 09:20:45
 */
@Mapper
public interface WlRegionDao extends BaseMapper<WlRegionEntity> {

}