package com.qzsoft.demomis.modules.cms.sansi.response;

import com.google.common.collect.Lists;


import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;
import java.util.List;

/**
 * 读取 LED 故障信息应答
 *
 * @author frank
 */
public class GetLEDErrorResponse extends Response {
    private long errorNum;
    private List<ErrorPixel> errorPixels = Lists.newArrayList();

    public GetLEDErrorResponse(byte[] bytes) throws IOException,
            CmsException {
        this.bytes = bytes;
        decoder();
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        basicDecoder();
        String num = CodecUtil.bytesToHexString(getArray(3, 8,
                CmsException.ERROR_PIXELS_NUM_EXCEPTION));
        errorNum = Long.parseLong(num, 16);
        for (int i = 0; i < errorNum; i++) {
            int m = 11 + i * 6;
            String x = CodecUtil.bytesToHexString(getArray(m, 3,
                    CmsException.CONTENT_EXCEPTION));
            String y = CodecUtil.bytesToHexString(getArray(m + 3, 3,
                    CmsException.CONTENT_EXCEPTION));
            errorPixels.add(new ErrorPixel(x, y));
        }
    }

    public long getErrorNum() {
        return errorNum;
    }

    public List<ErrorPixel> getErrorPixels() {
        return errorPixels;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public class ErrorPixel {
        private String x;
        private String y;

        public ErrorPixel(String x, String y) {
            this.x = x;
            this.y = y;
        }

        public String getX() {
            return x;
        }

        public String getY() {
            return y;
        }

        @Override
        public String toString() {
            return "(" + x + "," + y + ")";    //To change body of overridden methods use File | Settings | File Templates.
        }
    }
}
