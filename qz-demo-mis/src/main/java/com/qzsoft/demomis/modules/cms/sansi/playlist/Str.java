package com.qzsoft.demomis.modules.cms.sansi.playlist;

import com.qzsoft.demomis.modules.cms.sansi.playlist.esc.Esc;

import java.util.List;

/**
 * 所要显示的字符串,缺省为空串。字符串中可含有转义符,以字符 '\' 为标识。每个转义符的作用范围从其出现起开始至下一个同样的转义符出现止
 *
 * @author frank
 */
public class Str {
    private String control;
    private String msg;

    public Str(String str, Esc... escs) {
        msg = str;
        StringBuffer sb = new StringBuffer();
        for (Esc esc : escs) {
            sb.append(esc.getValue());
        }
        control = sb.toString();
    }

    public Str(String str, List<Esc> escList) {
        msg = str;
        StringBuffer sb = new StringBuffer();
        for (Esc esc : escList) {
            sb.append(esc.getValue());
        }
        control = sb.toString();
    }

    public String getControl() {
        return control;
    }

    public String getMsg() {
        return msg;
    }

}
