package com.qzsoft.demomis.modules.cms.sansi.request;

import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 读取 LED 故障信息请求
 *
 * @author frank
 */
public class GetLEDErrorRequest extends Request {

    public GetLEDErrorRequest() throws CmsException {
        this.type = int2bytes(16, 2);
        this.address = int2bytes(0, 2);
    }

    public GetLEDErrorRequest(int address) throws CmsException {
        this.type = int2bytes(16, 2);
        this.address = int2bytes(address, 2);
    }

    @Override
    protected void setData() throws CmsException, IOException {
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
