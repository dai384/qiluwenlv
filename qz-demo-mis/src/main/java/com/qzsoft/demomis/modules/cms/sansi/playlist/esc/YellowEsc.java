package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

/**
 * \yn n 可为 0 或 1,缺省为 0。当 n 为 0 时,则图形(bmp、ico、flc) 文件中的黄色组织到可变情报板的红、绿色 LED;当 n 为
 * 1 时,则 图形文件中的黄色组织到可变情报板的琥珀色像素管。<br/>
 * 0:true,1:false
 * 
 * @author frank
 * 
 */
public class YellowEsc extends Esc {

	@Override
	protected String getCommand() {
		return "\\y";
	}

	private YellowEsc(boolean yellow) {
		if (yellow) {
			value = "0";
		} else {
			value = "1";
		}
	}

	public static YellowEsc getInstance() {
		return new YellowEsc(true);
	}

	public static YellowEsc getInstance(boolean yellow) {
		return new YellowEsc(yellow);
	}
}
