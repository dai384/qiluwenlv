package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * \Sxx 字间距,xx 的范围为 [-9, 99],缺省为 0
 * 
 * @author frank
 * 
 */
public class SpacingEsc extends Esc {
	private static Logger logger = LoggerFactory.getLogger(LocationEsc.class);
	private final static int DEFAULT = 0;

	@Override
	protected String getCommand() {
		return "\\S";
	}

	private SpacingEsc(int m) {
		if (m < -9 || m > 99) {
			logger.warn("字间距的取值范围为-9~99，现在为 {}，将使用默认值 {}", value, DEFAULT);
			m = DEFAULT;
		}
		value = format2(m);
	}

	public static SpacingEsc getInstance() {
		return new SpacingEsc(DEFAULT);
	}

	public static SpacingEsc getInstance(int m) {
		return new SpacingEsc(m);
	}
}
