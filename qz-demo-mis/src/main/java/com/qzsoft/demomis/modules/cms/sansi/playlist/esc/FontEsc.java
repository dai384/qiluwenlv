package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * \fnHHWW 字体。<br/>
 * n 为字体名称：h 表示黑体、k 表示楷体、s 表示宋体、f 表示仿宋体；<br/>
 * HH 为字体 高度，WW 为字体宽度，HH、WW 的范围都是 1-64。<br/>
 * \fnHHWW 转义表示选用 font 目录下的 hzkHHWWn 汉字库文件和 ascHHVV ASCII 码字库文 件，其中 VV 等于 (WW+1)
 * 除于 2 后的整数部分。此转义默认为 \fs1616，即选用 font 目录下的 hzk1616s 汉字库文件和 asc1608 ASCII
 * 码字库文件。注意字库文件的组织以 8 点为 1 字节，先从左向右、再从上到下组织。若 WW 或 VV 不能被 8 整除，在组织字库文 件时仍需补齐 8
 * 点。
 * 
 * @author frank
 * 
 */
public class FontEsc extends Esc {
	private static Logger logger = LoggerFactory.getLogger(FontEsc.class);
	private final int DEFAULT = 16;

	@Override
	protected String getCommand() {
		return "\\f";
	}

	private FontEsc(Font font, int h, int w) {
		if (h < 1 || h > 64 || w < 1 || w > 64) {
			logger.warn("字体长宽的取值范围为1~64，现在为 {}，将使用默认值 {}", value, DEFAULT);
			value = font.getKey() + format2(DEFAULT) + format2(DEFAULT);
		} else {
			value = font.getKey() + format2(h) + format2(w);
		}
	}

	public static FontEsc getInstance() {
		return new FontEsc(Font.S, 16, 16);
	}

	public static FontEsc getInstance(Font font, int h, int w) {
		return new FontEsc(font, h, w);
	}

	public enum Font {
		S("s", "宋体"), H("h", "黑体"), K("k", "楷体"), F("f", "仿宋体");
		private String key;
		private String name;

		Font(String key, String name) {
			this.key = key;
			this.name = name;
		}

		public String getKey() {
			return key;
		}

		public String getName() {
			return name;
		}

        public static Font getFont(String name){
            for(Font font:values()){
                if(StringUtils.equals(name,font.getKey())){
                    return font;
                }
            }
            return S;
        }
	}
}
