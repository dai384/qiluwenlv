package com.qzsoft.demomis.modules.cockpit.controller;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.cockpit.service.LdCockpitService;
import com.qzsoft.demomis.modules.task.entity.WlTasksEntity;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.utils.Result;
import com.qzsoft.jeemis.common.validator.ValidatorUtils;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 新闻管理(TbNews)表控制层
 *
 * @author sdmq
 * @since 2019-09-26 21:01:45
 */
@RestController
@RequestMapping("cockpit/wlresource")
@Api(tags="领导驾驶舱")
public class LdCockpitController {
	@Autowired
	private LdCockpitService ldCockpitService;


	@GetMapping("queryCount/{index}")
	@ApiOperation("查询数量")
//	@RequiresPermissions("cockpit:wlresource:queryCount")
	public Result<Map<String,Integer>> queryCount(@PathVariable("index") String index){
		Integer i = null;
		if (index!=null){
			i = Integer.parseInt(index);
		}

		Map<String, Integer> map = ldCockpitService.queryCount(i);

		return new Result<Map<String,Integer>>().ok(map);
	}

	@GetMapping("queryNumber")
	@ApiOperation("查询头部数量")
//	@RequiresPermissions("cockpit:wlresource:queryCount")
	public Result<Map<String,Integer>> queryNumber(){
		Map<String, Integer> map = ldCockpitService.queryNumber();

		return new Result<Map<String,Integer>>().ok(map);
	}

	@PostMapping("page")
	@ApiOperation("分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
//			@ApiImplicitParam(name = "ge_createDate", value = "起始创建时间", paramType = "query", dataType="Date"),
//			@ApiImplicitParam(name = "le_createDate", value = "截止创建时间", paramType = "query", dataType="Date")
	})
//	@RequiresPermissions("progrem:wlprogrem:page")
	public Result<IPage<WlTasksEntity>> page(@ApiIgnore @RequestBody Map<String, Object> params){
		IPage<WlTasksEntity> page = ldCockpitService.page(params);

		return new Result<IPage<WlTasksEntity>>().ok(page);
	}

	@GetMapping("{id}")
	@ApiOperation("信息")
//    @RequiresPermissions("template:wltemplate:info")
	public Result<WlTasksEntity> get(@PathVariable("id") String id){
		WlTasksEntity data = ldCockpitService.get(id);

		return new Result<WlTasksEntity>().ok(data);
	}

	@GetMapping("queryTasksCount")
	@ApiOperation("查询扇形数量")
//	@RequiresPermissions("cockpit:wlresource:queryCount")
	public Result<Map<String,Integer>> queryTasksCount(){
		Map<String, Integer> map = ldCockpitService.queryTasksCount();

		return new Result<Map<String,Integer>>().ok(map);
	}

	@PutMapping
	@ApiOperation("修改")
	@LogOperation("修改")
//	@RequiresPermissions("progrem:wlprogrem:update")
	public Result update(@RequestBody WlTasksEntity dto){
		//效验数据
		ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);
		ldCockpitService.update(dto);
		return new Result();
	}

	@PostMapping("pageByResult")
	@ApiOperation("分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
//			@ApiImplicitParam(name = "ge_createDate", value = "起始创建时间", paramType = "query", dataType="Date"),
//			@ApiImplicitParam(name = "le_createDate", value = "截止创建时间", paramType = "query", dataType="Date")
	})
//	@RequiresPermissions("progrem:wlprogrem:page")
	public Result pageByResult(@ApiIgnore @RequestBody Map<String, Object> params){
		Map<String, Object> map = new HashMap<>();

		List<WlTasksEntity> pageByResult = ldCockpitService.getPageByResult(params);
		int total = ldCockpitService.total(params);

		map.put("total", total);
		map.put("records", pageByResult);

		return new Result<>().ok(map);
	}
}