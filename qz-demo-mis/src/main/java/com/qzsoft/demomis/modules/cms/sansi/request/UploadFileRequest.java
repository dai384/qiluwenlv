package com.qzsoft.demomis.modules.cms.sansi.request;

import com.google.common.collect.Lists;
import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * 向可变信息标志上载文件请求
 *
 * @author frank
 */
public class UploadFileRequest {
    private int address;
    private String name = "PLAY.LST";//默认播放列表文件名称
    private byte[] content;
    private List<UploadFileRequestItem> items = Lists.newArrayList();

    public UploadFileRequest() {
        this.address = 0;
    }

    public UploadFileRequest(int address) {
        this.address = address;
    }

    public byte[][] encoder() throws IOException, CmsException {
        ByteArrayInputStream in = new ByteArrayInputStream(content);
        byte[] read = new byte[2048];
        int len = 0;
        int i = 0;
        int last = 0;
        while ((len = in.read(read)) > 0) {
            last = len;
            UploadFileRequestItem item = new UploadFileRequestItem(address);
            item.setData(readData(len, i++, read));
            items.add(item);
        }
        in.close();
        if (last == 2048) {// 如果最后一次读取数据的长度为2048
            UploadFileRequestItem item = new UploadFileRequestItem(address);
            item.setData(readData(1, i++, (byte) 0x00));
            items.add(item);
        }
        List<byte[]> list = Lists.newArrayList();
        for (UploadFileRequestItem item : items) {
            list.add(item.encoder());
        }
        return list.toArray(new byte[list.size()][]);
    }

    /**
     * 读取数据
     *
     * @param len 要从byte数组中读取的长度，从0开始
     * @param num
     * @param bs
     * @return
     * @throws IOException
     */
    private byte[] readData(int len, int num, byte... bs) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(name.getBytes());
        out.write(0x2B);
        out.write(CodecUtil.getOffset(2048 * (num++)));
        out.write(bs, 0, len);
        byte[] data = out.toByteArray();
        out.close();
        return data;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    class UploadFileRequestItem extends Request {
        public UploadFileRequestItem(int address) throws CmsException {
            this.type = int2bytes(10, 2);
            this.address = int2bytes(address, 2);
        }

        @Override
        protected void setData() throws CmsException, IOException {
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }
    }

    // public static void main(String[] args) {
    // byte[] bb = new byte[2048];
    // try {
    // UploadFileRequest ufr = new UploadFileRequest(0);
    // ufr.content = bb;
    // ufr.name = "abc";
    // byte[][] list = ufr.encoder();
    // for (byte[] b : list) {
    // System.out.println(CodecUtil.bytesToHexString(b));
    // }
    // } catch (DisplayException e) {
    // e.printStackTrace();
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    //
    // }
}
