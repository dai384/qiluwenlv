package com.qzsoft.demomis.modules.cms.sansi.request;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 读取充放电控制器信息 (太阳能屏专用)请求
 *
 * @author frank
 */
public class GetSolarControlRequest extends Request {

    public GetSolarControlRequest() throws CmsException {
        this.type = int2bytes(21, 2);
        this.address = int2bytes(0, 2);
    }

    public GetSolarControlRequest(int address) throws CmsException {
        this.type = int2bytes(21, 2);
        this.address = int2bytes(address, 2);
    }

    @Override
    protected void setData() throws CmsException, IOException {
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
