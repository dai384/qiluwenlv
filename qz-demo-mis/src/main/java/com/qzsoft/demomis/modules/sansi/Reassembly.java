package com.qzsoft.demomis.modules.sansi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.qzsoft.demomis.modules.cms.common.CodecUtil;

/*
* 组装发送信息
* 先校验再转义
* */
public class Reassembly {
    protected static final Logger LOGGER = LoggerFactory.getLogger(Reassembly.class);

    public String commenPath = "D:";
    private String programPath = "D:/qiluwenlv/program/sansi/xMedia1.xml";

    protected final byte header = 0x02; // 帧头
    protected byte[] address ; // 地址
    protected byte[] type ; // 类型
    protected byte[] data;// 未转义的数据
    protected byte[] dataHex;// 未转义的数据
    protected byte[] offSet; // 偏移量
    protected byte[] offSetPlay; // 偏移量
    protected int check; // 校验
    protected final byte footer = 0x03; // 帧尾
    protected byte[] bytes;// 包含头帧与尾帧的数据

    Crc16UtilBySelect crc16UtilBySelect = new Crc16UtilBySelect();
    CodecUtil codecUtil = new CodecUtil();
//  524288
    static int blockSize = 524288;

    public byte[] reassemblyOperation(byte type, String resource, int n ) throws Exception{
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        out.write(header);  // 帧头
        out.write(type); // 类型

        // 类型，数据 -----》校验
        ByteArrayOutputStream outCheck = new ByteArrayOutputStream();
        outCheck.write(type); // 类型

        switch (type) {
            case (byte) 0x52:
                /*
                * 查看blocksize
                * */
                break;
            case (byte) 0x53:
                /*
                * 文件上传
                * */
                 // 1、目标文件名 :   /qiluwenlv/resource/imageSavePath/20200723/6c43da9eab33458e9bd8a27c6b46cd1b.jpg
                String[] stringsArr = resource.split("/");
                String title = ".\\Program\\Materials\\" + stringsArr[5];

                String path = commenPath + resource;
                String str = "";
                for (int i = 0; i < title.length(); i++) {
                    int ch = (int) title.charAt(i);
                    String s4 = Integer.toHexString(ch);
                    str = str + s4;
                }
                byte[] titleBytes = codecUtil.str2bytes(str);

                out.write(titleBytes);
                outCheck.write(titleBytes);

                // 2、分隔符
                out.write(0x00);
                outCheck.write(0x00);
                
                LOGGER.info("shangchuan wenjian n start num："+n);
                int start = n*blockSize;
                LOGGER.info("shangchuan wenjian n start num："+start);

                // 3、偏移量   offset为字节
                offSet = codecUtil.getOffset(start);
                out.write(offSet);
                outCheck.write(offSet);
                // 4、文件内容
                data = readFile(path);
                // 不需要做此操作
                /*
                *   src：byte源数组bai
                    srcPos：截取源byte数组起始位置（0位置有效）
                    dest,：byte目的数组（截取后存放的数组）
                    destPos：截取后存放的数组起始位置（0位置有效）
                    length：截取的数据长度
                * */
                LOGGER.info("data=======================:"+data.length);
                // 判断是不是最后一次
                int a = data.length / blockSize;
                int b = data.length % blockSize;
                byte[] subData;// 截取数据
                if (a == n) {
                    subData =  new byte[b];
                    LOGGER.info("=========123=======mowei shuju changdu================：" + b);
                    System.arraycopy(data, start, subData, 0, b);
                } else {
                    subData =  new byte[blockSize];
                    System.arraycopy(data, start, subData, 0, blockSize);
                }


                // 转进制[不需要]
//                String dataStr = codecUtil.encodeHex(subData);
//                dataHex = codecUtil.decodeHex(dataStr);

                if (subData != null) // 数据
                {
                    out.write(esc(subData));

                    // 组装校验数据
                    outCheck.write(subData); // 数据
                }
                break;
            case (byte) 0x01:
                /*
                 * 播放表上传
                 * */
                // 1、目标文件名 : xMedia1.xml
                String playTitle = ".\\Program\\NewPrograme\\General\\xMedia1\\xMedia1.xml";

                String strPlay = "";
                for (int i = 0; i < playTitle.length(); i++) {
                    int ch = (int) playTitle.charAt(i);
                    String s4 = Integer.toHexString(ch);
                    strPlay = strPlay + s4;
                }
                byte[] titleBytesPlay = codecUtil.str2bytes(strPlay);

                out.write(titleBytesPlay);
                outCheck.write(titleBytesPlay);

                // 2、分隔符
                out.write(0x00);
                outCheck.write(0x00);
                LOGGER.info("shangchuan wenjian n start num："+n);
                int startPlay = n*blockSize;
                LOGGER.info("shangchuan wenjian n start num："+startPlay);

                // 3、偏移量
                offSetPlay = codecUtil.getOffset(startPlay);
                out.write(offSetPlay);
                outCheck.write(offSetPlay);
                // 4、文件内容
                data = readFile(programPath);
                // 不需要做此操作
                /*
                *   src：byte源数组bai
                    srcPos：截取源byte数组起始位置（0位置有效）
                    dest,：byte目的数组（截取后存放的数组）
                    destPos：截取后存放的数组起始位置（0位置有效）
                    length：截取的数据长度
                * */
                LOGGER.info("data=======================:"+data.length);
                // 判断是不是最后一次
                int c = data.length / blockSize;
                int d = data.length % blockSize;
                byte[] subDataplay;// 截取数据
                if (c == n) {
                    subDataplay =  new byte[d];
                    LOGGER.info("=========456=======末尾数据长度================：" + d);
                    System.arraycopy(data, startPlay, subDataplay, 0, d);
                } else {
                    subDataplay =  new byte[blockSize];
                    System.arraycopy(data, startPlay, subDataplay, 0, blockSize);
                }


                // 转进制[不需要]
//                String dataStr = codecUtil.encodeHex(subData);
//                dataHex = codecUtil.decodeHex(dataStr);

                if (subDataplay != null) // 数据
                {
                    out.write(esc(subDataplay));

                    // 组装校验数据
                    outCheck.write(subDataplay); // 数据
                }
                break;
            case (byte) 0x06:
                /*
                 * 播放表上传完毕,指定播放
                 * */
                // 1、flag:(1字节，0—常规播放表，1—计划播放表，2—即时播放表)
                // 检验码转为一字节
                byte[] flag = codecUtil.int2bytes(0, 1);
                out.write(flag);
                outCheck.write(flag);

                // 2、待播放的播放表名（不定长）
                String toPlayTitle = "xMedia1";

                String toPlay = "";
                for (int i = 0; i < toPlayTitle.length(); i++) {
                    int ch = (int) toPlayTitle.charAt(i);
                    String s4 = Integer.toHexString(ch);
                    toPlay = toPlay + s4;
                }
                byte[] titleBytesToPlay = codecUtil.str2bytes(toPlay);

                out.write(esc(titleBytesToPlay));
                outCheck.write(titleBytesToPlay);

                break;
            default:
                break;
        }

//        LOGGER.info("============校验数组的长度========="+outCheck.toByteArray().length);

        check = crc16UtilBySelect.radar_crc16(outCheck.toByteArray(), outCheck.toByteArray().length);
        // 检验码转为两字节
        byte[] checks = codecUtil.int2bytes(check, 2);
        System.out.println("=====================查看校验数据==================="+String.format("0x%04x", check));
        out.write(esc(checks)); // 校验数据
        out.write(footer); // 帧尾
        bytes = out.toByteArray();
        out.close();
        return bytes;
    }

    /**
     * 数据转义<br/>
     * 帧数据或帧校验中如有某个字节等于帧头、帧尾 或 0x1B,则在发送此帧时需转换为两个字节,<br/>
     * 即 0x1B 和 (此字节减去 0x1B): 0x02 转换为 0x1B,<br/>
     * 0xE7 0x03 转换为 0x1B,<br/>
     * 0xE8 0x1B 转换为 0x1B, 0x00<br/>
     * 相应地,接收方在接收帧数据或帧校验时,如遇到 0x1B,则把它与随后的字节相加, 转换为一个字节。
     * 以上措施是为了保证帧数据或帧校验中不出现帧头、帧尾,以免影响接收方的同步。
     *
     * @param bytes
     * @return
     */
    protected byte[] esc(byte[] bytes) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        for (byte b : bytes) {
            switch (b) {
                case 0x02:
                    out.write(0x1B);
                    out.write(0xE7);
                    break;
                case 0x03:
                    out.write(0x1B);
                    out.write(0xE8);
                    break;
                case 0x1B:
                    out.write(0x1B);
                    out.write(0x00);
                    break;
                default:
                    out.write(b);
                    break;
            }
        }
        return out.toByteArray();
    }


    /**
     * 读取文件
     *
     * @param path
     * @return
     * @throws IOException
     */
    private static byte[] readFile( String path) throws IOException {
        File file = new File(path);
        FileInputStream in = new FileInputStream(file);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] bytes = new byte[1024];
        int len = 0;

        while ((len = in.read(bytes)) > 0) {
            out.write(bytes, 0, len);
        }

        byte[] data = out.toByteArray();
        in.close();
        out.close();

        return data;
    }



    /*
    * 查看文件长度: 分N次发送，返回N
    * */
    public int queryLength (String resource) throws Exception{
        String path = commenPath + resource;

        byte[] bytesAll = readFile(path);
        LOGGER.info("bytesAll=======================:"+bytesAll.length);

        int m = 0;
        int n = 0;

        m = bytesAll.length / blockSize; // 商
        n = bytesAll.length % blockSize; // 余数

        if (n>0){
            m = m + 1;
        }
        return m;
    }


    /*
     * 查看文件长度: 分N次发送，返回N
     * */
    public int queryCatalogueLength (String resource) throws Exception{
        byte[] bytesAll = readFile(resource);
        LOGGER.info("select wenjian zongchangdu fen N ci fasong，bytesAll=======================:"+bytesAll.length);

        int m = 0;
        int n = 0;

        m = bytesAll.length / blockSize; // 商
        n = bytesAll.length % blockSize; // 余数

        if (n>0){
            m = m + 1;
        }
        return m;
    }

}
