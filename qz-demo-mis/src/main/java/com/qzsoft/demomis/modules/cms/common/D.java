package com.qzsoft.demomis.modules.cms.common;

import com.google.common.collect.Maps;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * Created by frank on 15/12/29.
 */
public class D {
    public static final String HS = "startHour";//开始小时
    public static final String HE = "endHour";//结束小时
    public static final String DS = "startDay";//开始天
    public static final String DE = "endDay";//结束天
    public static final String MS = "startMonth";//开始月
    public static final String ME = "endMonth";//结束月
    public int year;
    public int month;
    public int day;
    public int hour;

    public D(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static void main(String[] args) {
        getPrevTime();
    }

    public static Map<String, Date> getPrevTime() {
        Map<String, Date> map = Maps.newLinkedHashMap();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND,0);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        boolean hourZero = false;
        boolean dayOne = false;
        if (hour == 0) {//是0点
            hourZero = true;
            if (day == 1) {
                dayOne = true;
            }
        }

        map.put(D.HE, calendar.getTime());//当前小时的0分,做小时查询结束时间
        calendar.add(Calendar.HOUR, -1);
        map.put(D.HS, calendar.getTime());//上一小时的0分,做小时查询开始时间
        calendar.add(Calendar.HOUR, 1);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        if (hourZero) {//当前实际时间为0点
            map.put(D.DE, calendar.getTime());//当前日期的0点0分,做日查询的结束时间
            calendar.add(Calendar.DAY_OF_YEAR, -1);
            map.put(D.DS, calendar.getTime());//前一天日期的0点0分,做日查询的开始时间
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        } else {
            map.put(D.DS, calendar.getTime());//当前日期的0点0分,做日查询的开始时间
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            map.put(D.DE, calendar.getTime());//下一天日期的0点0分,做日查询的结束时间
            calendar.add(Calendar.DAY_OF_YEAR, -1);
        }
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        if (dayOne) {//当前实际日期为1号
            map.put(D.ME, calendar.getTime());//当前月的1号0点0分,做月查询的结束时间
            calendar.add(Calendar.MONTH, -1);
            map.put(D.MS, calendar.getTime());//前一月的1号0点0分,做月查询的开始时间
        } else {
            map.put(D.MS, calendar.getTime());//当前月的1号0点0分,做月查询的开始时间
            calendar.add(Calendar.MONTH, 1);
            map.put(D.ME, calendar.getTime());//前一月的1号0点0分,做月查询的结束时间
        }
        return map;
    }
}
