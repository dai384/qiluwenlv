package com.qzsoft.demomis.modules.sansi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
/*
* w3c创建xml
* */
public class XmlUtil {
    protected static final Logger LOGGER = LoggerFactory.getLogger(XmlUtil.class);
    static private String programPath = "D:/qiluwenlv/program/sansi/";

    public static void main(String[] args){

        List<String> resourceList = new ArrayList<>();
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200723/6c43da9eab33458e9bd8a27c6b46cd1b.jpg");
        resourceList.add("/qiluwenlv/resource/imageSavePath/20200723/597fdee4b7094b85a38a8b3a9cd8c3d8.jpg");

        Long start = System.currentTimeMillis();
        Boolean bool = createXml(resourceList);
        LOGGER.info("sansi xml result ："+ bool);
        LOGGER.info("sansi xml run time ："+ (System.currentTimeMillis() - start));
    }

    /**
     * 生成xml方法
     */
    public static Boolean createXml( List<String> resourceList){
        try {
            // 创建解析器工厂
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = factory.newDocumentBuilder();
            Document document = db.newDocument();
            // 不显示standalone="no"
            document.setXmlStandalone(true);
            Element xMediaPlaylist = document.createElement("xMediaPlaylist");
            // 为xMediaPlaylist节点添加属性
            xMediaPlaylist.setAttribute("ProgrameName", "NewPrograme");
            xMediaPlaylist.setAttribute("Name", "xMedia1");
            xMediaPlaylist.setAttribute("Type", "0");
            xMediaPlaylist.setAttribute("Background", "");
            xMediaPlaylist.setAttribute("BgRed", "0");
            xMediaPlaylist.setAttribute("BgGreen", "0");
            xMediaPlaylist.setAttribute("BgBlue", "0");
            xMediaPlaylist.setAttribute("BgSound", "");
            xMediaPlaylist.setAttribute("NumScenes", "1");
            xMediaPlaylist.setAttribute("NumScheduledZones", "0");
            xMediaPlaylist.setAttribute("NumInteractiveZones", "0");
            xMediaPlaylist.setAttribute("NumItems", resourceList.size() + ""); // item数
            xMediaPlaylist.setAttribute("NumSchedules", "0");
            xMediaPlaylist.setAttribute("RepeatCount", "1");
            xMediaPlaylist.setAttribute("RepeatDur", "3600");

            // 向xMediaPlaylist根节点中添加子节点scene
            Element scene = document.createElement("Scene");
            // 为scene节点添加属性
            scene.setAttribute("Id", "0");
            scene.setAttribute("Name", "New Scene");
            scene.setAttribute("MainZoneId", "255");
            scene.setAttribute("NumZones", "1");

            // 向xMediaPlaylist根节点中添加子节点zone
            Element zone = document.createElement("Zone");
            // 为zone节点添加属性
            zone.setAttribute("Id", "0");
            zone.setAttribute("Name", "New Zone");
            zone.setAttribute("Left", "0");
            zone.setAttribute("Top", "0");
            zone.setAttribute("Left", "0");
            zone.setAttribute("Top", "0");
            zone.setAttribute("Width", "2625");
            zone.setAttribute("Height", "315");
            zone.setAttribute("RepeatCount", "1");
            zone.setAttribute("RepeatDur", "600");
            zone.setAttribute("Transparent", "0");
            zone.setAttribute("Background", "");
            zone.setAttribute("Red", "0");
            zone.setAttribute("Green", "0");
            zone.setAttribute("Blue", "0");
            zone.setAttribute("Alpha", "255");
            zone.setAttribute("NumItems", resourceList.size() + ""); // item 数目
            zone.setAttribute("AfterLastFrame", "0");

            // 向zone根节点中添加子节点Item
            // 遍历添加
            for (int i = 0; i < resourceList.size(); i++) {
                String path = resourceList.get(i);
                String id = i+"";
                if (path.contains("image")){
                    Element item = document.createElement("Item");
                    // 为Item节点添加属性
                    item.setAttribute("Id", id);
                    item.setAttribute("Transition", "0");
                    item.setAttribute("Speed", "1");
                    item.setAttribute("RepeatCount", "0");
                    item.setAttribute("RepeatDur", "15");
                    item.setAttribute("Alignment", "104");
                    item.setAttribute("NumberOfParams", "0");

                    zone.appendChild(item);
                } else if (path.contains("vedio")) {
                    Element item = document.createElement("Item");
                    // 为Item节点添加属性
                    item.setAttribute("Id", id);
                    item.setAttribute("Transition", "0");
                    item.setAttribute("Speed", "1");
                    item.setAttribute("RepeatCount", "1");
                    item.setAttribute("RepeatDur", "2");
                    item.setAttribute("Alignment", "1");
                    item.setAttribute("NumberOfParams", "0");

                    zone.appendChild(item);
                }
            }

            
            scene.appendChild(zone);

            // 将scene节点添加到xMediaPlaylist根节点中
            xMediaPlaylist.appendChild(scene);

            // 向xMediaPlaylist根节点中添加子节点Item
            // 遍历添加
            for (int i = 0; i < resourceList.size(); i++) {
                String id = i + "";
                String fileName = resourceList.get(i).split("/")[5];

                Element item = document.createElement("Item");
                // 为Item节点添加属性
                item.setAttribute("Id", id);
                item.setAttribute("Name", "");
                item.setAttribute("Type", "0");
                item.setAttribute("FileName", fileName);
                item.setAttribute("DeviceNo", "0");

                // 将item2节点添加到xMediaPlaylist根节点中
                xMediaPlaylist.appendChild(item);
            }

            // 将xMediaPlaylist节点（已包含scene）添加到dom树中
            document.appendChild(xMediaPlaylist);

            // 创建TransformerFactory对象
            TransformerFactory tff = TransformerFactory.newInstance();
            // 创建 Transformer对象
            Transformer tf = tff.newTransformer();

            // 输出内容是否使用换行
            tf.setOutputProperty(OutputKeys.ENCODING, "gb2312");
            tf.setOutputProperty(OutputKeys.INDENT, "yes");
            tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            // 创建xml文件并写入内容
            tf.transform(new DOMSource(document), new StreamResult(new File(programPath + "xMedia1.xml")));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
