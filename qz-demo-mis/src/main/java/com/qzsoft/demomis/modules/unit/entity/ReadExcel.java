package com.qzsoft.demomis.modules.unit.entity;

import lombok.Data;

/**
 * @author:
 * des:用于存放excel的相关信息
 */
@Data
public class ReadExcel {
    //总行数
    private int totalRows = 0;
    //总条数
    private int totalCells = 0;
    //错误信息收集
    private String errorMsg;
}
