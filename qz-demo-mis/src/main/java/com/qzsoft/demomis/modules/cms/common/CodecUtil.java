package com.qzsoft.demomis.modules.cms.common;

import com.qzsoft.demomis.modules.sansi.SanSiSystem;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class CodecUtil {
    protected static final Logger LOGGER = LoggerFactory.getLogger(CodecUtil.class);

    static CRC16 crc16 = new CRC16();

    public CodecUtil() {
    }

    /**
     * 将字节数组编码为HEX编码的字符串
     */
    public static String encodeHex(byte[] dataBytes) {

        String dataString = null;
        dataString = new String(Hex.encodeHex(dataBytes));
        return dataString;
    }

    /**
     * 将HEX编码的字符串解码为字节数组
     */
    public static byte[] decodeHex(String dataString) {
        byte[] dataBytes = null;
        if (dataString != null) {
            try {
                dataBytes = Hex.decodeHex(dataString.toCharArray());
            } catch (DecoderException e) {
                LOGGER.warn(e.toString());
            }
        }
        return dataBytes;
    }



    public static byte[] short2bytes(short s) {
        byte[] bytes = new byte[2];
        for (int i = 1; i >= 0; i--) {
            bytes[i] = (byte) (s % 256);
            s >>= 8;
        }
        return bytes;
    }

    public static byte[] int2bytes(int s, int size) {
        byte[] bytes = new byte[size];
        for (int i = size - 1; i >= 0; i--) {
            bytes[i] = (byte) (s % 256);
            s >>= 8;
        }
        return bytes;
    }

    /**
     * 16进制的字符串表示转成字节数组
     *
     * @param str
     * 16进制格式的字符串
     * @return 转换后的字节数组
     **/
    public static byte[] str2bytes(String str) {
        final byte[] byteArray = new byte[str.length() / 2];
        int k = 0;
        for (int i = 0; i < byteArray.length; i++) {// 因为是16进制，最多只会占用4位，转换成字节需要两个16进制的字符，高位在先
            byte high = (byte) (Character.digit(str.charAt(k), 16) & 0xff);
            byte low = (byte) (Character.digit(str.charAt(k + 1), 16) & 0xff);
            byteArray[i] = (byte) (high << 4 | low);
            k += 2;
        }
        return byteArray;
    }

    public static int byte2int(byte b) {
        return b & 0xff;
    }

    public static short bytes2short(byte... bytes) {
        short s = (short) (bytes[1] & 0xFF);
        s |= (bytes[0] << 8) & 0xFF00;
        return s;
    }

    public static int bytes2int(byte... bytes) {
        int s = (bytes[1] & 0xFF);
        s |= (bytes[0] << 8) & 0xFF00;
        return s;
    }

    public static int bytes2int4(byte... b) {
        int mask = 0xff;
        int temp = 0;
        int n = 0;
        for (int i = 0; i < 4; i++) {
            n <<= 8;
            temp = b[i] & mask;
            n |= temp;
        }
        return n;
    }

    public static float bytes2float(byte... b) {
        return Float.intBitsToFloat(bytes2int4(b));
    }

    public static int byteToIntByPosition(byte b, int num) {
        byte tmp = b;
        tmp >>= num;
        tmp &= 0x01;
        return tmp;
    }

    public static int bytesToIntByPosition(byte[] bytes, int num) {
        int i = num / 8;
        int j = num % 8;
        byte tmp = (byte) (bytes[i] >> j);
        return tmp & 0x01;
    }

    public static Map<Integer, Integer> map(byte[] bytes, int addStart, int len) {
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        int dataLen = bytes.length * 8;
        for (int i = 0; i < dataLen && i < len; i++) {
            map.put(addStart + i, (bytes[(i / 8)] >> (i % 8)) & 0x01);
        }
        return map;
    }

    public static void main(String[] args) {
        byte[] bytes = {(byte) 0x04};
        Map<Integer, Integer> map = map(bytes, 4800, 4);
        for (int key : map.keySet()) {
            System.out.println(key + "\t" + map.get(key));
        }
    }

    /*
     * 获取crc校验的byte形式
     */
    public static byte[] crc16Bytes(byte... data) {
        return short2bytes(crc16Short(data));
    }

    /*
     * 获取crc校验的short形式
     */
    public static short crc16Short(byte... data) {
        return crc16.getCrc(data);
    }

    /**
     * Convert byte[] to hex
     * string.这里我们可以将byte转换成int，然后利用Integer.toHexString(int)来转换成16进制字符串。
     *
     * @param src byte[] data
     * @return hex string
     */
    public static String bytesToHexString(byte... src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString().toUpperCase();
    }

    public static String bytesToHexString(byte[] src, int len) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length && i < len; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString().toUpperCase();
    }

    /**
     * Convert hex string to byte[]
     *
     * @param hexString the hex string
     * @return byte[]
     */
    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    /**
     * Convert char to byte
     *
     * @param c char
     * @return byte
     */
    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    /**
     * 4 字节十六进制数，主用于上传下载文件的
     *
     * @param num
     * @return
     */
    public static byte[] getOffset(long num) {
        System.out.println("4 字节十六进制数，主用于上传下载文件的, 接收量为："+num);
        // 按位与
        /*
        * 一个字节（byte） 8个比特（bit）
        *
        * 创建4个字节的比特数组，共32位；  2^32 即最大值为：4 294 967 296 b / 1024 = 4 194 304 kb / 1024 = 4096 M;
        * oxFF  二进制表示为：1111 1111 ； 十进制表示为255
        *
        * eg: num 数值 524288 二进制表示为 1000   0000 0000   0000 0000
        * 结果：00 08 00 00
        * */
        byte[] b = new byte[4];
        b[3] = (byte) (num & 0xFF); // 0000 0000
        b[2] = (byte) (num >> 8 & 0xFF); // 0000 0000
        b[1] = (byte) (num >> 16 & 0xFF); // 0000 1000
        b[0] = (byte) (num >> 24 & 0xFF); // 0000 0000
        return b;
    }

    /**
     * 8 字节十六进制数，主用于上传下载文件的偏移量
     *
     * @param num
     * @return
     */
    public static byte[] getOffset2(long num) {
        byte[] b = new byte[8];
        b[7] = (byte) (num & 0xFF);
        b[6] = (byte) (num >> 8 & 0xFF);
        b[5] = (byte) (num >> 16 & 0xFF);
        b[4] = (byte) (num >> 24 & 0xFF);;
        b[3] = (byte) (num >> 32 & 0xFF);
        b[2] = (byte) (num >> 40 & 0xFF);
        b[1] = (byte) (num >> 48 & 0xFF);
        b[0] = (byte) (num >> 56 & 0xFF);
        return b;
    }

    /**
     * 返回一个字节数组，该数组包含此int的二进制补码表示形式
     */
    public static byte[] toByteArray(int i) {
        byte[] dataBytes = null;
        dataBytes = new byte[4];
        dataBytes[0] = (byte) ((i >>> 24) & 0xff);
        dataBytes[1] = (byte) ((i >>> 16) & 0xff);
        dataBytes[2] = (byte) ((i >>> 8) & 0xff);
        dataBytes[3] = (byte) (i & 0xff);
        return dataBytes;
    }
}
