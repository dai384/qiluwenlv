package com.qzsoft.demomis.modules.nova;
import novj.platform.vxkit.common.bean.FirmwareInfo;
import novj.platform.vxkit.common.bean.login.LoginResultBean;
import novj.platform.vxkit.common.bean.search.SearchResult;
import novj.platform.vxkit.common.result.DefaultOnResultListener;
import novj.platform.vxkit.common.result.OnResultListenerN;
import novj.platform.vxkit.handy.api.ProgramSendManager;
import novj.platform.vxkit.handy.api.SearchManager;
import novj.publ.api.NovaOpt;
import novj.publ.net.exception.ErrorDetail;
import novj.publ.net.svolley.Request.IRequestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;


/*
 * 登录操作
 * */
@Service
@EnableScheduling
public class loginDemo {
    protected static final Logger LOGGER = LoggerFactory.getLogger(LogoutDemo.class);

    //        10.2.5.68 滨州
    //        10.17.3.24 淄博西
    //        10.2.136.19 日照
    //        10.2.227.251 临沂
    //        10.2.185.68 栖霞北
    static String remoteIp = "10.2.185.68";
    public static void main (String[] args){
        loginDemo nn = new loginDemo();
        nn.NovaOperation(remoteIp);
    }

    public void NovaOperation(String remoteIp) {
        NovaOpt novaOpt = new NovaOpt();
        // 1、SDK 初始化: 1=Android 系统环境 2=Windows 或 Linux 系统环境
        novaOpt.GetInstance().initialize(2);
        // 2、搜索设备
        novaOpt.GetInstance().searchScreen(new SearchManager.OnScreenSearchListener(){
            @Override
            public void onSuccess(SearchResult searchResult){
                // 发现设备，自行添加处理逻辑
                novaOpt.GetInstance().connectDevice(searchResult, new DefaultOnResultListener() {
                    @Override
                    public void onSuccess(Integer response) {
                        LOGGER.info("Nova System connect success=================================2==================================：terminal IP is："+remoteIp+"-----------------sn:"+searchResult.sn);
                        // connect success，自行添加处理逻辑
                        // 4、登录设备
                        novaOpt.GetInstance().login(searchResult.sn, "admin", "123456",
                                new OnResultListenerN<LoginResultBean, ErrorDetail>() {
                                    @Override
                                    public void onSuccess(IRequestBase requestBase, LoginResultBean response) {
                                    //执行成功，自行添加处理逻辑
                                        LOGGER.info("=============================================================登录成功==================================");
                                    }
                                    @Override
                                    public void onError(IRequestBase requestBase, ErrorDetail error) {
                                    //执行失败，自行添加处理逻辑
                                        LOGGER.info("=============================================================登录失败================================="+error.errorCode+"=="+error.description);
                                    }
                                });
                    }
                    @Override
                    public void onError(ErrorDetail error) {
                        //连接失败，自行添加处理逻辑
                        LOGGER.info("Nova System连接失败===================================================================：terminal IP is："+remoteIp);
                    }
                }, wrapper -> {
                    //连接断开，自行添加处理逻辑
                    LOGGER.info("Nova System连接断开===================================================================：terminal IP is："+remoteIp);
                });
            }
            @Override
            public void onError(ErrorDetail errorDetail){
                // 发生错误，自行添加处理逻辑=
                LOGGER.info("Nova System搜索设备失败===================================================================：搜索terminal IP is："+remoteIp+"; 错误码为："+errorDetail.errorCode+"; 错误描述为："+errorDetail.description);
            }
        }, remoteIp);
    }
}

