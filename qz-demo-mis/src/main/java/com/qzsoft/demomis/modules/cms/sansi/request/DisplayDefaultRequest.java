package com.qzsoft.demomis.modules.cms.sansi.request;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 使可变信息标志显示预置的播放表请求
 *
 * @author frank
 */
public class DisplayDefaultRequest extends Request {
    private int num;

    public DisplayDefaultRequest() throws CmsException {
        this.type = int2bytes(98, 2);
        this.address = int2bytes(0, 2);
    }

    public DisplayDefaultRequest(int address) throws CmsException {
        this.type = int2bytes(98, 2);
        this.address = int2bytes(address, 2);
    }

    @Override
    protected void setData() throws CmsException, IOException {
//        data = new byte[3];
//        data[0] = 'A';
//        byte[] b = int2bytes(num, 2);
//        data[1] = b[0];
//        data[2] = b[1];
        data=int2bytes(num,3);
    }

    public void setNum(int num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
