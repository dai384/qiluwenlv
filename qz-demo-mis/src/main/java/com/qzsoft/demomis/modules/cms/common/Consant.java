package com.qzsoft.demomis.modules.cms.common;

/**
 * 常量类
 * Created by frank on 15/11/19.
 */
public class Consant {
    //字典表配置
    /**
     * webSocket端口在系统中的配置id
     */
    public static final long WEBSOCKET_PORT = 1;

    /**
     * 情报板在字典表中的id
     */
    public static final long DICT_CMS_ID = 1;

    /**
     * F板在字典表中的id
     */
    public static final long DICT_CMSF_ID = 1;

    //常用常量
}
