package com.qzsoft.demomis.modules.resource.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.program.dao.WlProgramDao;
import com.qzsoft.demomis.modules.program.entity.PicEntity;
import com.qzsoft.demomis.modules.resource.entity.ResourceStrtistic;
import com.qzsoft.demomis.modules.sansi.SanSiSystem;
import com.qzsoft.demomis.modules.sansi.SanSiUploadSystem;
import com.qzsoft.jeemis.platform.security.user.SecurityUser;
import it.sauronsoftware.jave.MultimediaInfo;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.ibatis.session.SqlSession;
import com.qzsoft.demomis.modules.resource.dao.WlResourceDao;
import com.qzsoft.demomis.modules.resource.entity.WlResourceEntity;
import com.qzsoft.demomis.modules.resource.service.WlResourceService;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ExcelUtils;
import com.qzsoft.jeemis.common.utils.ConvertDictUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import it.sauronsoftware.jave.Encoder;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * 资源表(WlResource)表服务实现类
 *
 * @author sdmq
 * @since 2020-06-05 17:01:23
 */
@Service("wlResourceService")
public class WlResourceServiceImpl extends BaseService implements WlResourceService {
	protected static final Logger LOGGER = LoggerFactory.getLogger(WlResourceServiceImpl.class);

    private final SqlSession sqlSession;
    @Autowired
	private WlResourceDao wlResourceDao;
	@Autowired
	private WlProgramDao wlProgramDao;

	SanSiUploadSystem sanSiUploadSystem = new SanSiUploadSystem();

	@Value("${commenPath}")
	private String commenPath;
	@Value("${returnCommonPath}")
	private String returnCommonPath;
    @Value("${imageSavePath}")
	private String imageSavePath;
	@Value("${audioSavePath}")
	private String audioSavePath;
	@Value("${vedioSavePath}")
	private String vedioSavePath;


	public WlResourceServiceImpl(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}
	
	/**
	 * WlResource数据分页
	 * @param params
	 * @return IPage
	 */
	@Override
	public IPage<WlResourceEntity> page(Map<String, Object> params) {
		IPage<WlResourceEntity> page  = this.getPage(params,Constant.CREATE_DATE,false,WlResourceEntity.class) ;
		QueryWrapper<WlResourceEntity> queryWrapper  = this.getQueryWrapper(params,WlResourceEntity.class);
		IPage<WlResourceEntity> pageData  =  wlResourceDao.selectPage(page, queryWrapper);
		pageData =  ConvertDictUtils.formatDicDataPage(pageData);
		return pageData;
	}
    /**
	 * WlResource数据列表
	 * @param params
	 * @return
	 */
	@Override
	public List<WlResourceEntity> list(Map<String, Object> params) {
		QueryWrapper<WlResourceEntity> queryWrapper  =  this.getQueryWrapper(params,WlResourceEntity.class);
		List<WlResourceEntity> entityList  =  wlResourceDao.selectList(queryWrapper);
		entityList = ConvertDictUtils.formatDicDataList(entityList);
		return entityList;
	}

	@Override
	public WlResourceEntity get(String id) {
		WlResourceEntity entity  =  wlResourceDao.selectById(id);
		return entity;
	}
	public List<ResourceStrtistic> getResource() {
		List<ResourceStrtistic> entity  =  wlResourceDao.getResource();
		return entity;
	}
	public ResourceStrtistic getResourceSum() {
		ResourceStrtistic entity  =  wlResourceDao.getResourceSum();
		return entity;
	}


    /**
	 * WlResource保存
	 * createDate creator
	 * updateDate updater 
	 * 字段会自动注入值 如不需要注入请修改WlResourceEntity
	 * @param entity
	 */
	@Override
	@Transactional(rollbackFor  =  Exception.class)
	public void save(WlResourceEntity entity) {
		wlResourceDao.insert(entity);
	}


	@Override
	@Transactional(rollbackFor  =  Exception.class)
	public void update(Map<String, Object> params) {
		wlResourceDao.updateBySelfId(params);
	}

	@Override
	@Transactional(rollbackFor  =  Exception.class)
	public void updateAll(WlResourceEntity entity) {
		wlResourceDao.updateById(entity);
	}

	@Override
	@Transactional(rollbackFor  =  Exception.class)
	public Boolean delete(String[] ids) {
		Boolean bool = true;
		for(String resourceId : ids){
			List<PicEntity> picEntityList = new ArrayList<>();
			picEntityList = wlProgramDao.getResourceEntity(resourceId);
			if(picEntityList.size()>0){
				bool = false;
			}
		}

		if (bool){
			wlResourceDao.deleteBatchIds(Arrays.asList(ids));
		}

		return bool;
	}
	@Override
	public void exportXls(Map<String, Object> params , HttpServletResponse response)  {
		List<WlResourceEntity> list  =  this.list(params);
		ExcelUtils.exportExcelToTarget(response, "资源表导出列表", list, WlResourceEntity.class);
	}

	/*
	* 存储资源
	* */
	public void saveSource( HttpServletRequest request ){
		// 存储资源的实体类
		WlResourceEntity wlResourceEntity  =  new WlResourceEntity();
		MultipartHttpServletRequest params = ((MultipartHttpServletRequest) request);
		List<MultipartFile> files  =  ((MultipartHttpServletRequest) request).getFiles("file");
		Long id = null; // 初始化id
		String strId = params.getParameter("id");
		if( strId !=null && !strId.isEmpty() ){
			id = Long.parseLong(strId);
		}
		String type = params.getParameter("type");

		wlResourceEntity.setId(id);
		wlResourceEntity.setType(type);
		wlResourceEntity.setName(params.getParameter("name"));
		wlResourceEntity.setGroupName(params.getParameter("groupName"));
		wlResourceEntity.setMarker(params.getParameter("marker"));

		String remark = "";
		remark = params.getParameter("remark");

		wlResourceEntity.setRemark(remark);
		MultipartFile file  =  null;
		BufferedOutputStream stream  =  null;
		// 存储时长
		String duration = "";
		if( files.size()>0 && !files.get(0).isEmpty() ){
			file = files.get(0);
			String path  =  "";
			String returnPath  =  "";
			switch ( type ){
				case "1":
					returnPath = returnCommonPath + imageSavePath;
					path  =  commenPath + returnPath;
					break;
				case "2":
					returnPath = returnCommonPath + vedioSavePath;
					path  =  commenPath + returnPath;
					duration = this.getVideoTime(file);
					break;
				case "3":
					returnPath = returnCommonPath + audioSavePath;
					duration = this.getVideoTime(file);
					path  =  commenPath + returnPath;
					break;
				default: System.out.println("资源类型有误，请联系管理员！");
			}
			//将时间设置为一层路径
			Date d  =  new Date();
			SimpleDateFormat sdf  =  new SimpleDateFormat("yyyyMMdd");
			String date  =  sdf.format(d);
			path  =  path + date;
			returnPath = returnPath +date;
			File filePath  =  new File(path);
			if (!filePath.exists() && !filePath.isDirectory()) {
				filePath.mkdirs();
			}
//			//对文文件的全名进行截取然后在后缀名进行删选。
			String[] arg = file.getOriginalFilename().split("\\.");
			String suffix  =  arg[arg.length-1];
			String newFileName  =  UUID.randomUUID().toString().replace("-", "")+"."+suffix;
			//在指定路径下创建一个文件
			File targetFile  =  new File(path, newFileName);

			// 最终存储路径
			String finaPath = returnPath+"/"+newFileName;

			wlResourceEntity.setPath(finaPath);
			wlResourceEntity.setSuffix(suffix);
			wlResourceEntity.setDuration(duration);
			//将文件保存到服务器指定位置
			try {
				// 上传成功
				file.transferTo(targetFile);
				if(remark!=null && remark.equals("三思")){
					try {
						List<String> resourceList = new ArrayList<>();
						resourceList.add(finaPath);
						sanSiUploadSystem.sanSiOperation("20.0.0.1", 7211, resourceList);
						sanSiUploadSystem.sanSiOperation("20.0.0.2", 7211, resourceList);
					}catch (Exception e){
						e.printStackTrace();
					}
				}

			} catch (IOException e) {
				// 上传失败
				e.printStackTrace();
			}
			if(id != null){
				wlResourceEntity.setUpdator(SecurityUser.getUser().getId().toString());
				updateAll(wlResourceEntity);
			}else {
				wlResourceEntity.setCreator(SecurityUser.getUser().getId().toString());
				save(wlResourceEntity);
			}
		}
	}


	/**
	 * 获取视频时间
	 */
	private String getVideoTime(MultipartFile contentFile){
		// 将MultipartFile转换为Encoder所需的File
		CommonsMultipartFile cf= (CommonsMultipartFile)contentFile;
		DiskFileItem fi = (DiskFileItem)cf.getFileItem();
		File f = fi.getStoreLocation();
		// 获取视频时长
		Encoder encoder = new Encoder();
		MultimediaInfo m = new MultimediaInfo();
		try{
			m = encoder.getInfo(f); // 得到单位是毫秒
		}catch(Exception e){
			e.printStackTrace();
		}
		long ls = m.getDuration()/1000;
//		int hour = (int) (ls/3600);
//		int minute = (int) (ls%3600)/60;
//		int second = (int) (ls-hour*3600-minute*60);
//		String time = hour+":"+minute+":"+second;
		return ls+"";
	}

	public List<WlResourceEntity> selectReasourceByType(String type){
		// 0: 视频，1: 音频，2: 图片，3: 轮播图，4: 文本，5: 直播，6: 时钟，7: 天气，
		switch (type){
			case "0":
				type = "3";
				break;
			case "1":
				type = "2";
				break;
			case "2":
				type = "1";
				break;
			case "3":
				type = "1";
				break;
			case "4":
				type = "4";
				break;
			case "5":
				type = "5";
				break;
			default:
				LOGGER.info("资源类型有误: type = "+type);
		}
		return wlResourceDao.selectReasourceByType(type);
	}
}