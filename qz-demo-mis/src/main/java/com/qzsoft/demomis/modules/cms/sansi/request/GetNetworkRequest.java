package com.qzsoft.demomis.modules.cms.sansi.request;

import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 读取以太网配置信息请求
 *
 * @author frank
 */
public class GetNetworkRequest extends Request {

    public GetNetworkRequest() throws CmsException {
        this.type = int2bytes(32, 2);
        this.address = int2bytes(0, 2);
    }

    public GetNetworkRequest(int address) throws CmsException {
        this.type = int2bytes(32, 2);
        this.address = int2bytes(address, 2);
    }

    @Override
    protected void setData() throws CmsException, IOException {
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public static void main(String[] args) throws CmsException, IOException {
        GetNetworkRequest gdtr = new GetNetworkRequest(0);
        System.out.println(CodecUtil.bytesToHexString(gdtr.encoder()));
    }
}
