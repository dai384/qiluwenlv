package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

/**
 * \Innn 把 %SYSDIR%\ICO 目录下的 nnn.ico 文件显示到 \C 所规定的坐标。 若 nnn.ico 文件包含多幅图标,则只显示第一幅
 * 
 * @author frank
 * 
 */
public class IcoEsc extends Esc {

	@Override
	protected String getCommand() {
		return "\\I";
	}

	private IcoEsc(String name) {
		value = name;
	}

	public static IcoEsc getInstance(String name) {
		return new IcoEsc(name);
	}
}
