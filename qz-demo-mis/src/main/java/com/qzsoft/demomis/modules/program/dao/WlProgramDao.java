package com.qzsoft.demomis.modules.program.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qzsoft.demomis.modules.program.entity.PicEntity;
import com.qzsoft.demomis.modules.program.entity.WlProgramMoudleResourceEntity;
import com.qzsoft.demomis.modules.template.entity.WlTemplateMoudleEntity;
import org.apache.ibatis.annotations.Mapper;
import com.qzsoft.demomis.modules.program.entity.WlProgramEntity;

import java.util.List;

/**
 * 节目表(Wl_Program)表数据库访问层
 *
 * @author sdmq
 * @since 2020-06-10 17:45:46
 */
@Mapper
public interface WlProgramDao extends BaseMapper<WlProgramEntity> {

    int insertEntity(WlProgramEntity wlProgramEntity);

    int updateMoudleById(WlTemplateMoudleEntity wlTemplateMoudleEntity);

    int deleteAllPicList(String id);

    int saveAllPicList(List<PicEntity> picEntityList);

    List<PicEntity> getPicEntity(String id);
    List<PicEntity> getResourceEntity(String id);

    List<String> getPicEntityByProgramId(String id);

    List<String> getPicList(String programId);

    int updateById(WlProgramEntity wlProgramEntity);

    int saveRealtionEntity (WlProgramMoudleResourceEntity wlProblemMoudleResourceEntity);

    int deleteRealationByProgramId (String programId);

    int deletePidByMoudleId (String moudleId);

    List<WlProgramMoudleResourceEntity> selectRelationList(String id);
}