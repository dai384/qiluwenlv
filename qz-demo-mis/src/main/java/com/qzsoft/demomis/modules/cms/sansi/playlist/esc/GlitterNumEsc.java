package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * \Nnn 闪烁次数（出字方式为字符闪烁或区域闪烁时有用）。nn 的范围为 0-99，默认为 \N03
 * 
 * @author frank
 * 
 */
public class GlitterNumEsc extends Esc {
	private static Logger logger = LoggerFactory.getLogger(GlitterNumEsc.class);
	private final static int DEFAULT = 3;

	@Override
	protected String getCommand() {
		return "\\N";
	}

	private GlitterNumEsc(int n) {
		if (n < 0 || n > 99) {
			logger.warn("闪烁次数的取值范围为0~99，现在为 {}，将使用默认值 {}", value, DEFAULT);
			value = format2(DEFAULT);
		} else {
			value = format2(n);
		}
	}

	public static GlitterNumEsc getInstance() {
		return new GlitterNumEsc(DEFAULT);
	}

	public static GlitterNumEsc getInstance(int n) {
		return new GlitterNumEsc(n);
	}
}
