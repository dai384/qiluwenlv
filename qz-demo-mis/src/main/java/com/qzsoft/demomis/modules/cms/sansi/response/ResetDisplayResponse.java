package com.qzsoft.demomis.modules.cms.sansi.response;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 重启控制板应答
 * 
 * @author frank
 * 
 */
public class ResetDisplayResponse extends Response {
	private int code;

	public ResetDisplayResponse(byte[] bytes) throws IOException, CmsException {
		this.bytes = bytes;
		decoder();
	}

	@Override
	protected void decoder() throws IOException, CmsException {
		basicDecoder();
        code = getInt(3, 1, CmsException.CONTENT_EXCEPTION);
	}

	public int getCode() {
		return code;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
