package com.qzsoft.demomis.modules.unit.controller;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.unit.service.WlRegionService;
import com.qzsoft.demomis.modules.unit.entity.WlRegionEntity;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.utils.ExcelUtils;
import com.qzsoft.jeemis.common.utils.Result;
import com.qzsoft.jeemis.common.validator.AssertUtils;
import com.qzsoft.jeemis.common.validator.ValidatorUtils;
import com.qzsoft.jeemis.common.validator.group.AddGroup;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * (WlRegion)表控制层
 *
 * @author sdmq
 * @since 2020-05-28 09:20:47
 */
@RestController
@RequestMapping("unit/wlregion")
@Api(tags="")
public class WlRegionController  {
    @Autowired
    private WlRegionService wlRegionService;

	//    获取省列表
	@PostMapping("list")
	@ApiOperation("列表")
//	@RequiresPermissions("unit:wlregion:list")
	public Result<List<WlRegionEntity>> list(@ApiIgnore @RequestBody Map<String, Object> params){
		List<WlRegionEntity> data = wlRegionService.list(params);

		return new Result<List<WlRegionEntity>>().ok(data);
	}

}