package com.qzsoft.demomis.modules.cms;

import com.google.common.collect.Maps;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import com.qzsoft.demomis.modules.cms.sansi.request.Request;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.SerialPort;
import org.apache.commons.lang3.StringUtils;
import org.omg.CORBA.TIMEOUT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Map;


/**
 * 情报板的通信函数
 *
 * @author 郝文江<haowenjiang@163.com>
 */
public class CmsConn {
    private static Logger logger = LoggerFactory.getLogger(CmsConn.class);
    private static Map<String, String> map = Maps.newHashMap();

    /**
     * 网口通信
     *
     * @param ip
     * @param port
     * @param request
     * @return
     * @throws java.io.IOException
     * @throws InterruptedException
     * @throws CmsException
     */
    protected static byte[] send(String ip, int port, Request request) throws IOException, InterruptedException, CmsException {
        if (StringUtils.isEmpty(ip)) {
            throw new CmsException("IP 地址不能为空");
        }
        if (port < 0) {
            throw new CmsException("请使用正常的端口号，不能为" + port);
        }
        return send(ip, port, request.encoder());
    }

    /**
     * 网口通信
     *
     * @param ip
     * @param port
     * @param bytes
     * @return
     * @throws java.io.IOException
     * @throws InterruptedException
     * @throws CmsException
     */
    protected static byte[] send(String ip, int port, byte[] bytes) throws IOException, InterruptedException, CmsException {
        if (StringUtils.isEmpty(ip)) {
            throw new CmsException("IP 地址不能为空");
        }
        if (port < 0) {
            throw new CmsException("请使用正常的端口号，不能为" + port);
        }
        if (StringUtils.startsWithIgnoreCase(ip, "com")) {//串口通信
            return serialCommand(ip, port, bytes);
        }
        byte[] data = null;
        Socket socket = new Socket();
        try {
            socket.connect(new InetSocketAddress(InetAddress.getByName(ip), port),Constant.TIMEOUT);
            socket.setSoTimeout(Constant.TIMEOUT);
            InputStream in = socket.getInputStream();
            OutputStream out = socket.getOutputStream();
            out.write(bytes);
            out.flush();
            logger.info("网络：" + ip + ":" + port + "发送指令：" + (bytes));
            logger.info("网络：" + ip + ":" + port + "发送指令：" + CodecUtil.bytesToHexString(bytes));
            int waitTime = 0;
            while (true) {
                int len = in.available();
                if (len > 0) {
                    data = new byte[len];
                    in.read(data);
                    logger.info("网络：" + ip + ":" + port + "收到应答：" + (data));
                    logger.info("网络：" + ip + ":" + port + "收到应答：" + CodecUtil.bytesToHexString(data));
                    break;
                }
                waitTime += 500;
                if (waitTime >Constant.TIMEOUT) {
                    throw new CmsException("等待指令返回超时");
                }
                Thread.sleep(500);
            }
            out.close();
            in.close();
        } finally {
            socket.close();
        }
        return data;
    }

//    public static void main(String[] args) throws InterruptedException, IOException, CmsException {
//        send("127.0.0.1", 9999, new byte[]{0x01});
//    }

    /**
     * 串口通信
     *
     * @param serial
     * @param request
     * @return
     * @throws CmsException
     * @throws IOException
     */
    protected static byte[] serialCommand(String serial, int speed, Request request) throws CmsException, IOException {
        if (StringUtils.isEmpty(serial)) {
            throw new CmsException("串口标识不能为空");
        }
        if (!map.keySet().contains(serial)) {
            logger.info("第一次使用串口{}，建立同步锁", serial);
            map.put(serial, serial);
        } else {
            logger.info("串口{}，已在同步锁中", serial);
        }
        logger.info("串口{}，开始等待同步锁", serial);
        synchronized (map.get(serial)) {
            logger.info("串口{}，打开同步锁，开始串口通信", serial);
            return serialCommand(serial, speed, request.encoder());
        }
    }

    protected static byte[] serialCommand(String serial, int speed, byte[] bytes) throws CmsException, IOException {
        long start = System.currentTimeMillis();
        SerialPort serialPort = openSerialPort(serial, speed);
        logger.info("打开串口用时 {} 毫秒", (System.currentTimeMillis() - start));
        try {
            InputStream in = serialPort.getInputStream();
            OutputStream out = serialPort.getOutputStream();
            out.write(bytes);
            out.flush();
            logger.info("串口：" + serial + "发送指令：" + CodecUtil.bytesToHexString(bytes));
            byte[] temp = new byte[4096];
            int len = 0;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] data = null;
            logger.info("串口：" + serial + "等待返回数据");
            while ((len = in.read(temp)) > 0) {
                logger.info("串口：" + serial + "有返回数据，开始读取");
                bout.write(temp, 0, len);
                logger.info("串口：" + serial + "读取完毕，开始处理数据");
                for (byte b : temp) {
                    if (b == (byte) 0x03) {
                        data = bout.toByteArray();
                        logger.info("串口：" + serial + "收到应答：" + CodecUtil.bytesToHexString(data));
                        out.close();
                        in.close();
                        logger.info("串口：" + serial + "返回数据");
                        return data;
                    }
                }
            }
            out.close();
            in.close();
            logger.info("串口：" + serial + "串口通信不畅通、无数据返回、通信超时或返回数据不合法");
            throw new CmsException("串口通信不畅通、无数据返回、通信超时或返回数据不合法");
        } finally {
            logger.info("串口：" + serial + "开始关闭");
            serialPort.close();
            logger.info("串口：" + serial + "关闭成功，释放同步锁");
        }
    }


    private static SerialPort openSerialPort(String serial, int speed) throws CmsException {
        CommPortIdentifier port = null;
        try {
            port = CommPortIdentifier.getPortIdentifier(serial);
        } catch (NoSuchPortException e) {
            throw new CmsException("没有该串口：" + serial);
        }
        try {
            SerialPort serialPort = (SerialPort) port.open("Frank" + serial, 5000);
            serialPort.setSerialPortParams(speed, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            serialPort.enableReceiveTimeout(Constant.TIMEOUT);
            return serialPort;
        } catch (Exception e) {
            e.printStackTrace();
            throw new CmsException("串口异常:" + serial);
        }
//        catch (PortInUseException e) {
//            throw new CmsException("串口已被占用：" + serial);
//        } catch (UnsupportedCommOperationException e) {
//            throw new CmsException("串口参数设置失败：" + serial);
//        }
    }
}
