package com.qzsoft.demomis.modules.cms.sansi.response;

import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.common.Tools;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 读取以太网配置信息应答
 *
 * @author frank
 */
public class GetNetworkResponse extends Response {
    private String mac;// Mac地址，无分隔符
    private String ip;
    private String geteway;
    private String mask;
    private int port;

    public GetNetworkResponse(byte[] bytes) throws IOException,
            CmsException {
        this.bytes = bytes;
        decoder();
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        basicDecoder();
        mac = getString(3, 12, CmsException.MAC_EXCEPTION);
        ip = hextoIp(getString(15, 8, "IP"));
        geteway = hextoIp(getString(23, 8, "默认网关"));
        mask = hextoIp(getString(31, 8, "子网掩码"));
        port = getInt(39, 5, "端口");
    }

    private String hextoIp(String hex) {
        Integer[] ip = new Integer[4];
        String[] tmp = Tools.split(hex, 2);
        for (int i = 0; i < 4; i++) {
            ip[i] = Integer.parseInt(tmp[i], 16);
        }
        return StringUtils.join(ip, ".");
    }

    public String getMac() {
        return mac;
    }

    public String getIp() {
        return ip;
    }

    public String getGeteway() {
        return geteway;
    }

    public String getMask() {
        return mask;
    }

    public int getPort() {
        return port;
    }

    public static void main(String[] args) {
        String hex = "f3034323";
        Integer[] ip = new Integer[4];
        for (int i = 0; i < 4; i++) {
            String tmp = StringUtils.substring(hex, i * 2, i * 2 + 2);
            ip[i] = Integer.parseInt(tmp, 16);
        }
        System.out.println(StringUtils.join(ip, "."));
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
