package com.qzsoft.demomis.modules.task.dao;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qzsoft.demomis.modules.task.entity.WlTasksEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface WlTasksDao extends BaseMapper<WlTasksEntity> {
    WlTasksEntity selectById(String id);
    Integer selectAgreeCount();//同意数量
    Integer selectRejectCount();//驳回数量
    Integer selectWaitCount();//等待审批数量
    List<WlTasksEntity> getPageByResult(Map<String, Object> params);//根据结果分页查询
    int total(Map<String, Object> params);//查询总页数
}
