package com.qzsoft.demomis.modules.sys.notice.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * (SysNoticeFile)表实体类
 *
 * @author sdmq
 * @since 2019-09-25 15:30:43
 */
@ApiModel(value ="")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("sys_notice_file")
public class SysNoticeFileEntity extends Model<SysNoticeFileEntity> {
    private static final long serialVersionUID = 587820335411191380L;
    /**
    *id主键主键
    */
    @TableId
    @ApiModelProperty(value = "id主键")
    private String id;
    /**
    *通知id 外键
    */
    @ApiModelProperty(value = "通知id 外键")
    private String noticeId;
    /**
    *文件Id
    */
    @ApiModelProperty(value = "文件Id")
    private String fileId;
    /**
    *文件名
    */
    @ApiModelProperty(value = "文件名")
    private String fileName;
    /**
    *文件大小
    */
    @ApiModelProperty(value = "文件大小")
    private String fileSize;
    /**
    *扩展名类型
    */
    @ApiModelProperty(value = "扩展名类型")
    private String fileType;
    /**
    *排序
    */
    @ApiModelProperty(value = "排序")
    private Integer orderid;
    /**
    *创建时间
    */
    @ApiModelProperty(value = "创建时间")
	@TableField(fill = FieldFill.INSERT)
    private Date createDate;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}