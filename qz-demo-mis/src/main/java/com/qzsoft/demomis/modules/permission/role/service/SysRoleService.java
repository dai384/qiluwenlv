/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.permission.role.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.permission.role.dto.SysRoleDTO;
import com.qzsoft.demomis.repository.sys.entity.SysMenuEntity;
import com.qzsoft.demomis.repository.sys.entity.SysRoleEntity;
import com.qzsoft.jeemis.platform.security.user.UserDetail;

import java.util.List;
import java.util.Map;


/**
 * 角色
 * 
 * @author
 */
public interface SysRoleService {

	IPage<SysRoleEntity> page(Map<String, Object> params);

	List<SysRoleEntity> list(Map<String, Object> params);

	SysRoleDTO get(Long id);

	void save(SysRoleDTO dto);

	void update(SysRoleDTO dto);

	void delete(Long[] ids);

	void delete2(Long[] ids);

	List<SysMenuEntity> getUserMenuList(UserDetail user);

}
