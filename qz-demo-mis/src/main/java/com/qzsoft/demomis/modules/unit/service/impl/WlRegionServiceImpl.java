package com.qzsoft.demomis.modules.unit.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qzsoft.demomis.modules.unit.dao.WlRegionDao;
import com.qzsoft.demomis.modules.unit.entity.WlRegionEntity;
import com.qzsoft.demomis.modules.unit.service.WlRegionService;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ConvertDictUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * (WlRegion)表服务实现类
 *
 * @author sdmq
 * @since 2020-05-28 09:20:46
 */
@Service("wlRegionService")
public class WlRegionServiceImpl extends BaseService implements WlRegionService {
    private final SqlSession sqlSession;
    @Autowired
	private WlRegionDao wlRegionDao;
   
	public WlRegionServiceImpl(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}

    /**
	 * WlRegion数据列表
	 * @param params
	 * @return
	 */
	@Override
	public List<WlRegionEntity> list(Map<String, Object> params) {
		QueryWrapper<WlRegionEntity> queryWrapper = this.getQueryWrapper(params,WlRegionEntity.class);
		List<WlRegionEntity> entityList = wlRegionDao.selectList(queryWrapper);
		entityList= ConvertDictUtils.formatDicDataList(entityList);
		return entityList;
	}

}