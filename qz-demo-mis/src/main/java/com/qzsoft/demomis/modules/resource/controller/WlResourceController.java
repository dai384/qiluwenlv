package com.qzsoft.demomis.modules.resource.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.program.entity.PicEntity;
import com.qzsoft.demomis.modules.resource.entity.ResourceStrtistic;
import com.qzsoft.demomis.modules.resource.service.WlResourceService;
import com.qzsoft.demomis.modules.resource.entity.WlResourceEntity;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.utils.Result;
import com.qzsoft.jeemis.common.validator.AssertUtils;
import com.qzsoft.jeemis.common.validator.ValidatorUtils;
import com.qzsoft.jeemis.common.validator.group.AddGroup;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 资源表(WlResource)表控制层
 *
 * @author sdmq
 * @since 2020-06-05 17:01:23
 */
@RestController
@RequestMapping("resource/wlresource")
@Api(tags="资源表")
public class WlResourceController  {
    @Autowired
    private WlResourceService wlResourceService;

	@PostMapping("page")
	@ApiOperation("分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = "link_name", value = "名字", paramType = "query", dataType="String"),
			@ApiImplicitParam(name = "ge_createDate", value = "起始创建时间", paramType = "query", dataType="Date"),
			@ApiImplicitParam(name = "le_createDate", value = "截止创建时间", paramType = "query", dataType="Date")
	})
	@RequiresPermissions("resource:wlresource:page")
	public Result<IPage<WlResourceEntity>> page(@ApiIgnore @RequestBody Map<String, Object> params){
		IPage<WlResourceEntity> page = wlResourceService.page(params);

		return new Result<IPage<WlResourceEntity>>().ok(page);
	}

	@GetMapping("list")
	@ApiOperation("列表")
	@RequiresPermissions("resource:wlresource:list")
	public Result<List<WlResourceEntity>> list(@ApiIgnore @RequestParam Map<String, Object> params){
		List<WlResourceEntity> data = wlResourceService.list(params);

		return new Result<List<WlResourceEntity>>().ok(data);
	}

	@GetMapping("{id}")
	@ApiOperation("信息")
	@RequiresPermissions("resource:wlresource:info")
	public Result<WlResourceEntity> get(@PathVariable("id") String id){
		WlResourceEntity data = wlResourceService.get(id);

		return new Result<WlResourceEntity>().ok(data);
	}


	@GetMapping("selectReasourceByType/{type}")
	@ApiOperation("信息")
	@RequiresPermissions("resource:wlresource:info")
	public Result<List<WlResourceEntity>> selectReasourceByType(@PathVariable("type") String type){
		List<WlResourceEntity> data = wlResourceService.selectReasourceByType(type);

		return new Result<List<WlResourceEntity>>().ok(data);
	}

	@PostMapping
	@ApiOperation("保存")
	@LogOperation("保存")
	@RequiresPermissions("resource:wlresource:save")
	public Result save(@RequestBody WlResourceEntity dto){
		//效验数据
		ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);
		wlResourceService.save(dto);
		return new Result();
	}

	@PutMapping
	@ApiOperation("修改")
	@LogOperation("修改")
	public Result update(@RequestBody Map<String, Object> params){

		wlResourceService.update(params);

		return new Result();
	}
	
	@DeleteMapping
	@ApiOperation("删除")
	@LogOperation("删除")
	@RequiresPermissions("resource:wlresource:delete")
	public Result delete(@RequestBody String[] ids){
		//效验数据
		AssertUtils.isArrayEmpty(ids, "id");

		Boolean bool = wlResourceService.delete(ids);

		return new Result().ok(bool);
	}



	@GetMapping("export")
	@ApiOperation("导出")
	@LogOperation("导出")
	@RequiresPermissions("resource:wlresource:export")
	public void export(@ApiIgnore @RequestParam Map<String, Object> params , HttpServletResponse response) throws Exception {
		wlResourceService.exportXls(params,response);
	}


	/* 多文件上传——图片、视频、音频 */
	@PostMapping("/saveSource")
	@ApiOperation("保存资源")
	@LogOperation("保存资源")
    @ResponseBody
    public Result saveImg(HttpServletRequest request){
	    wlResourceService.saveSource( request );

        return new Result();
	}


	@GetMapping("/statistic")
	@ApiOperation("信息")
	@RequiresPermissions("resource:wlresource:info")
	public Result<Map<String, Object>> getResource(){

		List<ResourceStrtistic> list = wlResourceService.getResource();
		ResourceStrtistic resourceStrtistic = wlResourceService.getResourceSum();

		Map<String, Object> ma = new HashMap<>();
		ma.put("list", list);
		ma.put("freeSum", resourceStrtistic.getFreeSum());
		ma.put("businessSum", resourceStrtistic.getBusinessSum());

		return new Result<Map<String, Object>>().ok(ma);
	}
}