package com.qzsoft.demomis.modules.cms.common;

/**
 * json格式返回处理结果使用
 * Created by frank on 15/10/30.
 */
public class Msg {
    public static final int SUCCESS = 0;
    public static final int FAULT = 1;
    private int code;
    private String msg;
    private Object ext;

    public Msg() {
    }

    public Msg(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getExt() {
        return ext;
    }

    public void setExt(Object ext) {
        this.ext = ext;
    }
}
