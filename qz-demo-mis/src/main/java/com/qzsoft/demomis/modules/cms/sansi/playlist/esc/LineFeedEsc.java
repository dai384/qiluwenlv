package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

/**
 * \n 换行。即 \C 中的 xxx 不变,yyy 加上 \f 中的 HH
 * 
 * @author frank
 * 
 */
public class LineFeedEsc extends Esc {

	@Override
	protected String getCommand() {
		return "\\n";
	}

	private LineFeedEsc() {

	}

	public static LineFeedEsc getInstance() {
		return new LineFeedEsc();
	}

}
