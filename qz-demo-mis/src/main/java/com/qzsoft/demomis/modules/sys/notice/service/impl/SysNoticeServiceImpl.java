/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.sys.notice.service.impl;


import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.qzsoft.demomis.modules.sys.notice.dao.SysNoticeDao;
import com.qzsoft.demomis.modules.sys.notice.dao.SysNoticeFileDao;
import com.qzsoft.demomis.modules.sys.notice.entity.SysNoticeEntity;
import com.qzsoft.demomis.modules.sys.notice.entity.SysNoticeFileEntity;
import com.qzsoft.demomis.modules.sys.notice.service.SysNoticeService;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ConvertDictUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 系统用户
 * 
 * @author
 */
@Service
public class SysNoticeServiceImpl extends BaseService implements SysNoticeService {

	@Autowired
	private SysNoticeDao sysNoticeDao;
	@Autowired
	private SysNoticeFileDao sysNoticeFileDao;
	@Override
	public IPage<SysNoticeEntity> page(Map<String, Object> params) {
		IPage<SysNoticeEntity> page =this.getPage(params,Constant.CREATE_DATE,false,SysNoticeEntity.class) ;
		QueryWrapper<SysNoticeEntity> queryWrapper =this.getQueryWrapper(params,SysNoticeEntity.class,"title");
		IPage<SysNoticeEntity> pageData = sysNoticeDao.selectPage(page, queryWrapper);
		pageData= ConvertDictUtils.formatDicDataPage(pageData);
		return pageData;
	}

	@Override
	public List<SysNoticeEntity> list(Map<String, Object> params) {
		QueryWrapper<SysNoticeEntity> queryWrapper = this.getQueryWrapper(params,SysNoticeEntity.class,"title");;
		List<SysNoticeEntity> entityList = sysNoticeDao.selectList(queryWrapper);
		entityList=ConvertDictUtils.formatDicDataList(entityList);
		return entityList;
	}

	@Override
	public SysNoticeEntity get(String id) {
		SysNoticeEntity entity = sysNoticeDao.selectById(id);
		QueryWrapper<SysNoticeFileEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq("notice_id",id).orderByAsc("orderid","create_date");
		List<SysNoticeFileEntity> fileEntityList=sysNoticeFileDao.selectList(queryWrapper);
		entity=ConvertDictUtils.formatDicData(entity);
		entity.setFileList(fileEntityList);
		return entity;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(SysNoticeEntity entity) {
		entity.setId(IdWorker.get32UUID());
		if (entity.getOrderid()==null){
			entity.setOrderid(1);
		}
		entity.setDeptPkid(this.basegetLoginUser().getDeptPkid());
		sysNoticeDao.insert(entity);
		// 保存附件
		this.saveNoticeFiles(entity);
	}

	/**
	 * 保存附件
	 * @param entity
	 */
	private  void saveNoticeFiles(SysNoticeEntity entity){
		QueryWrapper<SysNoticeFileEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq("notice_id",entity.getId());
		sysNoticeFileDao.delete(queryWrapper);
		List<SysNoticeFileEntity> fileEntityList=entity.getFileList();
		int i=1;
		for (SysNoticeFileEntity fileEntity :  fileEntityList) {
			fileEntity.setId(IdWorker.get32UUID());
			fileEntity.setNoticeId(entity.getId());
			fileEntity.setOrderid(i++);
			fileEntity.setFileType(FileUtil.extName(fileEntity.getFileName()).toLowerCase());
			sysNoticeFileDao.insert(fileEntity);
		}
	}
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(SysNoticeEntity entity) {
		sysNoticeDao.updateById(entity);
		// 保存附件
		this.saveNoticeFiles(entity);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(String[] ids) {
		//删除通知
		sysNoticeDao.deleteBatchIds(Arrays.asList(ids));
		// 删除附件
		QueryWrapper<SysNoticeFileEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.in("notice_id",ids);
		sysNoticeFileDao.delete(queryWrapper);
	}

	@Override
	public void publishStart(String[] ids) {
		UpdateWrapper<SysNoticeEntity> updateWrapper=new UpdateWrapper<>();
		updateWrapper.set("has_publish",1).in("id",ids);
		sysNoticeDao.update(new SysNoticeEntity(),updateWrapper);
	}

	@Override
	public void publishStop(String[] ids) {
		UpdateWrapper<SysNoticeEntity> updateWrapper=new UpdateWrapper<>();
		updateWrapper.set("has_publish",0).in("id",ids);
		sysNoticeDao.update(new SysNoticeEntity(),updateWrapper);
	}

}
