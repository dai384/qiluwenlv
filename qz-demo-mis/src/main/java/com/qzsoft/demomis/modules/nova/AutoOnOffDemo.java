package com.qzsoft.demomis.modules.nova;

import novj.platform.vxkit.common.bean.SourceBean;
import novj.platform.vxkit.common.bean.search.SearchResult;
import novj.platform.vxkit.common.bean.task.ScreenTaskBean;
import novj.platform.vxkit.common.result.DefaultOnResultListener;
import novj.platform.vxkit.common.result.OnResultListenerN;
import novj.platform.vxkit.handy.api.SearchManager;
import novj.publ.api.NovaOpt;
import novj.publ.net.exception.ErrorDetail;
import novj.publ.net.svolley.Request.IRequestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/*
 * 开启电源操作
 * */
@Service
@EnableScheduling
public class AutoOnOffDemo {
    protected static final Logger LOGGER = LoggerFactory.getLogger(LogoutDemo.class);

    //        10.2.5.68 滨州
    //        10.17.3.24 淄博西
    //        10.2.136.19 日照
    //        10.2.227.251 临沂
    //        10.2.185.68 栖霞北
    //        10.2.42.87 88 郓城
    static String remoteIp = "10.2.42.88";
    public static void main (String[] args){
        OnOffDemo nn = new OnOffDemo();
        nn.NovaOperation(remoteIp);
    }

    public void NovaOperation(String remoteIp) {
        NovaOpt novaOpt = new NovaOpt();
        // 1、SDK 初始化: 1=Android 系统环境 2=Windows 或 Linux 系统环境
        novaOpt.GetInstance().initialize(2);
        // 2、搜索设备
        novaOpt.GetInstance().searchScreen(new SearchManager.OnScreenSearchListener(){
            @Override
            public void onSuccess(SearchResult searchResult){
                // 发现设备，自行添加处理逻辑
                novaOpt.GetInstance().connectDevice(searchResult, new DefaultOnResultListener() {
                    @Override
                    public void onSuccess(Integer response) {
                        LOGGER.info("Nova System connect success=================================2==================================：terminal IP is："+remoteIp+"-----------------sn:"+searchResult.sn);
                        // connect success，自行添加处理逻辑
                        // 自动开关电源, 控屏策略：ScreenTaskBean
                        ScreenTaskBean screenPowerCtrlPolicy = new ScreenTaskBean();
                        screenPowerCtrlPolicy.setType("SCREENPOWER");

                        SourceBean sourceBean = new SourceBean();
                        sourceBean.setType(1);
                        sourceBean.setPlatform(4);

                        screenPowerCtrlPolicy.setSource(sourceBean);
                        screenPowerCtrlPolicy.setAction("OPEN");
                        screenPowerCtrlPolicy.setEnable(true);

                        ScreenTaskBean.ScreenCondition screenCondition = new ScreenTaskBean.ScreenCondition();
                        screenCondition.setAction("OPEN");
                        List<String> cron = new ArrayList<>();

                        screenCondition.setCron(cron);
                        screenCondition.setEnable(true);

                        List<ScreenTaskBean.ScreenCondition> list = new ArrayList<ScreenTaskBean.ScreenCondition>();


                        screenPowerCtrlPolicy.setConditions(list);

                        NovaOpt.GetInstance().setScreenPowerPolicy(searchResult.sn, screenPowerCtrlPolicy, new
                                OnResultListenerN<Integer, ErrorDetail>() {
                                    @Override
                                    public void onSuccess(IRequestBase requestBase, Integer response) {
                                    //执行成功，自行添加处理逻辑
                                        LOGGER.info("Nova System 自动开关======================成功====================：搜索terminal IP is："+remoteIp+"; 为："+response.toString());
                                    }
                                    @Override
                                    public void onError(IRequestBase requestBase, ErrorDetail error) {
                                    //执行失败，自行添加处理逻辑
                                        LOGGER.info("Nova System 自动开关======================失败====================：搜索terminal IP is："+remoteIp+"; 错误码为："+error.errorCode+"; 错误描述为："+error.description);
                                    }
                                });
                    }
                    @Override
                    public void onError(ErrorDetail error) {
                        //连接失败，自行添加处理逻辑
                        LOGGER.info("Nova System连接失败===================================================================：terminal IP is："+remoteIp);
                    }
                }, wrapper -> {
                    //连接断开，自行添加处理逻辑
                    LOGGER.info("Nova System连接断开===================================================================：terminal IP is："+remoteIp);
                });
            }
            @Override
            public void onError(ErrorDetail errorDetail){
                // 发生错误，自行添加处理逻辑=
                LOGGER.info("Nova System搜索设备失败===================================================================：搜索terminal IP is："+remoteIp+"; 错误码为："+errorDetail.errorCode+"; 错误描述为："+errorDetail.description);
            }
        }, remoteIp);
    }
}


