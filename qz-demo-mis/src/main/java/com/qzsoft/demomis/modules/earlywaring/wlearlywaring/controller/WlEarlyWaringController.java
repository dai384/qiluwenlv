package com.qzsoft.demomis.modules.earlywaring.wlearlywaring.controller;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.earlywaring.wlearlywaring.service.WlEarlyWaringService;
import com.qzsoft.demomis.modules.earlywaring.wlearlywaring.entity.WlEarlyWaringEntity;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.utils.Result;
import com.qzsoft.jeemis.common.validator.AssertUtils;
import com.qzsoft.jeemis.common.validator.ValidatorUtils;
import com.qzsoft.jeemis.common.validator.group.AddGroup;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 智能箱报警数据推送接收表(WlEarlyWaring)表控制层
 *
 * @author sdmq
 * @since 2020-08-14 09:44:39
 */
@RestController
@RequestMapping("earlywaring/wlearlywaring")
@Api(tags="智能箱报警数据推送接收表")
public class WlEarlyWaringController  {
    @Autowired
    private WlEarlyWaringService wlEarlyWaringService;

	@PostMapping("page")
	@ApiOperation("分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = "link_name", value = "名字", paramType = "query", dataType="String"),
			@ApiImplicitParam(name = "ge_createDate", value = "起始创建时间", paramType = "query", dataType="Date"),
			@ApiImplicitParam(name = "le_createDate", value = "截止创建时间", paramType = "query", dataType="Date")
	})
//	@RequiresPermissions("earlywaring:wlearlywaring:page")
	public Result<IPage<WlEarlyWaringEntity>> page(@ApiIgnore @RequestBody Map<String, Object> params){
		IPage<WlEarlyWaringEntity> page = wlEarlyWaringService.page(params);

		return new Result<IPage<WlEarlyWaringEntity>>().ok(page);
	}

	@GetMapping("list")
	@ApiOperation("列表")
//	@RequiresPermissions("earlywaring:wlearlywaring:list")
	public Result<List<WlEarlyWaringEntity>> list(@ApiIgnore @RequestParam Map<String, Object> params){
		List<WlEarlyWaringEntity> data = wlEarlyWaringService.list(params);

		return new Result<List<WlEarlyWaringEntity>>().ok(data);
	}

	@GetMapping("{id}")
	@ApiOperation("信息")
//	@RequiresPermissions("earlywaring:wlearlywaring:info")
	public Result<WlEarlyWaringEntity> get(@PathVariable("id") String id){
		WlEarlyWaringEntity data = wlEarlyWaringService.get(id);

		return new Result<WlEarlyWaringEntity>().ok(data);
	}

	@PostMapping
	@ApiOperation("保存")
	@LogOperation("保存")
//	@RequiresPermissions("earlywaring:wlearlywaring:save")
	public Result save(@RequestBody WlEarlyWaringEntity dto){
		//效验数据
		ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);
		wlEarlyWaringService.save(dto);
		return new Result();
	}

	@PutMapping
	@ApiOperation("修改")
	@LogOperation("修改")
//	@RequiresPermissions("earlywaring:wlearlywaring:update")
	public Result update(@RequestBody WlEarlyWaringEntity dto){
		//效验数据
		ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

		wlEarlyWaringService.update(dto);

		return new Result();
	}
	
	@DeleteMapping
	@ApiOperation("删除")
	@LogOperation("删除")
//	@RequiresPermissions("earlywaring:wlearlywaring:delete")
	public Result delete(@RequestBody String[] ids){
		//效验数据
		AssertUtils.isArrayEmpty(ids, "id");

		wlEarlyWaringService.delete(ids);

		return new Result();
	}



	@GetMapping("export")
	@ApiOperation("导出")
	@LogOperation("导出")
//	@RequiresPermissions("earlywaring:wlearlywaring:export")
	public void export(@ApiIgnore @RequestParam Map<String, Object> params , HttpServletResponse response) throws Exception {
		wlEarlyWaringService.exportXls(params,response);
	}
    
}