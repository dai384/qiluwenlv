package com.qzsoft.demomis.modules.cms.sansi.request;

import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;


import java.io.IOException;

/**
 * 读取充放电控制器历史记录及时间(太阳能屏专用)请求
 *
 * @author frank
 */
public class GetSolarDateTimeRequest extends Request {

    public GetSolarDateTimeRequest() throws CmsException {
        this.type = int2bytes(23, 2);
        this.address = int2bytes(0, 2);
    }

    public GetSolarDateTimeRequest(int address) throws CmsException {
        this.type = int2bytes(23, 2);
        this.address = int2bytes(address, 2);
    }

    @Override
    protected void setData() throws CmsException, IOException {
    }

    public static void main(String[] args) throws CmsException, IOException {
        GetSolarDateTimeRequest gdtr = new GetSolarDateTimeRequest(0);
        System.out.println(CodecUtil.bytesToHexString(gdtr.encoder()));
    }
}
