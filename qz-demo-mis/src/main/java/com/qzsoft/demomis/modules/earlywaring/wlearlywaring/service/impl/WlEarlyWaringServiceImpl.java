package com.qzsoft.demomis.modules.earlywaring.wlearlywaring.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.session.SqlSession;
import com.qzsoft.demomis.modules.earlywaring.wlearlywaring.dao.WlEarlyWaringDao;
import com.qzsoft.demomis.modules.earlywaring.wlearlywaring.entity.WlEarlyWaringEntity;
import com.qzsoft.demomis.modules.earlywaring.wlearlywaring.service.WlEarlyWaringService;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ExcelUtils;
import com.qzsoft.jeemis.common.utils.ConvertDictUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 智能箱报警数据推送接收表(WlEarlyWaring)表服务实现类
 *
 * @author sdmq
 * @since 2020-08-14 09:44:38
 */
@Service("wlEarlyWaringService")
public class WlEarlyWaringServiceImpl extends BaseService implements WlEarlyWaringService {
    private final SqlSession sqlSession;
    @Autowired
	private WlEarlyWaringDao wlEarlyWaringDao;
   
	public WlEarlyWaringServiceImpl(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}
	
	/**
	 * WlEarlyWaring数据分页
	 * @param params
	 * @return IPage
	 */
	@Override
	public IPage<WlEarlyWaringEntity> page(Map<String, Object> params) {
		IPage<WlEarlyWaringEntity> page =this.getPage(params,Constant.CREATE_DATE,false,WlEarlyWaringEntity.class) ;
		QueryWrapper<WlEarlyWaringEntity> queryWrapper =this.getQueryWrapper(params,WlEarlyWaringEntity.class);
		IPage<WlEarlyWaringEntity> pageData = wlEarlyWaringDao.selectPage(page, queryWrapper);
		pageData= ConvertDictUtils.formatDicDataPage(pageData);
		return pageData;
	}
    /**
	 * WlEarlyWaring数据列表
	 * @param params
	 * @return
	 */
	@Override
	public List<WlEarlyWaringEntity> list(Map<String, Object> params) {
		QueryWrapper<WlEarlyWaringEntity> queryWrapper = this.getQueryWrapper(params,WlEarlyWaringEntity.class);
		List<WlEarlyWaringEntity> entityList = wlEarlyWaringDao.selectList(queryWrapper);
		entityList=ConvertDictUtils.formatDicDataList(entityList);
		return entityList;
	}

	@Override
	public WlEarlyWaringEntity get(String id) {
		WlEarlyWaringEntity entity = wlEarlyWaringDao.selectById(id);
		// 如无需代码翻译请屏蔽这句话 提高速度
		entity=ConvertDictUtils.formatDicData(entity);
		return entity;
	}
    /**
	 * WlEarlyWaring保存
	 * createDate creator
	 * updateDate updater 
	 * 字段会自动注入值 如不需要注入请修改WlEarlyWaringEntity
	 * @param entity
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(WlEarlyWaringEntity entity) {
		wlEarlyWaringDao.insert(entity);
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(WlEarlyWaringEntity entity) {
		wlEarlyWaringDao.updateById(entity);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(String[] ids) {
		List<Integer> idArray=new ArrayList<>(ids.length);
		for(String id : ids){
			idArray.add(Integer.valueOf(id));
		}
		wlEarlyWaringDao.deleteBatchIds(idArray);
	}
	@Override
	public void exportXls(Map<String, Object> params , HttpServletResponse response)  {
		List<WlEarlyWaringEntity> list = this.list(params);
		ExcelUtils.exportExcelToTarget(response, "智能箱报警数据推送接收表导出列表", list, WlEarlyWaringEntity.class);
	}
	
}