package com.qzsoft.demomis.modules.customer.wlcustomer.controller;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.customer.wlcustomer.service.WlCustomerService;
import com.qzsoft.demomis.modules.customer.wlcustomer.entity.WlCustomerEntity;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.utils.Result;
import com.qzsoft.jeemis.common.validator.AssertUtils;
import com.qzsoft.jeemis.common.validator.ValidatorUtils;
import com.qzsoft.jeemis.common.validator.group.AddGroup;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 客户信息表(WlCustomer)表控制层
 *
 * @author sdmq
 * @since 2020-07-14 13:50:08
 */
@RestController
@RequestMapping("customer/wlcustomer")
@Api(tags="客户信息表")
public class WlCustomerController  {
    @Autowired
    private WlCustomerService wlCustomerService;

	@PostMapping("page")
	@ApiOperation("分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = "link_name", value = "名字", paramType = "query", dataType="String"),
			@ApiImplicitParam(name = "ge_createDate", value = "起始创建时间", paramType = "query", dataType="Date"),
			@ApiImplicitParam(name = "le_createDate", value = "截止创建时间", paramType = "query", dataType="Date")
	})
	@RequiresPermissions("customer:wlcustomer:page")
	public Result<IPage<WlCustomerEntity>> page(@ApiIgnore @RequestBody Map<String, Object> params){
		IPage<WlCustomerEntity> page = wlCustomerService.page(params);

		return new Result<IPage<WlCustomerEntity>>().ok(page);
	}

	@GetMapping("list")
	@ApiOperation("列表")
	@RequiresPermissions("customer:wlcustomer:list")
	public Result<List<WlCustomerEntity>> list(@ApiIgnore @RequestParam Map<String, Object> params){
		List<WlCustomerEntity> data = wlCustomerService.list(params);

		return new Result<List<WlCustomerEntity>>().ok(data);
	}

	@GetMapping("{id}")
	@ApiOperation("信息")
	@RequiresPermissions("customer:wlcustomer:info")
	public Result<WlCustomerEntity> get(@PathVariable("id") String id){
		WlCustomerEntity data = wlCustomerService.get(id);

		return new Result<WlCustomerEntity>().ok(data);
	}

	@PostMapping
	@ApiOperation("保存")
	@LogOperation("保存")
	@RequiresPermissions("customer:wlcustomer:save")
	public Result save(@RequestBody WlCustomerEntity dto){
		//效验数据
		ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);
		wlCustomerService.save(dto);
		return new Result();
	}

	@PutMapping
	@ApiOperation("修改")
	@LogOperation("修改")
	@RequiresPermissions("customer:wlcustomer:update")
	public Result update(@RequestBody WlCustomerEntity dto){
		//效验数据
		ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

		wlCustomerService.update(dto);

		return new Result();
	}
	
	@DeleteMapping
	@ApiOperation("删除")
	@LogOperation("删除")
	@RequiresPermissions("customer:wlcustomer:delete")
	public Result delete(@RequestBody String[] ids){
		//效验数据
		AssertUtils.isArrayEmpty(ids, "id");

		wlCustomerService.delete(ids);

		return new Result();
	}



	@GetMapping("export")
	@ApiOperation("导出")
	@LogOperation("导出")
	@RequiresPermissions("customer:wlcustomer:export")
	public void export(@ApiIgnore @RequestParam Map<String, Object> params , HttpServletResponse response) throws Exception {
		wlCustomerService.exportXls(params,response);
	}
    
}