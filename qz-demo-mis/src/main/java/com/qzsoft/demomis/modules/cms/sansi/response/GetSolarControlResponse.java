package com.qzsoft.demomis.modules.cms.sansi.response;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 读取充放电控制器信息 (太阳能屏专用)应答
 *
 * @author frank
 */
public class GetSolarControlResponse extends Response {
    private double batt;// 蓄电池电压
    private double slar;// 极板电压
    private double ldv;// 负载电压
    private double ghg;// 充电电流
    private double ldc;// 负载电流
    private double temp;// 机箱温度
    private byte doorInf;// 门开关信息最低位为0 表示门关,最低位为 1,表示门开
    private byte addrBatt;// 电池检测板地址,目前为01
    private double batt1;// 第 1 节电池电压
    private double batt2;// 第 2 节电池电压
    private double batt3;// 第 3 节电池电压
    private double batt4;// 第 4 节电池电压
    private double batt5;// 第 5 节电池电压
    private double batt6;// 第 6 节电池电压
    private double batt7;// 第 7 节电池电压
    private double batt8;// 第 8 节电池电压
    private double batt9;// 第 9 节电池电压
    private double batt10;// 第 10 节电池电压
    private double batt11;// 第 11 节电池电压
    private double batt12;// 第 12 节电池电压
    private double battTemp;// 电池箱温度
    private double fda;// 风电电流
    private double fdv;// 风电电压

    public GetSolarControlResponse(byte[] bytes) throws IOException,
            CmsException {
        this.bytes = bytes;
        decoder();
        batt = getV1(bytes[3], bytes[4]);
        slar = getV2(bytes[5], bytes[6]);
        ldv = getV2(bytes[7], bytes[8]);
        ghg = getA(bytes[9], bytes[10]);
        ldc = getA(bytes[11], bytes[12]);
        temp = getTemp(bytes[13], bytes[14]);
        doorInf = bytes[15];
        addrBatt = bytes[16];
        batt1 = getV1(bytes[17], bytes[18]);
        batt2 = getV1(bytes[19], bytes[20]);
        batt3 = getV1(bytes[21], bytes[22]);
        batt4 = getV1(bytes[23], bytes[24]);
        batt5 = getV1(bytes[25], bytes[26]);
        batt6 = getV1(bytes[27], bytes[28]);
        batt7 = getV1(bytes[29], bytes[30]);
        batt8 = getV1(bytes[31], bytes[32]);
        batt9 = getV1(bytes[33], bytes[34]);
        batt10 = getV1(bytes[35], bytes[36]);
        batt11 = getV1(bytes[37], bytes[38]);
        batt12 = getV1(bytes[39], bytes[40]);
        battTemp = getTemp(bytes[41], bytes[42]);
        fda = getA(bytes[43], bytes[44]);
        fdv = getV1(bytes[45], bytes[46]);
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        basicDecoder();
    }

    /**
     * 蓄电池电压、风电电压计算方法: 电压 = (高字节低四位*256 + 低字节)*25/1000 (V)
     *
     * @param bh 高位
     * @param bl 低位
     * @return
     */
    private double getV1(byte bh, byte bl) {
        int h = Integer.parseInt(String.valueOf(bh & 0x0F), 16);
        int l = Integer.parseInt(String.valueOf(bl), 16);

        return (double) (h * 256 + l) * 25 / 1000;
    }

    /**
     * 极板电压、负载电压计算方法: 电压 = (高字节低四位*256+低字节)*500*25/1000000 (V)
     *
     * @param bh 高位
     * @param bl 低位
     * @return
     */
    private double getV2(byte bh, byte bl) {
        int h = Integer.parseInt(String.valueOf(bh & 0x0F), 16);
        int l = Integer.parseInt(String.valueOf(bl), 16);

        return (double) (h * 256 + l) * 500 * 25 / 1000000;
    }

    /**
     * 充电电流、负载电流、风电电流计算方法: 电流 = (高字节低四位*256+低字节)/5 (A)
     *
     * @param bh 高位
     * @param bl 低位
     * @return
     */
    private double getA(byte bh, byte bl) {
        int h = Integer.parseInt(String.valueOf(bh & 0x0F), 16);
        int l = Integer.parseInt(String.valueOf(bl), 16);
        return (double) (h * 256 + l) / 5;
    }

    /**
     * 温度的高低字节定义(MSB:高字节,LSB:低字节,MSb:高位,LSb: 低位)
     *
     * @param bh
     * @param bl
     * @return
     */
    private double getTemp(byte bh, byte bl) {
        double temp = 0;
        byte[] b = {0, 0, bh, bl};
        ByteBuffer bb = ByteBuffer.wrap(b);
        int num = bb.getInt();
        boolean f = false;

        if (((num >> 11) & 0x1F) != 0) {
            f = true;
            num = ~num + 1;
        }
        for (int i = 10; i > -1; i--) {
            if (((num >> i) & 1) != 0) {
                temp += Math.pow(2, i - 4);
            }
        }
        if (f) {
            temp = -temp;
        }
        return temp;
    }

    public double getBatt() {
        return batt;
    }

    public double getSlar() {
        return slar;
    }

    public double getLdv() {
        return ldv;
    }

    public double getGhg() {
        return ghg;
    }

    public double getLdc() {
        return ldc;
    }

    public double getTemp() {
        return temp;
    }

    public byte getDoorInf() {
        return doorInf;
    }

    public boolean isDoorOpen() {
        if ((doorInf & 1) != 0) {
            return true;
        } else {
            return false;
        }
    }

    public byte getAddrBatt() {
        return addrBatt;
    }

    public double getBatt1() {
        return batt1;
    }

    public double getBatt2() {
        return batt2;
    }

    public double getBatt3() {
        return batt3;
    }

    public double getBatt4() {
        return batt4;
    }

    public double getBatt5() {
        return batt5;
    }

    public double getBatt6() {
        return batt6;
    }

    public double getBatt7() {
        return batt7;
    }

    public double getBatt8() {
        return batt8;
    }

    public double getBatt9() {
        return batt9;
    }

    public double getBatt10() {
        return batt10;
    }

    public double getBatt11() {
        return batt11;
    }

    public double getBatt12() {
        return batt12;
    }

    public double getBattTemp() {
        return battTemp;
    }

    public double getFda() {
        return fda;
    }

    public double getFdv() {
        return fdv;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
