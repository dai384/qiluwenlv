package com.qzsoft.demomis.modules.terminal.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import org.apache.ibatis.annotations.Mapper;
import com.qzsoft.demomis.modules.terminal.entity.TerminalEntity;

import java.util.List;

/**
 * (WlEquipmentinfo)表数据库访问层
 *
 * @author sdmq
 * @since 2020-05-27 09:07:00
 */
@Mapper
public interface TerminalDao extends BaseMapper<TerminalEntity> {

    /*
    * 关联查询
    * */
    IPage<TerminalEntity>  selectPageOwn( IPage<TerminalEntity> page, QueryWrapper<TerminalEntity> wrapper);

    /*
     * 保存mysql数据入库
     * */
    int saveAll(List<TerminalEntity> list );


    TerminalEntity selectByIdBySelf(String id);
}