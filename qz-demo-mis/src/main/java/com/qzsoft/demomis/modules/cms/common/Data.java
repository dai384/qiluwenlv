package com.qzsoft.demomis.modules.cms.common;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * DataTables使用,数据封装
 * Created by frank on 15/10/29.
 */
public class Data<T> {
    private List<T> data = Lists.newArrayList();

    public Data(List<T> data) {
        this.data = data;
    }

    public static Data create(List data) {
        return new Data(data);
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
