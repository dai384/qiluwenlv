package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * \rxx1yy1xx2yy2 闪烁区域坐标(出字方式为区域闪烁时有用)。xx1、yy1、xx2、yy2 分别表示区域的左、上、右、下坐标值,范围为
 * [-99, 999],缺省的 闪烁区域为整个可变情报板
 * 
 * @author frank
 * 
 */
public class GlitterAreaEsc extends Esc {
	private static Logger logger = LoggerFactory
			.getLogger(GlitterAreaEsc.class);

	@Override
	protected String getCommand() {
		return "\\N";
	}

	private GlitterAreaEsc() {
		value = "";
	}

	private GlitterAreaEsc(int x1, int y1, int x2, int y2) {
		if (x1 < -99 || x1 > 999 || y1 < -99 || y1 > 999 || x2 < -99
				|| x2 > 999) {
			logger.warn("闪烁区域的取值范围为-99~999，现在为 {}，将使用默认值 {}", value, "整个可变情报板");
			value = "";
		} else {
			value = format2(x1) + format2(y1) + format2(x2) + format2(y2);
		}
	}

	@Override
	public String getValue() {
		if (StringUtils.isEmpty(value)) {
			return "";
		} else {
			return getCommand() + value;
		}
	}

	public static GlitterAreaEsc getInstance() {
		return new GlitterAreaEsc();
	}

	public static GlitterAreaEsc getInstance(int x1, int y1, int x2, int y2) {
		return new GlitterAreaEsc(x1, y1, x2, y2);
	}
}
