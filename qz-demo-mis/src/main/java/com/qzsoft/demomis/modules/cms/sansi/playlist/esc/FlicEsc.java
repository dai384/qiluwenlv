package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * \Fnnnmm 把 %SYSDIR%\FLC 目录下的 nnn.flc 文件显示到 \C 所规定的坐标, 并循环播放 mm 遍,mm 的范围为
 * 0-99。若 mm 为 0,则只显示 flic 动画的第一帧画面,而不是播放整个动画
 * 
 * @author frank
 * 
 */
public class FlicEsc extends Esc {
	private static Logger logger = LoggerFactory.getLogger(LocationEsc.class);
	private final int DEFAULT_M = 0;

	@Override
	protected String getCommand() {
		return "\\F";
	}

	private FlicEsc(String name, int m) {
		if (m < 0 || m > 99) {
			logger.warn("播放次数的取值范围为0~99，现在为 {}，将使用默认值 {}", value, DEFAULT_M);
			m = DEFAULT_M;
		}
		value = name + format2(m);
	}

	public static FlicEsc getInstance(String name) {
		return new FlicEsc(name, 0);
	}

	public static FlicEsc getInstance(String name, int m) {
		return new FlicEsc(name, m);
	}
}
