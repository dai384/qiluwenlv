package com.qzsoft.demomis.modules.task.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@ApiModel(value ="待办表")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("wl_tasks")
public class WlTasksEntity extends Model<WlTasksEntity> {
    private static final long serialVersionUID = 766819277934455759L;
    /**
     *主键主键
     */
    @TableId(value ="id",type = IdType.ID_WORKER) //主键ID_WORKER策略
    @NotNull(message="{id.require}", groups = UpdateGroup.class)
    @ApiModelProperty(value = "主键主键")
    private Long id;

    @ApiModelProperty(value = "关联审批id")
    private Long linkId;
    /**
     *名称
     */
    @ApiModelProperty(value = "名称")
    private String name;
    /**
     *类型
     */
    @ApiModelProperty(value = "类型")
    private String type;
    /**
     *内容
     */
    @ApiModelProperty(value = "内容")
    private String content;
    /**
     *内容
     */
    @ApiModelProperty(value = "审批结果")
    private String result;
    /**
     *内容
     */
    @ApiModelProperty(value = "审批内容")
    private String replyContent;
    /**
     *内容
     */
    @ApiModelProperty(value = "备注")
    private String remark;
    /**
     *内容
     */
    @ApiModelProperty(value = "创建人")
    private String creator;
    /**
     *创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
    /**
     *编辑人
     */
    @ApiModelProperty(value = "编辑人")
    private String updator;
    /**
     *编辑时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "编辑时间")
    private Date updateDate;
    /**
     *审批意见
     */
    @ApiModelProperty(value = "审批意见")
    private String adjust;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
