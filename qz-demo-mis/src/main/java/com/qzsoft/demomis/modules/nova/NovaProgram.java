package com.qzsoft.demomis.modules.nova;

import com.qzsoft.demomis.modules.program.dao.WlProgramDao;
import com.qzsoft.demomis.modules.program.entity.WlProgramEntity;
import novj.platform.vxkit.common.bean.*;
import novj.platform.vxkit.common.bean.login.LoginResultBean;
import novj.platform.vxkit.common.bean.programinfo.PageItem;
import novj.platform.vxkit.common.bean.search.SearchResult;
import novj.platform.vxkit.common.bean.task.BaseTaskItemBean;
import novj.platform.vxkit.common.bean.task.ScreenTaskBean;
import novj.platform.vxkit.common.result.DefaultOnResultListener;
import novj.platform.vxkit.common.result.OnResultListenerN;
import novj.platform.vxkit.handy.api.ProgramSendManager;
import novj.platform.vxkit.handy.api.SearchManager;
import novj.publ.api.NovaOpt;
import novj.publ.api.actions.ProgramManager;
import novj.publ.api.beans.TimingParamBean;
import novj.publ.net.exception.ErrorDetail;
import novj.publ.net.svolley.Request.IRequestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
public class NovaProgram {

    @Value("${programPath}")
    private String programPath;
    @Autowired
    private WlProgramDao wl_ProgramDao;

    protected static final Logger LOGGER = LoggerFactory.getLogger(NovaProgram.class);

    // 设备的 IPV4 地址, 设备序列号
    public void interfaceCall(String remoteIp, String operationType, int light, String autoswitch){

        NovaOpt novaOpt = new NovaOpt();

        // 1、初始化SDK：基于 SDK 的二次开发程序运行环境
        novaOpt.GetInstance().initialize(2);

        // 2、搜索设备：通过发送搜索命令发现局域网中的设备
        novaOpt.GetInstance().searchScreen(new SearchManager.OnScreenSearchListener(){
            @Override
            public void onSuccess(SearchResult searchResult){
                // 发现设备，自行添加处理逻辑
                // 3、连接设备：与设备建立业务通信连接---------------连接参数，来自于搜索结果
                /*
                * sn;设备序列号
                * productName;设备所属产品名称
                * width;屏宽,单位：像素
                * height;屏高，单位：像素
                * aliasName;设备名称
                * ipAddress;设备 IPV4 地址
                * ftpPort;
                * tcpPort;
                * syssetFtpPort;
                * key;
                * logined;登录状态，True=已登录，False=未登录
                * encodeType;编码类型
                * loginedUsernames;登录的用户名
                * */
                novaOpt.GetInstance().connectDevice(searchResult, new DefaultOnResultListener() {
                    @Override
                    public void onSuccess(Integer response) {
                        // connect success，自行添加处理逻辑
                        // 4、登录设备
                        String userName = "admin";
                        String passwords = "123456";
                        novaOpt.GetInstance().login(searchResult.sn, userName, passwords,
                                new OnResultListenerN<LoginResultBean, ErrorDetail>() {
                                    @Override
                                    public void onSuccess(IRequestBase requestBase, LoginResultBean response) {
                                        //执行成功，自行添加处理逻辑: 根据操作类型进行以下操作；
                                        System.out.println("登录成功");
                                        // 设置屏体电源控制模式
                                        // 1、开机，2、调亮度、自动开关机， 3、判断有无节目，进行节目发布
                                        // 2、关机操作
                                        switch (operationType) {
                                            case "1":
                                                // 登录
                                                startUp(searchResult, light, novaOpt);
                                                break;
                                            case "2":
                                                // 退出
                                                shutDown(searchResult, novaOpt);
                                                break;
                                            case "3":
                                                // 发布节目
                                                break;
                                            case "4":
                                                // 停止发布节目
                                                break;
                                            default:
                                                System.out.println("未知操作类型");
                                        }
                                    }
                                    @Override
                                    public void onError(IRequestBase requestBase, ErrorDetail error) {
                                        //执行失败，自行添加处理逻辑
                                        System.out.println("login fail !!!");
                                    }
                                });
                    }
                    @Override
                    public void onError(ErrorDetail error) {
                        //连接失败，自行添加处理逻辑
                        LOGGER.info("connectfail");
                    }
                }, wrapper -> {
                    //连接断开，自行添加处理逻辑
                    LOGGER.info("connect duankai");
                });
            }
            @Override
            public void onError(ErrorDetail errorDetail){
                // 发生错误，自行添加处理逻辑
                LOGGER.info("search fail！！！");
            }
        }, remoteIp);
    }


    /*
     * 开机：设置屏体电源开关状态（开启/关闭）
     */
    public void startUp (SearchResult searchResult, int light, NovaOpt novaOpt) {
        novaOpt.GetInstance().setScreenPowerState(searchResult.sn, true, new OnResultListenerN<Integer,
                ErrorDetail>() {
            @Override
            public void onSuccess(IRequestBase requestBase, Integer response) {
            // 执行成功，自行添加处理逻辑
            // 配置自动调节亮度
//             editLight(searchResult, light);
                // 操作完毕，退出
                if (novaOpt.GetInstance().logOut(searchResult.sn)) {
                    //logout success，自行添加处理逻辑
                    LOGGER.info("=============================================================logout success===============================================================");
                } else {
                    //logout fail，自行添加处理逻辑
                    LOGGER.info("=============================================================logout fail===============================================================");
                }
            }
            @Override
            public void onError(IRequestBase requestBase, ErrorDetail error) {
            //执行失败，自行添加处理逻辑
            }
        });
    }

    /*
     * 退出
     */
    public void shutDown (SearchResult searchResult, NovaOpt novaOpt) {
        novaOpt.GetInstance().setScreenPowerState(searchResult.sn, false, new OnResultListenerN<Integer,
                ErrorDetail>() {
            @Override
            public void onSuccess(IRequestBase requestBase, Integer response) {
                //执行成功，自行添加处理逻辑
                if (novaOpt.GetInstance().logOut(searchResult.sn)) {
                    //logout success，自行添加处理逻辑
                    LOGGER.info("=============================================================logout success===============================================================");
                } else {
                    //logout fail，自行添加处理逻辑
                    LOGGER.info("=============================================================logout fail===============================================================");
                }
            }
            @Override
            public void onError(IRequestBase requestBase, ErrorDetail error) {
                //执行失败，自行添加处理逻辑
            }
        });
    }


    /*
     * 创建节目
     */
    public void createProgram (SearchResult searchResult, String resourceType) throws  Exception{
        //  1.  创建节目：返回正数为播放节目 ID
        int programId = NovaOpt.GetInstance().createProgram(searchResult.aliasName, searchResult.width, searchResult.height);
        if (programId == 0){
            //        2.  添加 page：给播放节目添加一个（默认）播放页面,返回正数为播放页面的 ID
            int pageId = NovaOpt.GetInstance().addPage();
            if (pageId == 0){
                //        3.  修改 page 参数：
                /*
                * id: 播放页面 ID
                * name:名称
                * inAnimation:入场动画
                * outAnimation:出场动画
                * border:边框
                * widgetGroups:窗口挂件组
                * widgets:窗口挂件
                *widgetContainers:窗口挂件容器
                * */
                PageItem pageItem = NovaOpt.GetInstance().getPageItem(pageId);
                // TODO: 2020/7/19  设置页面参数，是否需要重设
//                NovaOpt.GetInstance().setPageParam(pageId, pageItem);
                //        4.  删除 page：
                //        如果 page 下创建了 widget，需要执行⑦, 直到删除所有关联 widget(播放页面添加播放窗口（挂件）)。
//                NovaOpt.GetInstance().deletePage(pageId);
                //        5.  添加 widget(给播放页面添加播放窗口（挂件）)：
                // TODO: 2020/7/19 param为播放媒体对象
                // 其中pageId 为需要添加 widget 的 page 的 id。mediaType 为
                // ProgramManager.WidgetMediaType.PICTURE/VIDEO/GIF/ARCH_TEXT。
                // 当 widget 是文本类型时param 为文本内容,widget 为其他类型时 param 为原平台文件路径。
                Object param = new Object();
                int widgetId = 0;
                switch (resourceType) {
                    case "PICTURE":
                        widgetId = NovaOpt.GetInstance().addWidget(pageId, ProgramManager.WidgetMediaType.PICTURE, param);
                        ;break;
                    case "VIDEO":
                        System.out.println("VIDEO类型暂未做对接 ");
                        widgetId = NovaOpt.GetInstance().addWidget(pageId, ProgramManager.WidgetMediaType.VIDEO, param);
                        ;break;
                    case "GIF":
                        System.out.println("GIF类型暂未做对接 ");
                        widgetId = NovaOpt.GetInstance().addWidget(pageId, ProgramManager.WidgetMediaType.GIF, param);
                        ;break;
                    case "ARCH_TEXT":
                        System.out.println("ARCH_TEXT类型暂未做对接 ");
                        widgetId = NovaOpt.GetInstance().addWidget(pageId, ProgramManager.WidgetMediaType.ARCH_TEXT, param);
                        ;break;
                    default: System.out.println("resourceType类型不明确，resourceType =" + resourceType);
                }


                //        6.  修改 Widget 参数：
//                novj.platform.vxkit.common.bean.programinfo.Widget widget = NovaOpt.GetInstance().getWidgetParam(pageId, widgetId);
//                int mediaId = 1;
                // pageID:播放页面 ID ; mediaId:   widgetID:播放窗口（挂件）ID
//                NovaOpt.GetInstance().setWidgetParam(pageId, mediaId, widget);
                //        7.  删除 widget：
//                NovaOpt.GetInstance().deleteWidget(pageId,widgetId);
                //        8.  生成并保存节目：
                SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = formatter.format(new Date());
                String programDir = programPath + date;

                int resulet = NovaOpt.GetInstance().makeProgram(programDir);

            } else {
                System.out.println("创建节目时，创建page失败：返回码为----"+pageId);
            }
        }else {
            System.out.println("创建节目失败：返回码为----"+programId);
        }
    }


    /*
    * 发布普通节目
    * */
    public void initProgarm (SearchResult searchResult, String programPath) throws Exception {
        //        设备序列号、播放方案保存目录 、播放方案名称、终端设备标识符 、发布后是否播放、发布过程监听器
        // TODO: 2020/7/19  播放方案名称, 终端设备标识符是啥
        NovaOpt.GetInstance().startTransfer(searchResult.sn, programPath, searchResult.sn, searchResult.sn, true, new
                ProgramSendManager.OnProgramTransferListener() {
                    @Override
                    public void onStarted() {
                        //开始发布
                    }

                    @Override
                    public void onTransfer(long l, long l1) {
                        //发布进度
                    }

                    @Override
                    public void onError(ErrorDetail errorDetail) {
                        //发布失败
                    }

                    @Override
                    public void onCompleted() {
                        // 发布完成
                    }

                    @Override
                    public void onAborted() {
                        //发布终止
                    }
                });
    }


    /*
    *  发布定时节目
    * */
    public void initTimerProgarm (SearchResult searchResult, String programPath) {
        // 初始化定时播放节目需要的定时参数。
        NovaOpt.GetInstance().initTimingProgram();

        // 创建一个默认的定时参数对象。
        TimingParamBean bean = NovaOpt.GetInstance().createDefaultTimingParam();
        bean.setStartDate("2020-07-22");
        bean.setStartTime("18:15:00");
        bean.setEndDate("2020-07-22");
        bean.setEndTime("18:20:00");

        // 添加一个（条）定时播放参数（控制）。
        int result = NovaOpt.GetInstance().addTimingPlayParam(bean);

        // 生成定时播放节目文件（节目发布的时候会把这些文件发送给终端设备）。
        int resultProgram = NovaOpt.GetInstance().makeTimingProgram(searchResult.sn, programPath);
    }

    /*
     * 停止发布节目
     */
    public void stopProgram (SearchResult searchResult) {
        // identifier: 播放方案标识符
        NovaOpt.GetInstance().stopPlay(searchResult.sn, searchResult.sn, new OnResultListenerN<Integer, ErrorDetail>() {
            @Override
            public void onSuccess(IRequestBase requestBase, Integer response) {
                //执行成功，自行添加处理逻辑
            }
            @Override
            public void onError(IRequestBase requestBase, ErrorDetail error) {
                //执行失败，自行添加处理逻辑
            }
        });
    }


    /*
     * 调亮度
     */
    public void lightProgram (SearchResult searchResult, float brightnessPercent) {
        NovaOpt.GetInstance().setScreenBrightness(searchResult.sn, brightnessPercent,
                new OnResultListenerN<Integer, ErrorDetail>() {
                    @Override
                    public void onSuccess(IRequestBase requestBase,Integer response) {
                    // 执行成功，自行添加处理逻辑
                    }
                    @Override
                    public void onError(IRequestBase requestBase,ErrorDetail error) {
                    //执行失败，自行添加处理逻辑
                    }
                });
    }


    /*
    * 定时重启
    * */
    public void reStart (SearchResult searchResult) {
        BaseTaskItemBean.ConditionsBean conditionsBean = new BaseTaskItemBean.ConditionsBean();

        conditionsBean.setEnable(true);
        List<String> crons = new ArrayList<>();
        // TODO: 2020/7/19 定时表达式待检验
        crons.add("0 30 8 * * ?");
        crons.add("0 0 22 * * ?");
        conditionsBean.setCron(crons);

        List<BaseTaskItemBean.ConditionsBean> rebootConditions = new ArrayList<>();
        rebootConditions.add(conditionsBean);
        NovaOpt.GetInstance().reboot(searchResult.sn, "定时重启", rebootConditions, false,new
                OnResultListenerN<Integer, ErrorDetail>() {
                    @Override
                    public void onSuccess(IRequestBase requestBase, Integer response) {
                    //执行成功，自行添加处理逻辑
                    }
                    @Override
                    public void onError(IRequestBase requestBase, ErrorDetail error) {
                    //执行失败，自行添加处理逻辑
                    }
                });
    }


    /*
    * 设置亮度
    * */
    public void editLight (SearchResult searchResult, int light) {
        float brightnessPercent = light;
        NovaOpt.GetInstance().setScreenBrightness(searchResult.sn, brightnessPercent,
                new OnResultListenerN<Integer, ErrorDetail>() {
                    @Override
                    public void onSuccess(IRequestBase requestBase,Integer response) {
                    //执行成功，自行添加处理逻辑
                    }
                    @Override
                    public void onError(IRequestBase requestBase,ErrorDetail error) {
                    //执行失败，自行添加处理逻辑
                    }
                });
    }



    // 系统配置初始化
    public void initNova(SearchResult searchResult) {
        // 设置屏体电源控制模式:  mode: MANUALLY=手动模式;AUTO =自动模式
        NovaOpt.GetInstance().setScreenPowerMode(searchResult.sn, "AUTO", new OnResultListenerN<Integer,
                ErrorDetail>() {
            @Override
            public void onSuccess(IRequestBase requestBase, Integer response) {
            //执行成功，自行添加处理逻辑
            }

            @Override
            public void onError(IRequestBase requestBase, ErrorDetail error) {

            }
        });

        // 设置屏体电源按照 CRON 表达式等条件进行自动控制的策略。
        ScreenTaskBean screenPowerCtrlPolicy = new ScreenTaskBean();
        screenPowerCtrlPolicy.setType("SCREENPOWER");
        // nova平台，电脑
        SourceBean sourceBean = new SourceBean(1,2);
        screenPowerCtrlPolicy.setSource(sourceBean);
        screenPowerCtrlPolicy.setAction("OPEN");
        screenPowerCtrlPolicy.setEnable(true);
        List<ScreenTaskBean.ScreenCondition> conditions = new ArrayList<>();
        ScreenTaskBean.ScreenCondition screenCondition1 = new ScreenTaskBean.ScreenCondition();
        screenCondition1.setAction("OPEN");
        List<String> cron1 = new ArrayList<>();
        cron1.add("0 30 7 ? * * *");
        screenCondition1.setCron(cron1);
        screenCondition1.setEnable(true);
        ScreenTaskBean.ScreenCondition screenCondition2 = new ScreenTaskBean.ScreenCondition();
        screenCondition2.setAction("CLOSE");
        List<String> cron2 = new ArrayList<>();
        cron2.add("0 30 22 ? * * *");
        screenCondition2.setCron(cron1);
        screenCondition2.setEnable(true);
        conditions.add(screenCondition1);
        conditions.add(screenCondition2);
        screenPowerCtrlPolicy.setConditions(conditions);
        screenCondition1.setAction("CLOSE");
        NovaOpt.GetInstance().setScreenPowerPolicy(searchResult.sn, screenPowerCtrlPolicy, new
                OnResultListenerN<Integer, ErrorDetail>() {
                    @Override
                    public void onSuccess(IRequestBase requestBase, Integer response) {
                    //执行成功，自行添加处理逻辑
                    }
                    @Override
                    public void onError(IRequestBase requestBase, ErrorDetail error) {
                    //执行失败，自行添加处理逻辑
                    }
                });

        // 设置屏体亮度调节策略
        BrightnessAdjustPolicy bright = new BrightnessAdjustPolicy();
        IBrightnessArgument a = new IBrightnessArgument(){
            @Override
            public int getType() {
                return 0;
            }
        };
        bright.setArgument(a);
        bright.setColorTempValue(1);
        bright.setEnableColorTemp(true);
        bright.setEnableGamma(true);
        bright.setGammaValue(1f);
        List<BrightnessAdjustPolicy> policyList = new ArrayList<>();
        BrightnessAdjustPolicyEntity brightnessAdjustPolicy = new BrightnessAdjustPolicyEntity(true, policyList);
        NovaOpt.GetInstance().setBrightnessPolicy(searchResult.sn, brightnessAdjustPolicy,
                new OnResultListenerN<Integer, ErrorDetail>() {
                    @Override
                    public void onSuccess(IRequestBase requestBase,Integer response) {
                    //执行成功，自行添加处理逻辑
                    }
                    @Override
                    public void onError(IRequestBase requestBase,ErrorDetail error) {
                    //执行失败，自行添加处理逻辑
                    }
                });


    }


}
