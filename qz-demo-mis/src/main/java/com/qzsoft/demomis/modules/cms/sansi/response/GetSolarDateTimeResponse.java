package com.qzsoft.demomis.modules.cms.sansi.response;

import com.google.common.collect.Lists;


import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 读取当前扫描板的日期和时间应答
 *
 * @author frank
 */
public class GetSolarDateTimeResponse extends Response {
    private Date date;
    private int week;
    private String formatDate;
    private String formatWeek;
    private List<History> histories = Lists.newArrayList();

    public GetSolarDateTimeResponse(byte[] bytes) throws IOException,
            CmsException {
        this.bytes = bytes;
        decoder();
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        basicDecoder();
        byte[] tmp = unesc(getArray(3, -3, CmsException.CONTENT_EXCEPTION));
        String strDate = "20"
                + CodecUtil.bytesToHexString(tmp[0], tmp[1], tmp[2], tmp[3],
                tmp[4], tmp[5]);
        week = Integer.parseInt(CodecUtil.bytesToHexString(tmp[6]));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            throw new CmsException("解析日期出错，日期内容：" + strDate);
        }
        Calendar calendar = Calendar.getInstance();
        for (int i = 0; i < 30; i++) {
            int getPow = tmp[i * 17 + 6] * 100 + tmp[i * 17 + 7] * 10
                    + tmp[i * 17 + 8];
            int usePow = tmp[i * 17 + 9] * 100 + tmp[i * 17 + 10] * 10
                    + tmp[i * 17 + 11];
            double maxV = tmp[i * 17 + 12] * 10 + tmp[i * 17 + 13]
                    + tmp[i * 17 + 14] * 0.1;
            double minV = tmp[i * 17 + 15] * 10 + tmp[i * 17 + 16]
                    + tmp[i * 17 + 17] * 0.1;
            double vPre = tmp[i * 17 + 18] * 10 + tmp[i * 17 + 19]
                    + tmp[i * 17 + 20] * 0.1;
            int temp = tmp[i * 17 + 21] * 100 + tmp[i * 17 + 22] * 10
                    + tmp[i * 17 + 23];
            if (tmp[i * 17 + 21] > 10) {
                temp = -(tmp[i * 17 + 22] * 10 + tmp[i * 17 + 23]);
            }
            calendar.add(Calendar.DAY_OF_YEAR, -1);
            histories.add(new History(calendar.getTime(), getPow, usePow, maxV,
                    minV, vPre, temp));
        }
    }

    /**
     * 已转义数据的反转<br/>
     * 如果返回的日期时间数据中含有 0xf2,0xf3,则转为 0x02,0x03 处理
     *
     * @param bytes
     * @return
     */
    protected byte[] unesc2(byte[] bytes) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        for (byte b : bytes) {
            switch (b) {
                case (byte) 0xF2:
                    out.write(0x02);
                    break;
                case (byte) 0xF3:
                    out.write(0x03);
                    break;
                default:
                    out.write(b);
                    break;
            }
        }
        return out.toByteArray();
    }

    public Date getDate() {
        return date;
    }

    public int getWeek() {
        return week;
    }

    public String getFormatDate() {
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.format(date);
        }
        return formatDate;
    }

    public String getFormatWeek() {
        if (week > 0) {
            switch (week) {
                case 1:
                    return "周一";
                case 2:
                    return "周二";
                case 3:
                    return "周三";
                case 4:
                    return "周四";
                case 5:
                    return "周五";
                case 6:
                    return "周六";
                case 7:
                    return "周日";
            }
        }
        return formatWeek;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public static void main(String[] args) {
        String str = "02 30 30 11 08 17 20 00 16 1B E8 FD F0 03";
        String[] arr = str.split(" ");
        byte[] bytes = new byte[arr.length];
        for (int i = 0; i < arr.length; i++) {
            bytes[i] = (byte) Integer.parseInt(arr[i], 16);
        }
        System.out.println(CodecUtil.bytesToHexString(bytes));
        try {
            GetSolarDateTimeResponse cdr = new GetSolarDateTimeResponse(bytes);
            System.out.println(cdr.date);
            System.out.println(cdr.week);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CmsException e) {
            e.printStackTrace();
        }
    }

    public class History {
        private Date date;// 日期
        private int getPow;// 收集能量数
        private int usePow;// 消耗能量数
        private double maxV;// 自0点以来最大电压
        private double minV;// 自0点以来最小电压
        private double vPre;// 下午5时的蓄电池电压/24V*100%数值
        private int temp;// 机箱温度

        public History(Date date, int getPow, int usePow, double maxV,
                       double minV, double vPre, int temp) {
            this.date = date;
            this.getPow = getPow;
            this.usePow = usePow;
            this.maxV = maxV;
            this.minV = minV;
            this.vPre = vPre;
            this.temp = temp;
        }

        public Date getDate() {
            return date;
        }

        public int getGetPow() {
            return getPow;
        }

        public int getUsePow() {
            return usePow;
        }

        public double getMaxV() {
            return maxV;
        }

        public double getMinV() {
            return minV;
        }

        public double getvPre() {
            return vPre;
        }

        public int getTemp() {
            return temp;
        }

    }
}
