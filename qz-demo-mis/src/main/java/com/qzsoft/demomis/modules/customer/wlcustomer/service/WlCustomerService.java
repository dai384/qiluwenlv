package com.qzsoft.demomis.modules.customer.wlcustomer.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.customer.wlcustomer.entity.WlCustomerEntity;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;
/**
 * 客户信息表(WlCustomer)表服务接口
 *
 * @author sdmq
 * @since 2020-07-14 13:50:09
 */
public interface WlCustomerService {
    /**
	 * 数据分页
	 * @param params
	 * @return IPage
	 */
	IPage<WlCustomerEntity> page(Map<String, Object> params);
    
	/**
	 * 数据列表
	 * @param params
	 * @return
	 */
	List<WlCustomerEntity> list(Map<String, Object> params);

	/**
	 * 单个数据
	 * @param id
	 * @return
	 */
	WlCustomerEntity get(String id);

	/**
	 * 保存
	 * @param entity
	 */
	void save(WlCustomerEntity entity);

	/**
	 * 更新
	 * @param entity
	 */
	void update(WlCustomerEntity entity);

	/**
	 * 批量删除
	 * @param ids
	 */
	void delete(String[] ids);

	/**
	 * 导出Excel
	 * @param params
	 * @param response
	 */
	void exportXls(Map<String, Object> params , HttpServletResponse response);

}