package com.qzsoft.demomis.modules.cms;

public class CmsException extends Exception {
    private static final long serialVersionUID = -1372522319562987609L;

    public final static String CHECK_DATA_EXCEPTION = "解析异常，返回数据校验失败";
    public final static String CONTENT_EXCEPTION = "帧内容";
    public final static String NUM_EXCEPTION = "序号";
    public final static String DOWNLOAD_CONTENT_EXCEPTION = "下载文件内容";
    public final static String UPLOAD_ERROR_EXCEPTION = "上传文件应答";
    public final static String ADDRESS_EXCEPTION = "目标地址";
    public final static String SRC_ADDRESS_EXCEPTION = "源地址";
    public final static String TYPE_EXCEPTION = "源地址";
    public final static String CHECK_EXCEPTION = "帧校验";
    public final static String DATA_EXCEPTION = "帧数据";
    public final static String STOP_TIME_EXCEPTION = "停留时间";
    public final static String DISPLAY_WAY_EXCEPTION = "出字方式";
    public final static String DISPLAY_SPEEK_EXCEPTION = "出字速度";
    public final static String LIGHT_SET_WAY_EXCEPTION = "亮度调节方式";
    public final static String DISPLAY_LIGHT_EXCEPTION = "显示亮度";
    public final static String ERROR_PIXELS_NUM_EXCEPTION = "故障个数";
    public static final String MAC_EXCEPTION = "MAC";

    public CmsException() {
        super();
    }

    public CmsException(String message) {
        super(message);
    }

    public CmsException(String message, int len) {
        super("解析" + message + "出错，数据长度小于 " + len);
    }

    public CmsException(Throwable cause) {
        super(cause);
    }

    public CmsException(String message, Throwable cause) {
        super(message, cause);
    }
}
