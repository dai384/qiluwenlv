package com.qzsoft.demomis.modules.permission.dept.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 部门管理(SysDept)Excel导出
 * 利用easypoi字典翻译type
 *
 * @author sdmq
 * @since 2019-08-20 16:36:15
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class SysDeptExcel2 {
    @Excel(name = "主键",width = 20)
    private String pkid;
    @Excel(name = "id")
    private String id;
    @Excel(name = "上级ID")
    private String pid;
    @Excel(name = "机构名称",width = 30)
    private String name;
	@Excel(name = "类型",dict = "Z3")
    private String type;
    @Excel(name = "简拼")
    private String spell;
}