package com.qzsoft.demomis.modules.programhistory.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;


/**
 * 节目时长(WlProgram)表实体类
 *
 * @author sdmq
 * @since 2020-06-30 10:16:58
 */
@ApiModel(value ="节目表")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("wl_program_history")
public class WlProgramHistoryEntity extends Model<WlProgramHistoryEntity> {
    private static final long serialVersionUID = -80698143780173771L;
    /**
     * 主键
     */
    @TableId(type = IdType.UUID) //主键UUID策略
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     *节目ID
     */
    @Excel(name = "节目ID")
    @ApiModelProperty(value = "节目ID")
    private String programId;

    /**
     *节目名称
     */
    @Excel(name = "节目名称")
    @ApiModelProperty(value = "节目名称")
    private String programName;

   /**
    *终端id
    */
    @Excel(name = "终端id")
    @ApiModelProperty(value = "终端id")
    private String equipmentId;
    /**
     *客户id
     */
    @Excel(name = "客户id")
    @ApiModelProperty(value = "客户id")
    private String customerId;

    /**
     *客户id
     */
    @Excel(name = "终端")
    @ApiModelProperty(value = "终端")
    private String equipmentName;

   /**
    *开播时间
    */
    @Excel(name = "开播时间" , format = "yyyy-MM-dd")
    @ApiModelProperty(value = "开播时间")
    private Date onDatetime;
   /**
    *关播时间
    */
    @Excel(name = "关播时间" , format = "yyyy-MM-dd")
    @ApiModelProperty(value = "关播时间")
    private Date offDatetime;

    /**
     *时长
     */
    @Excel(name = "时长" , format = "yyyy-MM-dd")
    @ApiModelProperty(value = "时长")
    private Integer timeCount;

   /**
    *创建人
    */
    @Excel(name = "创建人")
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private String creator;
   /**
    *创建时间
    */
    @Excel(name = "创建时间" , format = "yyyy-MM-dd")
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
   /**
    *编辑人
    */
    @Excel(name = "编辑人")
    @ApiModelProperty(value = "编辑人")
    private String updator;
   /**
    *编辑时间
    */
    @Excel(name = "编辑时间" , format = "yyyy-MM-dd")
   @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "编辑时间")
    private Date updateDate;

}