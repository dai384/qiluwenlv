package com.qzsoft.demomis.modules.cms.sansi.response;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 向可变信息标志上载文件应答
 *
 * @author frank
 */
public class UploadFileResponse extends Response {
    private int code;
    private String msg;

    public UploadFileResponse(byte[] bytes) throws IOException,
            CmsException {
        this.bytes = bytes;
        decoder();
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        basicDecoder();
        code = getInt(3, 1, CmsException.CONTENT_EXCEPTION);
        msg = getString(4, -3, CmsException.UPLOAD_ERROR_EXCEPTION);
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
