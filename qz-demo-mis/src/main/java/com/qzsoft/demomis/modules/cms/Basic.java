package com.qzsoft.demomis.modules.cms;



import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

public abstract class Basic {
    protected final byte header = 0x02;
    protected byte[] address;
    protected byte[] srcAddress;
    protected byte[] data;// 未转义的数据
    protected byte[] check;
    protected final byte footer = 0x03;

    protected byte[] bytes;// 包含头帧与尾帧的数据

    /**
     * 复制并返回新数组
     *
     * @param start   开始复制位置
     * @param len     要复制的长度,为负数时表示离结尾的距离
     * @param message 异常时的提示内容
     * @return 复制后的新数组
     * @throws CmsException
     */
    protected byte[] getArray(int start, int len, String message)
            throws CmsException {
        if (len < 0) {
            len = bytes.length - start + len;
        }
        if (bytes.length < start + len) {
            throw new CmsException(message, start + len);
        }
        byte[] array = new byte[len];
        System.arraycopy(bytes, start, array, 0, len);
        return array;
    }

    /**
     * 获取数组中指定位置的内容并转成字符串
     *
     * @param start   开始位置
     * @param len     长度,为负数时表示离结尾的距离
     * @param message 异常时的提示内容
     * @return 字符串
     * @throws CmsException
     * @throws java.io.UnsupportedEncodingException
     */
    protected String getString(int start, int len, String message)
            throws CmsException, UnsupportedEncodingException {
        return new String(unesc(getArray(start, len, message)), "gbk");
    }

    /**
     * 获取数组中指定位置的内容并转成int
     *
     * @param start   开始位置
     * @param len     长度,为负数时表示离结尾的距离
     * @param message 异常时的提示内容
     * @return int
     * @throws CmsException
     * @throws java.io.UnsupportedEncodingException
     * @throws NumberFormatException
     */
    protected int getInt(int start, int len, String message)
            throws CmsException, NumberFormatException,
            UnsupportedEncodingException {
        return Integer.parseInt(getString(start, len, message));
    }

    /**
     * 获取数组中指定位置的内容并转成long
     *
     * @param start   开始位置
     * @param len     长度,为负数时表示离结尾的距离
     * @param message 异常时的提示内容
     * @return long
     * @throws CmsException
     */
    protected long getLong(int start, int len, String message)
            throws CmsException {
        String str = new String(getArray(start, len, message));
        return Long.parseLong(str);
    }

    /**
     * 把int型转换成指定长度的ASCLL
     *
     * @param num 要转换的数据
     * @param len 指定长度
     * @return
     * @throws CmsException
     */
    protected byte[] int2bytes(int num, int len) throws CmsException {
        if (num > Math.pow(10, len)) {
            throw new CmsException("错误，范围为0~"
                    + (int) (Math.pow(10, len) - 1) + "，现为：" + num);
        }
        String tmp = String.format("%1$0" + len + "d", num);
        return tmp.getBytes();
    }

    /**
     * 把数值转成16进制后再转成一字节的byte数组，充放电控制器使用
     *
     * @param num
     * @return
     */
    protected byte[] setCharging(long num) {
        String tmp = Long.toHexString(num);
        if (tmp.length() == 1) {
            tmp = "0" + tmp;
        }
        return CodecUtil.hexStringToBytes(tmp);
    }

    /**
     * 把数值转成16进制后再转成两字节的byte数组，充放电控制器使用
     *
     * @param num
     * @return
     */
    protected byte[] setCharging2(long num) {
        String tmp = Long.toHexString(num);
        int len = tmp.length();
        for (int i = 0; i < (4 - len); i++) {
            tmp = "0" + tmp;
        }
        return CodecUtil.hexStringToBytes(tmp);
    }

    /**
     * 把byte数据转成数值，充放电控制器使用
     *
     * @param bytes
     * @return
     */
    protected long getCharging(byte... bytes) {
        String tmp = CodecUtil.bytesToHexString(bytes);
        return Long.valueOf(tmp, 16);
    }

    /**
     * 数据转义<br/>
     * 帧数据或帧校验中如有某个字节等于帧头、帧尾 或 0x1B,则在发送此帧时需转换为两个字节,<br/>
     * 即 0x1B 和 (此字节减去 0x1B): 0x02 转换为 0x1B,<br/>
     * 0xE7 0x03 转换为 0x1B,<br/>
     * 0xE8 0x1B 转换为 0x1B, 0x00<br/>
     * 相应地,接收方在接收帧数据或帧校验时,如遇到 0x1B,则把它与随后的字节相加, 转换为一个字节。
     * 以上措施是为了保证帧数据或帧校验中不出现帧头、帧尾,以免影响接收方的同步。
     *
     * @param bytes
     * @return
     */
    protected byte[] esc(byte[] bytes) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        for (byte b : bytes) {
            switch (b) {
                case 0x02:
                    out.write(0x1B);
                    out.write(0xE7);
                    break;
                case 0x03:
                    out.write(0x1B);
                    out.write(0xE8);
                    break;
                case 0x1B:
                    out.write(0x1B);
                    out.write(0x00);
                    break;

                default:
                    out.write(b);
                    break;
            }
        }
        return out.toByteArray();
    }

    /**
     * 已转义数据的反转<br/>
     * 帧数据或帧校验中如有某个字节等于帧头、帧尾 或 0x1B,则在发送此帧时需转换为两个字节,<br/>
     * 即 0x1B 和 (此字节减去 0x1B): 0x02 转换为 0x1B,<br/>
     * 0xE7 0x03 转换为 0x1B,<br/>
     * 0xE8 0x1B 转换为 0x1B, 0x00<br/>
     * 相应地,接收方在接收帧数据或帧校验时,如遇到 0x1B,则把它与随后的字节相加, 转换为一个字节。
     * 以上措施是为了保证帧数据或帧校验中不出现帧头、帧尾,以免影响接收方的同步。
     *
     * @param bytes
     * @return
     */
    protected byte[] unesc(byte[] bytes) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        boolean esc = false;// 出现转义标识时设置为true，下一个字节进行转义
        for (byte b : bytes) {
            switch (b) {
                case 0x1B:
                    esc = true;
                    break;
                case (byte) 0xE7:
                    if (esc) {
                        esc = false;
                        out.write(0x02);
                    } else {
                        out.write(b);
                    }
                    break;
                case (byte) 0xE8:
                    if (esc) {
                        esc = false;
                        out.write(0x03);
                    } else {
                        out.write(b);
                    }
                    break;
                case (byte) 0x00:
                    if (esc) {
                        esc = false;
                        out.write(0x1B);
                    }
                default:
                    out.write(b);
                    break;
            }
        }
        return out.toByteArray();
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getAddress() {
        return new String(address);
    }

    public String getSrcAddress() {
        return new String(srcAddress);
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
