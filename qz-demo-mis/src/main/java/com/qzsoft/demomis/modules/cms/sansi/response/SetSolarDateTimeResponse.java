package com.qzsoft.demomis.modules.cms.sansi.response;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 设置充放电控制器的日期和时间(太阳能屏专用)应答
 *
 * @author frank
 */
public class SetSolarDateTimeResponse extends Response {
    private int code;

    public SetSolarDateTimeResponse(byte[] bytes) throws IOException,
            CmsException {
        this.bytes = bytes;
        decoder();
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        basicDecoder();
        code = getInt(3, 1, CmsException.CONTENT_EXCEPTION);
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
