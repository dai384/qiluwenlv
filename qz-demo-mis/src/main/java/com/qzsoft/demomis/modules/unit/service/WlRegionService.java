package com.qzsoft.demomis.modules.unit.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.unit.entity.WlRegionEntity;
import com.qzsoft.jeemis.common.utils.Result;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;
/**
 * (WlRegion)表服务接口
 *
 * @author sdmq
 * @since 2020-05-28 09:20:47
 */
public interface WlRegionService {

	/**
	 * 数据列表-sheng
	 * @param params
	 * @return
	 */
	List<WlRegionEntity> list(Map<String, Object> params);

}