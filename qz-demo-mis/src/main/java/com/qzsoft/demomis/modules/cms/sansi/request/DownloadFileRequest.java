package com.qzsoft.demomis.modules.cms.sansi.request;

import com.qzsoft.demomis.modules.cms.CmsException;


import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * 从可变信息标志下载文件请求
 *
 * @author frank
 */
public class DownloadFileRequest extends Request {
    private String name;
    private long offset;

    public DownloadFileRequest() throws CmsException, IOException {
        this.type = int2bytes(9, 2);
        this.address = int2bytes(0, 2);
        offset = 0;
    }

    public DownloadFileRequest(int address) throws CmsException,
            IOException {
        this.type = int2bytes(9, 2);
        this.address = int2bytes(address, 2);
        offset = 0;
    }

    @Override
    protected void setData() throws CmsException, IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(name.getBytes());
        out.write(CodecUtil.getOffset(offset * 2048));
        data = out.toByteArray();
        out.close();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public void addOffset() {
        offset++;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
