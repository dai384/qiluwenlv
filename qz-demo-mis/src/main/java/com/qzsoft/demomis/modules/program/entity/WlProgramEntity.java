package com.qzsoft.demomis.modules.program.entity;

import java.util.Date;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * 节目表(WlProgram)表实体类
 *
 * @author sdmq
 * @since 2020-06-30 10:16:58
 */
@ApiModel(value ="节目表")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("wl_program")
public class WlProgramEntity extends Model<WlProgramEntity> {
    private static final long serialVersionUID = -80698143780173771L;
   /**
    *id
    */
    @Excel(name = "id")
    @ApiModelProperty(value = "id")
    private String id;
    /**
     *节目名称
     */
    @Excel(name = "节目名称")
    @ApiModelProperty(value = "节目名称")
    private String name;
    /**
     *节目状态
     */
    @Excel(name = "节目状态")
    @ApiModelProperty(value = "节目状态")
    private String status;
   /**
    *模板id
    */
    @Excel(name = "模板id")
    @ApiModelProperty(value = "模板id")
    private String type;
   /**
    *开播时间
    */
    @Excel(name = "开播时间" , format = "yyyy-MM-dd")
    @ApiModelProperty(value = "开播时间")
    private Date onDatetime;
   /**
    *关播时间
    */
    @Excel(name = "关播时间" , format = "yyyy-MM-dd")
    @ApiModelProperty(value = "关播时间")
    private Date offDatetime;
   /**
    *1:循环，0：不循环
    */
    @Excel(name = "1:循环，0：不循环")
    @ApiModelProperty(value = "1:循环，0：不循环")
    private String loopValue;
   /**
    *轮播图循环时间（单位：s）
    */
    @Excel(name = "轮播图循环时间（单位：s）")
    @ApiModelProperty(value = "轮播图循环时间（单位：s）")
    private Integer picInterval;
   /**
    *跑马灯循环时间（单位：s）
    */
    @Excel(name = "跑马灯循环时间（单位：s）")
    @ApiModelProperty(value = "跑马灯循环时间（单位：s）")
    private Integer textInterval;
   /**
    *字体大小
    */
    @Excel(name = "字体大小")
    @ApiModelProperty(value = "字体大小")
    private Integer fontWeight;
   /**
    *字体颜色
    */
    @Excel(name = "字体颜色")
    @ApiModelProperty(value = "字体颜色")
    private String fontColor;
   /**
    *是否为关注（1为关注，0为不关注）
    */
    @Excel(name = "是否为关注（1为关注，0为不关注）")
    @ApiModelProperty(value = "是否为关注（1为关注，0为不关注）")
    private String marker;
    /**
     *客户id
     */
    @Excel(name = "客户id")
    @ApiModelProperty(value = "客户id")
    private String customerId;
    /**
     *节目保存路径
     */
    @Excel(name = "节目保存路径")
    @ApiModelProperty(value = "节目保存路径")
    private String path;
   /**
    *备注
    */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remark;
   /**
    *创建人
    */
    @Excel(name = "创建人")
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private String creator;
   /**
    *创建时间
    */
    @Excel(name = "创建时间" , format = "yyyy-MM-dd")
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
   /**
    *编辑人
    */
    @Excel(name = "编辑人")
    @ApiModelProperty(value = "编辑人")
    private String updator;
   /**
    *编辑时间
    */
    @Excel(name = "编辑时间" , format = "yyyy-MM-dd")
   @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "编辑时间")
    private Date updateDate;

}