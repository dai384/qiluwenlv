package com.qzsoft.demomis.modules.cms.sansi.request;




import com.qzsoft.demomis.modules.cms.CmsException;

import java.io.IOException;

/**
 * 设置可变信息标志的亮度调节方式
 *
 * @author frank
 */
public class SetLightTypeRequest extends Request {
    private int way;

    public SetLightTypeRequest() throws CmsException {
        this.type = int2bytes(4, 2);
        this.address = int2bytes(0, 2);
    }

    public SetLightTypeRequest(int address) throws CmsException {
        this.type = int2bytes(3, 2);
        this.address = int2bytes(address, 2);
    }

    @Override
    protected void setData() throws CmsException, IOException {
        data = int2bytes(way, 1);
    }

    public int getWay() {
        return way;
    }

    public void setWay(int way) {
        this.way = way;
    }
}
