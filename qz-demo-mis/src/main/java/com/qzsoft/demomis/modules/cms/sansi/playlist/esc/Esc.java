package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

/**
 * 控制显示内容的指令转义基类
 * 
 * @author frank
 * 
 */
public abstract class Esc {
	protected String value;

	protected abstract String getCommand();

	public String getValue() {
		return getCommand() + value;
	}

	/**
	 * 把整数格式化为2位字符串，不足时前面补0
	 * 
	 * @param i
	 * @return
	 */
	protected String format2(int i) {
		return String.format("%1$02d", i);
	}

	/**
	 * 把整数格式化为3位字符串，不足时前面补0
	 * 
	 * @param i
	 * @return
	 */
	protected String format3(int i) {
		return String.format("%1$03d", i);
	}

}
