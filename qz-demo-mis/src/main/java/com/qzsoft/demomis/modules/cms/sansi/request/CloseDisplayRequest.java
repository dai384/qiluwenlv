package com.qzsoft.demomis.modules.cms.sansi.request;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 关闭情报板请求
 *
 * @author frank
 */
public class CloseDisplayRequest extends Request {

    public CloseDisplayRequest() throws CmsException {
        this.type = int2bytes(17, 2);
        this.address = int2bytes(0, 2);
    }

    public CloseDisplayRequest(int address) throws CmsException {
        this.type = int2bytes(17, 2);
        this.address = int2bytes(address, 2);
    }

    @Override
    protected void setData() throws CmsException, IOException {
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
