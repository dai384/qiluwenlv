package com.qzsoft.demomis.modules.cms.sansi.playlist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 当出字方式为 0 或 1 时,param 无用;当出字方式为 2-21 时,param 表速度,范围 0-49,缺省为 0。其中 0
 * 表示最快,即每幅画面停留 20 毫秒,param 每增加 1 停留 时间就增加 20 毫秒
 * 
 * @author frank
 * 
 */
public class Speed {

	private static Logger logger = LoggerFactory.getLogger(Speed.class);

	private final int DEFAULT = 0;// 缺省值
	private int value;

	public Speed() {
		value = DEFAULT;
	}

	public Speed(int value) {
		if (value < 0 || value > 49) {
			logger.warn("param的取值范围为0~49，现在为 {}，将使用默认值 {}", value, DEFAULT);
			value = DEFAULT;
		}
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
