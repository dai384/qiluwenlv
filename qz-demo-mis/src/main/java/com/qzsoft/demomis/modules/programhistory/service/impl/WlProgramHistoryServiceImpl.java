package com.qzsoft.demomis.modules.programhistory.service.impl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.program.dao.WlProgramDao;
import com.qzsoft.demomis.modules.program.entity.WlProgramEntity;
import com.qzsoft.demomis.modules.programhistory.dao.WlProgramHistoryDao;
import com.qzsoft.demomis.modules.programhistory.entity.WlProgramHistoryEntity;
import com.qzsoft.demomis.modules.programhistory.service.WlProgramHistoryService;
import com.qzsoft.demomis.modules.terminal.dao.TerminalDao;
import com.qzsoft.demomis.modules.terminal.entity.TerminalEntity;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ConvertDictUtils;
import com.qzsoft.jeemis.platform.security.user.SecurityUser;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 节目时长(Wl_Program)表服务实现类
 *
 * @author sdmq
 * @since 2020-06-10 17:45:47
 */
@Service("wl_ProgramHistoryService")
public class WlProgramHistoryServiceImpl extends BaseService implements WlProgramHistoryService {
    private final SqlSession sqlSession;
    @Autowired
	private WlProgramHistoryDao wlProgramHistoryDao;
    @Autowired
	private WlProgramDao wl_ProgramDao;
	@Autowired
	private TerminalDao terminalDao;

	public WlProgramHistoryServiceImpl(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}
	
	/**
	 * Wl_Program数据分页
	 * @param params
	 * @return IPage
	 */
	@Override
	public IPage<WlProgramHistoryEntity> page(Map<String, Object> params) {
		IPage<WlProgramHistoryEntity> page =this.getPage(params,Constant.CREATE_DATE,false, WlProgramHistoryEntity.class) ;
		QueryWrapper<WlProgramHistoryEntity> queryWrapper =this.getQueryWrapper(params, WlProgramHistoryEntity.class);
		IPage<WlProgramHistoryEntity> pageData = wlProgramHistoryDao.selectPage(page, queryWrapper);
		pageData= ConvertDictUtils.formatDicDataPage(pageData);
		return pageData;
	}
    /**
	 * 。；4
	 * @param params
	 * @return
	 */
	@Override
	public List<WlProgramHistoryEntity> list(Map<String, Object> params) {
		QueryWrapper<WlProgramHistoryEntity> queryWrapper = this.getQueryWrapper(params, WlProgramHistoryEntity.class);
		List<WlProgramHistoryEntity> entityList = wlProgramHistoryDao.selectList(queryWrapper);
		entityList=ConvertDictUtils.formatDicDataList(entityList);
		return entityList;
	}

	@Override
	public Map<String, Object> get(String programId,String equipmentId) {
		// 节目信息
		WlProgramHistoryEntity wlProgramHistoryEntity = wlProgramHistoryDao.getHistoryById(programId,equipmentId);
		Map<String, Object> map = new HashMap<>();
		map.put("wlProgramHistoryEntity", wlProgramHistoryEntity);
		return map;
	}
    /**
	 * 保存
	 * createDate creator
	 * updateDate updater 
	 * 字段会自动注入值 如不需要注入请修改
	 * @param map
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(Map<String, Object> map) {
		String user = SecurityUser.getUser().getId().toString();
		WlProgramHistoryEntity entity = new WlProgramHistoryEntity();
		String programId = String.valueOf(map.get("programId"));
		String terminalId = String.valueOf(map.get("terminalId"));
		//节目信息
		WlProgramEntity programEntity = wl_ProgramDao.selectById(programId);
		//设备信息
		TerminalEntity terminalEntity = terminalDao.selectById(terminalId);
		//历史信息表
		entity.setProgramId(programEntity.getId());
		entity.setProgramName(programEntity.getName());
		entity.setCustomerId(programEntity.getCustomerId());
		entity.setEquipmentId(String.valueOf(terminalEntity.getId()));
		entity.setEquipmentName(terminalEntity.getName());
		entity.setCreateDate(new Date());
		entity.setCreator(user);
		// 字符串转时间
		if (programEntity.getOnDatetime()!=null && programEntity.getOffDatetime()!=null) {
			Date onDatetimes = null;
			Date offDatetimes = null;
			try {
//				onDatetimes = programEntity.getOnDatetime();
				onDatetimes = new Date();
//				offDatetimes = programEntity.getOffDatetime();
				offDatetimes = new Date();
				//计算时差
				long   time  = DateUtil.between(onDatetimes,offDatetimes,DateUnit.MINUTE,false);
				entity.setTimeCount((int)time);
			}catch (Exception e) {
				e.printStackTrace();
			}
			entity.setOnDatetime(onDatetimes);
			entity.setOffDatetime(offDatetimes);
		}
		wlProgramHistoryDao.insert(entity);
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(Map<String, Object> map) {
		String programId = map.get("programId").toString();
		List equipmentIdList = (List)map.get("equipmentIdList");
		if(equipmentIdList.size() > 0){
			for(int i = 0; i < equipmentIdList.size(); i++){
				WlProgramHistoryEntity wlProgramHistoryEntity = wlProgramHistoryDao.getHistoryById(programId,equipmentIdList.get(i).toString());
			}
		}

	}

}