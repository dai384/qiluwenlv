package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

/**
 * \Bnnn 把 %SYSDIR%\BMP 目录下的 nnn.bmp 文件显示到 \C 所规定的坐标
 * 
 * @author frank
 * 
 */
public class BmpEsc extends Esc {

	@Override
	protected String getCommand() {
		return "\\B";
	}

	private BmpEsc(String name) {
		value = name;
	}

	public static BmpEsc getInstance(String name) {
		return new BmpEsc(name);
	}

}
