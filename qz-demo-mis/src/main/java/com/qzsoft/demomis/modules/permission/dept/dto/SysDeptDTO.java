package com.qzsoft.demomis.modules.permission.dept.dto;


import com.qzsoft.jeemis.common.utils.TreeNode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 部门管理(SysDept)表实体类
 *
 * @author sdmq
 * @since 2019-08-06 09:39:09
 */
@ApiModel(value ="部门树")
@Data
@EqualsAndHashCode(callSuper=false)
public class SysDeptDTO extends TreeNode<SysDeptDTO> implements Serializable{
    private static final long serialVersionUID = 826888907692018914L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "pkid")
	private String pkid;
    /**
    *id步长3唯一
    */
    @ApiModelProperty(value = "id步长3")
    private String id;
    /**
    *上级ID
    */
    @ApiModelProperty(value = "上级ID")
    private String pid;

    /**
    *部门名称
    */
    @ApiModelProperty(value = "部门名称")
    private String name;
    /**
    *1 单位 2 部门  3分组
    */
    @ApiModelProperty(value = "1 单位 2 部门  3分组")
    private String type;
    /**
    *排序默认和id一致为了统一排序方便
    */
    @ApiModelProperty(value = "排序默认和id一致为了统一排序方便")
    private String sort;
    /**
    *简拼
    */
    @ApiModelProperty(value = "简拼")
    private String spell;
    /**
    *是否叶子节点
    */
    @ApiModelProperty(value = "是否叶子节点")
    private Boolean hasLeaf;


	/**
	 *是否被选中
	 */
	@ApiModelProperty(value = "是否被选中")
	private Boolean checked;

	/**
	 *是否包含子节点
	 */
	@ApiModelProperty(value = "是否包含子节点")
	private Boolean inChildren ;

	/**
	 *是否所有的子节点都选中
	 */
	@ApiModelProperty(value = "是否所有的子节点都选中")
	private Boolean allChildrenChecked ;
}