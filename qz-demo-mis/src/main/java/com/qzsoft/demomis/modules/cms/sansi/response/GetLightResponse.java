package com.qzsoft.demomis.modules.cms.sansi.response;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 取可变信息标志的当前亮度调节方式和显示亮度应答
 *
 * @author frank
 */
public class GetLightResponse extends Response {
    private int way;
    private int light;

    public GetLightResponse(byte[] bytes) throws IOException, CmsException {
        this.bytes = bytes;
        decoder();
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        basicDecoder();
        way = getInt(3, 1, CmsException.LIGHT_SET_WAY_EXCEPTION);
        light = getInt(4, 2, CmsException.DISPLAY_LIGHT_EXCEPTION);
    }

    public int getWay() {
        return way;
    }

    public int getLight() {
        return light;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
