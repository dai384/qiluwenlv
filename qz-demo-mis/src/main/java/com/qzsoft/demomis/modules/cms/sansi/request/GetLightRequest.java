package com.qzsoft.demomis.modules.cms.sansi.request;

import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 取可变信息标志的当前亮度调节方式和显示亮度请求
 *
 * @author frank
 */
public class GetLightRequest extends Request {

    public GetLightRequest() throws CmsException {
        this.type = int2bytes(6, 2);
        this.address = int2bytes(0, 2);
    }

    public GetLightRequest(int address) throws CmsException {
        this.type = int2bytes(6, 2);
        this.address = int2bytes(address, 2);
    }

    @Override
    protected void setData() throws CmsException, IOException {
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
