package com.qzsoft.demomis.modules.cms.sansi.playlist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 出字方式。范围 0-21,缺省为 0
 *
 * @author frank
 *
 */
public class Transition {
	private static Logger logger = LoggerFactory.getLogger(Transition.class);

	private final int DEFAULT = 0;// 缺省值
	private int value;

	public Transition() {
		value = DEFAULT;
	}

	public Transition(int value) {
		if (value < 0 || value > 21) {
			logger.warn("transition的取值范围为0~21，现在为 {}，将使用默认值 {}", value, DEFAULT);
			value = DEFAULT;
		}
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	// 0: 清屏(全黑)
	// 1: 立即显示
	// 2: 上移
	// 3:下移
	// 4: 左移
	// 5: 右移
	// 6: 横百叶窗
	// 7: 竖百叶窗
	// 8: 上下合拢
	// 9: 上下展开
	// 10: 左右合拢
	// 11: 左右展开
	// 12: 中心合拢
	// 13: 中心展开
	// 14: 向下马赛克
	// 15: 向右马赛克
	// 16: 淡入
	// 17: 淡出
	// 18: 字符闪烁(闪后消失)
	// 19: 字符闪烁(闪后停留)
	// 20: 区域闪烁(闪后复原)
	// 21: 区域闪烁(闪后区域为黑)
}
