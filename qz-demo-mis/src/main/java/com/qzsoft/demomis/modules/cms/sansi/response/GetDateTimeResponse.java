package com.qzsoft.demomis.modules.cms.sansi.response;



import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 读取当前扫描板的日期和时间应答
 *
 * @author frank
 */
public class GetDateTimeResponse extends Response {
    private Date date;
    private int week;
    private String formatDate;
    private String formatWeek;

    public GetDateTimeResponse(byte[] bytes) throws IOException,
            CmsException {
        this.bytes = bytes;
        decoder();
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        basicDecoder();
        byte[] tmp = unesc(getArray(3, -3, CmsException.CONTENT_EXCEPTION));
        String strDate = "20"
                + CodecUtil.bytesToHexString(tmp[0], tmp[1], tmp[2], tmp[3],
                tmp[4], tmp[5]);
        week = Integer.parseInt(CodecUtil.bytesToHexString(tmp[6]));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            throw new CmsException("解析日期出错，日期内容：" + strDate);
        }
    }

    /**
     * 已转义数据的反转<br/>
     * 如果返回的日期时间数据中含有 0xf2,0xf3,则转为 0x02,0x03 处理
     *
     * @param bytes
     * @return
     */
    protected byte[] unesc2(byte[] bytes) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        for (byte b : bytes) {
            switch (b) {
                case (byte) 0xF2:
                    out.write(0x02);
                    break;
                case (byte) 0xF3:
                    out.write(0x03);
                    break;
                default:
                    out.write(b);
                    break;
            }
        }
        return out.toByteArray();
    }

    public Date getDate() {
        return date;
    }

    public int getWeek() {
        return week;
    }

    public String getFormatDate() {
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.format(date);
        }
        return formatDate;
    }

    public String getFormatWeek() {
        if (week > 0) {
            switch (week) {
                case 1:
                    return "周一";
                case 2:
                    return "周二";
                case 3:
                    return "周三";
                case 4:
                    return "周四";
                case 5:
                    return "周五";
                case 6:
                    return "周六";
                case 7:
                    return "周日";
            }
        }
        return formatWeek;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
