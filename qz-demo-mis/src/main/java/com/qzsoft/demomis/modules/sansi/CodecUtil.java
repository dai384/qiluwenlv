package com.qzsoft.demomis.modules.sansi;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.DecoderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;

public class CodecUtil {
    private static Logger logger = LoggerFactory.getLogger(CodecUtil.class);


    /**
     * 将字节数组编码为HEX编码的字符串
     */
    public static String encodeHex(byte[] dataBytes) {

        String dataString = null;
        dataString = new String(Hex.encodeHex(dataBytes));
        return dataString;
    }


    /**
     * 将HEX编码的字符串解码为字节数组
     */
    public static byte[] decodeHex(String dataString) {
        byte[] dataBytes = null;
        if (dataString != null) {
            try {
                dataBytes = Hex.decodeHex(dataString.toCharArray());
            } catch (DecoderException e) {
                logger.warn(e.toString());
            }
        }
        return dataBytes;
    }

    /**
     * 以十六进制的无符号整数形式返回一个byte参数的字符串表示形式(有前导0)
     */
    public static String toHexString(byte b) {
        String dataString = null;
        dataString = encodeHex(toByteArray(b));
        return dataString;
    }

    /**
     * 以十六进制的无符号整数形式返回一个short参数的字符串表示形式(有前导0)
     */
    public static String toHexString(short s) {
        String dataString = null;
        dataString = encodeHex(toByteArray(s));
        return dataString;
    }

    /**
     * 以十六进制的无符号整数形式返回一个int参数的字符串表示形式(有前导0)
     */
    public static String toHexString(int i) {
        String dataString = null;
        dataString = encodeHex(toByteArray(i));
        return dataString;
    }

    /**
     * 以十六进制的无符号整数形式返回一个long参数的字符串表示形式(有前导0)
     */
    public static String toHexString(long l) {
        String dataString = null;
        dataString = encodeHex(toByteArray(l));
        return dataString;
    }

    /**
     * 以十六进制的无符号整数形式返回一个BigInteger参数的字符串表示形式(有前导0)
     */
    public static String toHexString(BigInteger b) {
        String dataString = null;
        dataString = encodeHex(toByteArray(b));
        return dataString;
    }

    /**
     * 返回一个字节数组，该数组包含此byte的二进制补码表示形式
     */
    public static byte[] toByteArray(byte b) {
        byte[] dataBytes = null;
        dataBytes = new byte[1];
        dataBytes[0] = b;
        return dataBytes;
    }

    /**
     * 返回一个字节数组，该数组包含此short的二进制补码表示形式
     */
    public static byte[] toByteArray(short s) {
        byte[] dataBytes = null;
        dataBytes = new byte[2];
        dataBytes[0] = (byte) ((s >>> 8) & 0xff);
        dataBytes[1] = (byte) (s & 0xff);
        return dataBytes;
    }

    /**
     * 返回一个字节数组，该数组包含此int的二进制补码表示形式
     */
    public static byte[] toByteArray(int i) {
        byte[] dataBytes = null;
        dataBytes = new byte[4];
        dataBytes[0] = (byte) ((i >>> 24) & 0xff);
        dataBytes[1] = (byte) ((i >>> 16) & 0xff);
        dataBytes[2] = (byte) ((i >>> 8) & 0xff);
        dataBytes[3] = (byte) (i & 0xff);
        return dataBytes;
    }

    /**
     * 返回一个字节数组，该数组包含此long的二进制补码表示形式
     */
    public static byte[] toByteArray(long l) {
        byte[] dataBytes = null;
        dataBytes = new byte[8];
        dataBytes[0] = (byte) ((l >>> 56) & 0xff);
        dataBytes[1] = (byte) ((l >>> 48) & 0xff);
        dataBytes[2] = (byte) ((l >>> 40) & 0xff);
        dataBytes[3] = (byte) ((l >>> 32) & 0xff);
        dataBytes[4] = (byte) ((l >>> 24) & 0xff);
        dataBytes[5] = (byte) ((l >>> 16) & 0xff);
        dataBytes[6] = (byte) ((l >>> 8) & 0xff);
        dataBytes[7] = (byte) (l & 0xff);
        return dataBytes;
    }

    /**
     * 返回一个字节数组，该数组包含此BigInteger的二进制补码表示形式
     */
    public static byte[] toByteArray(BigInteger b) {
        byte[] dataBytes = null;
        dataBytes = b.toByteArray();
        return dataBytes;
    }

    /**
     * 根据EsmClass返回TpUdhi
     * @return
     */
    public static int getTpUdhiFromEsmClass(int esmClass) {
        int tpUdhi = (esmClass & 0x40) >> 6;
        return tpUdhi;
    }

    /**
     * 根据TpUdhi返回EsmClass
     */
    public static int getEsmClassFromTpUdhi(int tpUdhi) {
        int esmClass = tpUdhi << 6;
        return esmClass;
    }
}
