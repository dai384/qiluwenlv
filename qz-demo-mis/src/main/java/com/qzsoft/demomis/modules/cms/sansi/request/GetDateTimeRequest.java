package com.qzsoft.demomis.modules.cms.sansi.request;

import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;

import java.io.IOException;

/**
 * 读取当前扫描板的日期和时间请求
 *
 * @author frank
 */
public class GetDateTimeRequest extends Request {

    public GetDateTimeRequest() throws CmsException {
        this.type = int2bytes(11, 2);
        this.address = int2bytes(0, 2);
    }

    public GetDateTimeRequest(int address) throws CmsException {
        this.type = int2bytes(11, 2);
        this.address = int2bytes(address, 2);
    }

    @Override
    protected void setData() throws CmsException, IOException {
    }

    public static void main(String[] args) throws CmsException, IOException {
        GetDateTimeRequest gdtr = new GetDateTimeRequest(0);
        System.out.println(CodecUtil.bytesToHexString(gdtr.encoder()));
    }
}
