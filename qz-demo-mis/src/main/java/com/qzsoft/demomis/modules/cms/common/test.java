package com.qzsoft.demomis.modules.cms.common;

public class test {
    static CodecUtil codecUtil = new CodecUtil();
    public static void main(String[] args){
        int num = 524288;
        // 创建4个字节的比特数组，共32位；  2^32 即最大值为：4 294 967 296 b / 1024 = 4 194 304 kb / 1024 = 4096 M;
        byte[] b = new byte[4];
        //
        b[3] = (byte) (num & 0xFF);
        b[2] = (byte) (num >> 8 & 0xFF);
        b[1] = (byte) (num >> 16 & 0xFF);
        b[0] = (byte) (num >> 24 & 0xFF);
        String str = bytesToHexString(b);
//        String.format("0x%04x", crc16));
        System.out.println(str);
    }


    public static String bytesToHexString(byte... src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString().toUpperCase();
    }


    public static byte[] int2bytes(int s, int size) {
        byte[] bytes = new byte[size];
        for (int i = size - 1; i >= 0; i--) {
            bytes[i] = (byte) (s % 256);
            s >>= 8;
        }
        return bytes;
    }
}
