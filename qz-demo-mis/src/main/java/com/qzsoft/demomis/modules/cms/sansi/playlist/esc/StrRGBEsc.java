package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * \cRRRGGGBBBYYY 字符颜色,\ct 为透明色,RRR、GGG、BBB、YYY 分别表示可变 情报板红、绿、蓝、琥珀色LED的亮度,范围为
 * 0-255,缺省为 \c255255000000(即黄色)
 * 
 * @author frank
 * 
 */
public class StrRGBEsc extends Esc {
	private static Logger logger = LoggerFactory.getLogger(StrRGBEsc.class);
	private final String DEFAULT = "255255000000";// 黄色

	@Override
	protected String getCommand() {
		return "\\c";
	}

	private StrRGBEsc(int r, int g, int b, int y) {
		if (r < 0 || r > 255 || g < 0 || g > 255 || b < 0 || b > 255 || y < 0
				|| y > 255) {
			logger.warn("GRB颜色的取值范围为0~255，现在为 {}，将使用默认值 {}", value, "黄色");
			value = DEFAULT;
		} else {
			value = format3(r) + format3(g) + format3(b) + format3(y);
		}
	}

	private StrRGBEsc() {// 透明
		value = "t";
	}

	/**
	 * 默认黄色
	 * 
	 * @return
	 */
	public static StrRGBEsc getInstance() {
		return new StrRGBEsc(255, 255, 0, 0);
	}

	public static StrRGBEsc getInstance(int r, int g, int b, int y) {
		return new StrRGBEsc(r, g, b, y);
	}

	/**
	 * 透明
	 * 
	 * @return
	 */
	public static StrRGBEsc getTInstance() {
		return new StrRGBEsc();
	}

}
