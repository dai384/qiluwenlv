package com.qzsoft.demomis.modules.cms.sansi.request;

import com.qzsoft.demomis.modules.cms.Basic;
import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public abstract class Request extends  Basic{
	protected byte[] type;

	protected abstract void setData() throws CmsException, IOException;

	public byte[] encoder() throws IOException, CmsException {
		setData();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		out.write(header);
		out.write(address);
		out.write(type);
		if (data != null) {
			out.write(esc(data));
		}
		out.write(checkData());
		out.write(footer);
		bytes = out.toByteArray();
		out.close();
		return bytes;
	}

	/**
	 * 校验数据
	 * 
	 * @return 校验码
	 * @throws IOException
	 * @throws CmsException
	 */
	public byte[] checkData() throws IOException, CmsException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		out.write(address);
		out.write(type);
		if (data != null) {
			out.write(data);
		}
		check = CodecUtil.crc16Bytes(out.toByteArray());
		out.close();
		// ByteArrayOutputStream out2 = new ByteArrayOutputStream();
		// out2.write(address);
		// out2.write(type);
		// if (data != null) {
		// out2.write(esc(data));
		// }
		// out2.write(check);
		return check;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	// public static void main(String[] args) {
	// long a = Long.parseLong("fffd", 16);
	// for (int i = 1; i < 11; i++) {
	// if ((a & (1 << i)) != 0) {
	// System.out.println(i + " = " + a);
	// }
	// }
	// }
}
