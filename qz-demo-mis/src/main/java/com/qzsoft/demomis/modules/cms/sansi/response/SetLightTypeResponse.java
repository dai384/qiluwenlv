package com.qzsoft.demomis.modules.cms.sansi.response;


import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 设置可变信息标志的亮度调节方式和显示亮度应答
 *
 * @author frank
 */
public class SetLightTypeResponse extends Response {
    private int code;

    public SetLightTypeResponse(byte[] bytes) throws IOException, CmsException {
        this.bytes = bytes;
        decoder();
    }

    @Override
    protected void decoder() throws IOException, CmsException {
        code = getInt(3, 1, CmsException.CONTENT_EXCEPTION);
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
