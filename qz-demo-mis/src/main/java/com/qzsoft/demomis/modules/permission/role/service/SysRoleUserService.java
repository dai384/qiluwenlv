/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.permission.role.service;


import java.util.List;

/**
 * 角色用户关系
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0
 */
public interface SysRoleUserService  {

    /**
     * 保存或修改
     * @param userId      用户ID
     * @param roleIdList  角色ID列表
     */
    void saveOrUpdate(Long userId, List<Long> roleIdList);

	void deleteByRoleIds(Long[] roleIds);

	void  deleteByUserIds(Long[] userIds);

	List<Long> getRoleIdList(Long userId);

}