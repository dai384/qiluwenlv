package com.qzsoft.demomis.modules.unit.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.*;
import java.io.Serializable;
import com.qzsoft.jeemis.common.annotation.Dict;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import java.util.Map;


/**
 * (WlRegion)表实体类
 *
 * @author sdmq
 * @since 2020-05-28 09:20:44
 */
@ApiModel(value ="")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("wl_region")
public class WlRegionEntity extends Model<WlRegionEntity> {
    private static final long serialVersionUID = 533247414625154069L;
   /**
    *区域主键主键
    */
        @TableId
    @NotNull(message="{id.require}", groups = UpdateGroup.class)
    @Excel(name = "区域主键主键")
    @ApiModelProperty(value = "区域主键主键")
    private Integer id;
   /**
    *区域名称
    */
    @Excel(name = "区域名称")
    @ApiModelProperty(value = "区域名称")
    private String name;
   /**
    *区域上级标识
    */
    @Excel(name = "区域上级标识")
    @ApiModelProperty(value = "区域上级标识")
    private Integer pid;
   /**
    *地名简称
    */
    @Excel(name = "地名简称")
    @ApiModelProperty(value = "地名简称")
    private String sname;
   /**
    *区域等级
    */
    @Excel(name = "区域等级")
    @ApiModelProperty(value = "区域等级")
    private Integer level;
   /**
    *区域编码
    */
    @Excel(name = "区域编码")
    @ApiModelProperty(value = "区域编码")
    private String citycode;
   /**
    *邮政编码
    */
    @Excel(name = "邮政编码")
    @ApiModelProperty(value = "邮政编码")
    private String yzcode;
   /**
    *组合名称
    */
    @Excel(name = "组合名称")
    @ApiModelProperty(value = "组合名称")
    private String mername;
   /**
    *经度
    */
    @Excel(name = "经度")
    @ApiModelProperty(value = "经度")
    private Double lng;
   /**
    *纬度
    */
    @Excel(name = "纬度")
    @ApiModelProperty(value = "纬度")
    private Double lat;
   /**
    *拼音
    */
    @Excel(name = "拼音")
    @ApiModelProperty(value = "拼音")
    private String pinyin;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}