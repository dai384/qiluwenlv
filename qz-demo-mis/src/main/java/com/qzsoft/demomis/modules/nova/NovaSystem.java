package com.qzsoft.demomis.modules.nova;

import novj.platform.vxkit.common.bean.FirmwareInfo;
import novj.platform.vxkit.common.bean.login.LoginResultBean;
import novj.platform.vxkit.common.bean.programinfo.Widget;
import novj.platform.vxkit.common.bean.search.SearchResult;
import novj.platform.vxkit.common.result.DefaultOnResultListener;
import novj.platform.vxkit.common.result.OnResultListenerN;
import novj.platform.vxkit.handy.api.ProgramSendManager;
import novj.platform.vxkit.handy.api.SearchManager;
import novj.publ.api.NovaOpt;
import novj.publ.api.actions.ProgramManager;
import novj.publ.net.exception.ErrorDetail;
import novj.publ.net.svolley.Request.IRequestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/*
 * 发布普通节目
 * */
public class NovaSystem {
    protected static final Logger LOGGER = LoggerFactory.getLogger(NovaSystem.class);
    private String commenPath = "H:";
    private String programPath = "H:/qiluwenlv/program/nova/";
    String res ="";

    public void NovaOperation(String remoteIp, List<String> resourceList) {
        NovaOpt novaOpt = new NovaOpt();
        // 1、SDK 初始化: 1=Android 系统环境 2=Windows 或 Linux 系统环境
        novaOpt.GetInstance().initialize(2);
        // 2、搜索设备
        novaOpt.GetInstance().searchScreen(new SearchManager.OnScreenSearchListener(){
            @Override
            public void onSuccess(SearchResult searchResult){
                // 发现设备，自行添加处理逻辑
                novaOpt.GetInstance().connectDevice(searchResult, new DefaultOnResultListener() {
                    @Override
                    public void onSuccess(Integer response) {
                        LOGGER.info("Nova System connect success===================================================================：terminal IP is："+remoteIp);
                        // connect success，自行添加处理逻辑
                        // 4、登录设备
                        String userName = "admin";
                        String passwords = "123456";
                        novaOpt.GetInstance().login(searchResult.sn, userName, passwords,
                                new OnResultListenerN<LoginResultBean, ErrorDetail>() {
                                    @Override
                                    public void onSuccess(IRequestBase requestBase, LoginResultBean response) {
                                        LOGGER.info("Nova System====login Success===============================================================：terminal IP is："+remoteIp);
//                                      执行成功，自行添加处理逻辑: 根据操作类型进行以下操作；
                                        //  1.  创建节目：返回正数为播放节目 ID
                                        Date date = new Date();
                                        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMddHHmmss");
                                        String dateStr = formatter1.format(date);
                                        String programName = "newProgram"+dateStr;
                                        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyMMdd");
                                        novaOpt.GetInstance().createProgram(programName, searchResult.width, searchResult.height);

                                        //  2.  添加 page：给播放节目添加一个（默认）播放页面,返回正数为播放页面的 ID
                                        //  5.  添加 widget(给播放页面添加播放窗口（挂件）)：
                                        //  2020/7/19 param为播放媒体对象
                                        // 其中pageId 为需要添加 widget 的 page 的 id。mediaType 为
                                        // ProgramManager.WidgetMediaType.PICTURE/VIDEO/GIF/ARCH_TEXT。
                                        // 当 widget 是文本类型时param 为文本内容,widget 为其他类型时 param 为原平台文件路径。

                                        // 根据programId获取资源，遍历资源进行添加
                                        for (String path: resourceList) {
                                            int pageId = novaOpt.GetInstance().addPage();
                                            if (path.contains("image")){
                                                int widgetId = novaOpt.GetInstance().addWidget(pageId, ProgramManager.WidgetMediaType.PICTURE, commenPath+path);
                                                // 修改播放时长
                                                Widget widget = novaOpt.GetInstance().getWidgetParam(pageId, widgetId);
                                                widget.setDuration(15*1000); // 15s
                                                novaOpt.GetInstance().setWidgetParam(pageId, widgetId, widget);
                                            } else if (path.contains("vedio")) {
                                                novaOpt.GetInstance().addWidget(pageId, ProgramManager.WidgetMediaType.VIDEO, commenPath+path);
                                            }
                                        }

                                        String programDir = programPath+formatter2.format(date);

                                        // 生成并保存节目
                                        int result = novaOpt.GetInstance().makeProgram(programDir);

                                        if(result == 0){
                                            // 获取mac号发布节目
                                            novaOpt.GetInstance().getVersionMessage(searchResult.sn, new OnResultListenerN<FirmwareInfo, ErrorDetail>() {
                                                @Override
                                                public void onSuccess(IRequestBase requestBase, FirmwareInfo response) {
                                                //执行成功，自行添加处理逻辑
                                                    try {
                                                        LOGGER.info("Nova System   shengchengjiemujieguo=====================：terminal IP is："+remoteIp+";lujing："+programDir+";jieguo："+result+";mac："+response.mac);
                                                        novaOpt.GetInstance().startTransfer(searchResult.sn, programDir, programName, "", true, new ProgramSendManager.OnProgramTransferListener() {
                                                            @Override
                                                            public void onStarted() {
                                                                //开始发布
                                                                LOGGER.info("Nova System kaishifabu===================================================================：terminal IP is："+remoteIp);
                                                            }
                                                            @Override
                                                            public void onTransfer(long l, long l1) {
                                                                //发布进度
                                                                LOGGER.info("Nova System fabujindu===================================== =====：terminal IP is："+remoteIp+"； yishangchuan："+l+"； gongshangchuan："+l1);
                                                                res = "jiemufabuchenggong";
                                                            }
                                                            @Override
                                                            public void onError(ErrorDetail errorDetail) {
                                                                //发布失败
                                                                LOGGER.info("Nova System fabushibai===================================================================：sousuo terminal IP is："+remoteIp+"; cuowumawei："+errorDetail.errorCode+";cuowumiaoshu："+errorDetail.description);
                                                                res = "节目发布失败";
                                                                if (novaOpt.GetInstance().logOut(searchResult.sn)) {
                                                                    //logout success，自行添加处理逻辑
                                                                    LOGGER.info("============================================================="+remoteIp+"logout success===============================================================");
                                                                    res = "jiemufabuchenggong,tuichudenglu";
                                                                } else {
                                                                    //logout fail，自行添加处理逻辑
                                                                    LOGGER.info("============================================================="+remoteIp+"logout fail===============================================================");
                                                                    res = "jiemufabuchenggong，logout fail";
                                                                }
                                                            }
                                                            @Override
                                                            public void onCompleted() {
                                                                // 发布完成
                                                                LOGGER.info("Nova System发布完成===================================================================：terminal IP is："+remoteIp);
                                                                if (novaOpt.GetInstance().logOut(searchResult.sn)) {
                                                                    //logout success，自行添加处理逻辑
                                                                    LOGGER.info("============================================================="+remoteIp+"logout success===============================================================");
                                                                    res = "jiemufabuchenggong,tuichudenglu";
                                                                } else {
                                                                    //logout fail，自行添加处理逻辑
                                                                    LOGGER.info("============================================================="+remoteIp+"logout fail=============================================================");
                                                                    res = "jiemufabuchenggong，logout fail";
                                                                }
                                                            }
                                                            @Override
                                                            public void onAborted() {
                                                                //发布终止
                                                                LOGGER.info("Nova System发布终止===================================================================：terminal IP is："+remoteIp);
                                                                if (novaOpt.GetInstance().logOut(searchResult.sn)) {
                                                                    //logout success，自行添加处理逻辑
                                                                    LOGGER.info("=============================================================logout success===============================================================");
                                                                    res = "jiemufabuchenggong,tuichudenglu";
                                                                } else {
                                                                    //logout fail，自行添加处理逻辑
                                                                    LOGGER.info("=============================================================logout fail===============================================================");
                                                                    res = "jiemufabuchenggong，logout fail";
                                                                }
                                                            }
                                                        });
                                                    }catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                @Override
                                                public void onError(IRequestBase requestBase, ErrorDetail error) {
                                                //执行失败，自行添加处理逻辑
                                                }
                                            });
                                            // 发布节目
                                        } else {
                                            res = "生成节目失败";
                                            if (novaOpt.GetInstance().logOut(searchResult.sn)) {
                                                //logout success，自行添加处理逻辑
                                                LOGGER.info("=============================================================logout success===============================================================");
                                                res = "jie mufabu chenggong,tuichudenglu";
                                            } else {
                                                //logout fail，自行添加处理逻辑
                                                LOGGER.info("=============================================================logout fail===============================================================");
                                                res = "jiemu fabu chenggong，logout fail";
                                            }
                                        }

                                    }
                                    @Override
                                    public void onError(IRequestBase requestBase, ErrorDetail error) {
                                        //执行失败，自行添加处理逻辑=
                                        LOGGER.info("Nova System denglu shibai===================================================================：terminal IP is："+remoteIp);
                                        res = "Nova Systemdenglu shibai，cuowuma："+error.errorCode+"; cuowumiaoshu："+error.description;
                                    }
                                });
                    }
                    @Override
                    public void onError(ErrorDetail error) {
                        //连接失败，自行添加处理逻辑
                        LOGGER.info("Nova System denglu shibai===================================================================：terminal IP is："+remoteIp);
                        res = "发布节目失败,Nova System连接失败，cuowuma："+error.errorCode+"; cuowumiaoshu："+error.description;
                    }
                }, wrapper -> {
                    //连接断开，自行添加处理逻辑
                    LOGGER.info("Nova lian jie duan kai===================================================================：terminal IP is："+remoteIp);
                });
            }
            @Override
            public void onError(ErrorDetail errorDetail){
                // 发生错误，自行添加处理逻辑=
                LOGGER.info("Nova Systemsousuoshebeishibai===================================================================：terminal IP is："+remoteIp+"; cuowuma："+errorDetail.errorCode+"; cuowumiaoshu："+errorDetail.description);
                res = "fabujiemushibai,Nova System sousuoshibai，cuowuma："+errorDetail.errorCode+"; cuowumiaoshu："+errorDetail.description;
            }
        }, remoteIp);
    }
}
