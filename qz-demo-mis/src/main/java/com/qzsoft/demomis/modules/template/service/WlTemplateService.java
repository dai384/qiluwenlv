package com.qzsoft.demomis.modules.template.service;

import com.qzsoft.demomis.modules.template.entity.WlTemplateEntity;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;
/**
 * 模板表(Wl_Template)表服务接口
 *
 * @author sdmq
 * @since 2020-06-10 17:46:08
 */
public interface WlTemplateService {
    /**
	 * 数据分页
	 * @param params
	 * @return IPage
	 */
	List<WlTemplateEntity> getPage(Map<String, Object> params);
	int total(Map<String, Object> params);

    
	/**
	 * 数据列表
	 * @param params
	 * @return
	 */
	List<WlTemplateEntity> list(Map<String, Object> params);

	/**
	 * 单个数据
	 * @param id
	 * @return
	 */
	WlTemplateEntity get(String id);

	/**
	 * 保存
	 */
	void save(Map<String, Object> map);

	/**
	 * 更新
	 */
	String update(Map<String, Object> map);

	/**
	 * 批量删除
	 * @param ids
	 */
	String delete(String[] ids);

	/**
	 * 导出Excel
	 * @param params
	 * @param response
	 */
	void exportXls(Map<String, Object> params , HttpServletResponse response);
}