package com.qzsoft.demomis.modules.cms.sansi.playlist.esc;

public class LatticeEsc extends Esc {

	@Override
	protected String getCommand() {
		return "\\P";
	}

	private LatticeEsc(Lattice lattice) {
		value = lattice.getNum();
	}

	public static LatticeEsc getInstance(Lattice lattice) {
		return new LatticeEsc(lattice);
	}

	public enum Lattice {
		P32("32"), P40("40"), P48("48");
		private String num;

		Lattice(String num) {
			this.num = num;
		}

		public String getNum() {
			return num;
		}
	}
}
