package com.qzsoft.demomis.modules.program.entity;

import java.util.Date;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.*;
import java.io.Serializable;

import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;


/**
 * 节目（模板）组件资源表[模板id非主要关系，主要存储节目中各组件信息和组件中上传的资源信息](WlProblemMoudleResource)表实体类
 *
 * @author sdmq
 * @since 2020-07-07 09:35:18
 */
@ApiModel(value ="节目（模板）组件资源表[模板id非主要关系，主要存储节目中各组件信息和组件中上传的资源信息]")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("wl_program_moudle_resource")
public class WlProgramMoudleResourceEntity extends Model<WlProgramMoudleResourceEntity> {
    private static final long serialVersionUID = 859665709403166137L;
   /**
    *id主键
    */
        @TableId(value ="id",type = IdType.AUTO) //自增策略
    @NotNull(message="{id.require}", groups = UpdateGroup.class)
    @Excel(name = "id主键")
    @ApiModelProperty(value = "id主键")
    private Integer id;
   /**
    *关联模板组件表id
    */
    @Excel(name = "关联模板组件表id")
    @ApiModelProperty(value = "关联模板组件表id")
    private String programId;
   /**
    *关联资源id
    */
    @Excel(name = "关联资源id")
    @ApiModelProperty(value = "关联资源id")
    private String templateId;
   /**
    * 组件样式
    */
    @Excel(name = "组件样式")
    @ApiModelProperty(value = "组件样式")
    private String itemStyle;
   /**
    *组件类型( 视频：0； 音频：1； 图片：2； 轮播图：3；文本：4； 直播：5； 时钟：6； 天气：7；)
    */
    @Excel(name = "组件类型( 视频：0； 音频：1； 图片：2； 轮播图：3；文本：4； 直播：5； 时钟：6； 天气：7；)")
    @ApiModelProperty(value = "组件类型( 视频：0； 音频：1； 图片：2； 轮播图：3；文本：4； 直播：5； 时钟：6； 天气：7；)")
    private String type;
   /**
    *x轴位置
    */
    @Excel(name = "x轴位置")
    @ApiModelProperty(value = "x轴位置")
    private Integer x;
   /**
    *y轴位置
    */
    @Excel(name = "y轴位置")
    @ApiModelProperty(value = "y轴位置")
    private Integer y;
   /**
    *宽度
    */
    @Excel(name = "宽度")
    @ApiModelProperty(value = "宽度")
    private Integer w;
   /**
    *高度
    */
    @Excel(name = "高度")
    @ApiModelProperty(value = "高度")
    private Integer h;
   /**
    *排序
    */
    @Excel(name = "排序")
    @ApiModelProperty(value = "排序")
    private Integer z;
   /**
    *关联资源id
    */
    @Excel(name = "关联资源id")
    @ApiModelProperty(value = "关联资源id")
    private String resourceId;
   /**
    *1:循环，0：不循环
    */
    @Excel(name = "1:循环，0：不循环")
    @ApiModelProperty(value = "1:循环，0：不循环")
    private String loopValue;
   /**
    *循环时间（单位：s）
    */
    @Excel(name = "循环时间（单位：s）")
    @ApiModelProperty(value = "循环时间（单位：s）")
    private int intervalTime;
   /**
    *字体颜色
    */
    @Excel(name = "字体颜色")
    @ApiModelProperty(value = "字体颜色")
    private String fontColor;
   /**
    *字体大小
    */
    @Excel(name = "字体大小")
    @ApiModelProperty(value = "字体大小")
    private Integer fontWeight;
   /**
    *备注（暂存资源地址，无实际用处）
    */
    @Excel(name = "备注（暂存资源地址，无实际用处）")
    @ApiModelProperty(value = "备注（暂存资源地址，无实际用处）")
    private String remark;
   /**
    *创建人
    */
    @Excel(name = "创建人")
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private String creator;
   /**
    *创建时间
    */
    @Excel(name = "创建时间" , format = "yyyy-MM-dd")
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
   /**
    *编辑人
    */
    @Excel(name = "编辑人")
    @ApiModelProperty(value = "编辑人")
    private String updator;
   /**
    *编辑时间
    */
    @Excel(name = "编辑时间" , format = "yyyy-MM-dd")
   @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "编辑时间")
    private Date updateDate;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}