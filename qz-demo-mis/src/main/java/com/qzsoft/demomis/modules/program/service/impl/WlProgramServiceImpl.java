package com.qzsoft.demomis.modules.program.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qzsoft.demomis.modules.nova.FabuDemo;
import com.qzsoft.demomis.modules.nova.NovaLogOut;
import com.qzsoft.demomis.modules.nova.NovaSystem;
import com.qzsoft.demomis.modules.program.entity.PicEntity;
import com.qzsoft.demomis.modules.program.entity.WlProgramMoudleResourceEntity;
import com.qzsoft.demomis.modules.programhistory.service.WlProgramHistoryService;
import com.qzsoft.demomis.modules.resource.dao.WlResourceDao;
import com.qzsoft.demomis.modules.resource.entity.WlResourceEntity;
import com.qzsoft.demomis.modules.sansi.SanSiSystem;
import com.qzsoft.demomis.modules.template.dao.WlTemplateDao;
import com.qzsoft.demomis.modules.template.entity.WlTemplateEntity;
import com.qzsoft.demomis.modules.template.entity.WlTemplateMoudleEntity;
import com.qzsoft.demomis.modules.terminal.dao.TerminalDao;
import com.qzsoft.demomis.modules.terminal.entity.TerminalEntity;
import com.qzsoft.jeemis.platform.security.user.SecurityUser;
import org.apache.ibatis.session.SqlSession;
import com.qzsoft.demomis.modules.program.dao.WlProgramDao;
import com.qzsoft.demomis.modules.program.entity.WlProgramEntity;
import com.qzsoft.demomis.modules.program.service.WlProgramService;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.common.utils.ExcelUtils;
import com.qzsoft.jeemis.common.utils.ConvertDictUtils;
import org.checkerframework.checker.units.qual.A;
import org.h2.util.New;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;


/**
 * 节目表(Wl_Program)表服务实现类
 *
 * @author sdmq
 * @since 2020-06-10 17:45:47
 */
@Service("wlProgramService")
public class WlProgramServiceImpl extends BaseService implements WlProgramService {
    private final SqlSession sqlSession;
    @Autowired
	private WlProgramDao wlProgramDao;
	@Autowired
	private WlTemplateDao wlTemplateDao;
	@Autowired
	private WlResourceDao wlResourceDao;
	@Autowired
	private TerminalDao terminalDao;

	@Autowired
	private WlProgramHistoryService wlProgramHistoryService;

	NovaSystem novaSystem = new NovaSystem();
	SanSiSystem sanSiSystem = new SanSiSystem();

	NovaLogOut novaLogOut = new NovaLogOut();

	public WlProgramServiceImpl(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}
	
	/**
	 * Wl_Program数据分页
	 * @param params
	 * @return IPage
	 */
	@Override
	public IPage<WlProgramEntity> page(Map<String, Object> params) {
		IPage<WlProgramEntity> page =this.getPage(params,Constant.CREATE_DATE,false, WlProgramEntity.class) ;
		QueryWrapper<WlProgramEntity> queryWrapper =this.getQueryWrapper(params, WlProgramEntity.class);
		IPage<WlProgramEntity> pageData = wlProgramDao.selectPage(page, queryWrapper);
		pageData= ConvertDictUtils.formatDicDataPage(pageData);
		return pageData;
	}
    /**
	 * Wl_Program数据列表
	 * @param params
	 * @return
	 */
	@Override
	public List<WlProgramEntity> list(Map<String, Object> params) {
		QueryWrapper<WlProgramEntity> queryWrapper = this.getQueryWrapper(params, WlProgramEntity.class);
		List<WlProgramEntity> entityList = wlProgramDao.selectList(queryWrapper);
		entityList=ConvertDictUtils.formatDicDataList(entityList);
		return entityList;
	}

	@Override
	public Map<String, Object> get(String id) {
		// 节目信息
		WlProgramEntity wlProgramEntity = wlProgramDao.selectById(id);
		// 模板、组件信息
		String temId = wlProgramEntity.getType();
		WlTemplateEntity wlTemplateEntity = wlTemplateDao.selectById(temId);
		List<WlTemplateMoudleEntity> moudleEntityList = wlTemplateDao.selectModuleList(temId);
		// 图片信息
		List<PicEntity> picEntityList = new ArrayList<>();
		if(moudleEntityList.size()>0){
			wlTemplateEntity.setDragList(moudleEntityList);
			String resourceId = "";
			WlResourceEntity wlResourceEntity = new WlResourceEntity();
			for (int i = 0; i < moudleEntityList.size(); i++) {
				String type = moudleEntityList.get(i).getType();
				resourceId = moudleEntityList.get(i).getResourceId();
				if (resourceId != null && !(resourceId.isEmpty())){
					wlResourceEntity = wlResourceDao.selectById(resourceId);
					switch (type) {
                        case "0": moudleEntityList.get(i).setRemark(wlResourceEntity.getPath()); break;
                        case "1": moudleEntityList.get(i).setRemark(wlResourceEntity.getPath()); break;
                        case "2": moudleEntityList.get(i).setRemark(wlResourceEntity.getPath()); break;
                        case "4": moudleEntityList.get(i).setRemark(wlResourceEntity.getTextContent()); break;
                        case "5": moudleEntityList.get(i).setRemark(wlResourceEntity.getUrlPath());; break;
                        default: System.out.println("type类型为："+type);
                    }
				}
				picEntityList = wlProgramDao.getPicEntity(moudleEntityList.get(i).getId());
				for (int j = 0; j < picEntityList.size(); j++) {
					resourceId = picEntityList.get(j).getResourceId().toString();
					wlResourceEntity = wlResourceDao.selectById(resourceId);
					picEntityList.get(j).setRemark(wlResourceEntity.getPath());
				}
			}
		}
		Map<String, Object> map = new HashMap<>();
		map.put("wlProgramEntity", wlProgramEntity);
		map.put("wlTemplateEntity", wlTemplateEntity);
		map.put("picEntityList", picEntityList);
		return map;
	}
    /**
	 * Wl_Program保存
	 * createDate creator
	 * updateDate updater 
	 * 字段会自动注入值 如不需要注入请修改Wl_ProgramEntity
	 * @param map
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(Map<String, Object> map) {
		String user = SecurityUser.getUser().getId().toString();
		WlProgramEntity entity = new WlProgramEntity();
		// 节目属性，按实体插入
		String name = map.get("name").toString();
		String marker = map.get("marker").toString();
		String type = map.get("type").toString();
		String loopValue = map.get("loopValue").toString();
		int picInterval = Integer.valueOf(map.get("picInterval").toString());
		String fontColor = map.get("fontColor").toString();
		int fontWeight = Integer.valueOf(map.get("fontWeight").toString());
		int textInterval = Integer.valueOf(map.get("textInterval").toString());
		Object datePiker = map.get("datePiker");
		List<String> datePikerList = castList(datePiker, String.class);
		// 字符串转时间
		Date onDatetime = null;
		Date offDatetime = null;
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			onDatetime = formatter.parse(datePikerList.get(0));
			offDatetime = formatter.parse(datePikerList.get(1));
		}catch (Exception e) {
			e.printStackTrace();
		}
		String remark = map.get("remark").toString();
		entity.setId(UUID.randomUUID().toString().replace("-", ""));
		entity.setName(name);
		entity.setMarker(marker);
		entity.setType(type);
		entity.setStatus("0");
		entity.setLoopValue(loopValue);
		entity.setPicInterval(picInterval);
		entity.setFontColor(fontColor);
		entity.setFontWeight(fontWeight);
		entity.setTextInterval(textInterval);
		entity.setOnDatetime(onDatetime);
		entity.setOffDatetime(offDatetime);
		entity.setRemark(remark);
		entity.setCreator(user);
		wlProgramDao.insertEntity(entity);
		// 模板组件属性，根据更改资源值 dragList, picList
		ObjectMapper objectMapper = new ObjectMapper();
		List dragList = (List)map.get("dragList");
		for(int i = 0; i < dragList.size(); i++){
			WlTemplateMoudleEntity wlTemplateMoudleEntity = objectMapper.convertValue(dragList.get(i), WlTemplateMoudleEntity.class);
			// 根据id,修改resourceId
			wlTemplateMoudleEntity.setUpdator(user);
			wlProgramDao.updateMoudleById(wlTemplateMoudleEntity);
		}
		List picList = (List)map.get("picList");
		List<PicEntity> picEntityList = new ArrayList<>();
		if (picList.size()>0) {
			for(int i = 0; i < picList.size(); i++){
				PicEntity picEntity = objectMapper.convertValue(picList.get(i), PicEntity.class);
				picEntity.setCreator(user);
				// 保存
				picEntityList.add(picEntity);
			}
			wlProgramDao.saveAllPicList(picEntityList);
		}
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(Map<String, Object> map) {
	    // 1、更改节目记录 2、更改模板组件资源id 3、若为轮播图，先删除，后添加
        String user = SecurityUser.getUser().getId().toString();
        WlProgramEntity entity = new WlProgramEntity();
        // 节目属性，按实体插入
        String id = map.get("id").toString();
        String name = map.get("name").toString();
        String marker = map.get("marker").toString();
        String type = map.get("type").toString();
        String loopValue = map.get("loopValue").toString();
        int picInterval = Integer.valueOf(map.get("picInterval").toString());
        String fontColor = map.get("fontColor").toString();
        int fontWeight = Integer.valueOf(map.get("fontWeight").toString());
        int textInterval = Integer.valueOf(map.get("textInterval").toString());

		// TODO: 2020/7/31 暂不处理时间问题
//        Object datePiker = map.get("datePiker");
//        List<String> datePikerList = castList(datePiker, String.class);
//        // 字符串转时间
//        Date onDatetime = null;
//        Date offDatetime = null;
//        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        try {
//            onDatetime = formatter.parse(datePikerList.get(0));
//            offDatetime = formatter.parse(datePikerList.get(1));
//        }catch (Exception e) {
//            e.printStackTrace();
//        }

        String remark = map.get("remark").toString();
        entity.setId(id);
        entity.setName(name);
        entity.setMarker(marker);
        entity.setType(type);
        entity.setLoopValue(loopValue);
        entity.setPicInterval(picInterval);
        entity.setFontColor(fontColor);
        entity.setFontWeight(fontWeight);
        entity.setTextInterval(textInterval);
//        entity.setOnDatetime(onDatetime);
//        entity.setOffDatetime(offDatetime);
        entity.setRemark(remark);
        entity.setUpdator(user);
        wlProgramDao.updateById(entity);
        // 模板组件属性，根据更改资源值 dragList, picList
        ObjectMapper objectMapper = new ObjectMapper();
        List dragList = (List)map.get("dragList");
        for(int i = 0; i < dragList.size(); i++){
            WlTemplateMoudleEntity wlTemplateMoudleEntity = objectMapper.convertValue(dragList.get(i), WlTemplateMoudleEntity.class);
            // 根据id,修改resourceId
            wlTemplateMoudleEntity.setUpdator(user);
            wlProgramDao.updateMoudleById(wlTemplateMoudleEntity);
        }
        List picList = (List)map.get("picList");
        List<PicEntity> picEntityList = new ArrayList<>();
        if (picList.size()>0) {
            for(int i = 0; i < picList.size(); i++){
                PicEntity picEntity = objectMapper.convertValue(picList.get(i), PicEntity.class);
                // 1、删除
                wlProgramDao.deleteAllPicList(picEntity.getId().toString());
                picEntity.setCreator(user);
                // 2、保存、
                picEntityList.add(picEntity);
            }
            // 2、保存
            wlProgramDao.saveAllPicList(picEntityList);
        }
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(String[] ids) {
		wlProgramDao.deleteBatchIds(Arrays.asList(ids));
	}
	@Override
	public void exportXls(Map<String, Object> params , HttpServletResponse response)  {
		List<WlProgramEntity> list = this.list(params);
		ExcelUtils.exportExcelToTarget(response, "节目表导出列表", list, WlProgramEntity.class);
	}


	/*
	* object转list
	* */
	public static <T> List<T> castList(Object obj, Class<T> clazz)
	{
		List<T> result = new ArrayList<T>();
		if(obj instanceof List<?>)
		{
			for (Object o : (List<?>) obj)
			{
				result.add(clazz.cast(o));
			}
			return result;
		}
		return null;
	}



	/*
	 * 新get方法
	 * 模板、节目一对多
	 * wl_problem_moudle_resource
	 * */
	@Override
	public Map<String, Object> newGet(String id) {
		// 节目信息
		WlProgramEntity wlProgramEntity = wlProgramDao.selectById(id);
		// 模板、组件信息
		String temId = wlProgramEntity.getType();
		WlTemplateEntity wlTemplateEntity = wlTemplateDao.selectById(temId);
		List<WlProgramMoudleResourceEntity> relationEntityList = wlProgramDao.selectRelationList(id);

		// 图片信息 [现所有资源]
		List<PicEntity> picEntityList = new ArrayList<>();

		WlResourceEntity wlResourceEntity = new WlResourceEntity();
		for (int i = 0; i < relationEntityList.size(); i++) {
			String resourceId = "";
			String type = "";
			type = relationEntityList.get(i).getType();
			resourceId = relationEntityList.get(i).getResourceId();
//			if (resourceId != null && !(resourceId.isEmpty())){
//				wlResourceEntity = wlResourceDao.selectById(resourceId);
//				switch (type) {
//					case "1": relationEntityList.get(i).setRemark(wlResourceEntity.getPath()); break;
//					case "2": relationEntityList.get(i).setRemark(wlResourceEntity.getPath()); break;
//					case "4": relationEntityList.get(i).setRemark(wlResourceEntity.getTextContent()); break;
//					case "5": relationEntityList.get(i).setRemark(wlResourceEntity.getUrlPath());; break;
//					default: System.out.println("type类型为："+type);
//				}
//			}
			picEntityList = wlProgramDao.getPicEntity(resourceId);
			for (int j = 0; j < picEntityList.size(); j++) {
				String resourcePath = "";
				resourcePath = picEntityList.get(j).getResourceId();
				wlResourceEntity = wlResourceDao.selectById(resourcePath);
				String path = "";
				path = wlResourceEntity.getPath();
				picEntityList.get(j).setRemark(path);
			}
		}
		wlTemplateEntity.setDragResourceList(relationEntityList);

		Map<String, Object> map = new HashMap<>();
		map.put("wlProgramEntity", wlProgramEntity);
		map.put("wlTemplateEntity", wlTemplateEntity);
		map.put("picEntityList", picEntityList);
		return map;
	}


	/*
	* 新保存方法
	* 模板、节目一对多
	* wl_problem_moudle_resource保存
	* */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void newSave(Map<String, Object> map) {
		// 节目主信息存储至wl_problem表
		// 最终是将节目、组件、资源的对应关系存储至wl_problem_moudle_resource表
		String user = SecurityUser.getUser().getId().toString();
		WlProgramEntity entity = new WlProgramEntity();

		// 节目属性，按实体插入
		String programId = UUID.randomUUID().toString().replace("-", "");
		String name = map.get("name").toString();
		String marker = map.get("marker").toString();
		String type = map.get("type").toString();
//		String customerId = map.get("customerId").toString();
//		String loopValue = map.get("loopValue").toString();
		Object datePiker = map.get("datePiker");
		List<String> datePikerList = castList(datePiker, String.class);
		// 字符串转时间
		Date onDatetime = null;
		Date offDatetime = null;
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			onDatetime = formatter.parse(datePikerList.get(0));
			offDatetime = formatter.parse(datePikerList.get(1));
		}catch (Exception e) {
			e.printStackTrace();
		}
		String remark = map.get("remark").toString();
		entity.setId(programId);
		entity.setName(name);
		entity.setType(type); // 模板id
		entity.setMarker(marker);
//		entity.setLoopValue(loopValue); // 节目是否循环播放
		entity.setOnDatetime(onDatetime);
		entity.setOffDatetime(offDatetime);
		entity.setRemark(remark);
		entity.setCreator(user);
//		entity.setCustomerId(customerId);
		wlProgramDao.insertEntity(entity);

		// 节目、模板、组件（、资源）关系属性: 节目、模板信息存入
		WlProgramMoudleResourceEntity relationEntity = new WlProgramMoudleResourceEntity();
		relationEntity.setProgramId(programId);
		relationEntity.setTemplateId(type);
		relationEntity.setCreator(user);
		// 节目、模板、组件（、资源）关系属性: 组件（、资源）信息遍历存入
		// 模板组件属性，根据更改资源值 dragList, picList
		ObjectMapper objectMapper = new ObjectMapper();
		List picList = (List)map.get("picList"); // 获取picList
		List<PicEntity> picEntityList = new ArrayList<>();// 修改picList后，保存至此list

		List dragResourceList = (List)map.get("dragResourceList");
		for(int i = 0; i < dragResourceList.size(); i++){
			WlTemplateMoudleEntity wlTemplateMoudleEntity = objectMapper.convertValue(dragResourceList.get(i), WlTemplateMoudleEntity.class);
			String moudleType = wlTemplateMoudleEntity.getType();
			relationEntity.setType(moudleType);
			relationEntity.setX(wlTemplateMoudleEntity.getX());
			relationEntity.setY(wlTemplateMoudleEntity.getY());
			relationEntity.setW(wlTemplateMoudleEntity.getW());
			relationEntity.setH(wlTemplateMoudleEntity.getH());
			relationEntity.setZ(wlTemplateMoudleEntity.getZ());
			relationEntity.setLoopValue(wlTemplateMoudleEntity.getLoopValue());
			relationEntity.setIntervalTime(wlTemplateMoudleEntity.getIntervalTime());
			relationEntity.setFontColor(wlTemplateMoudleEntity.getFontColor());
			relationEntity.setFontWeight(wlTemplateMoudleEntity.getFontWeight());
			// 判断单资源还是轮播图类资源

			String moudleId = wlTemplateMoudleEntity.getId();
			String relationMoudleId = UUID.randomUUID().toString().replace("-", "");
			relationEntity.setResourceId(relationMoudleId);
			if (picList.size()>0) {
				for (int j = 0; j < picList.size(); j++) {
					PicEntity picEntity = objectMapper.convertValue(picList.get(j), PicEntity.class);
					if (moudleId.equals(picEntity.getMoudleId())) {
						picEntity.setMoudleId(relationMoudleId);
						picEntity.setCreator(user);
						// 保存
						picEntityList.add(picEntity);
					}
				}
			}

			// 保存关系实体
			wlProgramDao.saveRealtionEntity(relationEntity);
		}

		// 保存轮播图集合
		if (picEntityList.size()>0) {
			wlProgramDao.saveAllPicList(picEntityList);
		}
	}




	/*
	 * 新更新方法
	 * 模板、节目一对多
	 * wl_problem_moudle_resource更新
	 * */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void newUpdate(Map<String, Object> map) {
		// 1、更改节目记录 2、更改模板组件资源id 3、若为轮播图，先删除，后添加
		// 节目主信息存储至wl_problem表
		// 最终是将节目、组件、资源的对应关系存储至wl_problem_moudle_resource表
		String user = SecurityUser.getUser().getId().toString();
		WlProgramEntity entity = new WlProgramEntity();

		// 节目属性，按实体插入
		String programId = map.get("id").toString();
		String name = map.get("name").toString();
		String marker = map.get("marker").toString();
		String type = map.get("type").toString();
//		String loopValue = map.get("loopValue").toString();
		Object datePiker = map.get("datePiker");
		List<String> datePikerList = castList(datePiker, String.class);
		// 字符串转时间
		Date onDatetime = null;
		Date offDatetime = null;
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(datePikerList.size()==2){
			try {
				onDatetime = formatter.parse(datePikerList.get(0));
				offDatetime = formatter.parse(datePikerList.get(1));
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		String remark = map.get("remark").toString();
		entity.setId(programId);
		entity.setName(name);
		entity.setType(type); // 模板id
		entity.setMarker(marker);
//		entity.setLoopValue(loopValue); // 节目是否循环播放
		entity.setOnDatetime(onDatetime);
		entity.setOffDatetime(offDatetime);
		entity.setRemark(remark);
		entity.setCreator(user);
		wlProgramDao.updateById(entity);

		// 1、删除之前所有已存关系两张表: 关系主表  和 type为3的wl_pic_list
		List<String> wlPidList = wlProgramDao.getPicList(programId);
		wlProgramDao.deleteRealationByProgramId(programId);
		if (wlPidList.size()>0) {
			for( int k = 0; k < wlPidList.size(); k++) {
				wlProgramDao.deletePidByMoudleId(wlPidList.get(k));
			}
		}

		// 2、重新录入：节目、模板、组件（、资源）关系属性: 节目、模板信息存入
		WlProgramMoudleResourceEntity relationEntity = new WlProgramMoudleResourceEntity();
		relationEntity.setProgramId(programId);
		relationEntity.setTemplateId(type);
		relationEntity.setCreator(user);
		// 节目、模板、组件（、资源）关系属性: 组件（、资源）信息遍历存入
		// 模板组件属性，根据更改资源值 dragList, picList
		ObjectMapper objectMapper = new ObjectMapper();
		List picList = (List)map.get("picList"); // 获取picList
		List<PicEntity> picEntityList = new ArrayList<>();// 修改picList后，保存至此list

		List dragList = (List)map.get("dragResourceList");
		for(int i = 0; i < dragList.size(); i++){
			WlTemplateMoudleEntity wlTemplateMoudleEntity = objectMapper.convertValue(dragList.get(i), WlTemplateMoudleEntity.class);
			String moudleType = wlTemplateMoudleEntity.getType();
			relationEntity.setType(moudleType);
			relationEntity.setX(wlTemplateMoudleEntity.getX());
			relationEntity.setY(wlTemplateMoudleEntity.getY());
			relationEntity.setW(wlTemplateMoudleEntity.getW());
			relationEntity.setH(wlTemplateMoudleEntity.getH());
			relationEntity.setZ(wlTemplateMoudleEntity.getZ());
			relationEntity.setLoopValue(wlTemplateMoudleEntity.getLoopValue());
			relationEntity.setIntervalTime(wlTemplateMoudleEntity.getIntervalTime());
			relationEntity.setFontColor(wlTemplateMoudleEntity.getFontColor());
			relationEntity.setFontWeight(wlTemplateMoudleEntity.getFontWeight());
			relationEntity.setProgramId(wlTemplateMoudleEntity.getProgramId());


			String moudleId = wlTemplateMoudleEntity.getId();
			String relationMoudleId = UUID.randomUUID().toString().replace("-", "");
			relationEntity.setResourceId(relationMoudleId);
			if (picList.size()>0) {
				for (int j = 0; j < picList.size(); j++) {
					PicEntity picEntity = objectMapper.convertValue(picList.get(j), PicEntity.class);
					System.out.println("====================picEntity====================="+picEntity.toString());

					picEntity.setMoudleId(relationMoudleId);
					picEntity.setCreator(user);
					// 保存
					picEntityList.add(picEntity);

				}
			}


			// 保存关系实体
			wlProgramDao.saveRealtionEntity(relationEntity);
		}

		// 保存轮播图集合
		if (picEntityList.size()>0) {
			wlProgramDao.saveAllPicList(picEntityList);
		}
	}


	/*
	* tuichudenglu
	* */
	public void logout(Map<String, Object> map){
		// 根据id查取节目实体信息/资源集合
		String programId = map.get("id").toString();
		List<String> list = wlProgramDao.getPicEntityByProgramId(programId);

		List equipList = (List)map.get("data");
		for (int i=0; i<equipList.size(); i++){
			// 根据终端ID，获取终端实体
			String id = "";
			id = String.valueOf(equipList.get(i));
			TerminalEntity terminalEntity = terminalDao.selectById(id);

			// 终端IP地址
			String ip = terminalEntity.getIp();
			// tuichudenglu
			novaLogOut.NovaOperation(ip);
		}
	}

	/*
	* 发布节目
	* */
	public String programStart( Map<String, Object> map){
		String result = "";
		// 根据id查取节目实体信息/资源集合
		String programId = map.get("id").toString();
		List<String> list = wlProgramDao.getPicEntityByProgramId(programId);

		String id = map.get("data").toString();
//		List equipList = (List)map.get("data");

//		for (int i=0; i<equipList.size(); i++){
			// 根据终端ID，获取终端实体
//			String id = "";
//			id = String.valueOf(equipList.get(i));
			TerminalEntity terminalEntity = terminalDao.selectById(id);

			// 终端IP地址
			String ip = terminalEntity.getIp();
			// 发布节目, 判断厂商类型进行节目发布
			if (terminalEntity.getType().equals("0")){
				// 三思厂商
				try {
					result = result+terminalEntity.getName();
					// sansi
					sanSiSystem.sanSiOperation(ip,7211, list) ;

					// 1、修改节目发布状态
					// 2、修改终端发布节目
					WlProgramEntity wlProgramEntity = new WlProgramEntity();
					wlProgramEntity.setId(programId);
					wlProgramEntity.setStatus("1");
					wlProgramDao.updateById(wlProgramEntity);

					TerminalEntity entity = new TerminalEntity();
					entity.setId(Long.parseLong(id));
					entity.setProgram(programId);
					terminalDao.updateById(entity);

					/*
					 * 增加相关记录
					 * */
					Map<String, Object> newMap  = new HashMap<>();
					newMap.put("programId",programId);
					newMap.put("terminalId",id);
					//新增设备时长数据记录
					if(terminalEntity.getOnOff().equals("1")){
						wlProgramHistoryService.save(newMap);
					}

				}catch (Exception e){
					e.printStackTrace();
				}
			}else {
				// 鑫青松厂商：Nova System
				result = result+terminalEntity.getName();
				// 创建节目
				novaSystem.NovaOperation(ip, list);


				// 1、修改节目发布状态
				// 2、修改终端发布节目
				WlProgramEntity wlProgramEntity = new WlProgramEntity();
				wlProgramEntity.setId(programId);
				wlProgramEntity.setStatus("1");
				wlProgramDao.updateById(wlProgramEntity);

				TerminalEntity entity = new TerminalEntity();
				entity.setId(Long.parseLong(id));
				entity.setProgram(programId);
				terminalDao.updateById(entity);

				/*
				 * 增加相关记录
				 * */
				Map<String, Object> newMap  = new HashMap<>();
				newMap.put("programId",programId);
				newMap.put("terminalId",id);
				//新增设备时长数据记录
				if(terminalEntity.getOnOff().equals("1")){
					wlProgramHistoryService.save(newMap);
				}

			}
//		}
		logger.info("============================发布节目返回结果========================================:"+result);
		return result;
	}




	/*
	* 方法待重写
	* */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void newDelete(String[] ids) {
		for(String programId : ids){
			List<String> wlPidList = wlProgramDao.getPicList(programId);
			wlProgramDao.deleteRealationByProgramId(programId);
			if (wlPidList.size()>0) {
				for( int k = 0; k < wlPidList.size(); k++) {
					wlProgramDao.deletePidByMoudleId(wlPidList.get(k));
				}
			}
		}

		wlProgramDao.deleteBatchIds(Arrays.asList(ids));
	}
}