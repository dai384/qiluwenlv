package com.qzsoft.demomis.modules.template.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qzsoft.demomis.modules.program.entity.WlProgramMoudleResourceEntity;
import com.qzsoft.demomis.modules.template.entity.WlTemplateMoudleEntity;
import org.apache.ibatis.annotations.Mapper;
import com.qzsoft.demomis.modules.template.entity.WlTemplateEntity;

import java.util.List;
import java.util.Map;

/**
 * 模板表(Wl_Template)表数据库访问层
 *
 * @author sdmq
 * @since 2020-06-10 17:46:06
 */
@Mapper
public interface WlTemplateDao extends BaseMapper<WlTemplateEntity> {

    int insert(Map<String, Object> map);

    int insertMoudle(List<WlTemplateMoudleEntity> list);

    List<WlTemplateEntity> getPage(Map<String, Object> params);

    int total(Map<String, Object> params);

    WlTemplateEntity selectById(String id);

    List<WlTemplateMoudleEntity> selectModuleList(String id);

    int updateById(Map<String, Object> params);

    int deleteById(String id);

    int deleteMoudle(String id);

    List<WlTemplateEntity> selectList(Map<String, Object> params);

    int selectProgramCount(String id);
}