package com.qzsoft.demomis.modules.program.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.program.entity.WlProgramEntity;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;
/**
 * 节目表(Wl_Program)表服务接口
 *
 * @author sdmq
 * @since 2020-06-10 17:45:48
 */
public interface WlProgramService {
    /**
	 * 数据分页
	 * @param params
	 * @return IPage
	 */
	IPage<WlProgramEntity> page(Map<String, Object> params);
    
	/**
	 * 数据列表
	 * @param params
	 * @return
	 */
	List<WlProgramEntity> list(Map<String, Object> params);

	/**
	 * 单个数据
	 * @param id
	 * @return
	 */
	Map<String, Object> get(String id);

	/**
	 * 保存
	 * @param map
	 */
	void save( Map<String, Object> map);

	/**
	 * 更新
	 * @param map
	 */
	void update( Map<String, Object> map);

	/**
	 * 批量删除
	 * @param ids
	 */
	void delete(String[] ids);

	/**
	 * 批量删除
	 * @param ids
	 */
	void newDelete(String[] ids);

	/**
	 * 导出Excel
	 * @param params
	 * @param response
	 */
	void exportXls(Map<String, Object> params , HttpServletResponse response);



	/**
	 * 单个数据
	 * @param id
	 * @return
	 */
	Map<String, Object> newGet(String id);


	/*
	* 关于对应关系的新保存方法
	* */
	void newSave(Map<String, Object> map);


	/**
	 * 更新
	 * @param map
	 */
	void newUpdate( Map<String, Object> map);


	String programStart( Map<String, Object> map);
	void logout( Map<String, Object> map);
}