package com.qzsoft.demomis.modules.cms.sansi.playlist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 动作执行完后的停留时间。单位为百分之一秒,范围 2-30000,缺省为 2
 *
 * @author frank
 */
public class Delay {
    private static Logger logger = LoggerFactory.getLogger(Delay.class);

    private final int DEFAULT = 2;// 缺省值
    private int value;

    public Delay() {
        value = DEFAULT;
    }

    public Delay(int value) {
        if (value == 0) {
            this.value = value;
        } else {
            if (value < 2 || value > 30000) {
                logger.warn("delay的取值范围为2~30000，现在为 {}，将使用默认值 {}", value, DEFAULT);
                value = DEFAULT;
            }
            this.value = value;
        }
    }

    public int getValue() {
        return value;
    }
}
