package com.qzsoft.demomis.modules.cms.sjctdz;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.qzsoft.demomis.modules.cms.CmsConn;
import com.qzsoft.demomis.modules.cms.CmsException;

import com.qzsoft.demomis.modules.cms.common.Msg;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * Created by frank on 2017/3/7.
 */
public class SjctdzConn extends CmsConn {
    private static String connIp = "localhost";
    private static int connPort = 8809;

    public static Msg getBug(String ip, int port) throws InterruptedException, IOException, CmsException {
        return send(ip, port, 1);
    }

    public static Msg close(String ip, int port) throws InterruptedException, IOException, CmsException {
        return send(ip, port, 2);
    }

    public static Msg open(String ip, int port) throws InterruptedException, IOException, CmsException {
        return send(ip, port, 3);
    }

    public static Msg getLight(String ip, int port) throws InterruptedException, IOException, CmsException {
        return send(ip, port, 4);
    }

    public static Msg setLight(String ip, int port, int light) throws InterruptedException, IOException, CmsException {
        return send(ip, port, 5, String.valueOf(light));
    }

    public static Msg uploadFile(String ip, int port, String content) throws InterruptedException, IOException, CmsException {
        return send(ip, port, 6, content);
    }

    private static Msg send(String ip, int port, int type) throws InterruptedException, IOException, CmsException {
        return send(ip, port, type, null);
    }

    private static synchronized Msg send(String ip, int port, int type, String content) throws InterruptedException, IOException, CmsException {
        String strPort = String.valueOf(port);
        if (StringUtils.isEmpty(content)) {
            content = "";
        }
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        byte[] bb = content.getBytes("utf-8");
        out.writeInt(ip.length() + strPort.length() + bb.length + 9);
        out.write(ip.getBytes());
        out.write(0x0);
        out.write(strPort.getBytes());
        out.write(0x0);
        out.write(String.valueOf(type).getBytes());
        out.write(0x0);
        out.write(bb);
        out.write(0x0);
        //System.out.println(Hex.encodeHexString(out.toByteArray()));
        byte[] bytes = send(connIp, connPort, out.toByteArray());
        ByteArrayDataInput in = ByteStreams.newDataInput(bytes);
        in.readInt();
        String s = in.readLine();
        String[] aa = s.split("\u0000");
        Msg msg = new Msg(Msg.SUCCESS, null);
        if (aa[0].equals("2") || aa[0].equals("3") || aa[0].equals("5") || aa[0].equals("6")) {
            if (aa[1].equals("1")) {
                msg.setMsg("操作成功");
            } else {
                msg.setCode(Msg.FAULT);
                msg.setMsg("操作失败");
            }
        } else if (aa[0].equals("1")) {
            if (aa[1].equals("0")) {
                msg.setMsg("设备无故障");
            } else {
                msg.setCode(Msg.FAULT);
                msg.setMsg("设备故障");
            }
        } else {
            msg.setCode(Integer.parseInt(aa[1]));
            msg.setMsg(aa[2]);
        }
        return msg;
    }
}
