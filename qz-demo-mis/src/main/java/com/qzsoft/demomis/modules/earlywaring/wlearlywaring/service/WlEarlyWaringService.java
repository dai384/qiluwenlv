package com.qzsoft.demomis.modules.earlywaring.wlearlywaring.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.earlywaring.wlearlywaring.entity.WlEarlyWaringEntity;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;
/**
 * 智能箱报警数据推送接收表(WlEarlyWaring)表服务接口
 *
 * @author sdmq
 * @since 2020-08-14 09:44:39
 */
public interface WlEarlyWaringService {
    /**
	 * 数据分页
	 * @param params
	 * @return IPage
	 */
	IPage<WlEarlyWaringEntity> page(Map<String, Object> params);
    
	/**
	 * 数据列表
	 * @param params
	 * @return
	 */
	List<WlEarlyWaringEntity> list(Map<String, Object> params);

	/**
	 * 单个数据
	 * @param id
	 * @return
	 */
	WlEarlyWaringEntity get(String id);

	/**
	 * 保存
	 * @param entity
	 */
	void save(WlEarlyWaringEntity entity);

	/**
	 * 更新
	 * @param entity
	 */
	void update(WlEarlyWaringEntity entity);

	/**
	 * 批量删除
	 * @param ids
	 */
	void delete(String[] ids);

	/**
	 * 导出Excel
	 * @param params
	 * @param response
	 */
	void exportXls(Map<String, Object> params , HttpServletResponse response);

}