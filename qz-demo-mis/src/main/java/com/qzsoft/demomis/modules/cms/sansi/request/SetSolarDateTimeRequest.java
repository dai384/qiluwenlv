package com.qzsoft.demomis.modules.cms.sansi.request;



import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

/**
 * 设置充放电控制器的日期和时间(太阳能屏专用)请求
 *
 * @author frank
 */
public class SetSolarDateTimeRequest extends Request {
    private Date date;

    public SetSolarDateTimeRequest() throws CmsException {
        this.type = int2bytes(22, 2);
        this.address = int2bytes(0, 2);
    }

    public SetSolarDateTimeRequest(int address) throws CmsException {
        this.type = int2bytes(22, 2);
        this.address = int2bytes(address, 2);
    }

    @Override
    protected void setData() throws CmsException, IOException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR) - 2000;
        int week = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (week == 0) {
            week = 7;
        }
        data = new byte[7];
        data[0] = Byte.parseByte(String.valueOf(hour), 16);
        data[1] = Byte.parseByte(String.valueOf(minute), 16);
        data[2] = Byte.parseByte(String.valueOf(second), 16);
        data[3] = Byte.parseByte(String.valueOf(day), 16);
        data[4] = Byte.parseByte(String.valueOf(month), 16);
        data[5] = Byte.parseByte(String.valueOf(year), 16);
        data[6] = Byte.parseByte(String.valueOf(week), 16);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * 数据转义,<br/>
     * 如果日期时间数据中含有 0x02,0x03,则转为 0xf2,0xf3 下发
     *
     * @param bytes
     * @return
     */
    @Override
    protected byte[] esc(byte[] bytes) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        for (byte b : bytes) {
            switch (b) {
                case 0x02:
                    out.write(0xF2);
                    break;
                case 0x03:
                    out.write(0xF3);
                    break;
                default:
                    out.write(b);
                    break;
            }
        }
        return out.toByteArray();
    }

    public static void main(String[] args) {
        SetSolarDateTimeRequest str;
        try {
            str = new SetSolarDateTimeRequest(0);
            str.setDate(new Date());
            System.out.println(CodecUtil.bytesToHexString(str.encoder()));
        } catch (CmsException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
