package com.qzsoft.demomis.modules.cockpit.service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.task.entity.WlTasksEntity;
import com.qzsoft.demomis.modules.template.entity.WlTemplateEntity;

import java.util.List;
import java.util.Map;


public interface LdCockpitService {
    Map<String,Integer> queryCount(Integer index);//echart柱状数据显示
    Map<String,Integer> queryNumber();//头部数量显示
    IPage<WlTasksEntity> page(Map<String, Object> params);
    WlTasksEntity get(String id);
    Map<String,Integer> queryTasksCount();//echart扇形数据显示
    void update(WlTasksEntity entity);//驳回或者通过

    List<WlTasksEntity> getPageByResult(Map<String, Object> params);//按条件分页查询
    int total(Map<String, Object> params);//按条件分页查询数据总量
}
