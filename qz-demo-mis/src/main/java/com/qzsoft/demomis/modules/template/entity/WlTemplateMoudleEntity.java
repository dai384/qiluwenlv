package com.qzsoft.demomis.modules.template.entity;

import java.util.Date;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.*;
import java.io.Serializable;

import com.qzsoft.jeemis.common.validator.group.UpdateGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;


/**
 * 模板组件表(WlTemplateModule)表实体类
 *
 * @author sdmq
 * @since 2020-06-19 17:30:36
 */
@ApiModel(value ="模板组件表")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("wl_template_module")
public class WlTemplateMoudleEntity extends Model<WlTemplateMoudleEntity> {
    private static final long serialVersionUID = -19646502544106377L;
   /**
    *id主键
    */
    @TableId(value ="id",type = IdType.UUID) //主键UUID策略
    @NotNull(message="{id.require}", groups = UpdateGroup.class)
    @Excel(name = "id主键")
    @ApiModelProperty(value = "id主键")
    private String id;
   /**
    *关联模板id
    */
    @Excel(name = "关联模板id")
    @ApiModelProperty(value = "关联模板id")
    private String templateId;
    /**
     *关联资源id
     */
    @Excel(name = "关联资源id")
    @ApiModelProperty(value = "关联资源id")
    private String resourceId;
   /**
    *组件类型(1:图片；2：视频；3：音频；4：文本；5：直播；6：图片集合；7：时钟；8：天气；9：滚动字幕)
    */
    @Excel(name = "组件类型(1:图片；2：视频；3：音频；4：文本；5：直播；6：图片集合；7：时钟；8：天气；9：滚动字幕)")
    @ApiModelProperty(value = "组件类型(1:图片；2：视频；3：音频；4：文本；5：直播；6：图片集合；7：时钟；8：天气；9：滚动字幕)")
    private String type;
   /**
    *x轴位置
    */
    @Excel(name = "x轴位置")
    @ApiModelProperty(value = "x轴位置")
    private int x;
   /**
    *y轴位置
    */
    @Excel(name = "y轴位置")
    @ApiModelProperty(value = "y轴位置")
    private int y;
   /**
    *宽度
    */
    @Excel(name = "宽度")
    @ApiModelProperty(value = "宽度")
    private int w;
   /**
    *高度
    */
    @Excel(name = "高度")
    @ApiModelProperty(value = "高度")
    private int h;
   /**
    *排序
    */
    @Excel(name = "排序")
    @ApiModelProperty(value = "排序")
    private int z;
    /**
     *是否可见
     */
    @Excel(name = "是否可见")
    @ApiModelProperty(value = "是否可见")
    private String showVisible;
    /**
     *背景色
     */
    @Excel(name = "背景色")
    @ApiModelProperty(value = "背景色")
    private String styleColor;
    /**
     *icon
     */
    @Excel(name = "icon")
    @ApiModelProperty(value = "icon")
    private String icon;
    /**
     *1:循环，0：不循环
     */
    @Excel(name = "1:循环，0：不循环")
    @ApiModelProperty(value = "1:循环，0：不循环")
    private String loopValue;
    /**
     *循环时间（单位：s）
     */
    @Excel(name = "循环时间（单位：s）")
    @ApiModelProperty(value = "循环时间（单位：s）")
    private int intervalTime;
    /**
     *字体颜色
     */
    @Excel(name = "字体颜色")
    @ApiModelProperty(value = "字体颜色")
    private String fontColor;
    /**
     *字体大小
     */
    @Excel(name = "字体大小")
    @ApiModelProperty(value = "字体大小")
    private Integer fontWeight;
    /**
     * 组件样式
     */
    @Excel(name = "组件样式")
    @ApiModelProperty(value = "组件样式")
    private String itemStyle;
   /**
    * 组件样式
    */
   @Excel(name = "组件样式")
   @ApiModelProperty(value = "组件样式")
   private String programId;
    /**
   /**
    *备注
    */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remark;
   /**
    *创建人
    */
    @Excel(name = "创建人")
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private String creator;
   /**
    *创建时间
    */
    @Excel(name = "创建时间" , format = "yyyy-MM-dd")
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
   /**
    *编辑人
    */
    @Excel(name = "编辑人")
    @ApiModelProperty(value = "编辑人")
    private String updator;
   /**
    *编辑时间
    */
    @Excel(name = "编辑时间" , format = "yyyy-MM-dd")
   @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "编辑时间")
    private Date updateDate;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}