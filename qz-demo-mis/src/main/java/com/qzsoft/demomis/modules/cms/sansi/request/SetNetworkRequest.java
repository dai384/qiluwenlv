package com.qzsoft.demomis.modules.cms.sansi.request;

import com.qzsoft.demomis.modules.cms.CmsException;
import com.qzsoft.demomis.modules.cms.common.CodecUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * 设置以太网信息请求
 *
 * @author frank
 */
public class SetNetworkRequest extends Request {
    private String mac;// Mac地址，无分隔符
    private String ip;
    private String geteway;
    private String mask;
    private int port;

    public SetNetworkRequest() throws CmsException {
        this.type = int2bytes(31, 2);
        this.address = int2bytes(0, 2);
    }

    public SetNetworkRequest(int address) throws CmsException {
        this.type = int2bytes(31, 2);
        this.address = int2bytes(address, 2);
    }

    @Override
    protected void setData() throws CmsException, IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(mac.getBytes());
        out.write(ipToHex(ip).getBytes());
        out.write(ipToHex(geteway).getBytes());
        out.write(ipToHex(mask).getBytes());
        out.write(int2bytes(port, 5));
        data = out.toByteArray();
        out.close();
    }

    private String ipToHex(String ip) throws CmsException {
        String[] tmp = StringUtils.split(ip, ".");
        if (tmp.length != 4) {
            throw new CmsException("IP格式错误：" + ip);
        }
        byte[] bs = new byte[4];
        for (int i = 0; i < 4; i++) {
            bs[i] = (byte) Integer.parseInt(tmp[i]);
        }
        return CodecUtil.bytesToHexString(bs);
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setGeteway(String geteway) {
        this.geteway = geteway;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
