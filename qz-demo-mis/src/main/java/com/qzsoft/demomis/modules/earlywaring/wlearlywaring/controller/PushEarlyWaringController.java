package com.qzsoft.demomis.modules.earlywaring.wlearlywaring.controller;

import com.alibaba.fastjson.JSONObject;
import com.qzsoft.demomis.modules.earlywaring.wlearlywaring.entity.WlEarlyWaringEntity;
import com.qzsoft.demomis.modules.earlywaring.wlearlywaring.service.WlEarlyWaringService;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.utils.Result;
import com.qzsoft.jeemis.common.validator.ValidatorUtils;
import com.qzsoft.jeemis.common.validator.group.AddGroup;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("public/earlywaring")
public class PushEarlyWaringController {
    @Autowired
    private WlEarlyWaringService wlEarlyWaringService;


    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    public Result save(@RequestBody JSONObject json){
        String desc = json.getString("desc");
        String name = json.getString("name");
        int level = json.getInteger("level");
        String time = json.getString("time");

        WlEarlyWaringEntity dto = new WlEarlyWaringEntity();
        dto.setDescription(desc);
        dto.setName(name);
        dto.setLevel(level);
        dto.setTime(time);

        //效验数据
        wlEarlyWaringService.save(dto);
        return new Result().ok("接收成功");
    }

}
