package com.qzsoft.demomis.modules.sys.notice.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qzsoft.demomis.modules.sys.notice.entity.SysNoticeEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 通知公告(SysNotice)表数据库访问层
 *
 * @author sdmq
 * @since 2019-09-17 20:42:45
 */
@Mapper
public interface SysNoticeDao extends BaseMapper<SysNoticeEntity> {

}