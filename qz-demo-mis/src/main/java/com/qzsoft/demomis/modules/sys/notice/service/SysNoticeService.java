package com.qzsoft.demomis.modules.sys.notice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.sys.notice.entity.SysNoticeEntity;

import java.util.List;
import java.util.Map;

/**
 * 通知公告
 * @author sdmq
 */
public interface SysNoticeService {
	/**
	 * 数据分页
	 * @param params
	 * @return IPage
	 */
	IPage<SysNoticeEntity> page(Map<String, Object> params);

	/**
	 * 数据列表
	 * @param params
	 * @return
	 */
	List<SysNoticeEntity> list(Map<String, Object> params);

	/**
	 * 单个数据
	 * @param id
	 * @return
	 */
	SysNoticeEntity get(String id);

	/**
	 * 保存
	 * @param entity
	 */
	void save(SysNoticeEntity entity);

	/**
	 * 更新
	 * @param entity
	 */
	void update(SysNoticeEntity entity);

	/**
	 * 批量删除
	 * @param ids
	 */
	void delete(String[] ids);

	/**
	 * 发布
	 * @param
	 */
	void publishStart(String[] ids) ;

	/**
	 * 停止
	 * @param ids
	 */
	void publishStop(String[] ids) ;

}
