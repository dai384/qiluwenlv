package com.qzsoft.demomis.modules.programhistory.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.modules.programhistory.entity.WlProgramHistoryInfoEntity;
import com.qzsoft.demomis.modules.programhistory.service.WlProgramHistoryInfoService;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.constant.Constant;
import com.qzsoft.jeemis.common.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;

/**
 * 节目时长表(Wl_Program)表控制层
 *
 * @author sdmq
 * @since 2020-06-10 17:45:47
 */
@RestController
@RequestMapping("programHistoryInfo/programHistoryInfo")
@Api(tags="节目表时长历史表")
public class WlProgramHistoryInfoController {
    @Autowired
    private WlProgramHistoryInfoService wlProgramHistoryInfoService;

	@PostMapping("page")
	@ApiOperation("分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
			@ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
			@ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String") ,
	})
//	@RequiresPermissions("programHistoryInfo:programHistoryInfo:page")
	public Result<IPage<WlProgramHistoryInfoEntity>> page(@ApiIgnore @RequestBody Map<String, Object> params){
		IPage<WlProgramHistoryInfoEntity> page = wlProgramHistoryInfoService.page(params);

		return new Result<IPage<WlProgramHistoryInfoEntity>>().ok(page);
	}

	@GetMapping("list")
	@ApiOperation("列表")
//	@RequiresPermissions("programHistoryInfo:programHistoryInfo:list")
	public Result<List<WlProgramHistoryInfoEntity>> list(@ApiIgnore @RequestParam Map<String, Object> params){
		List<WlProgramHistoryInfoEntity> data = wlProgramHistoryInfoService.list(params);

		return new Result<List<WlProgramHistoryInfoEntity>>().ok(data);
	}


	@GetMapping("info")
	@ApiOperation("信息")
//	@RequiresPermissions("programHistoryInfo:programHistoryInfo:info")
	public Result<Map<String, Object>> newGet(@ApiIgnore @RequestParam("programId") String programId, @RequestParam("equipmentId") String equipmentId){

		Map<String, Object> map = wlProgramHistoryInfoService.get(programId,equipmentId);
		return new Result<Map<String, Object>>().ok(map);
	}

	@PostMapping
	@ApiOperation("保存")
	@LogOperation("保存")
//	@RequiresPermissions("programHistoryInfo:programHistoryInfo:save")
	public Result save(@RequestBody Map<String, Object> map){
		wlProgramHistoryInfoService.save(map);
		return new Result();
	}

	@PutMapping
	@ApiOperation("修改")
	@LogOperation("修改")
//	@RequiresPermissions("programHistoryInfo:programHistoryInfo:update")
	public Result update(@RequestBody Map<String, Object> map){
		wlProgramHistoryInfoService.update(map);
		return new Result();
	}

}