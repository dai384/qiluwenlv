/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.permission.role.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qzsoft.demomis.modules.permission.role.service.SysRoleMenuService;
import com.qzsoft.demomis.repository.sys.dao.SysRoleMenuDao;
import com.qzsoft.demomis.repository.sys.entity.SysRoleMenuEntity;
import com.qzsoft.jeemis.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * 角色与菜单对应关系
 * 
 * @author
 */
@Service
public class SysRoleMenuServiceImpl extends BaseService  implements SysRoleMenuService {

	@Autowired
	SysRoleMenuDao sysRoleMenuDao;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveOrUpdate(Long roleId, List<String> menuIdList) {
		//先删除角色菜单关系
		deleteByRoleIds(new Long[]{roleId});

		//角色没有一个菜单权限的情况
		if(CollUtil.isEmpty(menuIdList)){
			return ;
		}

		//保存角色菜单关系
		for(String menuId : menuIdList){
			SysRoleMenuEntity sysRoleMenuEntity = new SysRoleMenuEntity();
			sysRoleMenuEntity.setMenuId(menuId);
			sysRoleMenuEntity.setRoleId(roleId);

			//保存
			sysRoleMenuDao.insert(sysRoleMenuEntity);
		}
	}

	@Override
	public List<String> getMenuIdList(Long roleId){
		QueryWrapper<SysRoleMenuEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq("role_id",roleId).select("menu_id");
		List<SysRoleMenuEntity> menuList=sysRoleMenuDao.selectList(queryWrapper);
		List<String> menuIdList= menuList.stream().map(obj->{return obj.getMenuId();}).collect(Collectors.toList());
		return menuIdList;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteByRoleIds(Long[] roleIds) {
		QueryWrapper<SysRoleMenuEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.in("role_id",roleIds);
		sysRoleMenuDao.delete(queryWrapper);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteByMenuId(String menuId) {
		QueryWrapper<SysRoleMenuEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq("menu_id",menuId);
		sysRoleMenuDao.delete(queryWrapper);
	}

}