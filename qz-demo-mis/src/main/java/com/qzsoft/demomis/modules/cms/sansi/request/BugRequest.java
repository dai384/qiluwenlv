package com.qzsoft.demomis.modules.cms.sansi.request;

import com.qzsoft.demomis.modules.cms.CmsException;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;

/**
 * 可变信息标志的当前故障请求
 * 
 * @author frank
 * 
 */
public class BugRequest extends Request {

	public BugRequest() throws CmsException {
		this.type = int2bytes(1, 2);
		this.address = int2bytes(0, 2);
	}

	public BugRequest(int address) throws CmsException {
		this.type = int2bytes(1, 2);
		this.address = int2bytes(address, 2);
	}

	@Override
	protected void setData() throws CmsException, IOException {
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
