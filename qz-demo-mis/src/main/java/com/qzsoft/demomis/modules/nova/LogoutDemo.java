package com.qzsoft.demomis.modules.nova;
import novj.platform.vxkit.common.bean.search.SearchResult;
import novj.platform.vxkit.common.result.DefaultOnResultListener;
import novj.platform.vxkit.handy.api.SearchManager;
import novj.publ.api.NovaOpt;
import novj.publ.net.exception.ErrorDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;


/*
 * 退出操作
 * */
@Service
@EnableScheduling
public class LogoutDemo {
    protected static final Logger LOGGER = LoggerFactory.getLogger(LogoutDemo.class);

//        10.2.5.68 滨州
    static String remoteIp = "10.2.185.69";
    public static void main (String[] args){
        LogoutDemo nn = new LogoutDemo();
        nn.NovaOperation(remoteIp);
    }


    public void NovaOperation(String remoteIp) {
        // 1、SDK 初始化: 1=Android 系统环境 2=Windows 或 Linux 系统环境
        NovaOpt.GetInstance().initialize(2);
        // 2、搜索设备
        NovaOpt.GetInstance().searchScreen(new SearchManager.OnScreenSearchListener(){
            @Override
            public void onSuccess(SearchResult searchResult){
                // 发现设备，自行添加处理逻辑
                NovaOpt.GetInstance().connectDevice(searchResult, new DefaultOnResultListener() {
                    @Override
                    public void onSuccess(Integer response) {
                        LOGGER.info("Nova System connect success=================================2==================================：terminal IP is："+remoteIp+"-----------------sn:"+searchResult.sn);
                        // connect success，自行添加处理逻辑
                        // 4、退出设备
                        if (NovaOpt.GetInstance().logOut(searchResult.sn)) {
                            //logout success，自行添加处理逻辑
                            LOGGER.info("=============================================================logout success===============================================================");
                        } else {
                            //logout fail，自行添加处理逻辑
                            LOGGER.info("=============================================================logout fail===============================================================");
                        }
                    }
                    @Override
                    public void onError(ErrorDetail error) {
                        //连接失败，自行添加处理逻辑
                        LOGGER.info("Nova System连接失败===================================================================：terminal IP is："+remoteIp);
                    }
                }, wrapper -> {
                    //连接断开，自行添加处理逻辑
                    LOGGER.info("Nova System连接断开===================================================================：terminal IP is："+remoteIp);
                });
            }
            @Override
            public void onError(ErrorDetail errorDetail){
                // 发生错误，自行添加处理逻辑=
                LOGGER.info("Nova System搜索设备失败===================================================================：搜索terminal IP is："+remoteIp+"; 错误码为："+errorDetail.errorCode+"; 错误描述为："+errorDetail.description);
            }
        }, remoteIp);
    }
}

