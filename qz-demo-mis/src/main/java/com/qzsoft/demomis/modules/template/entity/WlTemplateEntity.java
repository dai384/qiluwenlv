package com.qzsoft.demomis.modules.template.entity;

import java.util.Date;
import java.util.List;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.*;
import com.qzsoft.demomis.modules.program.entity.WlProgramMoudleResourceEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * 模板表(Wl_Template)表实体类
 *
 * @author sdmq
 * @since 2020-06-10 17:46:05
 */
@ApiModel(value ="模板表")
@Data
@EqualsAndHashCode(callSuper=false)
//@TableName("wl_template")
public class WlTemplateEntity extends Model<WlTemplateEntity> {
    private static final long serialVersionUID = -12522973443559199L;
   /**
    *id
    */
    @Excel(name = "id")
    @ApiModelProperty(value = "id")
    private String id;
    /**
     *bg类型
     */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;
   /**
    *bg类型
    */
    @Excel(name = "bg类型")
    @ApiModelProperty(value = "bg类型")
    private String bgType;
    /**
     *是否关注
     */
    @Excel(name = "是否关注")
    @ApiModelProperty(value = "是否关注")
    private String marker;
   /**
    *背景颜色
    */
    @Excel(name = "背景颜色")
    @ApiModelProperty(value = "背景颜色")
    private String bgColor;
    /*
    * 附件组件
    * */
    @Excel(name = "附件组件")
    @ApiModelProperty(value = "附件组件")
    private List<WlTemplateMoudleEntity> dragList;
    /*
     * 附件组件
     * */
    @Excel(name = "附件资源组件")
    @ApiModelProperty(value = "附件组件")
    private List<WlProgramMoudleResourceEntity> dragResourceList;
   /**
    *备注
    */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remark;
   /**
    *创建人
    */
    @Excel(name = "创建人")
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private String creator;
   /**
    *创建时间
    */
    @Excel(name = "创建时间" , format = "yyyy-MM-dd")
   @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
   /**
    *编辑人
    */
    @Excel(name = "编辑人")
    @ApiModelProperty(value = "编辑人")
    private String updator;
   /**
    *编辑时间
    */
    @Excel(name = "编辑时间" , format = "yyyy-MM-dd")
   @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "编辑时间")
    private Date updateDate;

}