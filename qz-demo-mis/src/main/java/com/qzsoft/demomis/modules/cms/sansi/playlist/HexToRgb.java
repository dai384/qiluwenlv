package com.qzsoft.demomis.modules.cms.sansi.playlist;

public class HexToRgb {
	
	//将ff00ff 的颜色格式转换为255000255的格式
	public static Integer getRgb(String s, int n) {
		Integer rgb = 16 * HexDigit2Dec(s.charAt(n)) + HexDigit2Dec(s.charAt(n + 1));
		return rgb;
	}
	
	public static Integer getRed(String s) {
		Integer rgb = 16 * HexDigit2Dec(s.charAt(0)) + HexDigit2Dec(s.charAt( 0 + 1));
		return rgb;
	}
	
	public static Integer getGreen(String s) {
		Integer rgb = 16 * HexDigit2Dec(s.charAt(2)) + HexDigit2Dec(s.charAt(2 + 1));
		return rgb;
	}
	
	public static Integer getBule(String s) {
		Integer rgb = 16 * HexDigit2Dec(s.charAt(4)) + HexDigit2Dec(s.charAt(4 + 1));
		return rgb;
	}
	

	public static Integer HexDigit2Dec(char hex) {
		if ('0' <= hex && hex <= '9') {
			Integer value = Integer.parseInt(String.valueOf(hex));
			return value;
		}
		if (hex == 'A' || hex == 'a')
			return 10;

		if (hex == 'B' || hex == 'b')
			return 11;

		if (hex == 'C' || hex == 'c')
			return 12;

		if (hex == 'D' || hex == 'd')
			return 13;

		if (hex == 'E' || hex == 'e')
			return 14;

		if (hex == 'F' || hex == 'f')
			return 15;
		return 0;
	}
}
