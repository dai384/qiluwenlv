/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.modules.permission.dept.controller;


import com.qzsoft.demomis.modules.permission.dept.excel.SysDeptExcel;
import com.qzsoft.demomis.modules.permission.dept.excel.SysDeptExcel2;
import com.qzsoft.demomis.modules.permission.dept.service.SysDeptService;
import com.qzsoft.demomis.repository.sys.entity.SysDeptEntity;
import com.qzsoft.jeemis.common.annotation.LogOperation;
import com.qzsoft.jeemis.common.utils.ExcelDictHandlerImpl;
import com.qzsoft.jeemis.common.utils.ExcelUtils;
import com.qzsoft.jeemis.common.utils.Result;
import com.qzsoft.jeemis.common.validator.AssertUtils;
import com.qzsoft.jeemis.common.validator.ValidatorUtils;
import com.qzsoft.jeemis.common.validator.group.AddGroup;
import com.qzsoft.jeemis.common.validator.group.DefaultGroup;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 部门管理
 * 
 * @author Mark sunlightcs@gmail.com
 */
@RestController
@RequestMapping("/api/sys/dept")
@Api(tags="部门管理")
public class SysDeptController {
	@Resource
	private SysDeptService sysDeptService;

	@GetMapping("list")
	@ApiOperation("列表")
	@RequiresPermissions("sys:dept:list")
	public Result<List<SysDeptEntity>> list(String pId){
		List<SysDeptEntity> list = sysDeptService.list(pId);

		return new Result<List<SysDeptEntity>>().ok(list);
	}

	@GetMapping("get")
	@ApiOperation("信息")
	@RequiresPermissions("sys:dept:info")
	public Result<SysDeptEntity> get( String pkId){
		SysDeptEntity data = sysDeptService.get(pkId);

		return new Result<SysDeptEntity>().ok(data);
	}

	@PostMapping("save")
	@ApiOperation("保存")
	@LogOperation("保存")
	@RequiresPermissions("sys:dept:save")
	public Result<SysDeptEntity> save(@RequestBody SysDeptEntity dto){
		//效验数据
		ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);
		return new Result<SysDeptEntity>().ok(sysDeptService.saveDept(dto));
	}

	@PostMapping("delete")
	@ApiOperation("删除")
	@LogOperation("删除")
	@RequiresPermissions("sys:dept:delete")
	public Result delete(String pkId){
		//效验数据
		AssertUtils.isNull(pkId, "pkId");

		sysDeptService.delete(pkId);

		return new Result();
	}
	@PostMapping("sort")
	@ApiOperation("排序")
	@LogOperation("排序")
	@RequiresPermissions("sys:dept:save")
	public Result sort(String pkId1,String pkId2){
		//效验数据
		AssertUtils.isNull(pkId1, "pkId1");
		AssertUtils.isNull(pkId2, "pkId2");
		sysDeptService.saveDeptSort(pkId1,pkId2);
		return new Result();
	}


	@GetMapping("export")
	@ApiOperation("导出")
	@LogOperation("导出")
	@RequiresPermissions("sys:dept:list")
	public void export(HttpServletResponse response) throws Exception {
		List<SysDeptExcel> list = sysDeptService.listAll();
		ExcelUtils.exportExcelToTarget(response, "机构列表导出", list, SysDeptExcel.class);
	}

	@GetMapping("export2")
	@ApiOperation("导出")
	@LogOperation("导出")
	@RequiresPermissions("sys:dept:list")
	public void export2(HttpServletResponse response) throws Exception {
		List<SysDeptEntity> list = sysDeptService.listAll2();
		ExcelUtils.exportExcelToTarget(response, "机构列表导出","机构列表","机构",list,  SysDeptExcel2.class,new ExcelDictHandlerImpl());
	}

}