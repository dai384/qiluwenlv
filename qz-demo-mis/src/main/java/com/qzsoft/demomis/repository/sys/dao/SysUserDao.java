/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.repository.sys.dao;


import com.qzsoft.demomis.repository.sys.entity.SysUserEntity;
import com.qzsoft.jeemis.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统用户
 *
 */
@Mapper
public interface SysUserDao extends BaseDao<SysUserEntity> {

}