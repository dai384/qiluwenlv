package com.qzsoft.demomis.repository.sqlmapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.qzsoft.jeemis.repository.sqlmapper.SqlMapper;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

/**
 * @author sdmq
 * @date 2019/10/16 10:22
 */
@Repository
@DS("slave")
public class MySqlMapper extends SqlMapper {

	/**
	 * 构造方法，默认缓存MappedStatement
	 *
	 * @param sqlSession
	 */
	public MySqlMapper(SqlSession sqlSession) {
		super(sqlSession);
	}
}
