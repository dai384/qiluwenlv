package com.qzsoft.demomis.repository.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 部门管理(SysDept)表实体类
 *
 * @author sdmq
 * @since 2019-08-06 09:56:27
 */
@ApiModel(value ="部门管理")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("sys_dept")
public class SysDeptEntity extends Model<SysDeptEntity> {
    private static final long serialVersionUID = 248808962922530762L;
    /**
    *主键主键
    */
    @TableId
    @ApiModelProperty(value = "主键")
    private String pkid;
    /**
    *id步长3 唯一
    */
    @ApiModelProperty(value = "id步长3 唯一")
    private String id;
    /**
    *上级ID
    */
    @ApiModelProperty(value = "上级ID")
    private String pid;
    /**
    *所有上级ID，用逗号分开 废弃
    */
    @ApiModelProperty(value = "所有上级ID，用逗号分开 废弃")
    private String pids;
    /**
    *部门名称
    */
    @ApiModelProperty(value = "部门名称")
    private String name;
    /**
    *1 单位 2 部门  3分组
    */
    @ApiModelProperty(value = "1 单位 2 部门  3分组")
    private String type;
    /**
    *排序默认和id一致为了统一排序方便
    */
    @ApiModelProperty(value = "排序默认和id一致为了统一排序方便")
    private String sort;
    /**
    *简拼
    */
    @ApiModelProperty(value = "简拼")
    private String spell;
    /**
    *是否叶子节点
    */
    @ApiModelProperty(value = "是否叶子节点")
    private Boolean hasLeaf;
    /**
    *创建者
    */
    @ApiModelProperty(value = "创建者")
    private Long creator;
    /**
    *创建时间
    */
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
    /**
    *更新者
    */
    @ApiModelProperty(value = "更新者")
    private Long updater;
    /**
    *更新时间
    */
    @ApiModelProperty(value = "更新时间")
    private Date updateDate;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.pkid;
    }
}