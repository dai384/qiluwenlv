package com.qzsoft.demomis.repository.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 菜单管理(SysMenu)表实体类
 *
 * @author sdmq
 * @since 2019-09-04 16:16:51
 */
@ApiModel(value ="菜单管理")
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("sys_menu")
public class SysMenuEntity extends Model<SysMenuEntity> {
    private static final long serialVersionUID = 904657945749561092L;
    /**
    *id主键
    */
    @TableId
    @ApiModelProperty(value = "id")
    private String id;
    /**
    *id主键
    */
    @ApiModelProperty(value = "")
    private String title;
    /**
    *上级ID，一级菜单为0 步长是3
    */
    @ApiModelProperty(value = "上级ID，一级菜单为0 步长是3")
    private String pid;
    /**
    *菜单URL,在pages下的路径
    */
    @ApiModelProperty(value = "菜单URL,在pages下的路径")
    private String path;
    /**
    *组件与组件名字一致 才能启用缓存 如果名字空则为layout
    */
    @ApiModelProperty(value = "组件与组件名字一致 才能启用缓存 如果名字空则为layout")
    private String component;
    /**
    *组件路径,为了灵活
    */
    @ApiModelProperty(value = "组件路径,为了灵活")
    private String componentpath;
    /**
    *授权(多个用逗号分隔，如：sys:user:list,sys:user:save)
    */
    @ApiModelProperty(value = "授权(多个用逗号分隔，如：sys:user:list,sys:user:save)")
    private String permissions;
    /**
    *类型   0：菜单   1：按钮
    */
    @ApiModelProperty(value = "类型   0：菜单   1：按钮")
    private Object type;
    /**
    *菜单图标
    */
    @ApiModelProperty(value = "菜单图标")
    private String icon;

    /**
    *菜单图标SVG
    */
    @ApiModelProperty(value = "菜单图标SVG")
    private String iconSvg;

    /**
    *排序
    */
    @ApiModelProperty(value = "排序")
    private Integer sort;
    /**
    *是否顶部菜单栏默认无需设置
    */
    @ApiModelProperty(value = "是否顶部菜单栏默认无需设置")
    private Boolean hasTop;
    /**
    *是否停用
    */
    @ApiModelProperty(value = "是否停用")
    private Boolean hasStop;
    /**
    *是否开启页面缓存
    */
    @ApiModelProperty(value = "是否开启页面缓存")
    private Boolean hasCache;
    /**
    *创建者
    */
    @ApiModelProperty(value = "创建者")
    private Long creator;
    /**
    *创建时间
    */
    @ApiModelProperty(value = "创建时间")
    private Date createDate;
    /**
    *更新者
    */
    @ApiModelProperty(value = "更新者")
    private Long updater;
    /**
    *更新时间
    */
    @ApiModelProperty(value = "更新时间")
    private Date updateDate;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}