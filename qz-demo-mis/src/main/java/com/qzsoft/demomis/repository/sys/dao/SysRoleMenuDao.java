/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.repository.sys.dao;

import com.qzsoft.demomis.repository.sys.entity.SysRoleMenuEntity;
import com.qzsoft.jeemis.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色与菜单对应关系
 * 
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface SysRoleMenuDao extends BaseDao<SysRoleMenuEntity> {


}
