package com.qzsoft.demomis.repository.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qzsoft.demomis.repository.sys.entity.SysDeptEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 部门管理(SysDept)表数据库访问层
 *
 * @author sdmq
 * @since 2019-08-06 09:39:12
 */
@Mapper
public interface SysDeptDao extends BaseMapper<SysDeptEntity> {

}