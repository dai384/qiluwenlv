package com.qzsoft.demomis.repository.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qzsoft.demomis.repository.sys.entity.SysMenuEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 菜单管理(SysMenu)表数据库访问层
 *
 * @author sdmq
 * @since 2019-09-04 16:16:28
 */
@Mapper
public interface SysMenuDao extends BaseMapper<SysMenuEntity> {

}