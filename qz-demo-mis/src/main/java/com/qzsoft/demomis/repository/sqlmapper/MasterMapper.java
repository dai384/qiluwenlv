package com.qzsoft.demomis.repository.sqlmapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.qzsoft.jeemis.repository.sqlmapper.SqlMapper;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

/**
 * 非必要类 只为了演示
 * @author sdmq
 * @date 2019/10/16 10:22
 */
@Repository
@DS("master")
public class MasterMapper extends SqlMapper {

	/**
	 * 构造方法，默认缓存MappedStatement
	 *
	 * @param sqlSession
	 */
	public MasterMapper(SqlSession sqlSession) {
		super(sqlSession);
	}
}
