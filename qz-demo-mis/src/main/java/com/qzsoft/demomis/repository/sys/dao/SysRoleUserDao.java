/**
 * Copyright (c) qzsoft All rights reserved.
 *
 * qzsoft.cn
 *
 * 版权所有，侵权必究！
 */

package com.qzsoft.demomis.repository.sys.dao;

import com.qzsoft.demomis.repository.sys.entity.SysRoleUserEntity;
import com.qzsoft.jeemis.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色用户关系
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0
 */
@Mapper
public interface SysRoleUserDao extends BaseDao<SysRoleUserEntity> {


}