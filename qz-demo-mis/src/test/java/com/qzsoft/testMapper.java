package com.qzsoft;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qzsoft.demomis.repository.sqlmapper.MasterMapper;
import com.qzsoft.demomis.repository.sqlmapper.MySqlMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class testMapper {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	MySqlMapper mySqlMapper;
	@Autowired
	MasterMapper masterMapper;

	/**
	 * 多数据源sql执行
	 * 不能加事务 不支持多数据库事务
	 * 建议采用dao
	 */
	@Test
	public void test6() {
		List<Map<String,Object>>  lst= mySqlMapper.selectList("select * from sys_oss");
		logger.info("数据源slave");
		System.out.print(JSONUtil.toJsonStr(lst));

		IPage<Map<String,Object>>  page=masterMapper.selectPage(new Page(1,10),"select * from sys_file_oss");
		logger.info("数据源master");
		System.out.print(JSONUtil.toJsonStr(page));
	}
}
