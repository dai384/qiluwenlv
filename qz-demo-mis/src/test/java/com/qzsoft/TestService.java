package com.qzsoft;

import cn.hutool.json.JSONUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qzsoft.demomis.repository.sys.dao.SysUserDao;
import com.qzsoft.demomis.repository.sys.entity.SysUserEntity;
import com.qzsoft.jeemis.common.redis.RedisUtils;
import com.qzsoft.jeemis.common.service.BaseService;
import com.qzsoft.jeemis.repository.sqlmapper.SqlMapper;
import com.qzsoft.jeemis.repository.sys.entity.BasicUserEntity;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author sdmq
 * @date 2019/11/19 15:21
 */
@Service
public class TestService extends BaseService {
	@Autowired
	SysUserDao sysUserDao;
	@Autowired
	RedisUtils redisUtils;
	@Autowired
	RedisTemplate<String, Object> redisTemplate;
	private final SqlSession sqlSession;
	@Autowired
	JdbcTemplate jdbcTemplate;



	/**
	 * 构造方法
	 * @param sqlSession
	 */
	public TestService(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}

	@Cacheable(cacheNames="test1", key = "#root.targetClass.name +'.' + #root.methodName + '.'+ #p0",sync = true)
	public List<BasicUserEntity> test1(long id){
		Map<String,Object> map=new HashMap<>();
		map.put("order","ASC");
		map.put("orderField","id");
		map.put("page","1");
		map.put("limit","5");
		map.put("id",id);
		//map.put("username",1);
		QueryWrapper queryWrapper= this.getQueryWrapper(map, SysUserEntity.class);
		SqlMapper sqlMapper=new SqlMapper(sqlSession);
		if (queryWrapper.isEmptyOfWhere()) queryWrapper.apply("1=1");
		String strSql = "select * from  sys_user where ${ew.getSqlSegment}";
		System.out.println("---------"+ JSONUtil.toJsonStr(sqlMapper.selectList(strSql,queryWrapper,SysUserEntity.class)));
		return sysUserDao.selectList(queryWrapper);

	}
	@CacheEvict(cacheNames="test1", key = "#root.targetClass.name +'.test1.'+ #p0")
	public  void test1Update(long id){
		// 更新操作

	}

	@Cacheable(cacheNames="test1", key = "'test1_'+ #p0",sync = true)
	public List<BasicUserEntity> test2(long id){
		Map<String,Object> map=new HashMap<>();
		map.put("order","ASC");
		map.put("orderField","id");
		map.put("page","1");
		map.put("limit","5");
		map.put("id",id);
		//map.put("username",1);
		QueryWrapper queryWrapper= this.getQueryWrapper(map, SysUserEntity.class);
		SqlMapper sqlMapper=new SqlMapper(sqlSession);
		if (queryWrapper.isEmptyOfWhere()) queryWrapper.apply("1=1");
		String strSql = "select * from  sys_user where ${ew.getSqlSegment}";
		System.out.println("---------"+ JSONUtil.toJsonStr(sqlMapper.selectList(strSql,queryWrapper,SysUserEntity.class)));
		return sysUserDao.selectList(queryWrapper);

	}


	@Cacheable(cacheNames="test1", key = "#root.targetClass.name +'.' + #root.methodName + '.'+ #p0",sync = true)
	public List<BasicUserEntity> test3(Map<String,Object> map){
		//map.put("username",1);
		QueryWrapper queryWrapper= this.getQueryWrapper(map, SysUserEntity.class);
		SqlMapper sqlMapper=new SqlMapper(sqlSession);
		if (queryWrapper.isEmptyOfWhere()) queryWrapper.apply("1=1");
		String strSql = "select * from  sys_user where ${ew.getSqlSegment}";
		System.out.println("---------"+ JSONUtil.toJsonStr(sqlMapper.selectList(strSql,queryWrapper,SysUserEntity.class)));
		return sysUserDao.selectList(queryWrapper);

	}


	public List<BasicUserEntity> test5(long id){
		Map<String,Object> map=new HashMap<>();
		map.put("order","ASC");
		map.put("orderField","id");
		map.put("page","1");
		map.put("limit","5");
		map.put("id",id);
		//map.put("username",1);
		QueryWrapper queryWrapper= this.getQueryWrapper(map, SysUserEntity.class);
		SqlMapper sqlMapper=new SqlMapper(sqlSession);
		if (queryWrapper.isEmptyOfWhere()) queryWrapper.apply("1=1");
		String strSql = "select * from  sys_user where ${ew.getSqlSegment}";
		System.out.println("---------"+ JSONUtil.toJsonStr(sqlMapper.selectList(strSql,queryWrapper,SysUserEntity.class)));
		List<BasicUserEntity> basicUserEntityList= sysUserDao.selectList(queryWrapper);

		redisTemplate.opsForValue().set(String.valueOf(id) ,basicUserEntityList);
		redisTemplate.opsForValue().get(String.valueOf(id));

		redisTemplate.opsForHash().put("www",String.valueOf(id) ,basicUserEntityList);
		redisTemplate.opsForHash().get("www",String.valueOf(id));

		redisUtils.set("111"+ String.valueOf(id) ,basicUserEntityList,-1);
		redisUtils.hSet("www","111"+ String.valueOf(id) ,basicUserEntityList,300);
		return basicUserEntityList;
	}

	/*public List<SysUserEntity> test6(long id){
		QueryWrapper<SysUserEntity> queryWrapper=new QueryWrapper<>();
		queryWrapper.eq(StringUtils.isNotBlank(value),"sys_message_user.id",value);
		queryWrapper.lt("sys_message_send.id",11);
		queryWrapper.orderByDesc("id");
		String  strSql="select sys_message_user.*,sys_message_send.title,sys_message_send.sender,sys_message_send.sender_name,"+
				"sys_message_send.action,sys_message_send.dept_pkid,sys_message_send.dept_id,sys_message_send.dept_name,"+
				"sys_message_send.receiver_type,sys_message_send.addressee,sys_message_send.addressee_name,"+
				"sys_message_send.type,sys_message_send.module,sys_message_send.node,sys_message_send.has_sms,"+
				"sys_message_send.has_revoke,sys_message_send.send_time"+
				" from sys_message_user inner join sys_message_send on sys_message_user.msg_id=sys_message_send.id "+
				"where ${ew.getSqlSegment}";
		return sqlMapper.selectPage(page,strSql,queryWrapper, SysMessageDTO.class);

		return  sysUserDao.selectList(queryWrapper);
	}
*/


	public List<BasicUserEntity> test15(long id){
		Map<String,Object> map=new HashMap<>();
		map.put("order","ASC");
		map.put("orderField","id");
		map.put("page","1");
		map.put("limit","5");
		map.put("id",id);
		map.put("like_username","1");
		QueryWrapper queryWrapper= this.getQueryWrapper(map, SysUserEntity.class);
		IPage<SysUserEntity> page =this.getPage(map,SysUserEntity.class);
		SqlMapper sqlMapper=new SqlMapper(sqlSession);
		if (queryWrapper.isEmptyOfWhere()) queryWrapper.apply("1=1");
		String strSql = "select * from  sys_user where ${ew.getSqlSegment}";
		System.out.println("---------"+ JSONUtil.toJsonStr(sqlMapper.selectList(strSql,queryWrapper,SysUserEntity.class)));
		sqlMapper.selectListArgs(page,strSql,SysUserEntity.class,queryWrapper);
		return sysUserDao.selectList(queryWrapper);

	}

	@DS("master")
	public void testDataMaster(){
		String strSql="select * from sys_user";
		List<Map<String,Object>> lst= jdbcTemplate.queryForList(strSql);
		System.out.println("--------------------------");
		System.out.println(lst.size());
	}

	@DS("slave")
	public void testDataSlave(){
		String strSql="select * from js_sys_user";
		List<Map<String,Object>> lst= jdbcTemplate.queryForList(strSql);
			System.out.println("--------------------------");
		System.out.println(lst.size());
	}

	@DS("h2")
	@Transactional(rollbackFor = Exception.class)
	public void testDataH2_3(){
		Random rand = new Random();
        //精度要求不高，就用这个计时吧
        long start = System.currentTimeMillis();
        SqlMapper sqlMapper=new SqlMapper(sqlSession);
        String strSql;
        strSql="CREATE TABLE cc (GUID varchar(40) PRIMARY KEY,Data varchar(50),AddTime datetime)";
        sqlMapper.update(strSql);
        strSql="INSERT INTO cc VALUES (RANDOM_UUID(), #{0}, NOW())";

        for(int i = 0;i < 10000;++i){
        	strSql="INSERT INTO cc VALUES (RANDOM_UUID(), '王小虎"+rand.nextInt()  +"', NOW())";
        	sqlMapper.insert(strSql);
        }
         long duration = System.currentTimeMillis() - start;

        System.out.println("插入1万数据耗时:" + duration + " ms");
         for(int i = 0;i < 10000;++i){
        	  strSql="select  count(*) as a from cc where Data like '"+ i +"%'";
			sqlMapper.selectOne(strSql);
        }
         duration = System.currentTimeMillis() - start;

        System.out.println("查询1万次数据耗时:" + duration + " ms");
	}

	@DS("h2")
	public void testDataH2(){
		Random rand = new Random();
        //精度要求不高，就用这个计时吧
        long start = System.currentTimeMillis();
        SqlMapper sqlMapper=new SqlMapper(sqlSession);
        String strSql;
        strSql="CREATE TABLE cc (GUID varchar(40) PRIMARY KEY,Data varchar(40),AddTime datetime)";
        jdbcTemplate.execute(strSql);

        for(int i = 0;i < 10000;++i){
        	  strSql="INSERT INTO cc VALUES (RANDOM_UUID(), '"+ rand.nextInt() +"王小虎', NOW())";
        	jdbcTemplate.execute(strSql);
        }
 		 long duration = System.currentTimeMillis() - start;

        System.out.println("插入1万数据耗时:" + duration + " ms");
         for(int i = 0;i < 10000;++i){
        	  strSql="select  count(*) as a from cc where Data like '"+ i +"%'";
			jdbcTemplate.queryForObject(strSql,Long.class);
        }
        duration = System.currentTimeMillis() - start;

        System.out.println("查询1万次数据耗时:" + duration + " ms");
	}

}
