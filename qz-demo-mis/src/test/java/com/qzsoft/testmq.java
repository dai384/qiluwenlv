package com.qzsoft;


import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ReflectUtil;
import com.qzsoft.jeemis.common.utils.KeyStoreUtil;
import groovy.lang.GroovyClassLoader;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qzsoft.demomis.modules.permission.dept.service.SysDeptService;
import com.qzsoft.demomis.repository.sqlmapper.MasterMapper;
import com.qzsoft.demomis.repository.sqlmapper.MySqlMapper;
import com.qzsoft.demomis.repository.sys.dao.SysDeptDao;
import com.qzsoft.demomis.repository.sys.entity.SysDeptEntity;
import com.qzsoft.jeemis.common.utils.Ten2ThirtyUtils;
import com.qzsoft.jeemis.repository.sqlmapper.SqlMapper;
import com.qzsoft.testdata.TestService2;
import com.qzsoft.testdata.dao.TestUserDao;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngine;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scripting.ScriptEvaluator;
import org.springframework.scripting.ScriptSource;
import org.springframework.scripting.groovy.GroovyScriptEvaluator;
import org.springframework.scripting.support.StaticScriptSource;
import org.springframework.test.context.junit4.SpringRunner;
import redis.clients.jedis.JedisPool;

import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class testmq {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	SqlSessionTemplate sqlSession;
	@Autowired
	SysDeptDao sysDeptDao;
	@Autowired
	SysDeptService sysDeptService;
	@Autowired
	MySqlMapper mySqlMapper;
	@Autowired
	MasterMapper masterMapper;
	@Autowired
	TestService testService;
	@Autowired
	SpringProcessEngineConfiguration processEngineConfiguration;
	@Autowired
	TestUserDao testUserDao;
	@Autowired
	TestService2 testService2;
	@Autowired
	JdbcTemplate jdbcTemplate;
	//@Autowired
	//TestQueue testQueue;
	/*@Autowired
	private  JedisPool jedisPool;*/
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


	@Test
	public void test1() {
		System.out.println( Ten2ThirtyUtils.tenTo36(99,"000"));
		QueryWrapper<SysDeptEntity> query=new QueryWrapper<>();
		query.eq("pid","001x001002").select("max(id) as id").comment("取最大值");
		System.out.println(query.getCustomSqlSegment());
		System.out.println(JSONUtil.toJsonStr(sysDeptDao.selectMaps(query)));
	}

	@Test
	public void test2() {
		for(int i=0;i<1000 ;i++){
			System.out.println(IdWorker.get32UUID());
		}

	}

	/**
	 * 可以传字符串
	 */
	@Test
	public void test3() {
		SqlMapper sqlMapper=new SqlMapper(sqlSession);
		String strSql = "select * FROM sys_user_token  WHERE (expire_date <= '2020-10-14 10:41:50')";
		List<Map<String,Object>> lst= sqlMapper.selectList(strSql);
		System.out.println( lst.size());
	}

	/**
	 * 不可以传字符串
	 */
	@Test
	public void test5() {
		SqlMapper sqlMapper=new SqlMapper(sqlSession);
		String d="2020-10-14 10:41:50";
		//Date d=new Date();
		String strSql = "select * FROM sys_user_token  WHERE (expire_date <= #{0})";
		List<Map<String,Object>> lst= sqlMapper.selectListArgs(strSql,d);
		System.out.println( lst.size());

	}

	/**
	 *
	 */
	@Test
	public void test6() {
		List<Map<String,Object>>  lst= mySqlMapper.selectList("select * from sys_oss");
		log.info("数据源slave");
		System.out.print(JSONUtil.toJsonStr(lst));

		IPage<Map<String,Object>>  page=masterMapper.selectPage(new Page(1,10),"select * from sys_file_oss");
		log.info("数据源master");
		System.out.print(JSONUtil.toJsonStr(page));
	}
	/**
	 *
	 */
	@Test
	public void test7() {
		System.out.println(FileUtil.getMimeType("www.pdf"));
		System.out.println(FileUtil.getMimeType("www.doc"));
		System.out.println(FileUtil.getMimeType("d:\\www.docx"));
		FileNameMap fileNameMap = URLConnection.getFileNameMap();
		String contentType = fileNameMap.getContentTypeFor("d:\\www.docx");
		System.out.println(contentType);
		/*SysCloudDiskDTO dto=new SysCloudDiskDTO();
		SysCloudDiskEntity domain =new SysCloudDiskEntity();
		domain.setId("1111111");
		domain.setHasSystem(true);
		System.out.println(JSONUtil.toJsonStr(dto.asDto(domain)));

		dto.setFileName("wwwwwwwwww");
		dto.setId("1111");
		System.out.println(JSONUtil.toJsonStr(dto.asEntity(SysCloudDiskEntity.class)));*/

	}
	@Test
	public void test9() {
		Map<String,String> map=new HashMap<>();
		map.put("11","11");
		map.put("11","22");
		System.out.println(JSONUtil.toJsonStr(map) );
	}


	@Test
	public void test10() {
		SqlMapper sqlMapper=new SqlMapper(sqlSession);
		QueryWrapper<Object> ew=new QueryWrapper<>();
		Page  page=new Page(1,5);
		OrderItem orderItem= new OrderItem();
		orderItem.setAsc(true);
		orderItem.setColumn("username");
		((Page) page).addOrder(orderItem);
		ew.eq("sys_user.id",1067246875800000001L).like("LOWER(username)","a");
		ew.orderByAsc("id");
		//ew.apply("username={0}","'");
		System.out.println("----------------"+ JSONUtil.toJsonStr(ew.getParamNameValuePairs()));
		System.out.println("----------------"+ ew.getExpression().getSqlSegment());
		System.out.println("-----------.getNormal()-----"+ JSONUtil.toJsonStr(ew.getExpression().getNormal()));
		System.out.println("-----------getCustomSqlSegment-----"+ JSONUtil.toJsonStr(ew.getCustomSqlSegment()));
		System.out.println("-----------getParamNameValuePairs-----"+ JSONUtil.toJsonStr(ew.getParamNameValuePairs().keySet()));
		System.out.println("-----------getParamNameValuePairs-----"+ JSONUtil.toJsonStr(ew.getParamNameValuePairs().values()));
		String strSql = "select * FROM sys_user  WHERE ${ew.getSqlSegment} ";
		List<Map<String,Object>> lst= sqlMapper.selectListArgs(strSql,ew);
		System.out.println( lst.size());

		strSql = "select * FROM sys_user  WHERE ${ew.getSqlSegment} ";
		List<Map<String,Object>> lst2= sqlMapper.selectList(page,strSql,ew);
		System.out.println( lst2.size());

		strSql = "select * FROM ${tbname}  WHERE ${ew.getSqlSegment} ";
		Map<String,Object> map=new HashMap<>();
		map.put("ew",ew);
		map.put("tbname","sys_user");
		List<Map<String,Object>> lst3= sqlMapper.selectList(page,strSql,map);
		System.out.print( "lst3-------");
		System.out.println( lst3.size());

		int i=0;
		String s="1";
		//sqlMapper.getParam2(s);
		//queryWrapper.isNotNull("b");

	}

	@Test
	public void test11() {
		//System.out.println("----"+ JSONUtil.toJsonStr(initDbFieldMapper(SysMessageDTO.class)));
		testService.test1(1067246875800000001L);
		System.out.println("----1-----");
		testService.test1(1067246875800000001L);
		System.out.println("----2-----");
		testService.test1(1067246875800000002L);
		System.out.println("----3-----");
		testService.test1(1067246875800000001L);
		System.out.println("----4-----");
		testService.test1(1067246875800000005L);
		System.out.println("----5-----");
		testService.test2(1067246875800000001L);
		System.out.println("----11-----");
		testService.test1Update(1067246875800000001L);
		System.out.println("----清除缓存-----");


		Map<String,Object> map=new HashMap<>();
		map.put("order","ASC");
		map.put("orderField","id");
		map.put("page","1");
		map.put("limit","5");
		map.put("id",1067246875800000001L);
		testService.test3(map);
		testService.test5(1067246875800000001L);

	}

	@Test
	public void test12() {
		System.out.println("--第一次取数---");
		testService.test1(1067246875800000001L);
		System.out.println("--第二次取数---");
		testService.test1(1067246875800000001L);
		System.out.println("--清缓存---");
		testService.test1Update(1067246875800000001L);
		System.out.println("--第三次取数---");
		testService.test1(1067246875800000001L);
	}

	@Test
	public void test15() {
		testService.test15(1067246875800000001L);
	}

	@Test
	public  void test16(){
		String jdbcUrl= processEngineConfiguration.getJdbcUrl();
		System.out.println(processEngineConfiguration.getDatabaseSchema());
		System.out.println(processEngineConfiguration.getJdbcDriver());
		System.out.println(jdbcUrl );
		ProcessEngine processEngine = processEngineConfiguration.buildProcessEngine();
		log.info("获取流程引擎[{}]",processEngine.getName());
		processEngine.close();
		IdentityService idm= processEngine.getIdentityService();
		idm.createUserQuery().list();
	}

	@Test
	public  void test18(){
		// 单表
		QueryWrapper queryWrapper=new QueryWrapper();
		queryWrapper.eq("id","1067246875800000001");
		testUserDao.getUser(queryWrapper);

		//多表
		queryWrapper=new QueryWrapper();
		//假如从前端传入用户名参数
		String userName= "";
		// 如果传入的参数不为空才组织条件
		queryWrapper.like(StringUtils.isNotBlank(userName),"username",userName);
		// 常规条件
		queryWrapper.eq("sys_user.id","1");
		queryWrapper.like("real_name","a");
		queryWrapper.orderByDesc("sys_user.id");
		if (queryWrapper.isEmptyOfWhere()) queryWrapper.apply("1=1");
		testUserDao.getRoleUser(queryWrapper);
	}

	@Test
	public  void test19(){
		//System.out.println(testInterface);
		//testInterface.get();
		testService2.get();
	}

	@Test
	public void test99() throws InterruptedException, IllegalAccessException, InstantiationException {
		ScriptEvaluator scriptEvaluator = new GroovyScriptEvaluator();
		System.out.println("---------------------------------");
		//ScriptSource scriptSource1=new data
		//ResourceScriptSource 外部的
		//Resource res=new ClassPathResource("TestGroovy.groovy");
		//ScriptSource source2 = new ResourceScriptSource(new EncodedResource(res));
		 GroovyClassLoader d= new  GroovyClassLoader();
		 Class c= d.parseClass("TestGroovy.groovy");
		 System.out.println(JSONUtil.toJsonStr(c.getDeclaredFields()));

		//ReflectUtil.invoke(c, "sayHello");
		/*ScriptSource source = new StaticScriptSource("i+j");
		Map<String, Object> args = new HashMap<>();
		args.put("i", 1);
		args.put("j", 2);
		System.out.println(scriptEvaluator.evaluate(source, args));*/
		//testGroovy.sayHello();
	}

		@Test
	public void test100() throws InterruptedException {
		System.out.println("-----------------------------------");
	/*	Map map= jdbcTemplate.queryForMap("select  *  from test_demo where id=1 ");
		System.out.println(JSON.toJSON(map));*/
		System.out.println("-----------------");
		System.out.println(KeyStoreUtil.getPublicKeyStr());
	}

	@Test
	public void test101() throws InterruptedException {
		System.out.println("-----------------------------------");
		//System.out.println(jedisPool.getResource().toString());
		//testQueue.sendMessage1("消息队列小dd");
		//redisTemplate.getConnectionFactory().getConnection()
		redisTemplate.convertAndSend("主题2","-----22222222222xxxxxxxxxxxx");
		redisTemplate.convertAndSend("topic2","-----11111111111xxxxxxxxxxxx");
		redisTemplate.convertAndSend("topic2","-----222222222222xxxxxxxxxxxx");
		System.out.println("-----------------------------------");

	}


@Test
	public void test102()  {
		//testService.testDataMaster();
		//testService.testDataSlave();
		testService.testDataH2();
	  //testService.testDataH2_3();
	}

	@Test
	public void test103()  {
		SqlMapper sqlMapper=new SqlMapper(sqlSession);
		Map<String,Object> m=new HashMap<>();
		m.put("a","'1','2'");
		String strSql="";
		strSql="select * from  sys_user where  id in (${a}) ";
		sqlMapper.selectListArgs(strSql,m);


	}

}



