package com.qzsoft.testdata.dto;

import com.qzsoft.jeemis.common.annotation.DbField;
import lombok.Data;

/**
 * @author sdmq
 * @date 2019/12/4 13:43
 */
@Data
public class TestUserDto {
	private static final long serialVersionUID = 1L;

	/**
	 * 角色名称
	 */
	private String roleName;

	/**
	 * 角色id
	 */
	@DbField(value = "sys_role_user.role_id")
	private String roleId;

	/**
	 * 角色id
	 */
	@DbField(value = "sys_role_user.user_id")
	private String userId;

	@DbField(value = "sys_user.id")
	private String id;

	/**
	 * 用户名
	 */
	private String username;

	/**
	 * 姓名
	 */
	private String realName;
}
