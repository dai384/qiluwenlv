package com.qzsoft.testdata;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author sdmq
 * @date 2019/12/10 11:21
 */
@Configuration
public class TestConfiguration {


	/*@Bean
	@ConditionalOnMissingBean
	public TestService2 testService1() {
		return new TestService2();
	}*/

	@Bean
	@ConditionalOnClass(TestInterface.class)
	public TestService2 testServicex(TestInterface testInterface) {
		return new TestService2(testInterface);
	}
}
