package com.qzsoft.testdata;

import com.qzsoft.demomis.repository.sys.dao.SysUserDao;
import com.qzsoft.jeemis.common.redis.RedisUtils;
import com.qzsoft.jeemis.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author sdmq
 * @date 2019/11/19 15:21
 */
@Service
public class TestService2 extends BaseService {
	@Autowired
	SysUserDao sysUserDao;
	@Autowired
	RedisUtils redisUtils;
	@Autowired
	RedisTemplate<String, Object> redisTemplate;
	private final TestInterface testInterface;


	public TestService2(TestInterface testInterface) {
		this.testInterface = testInterface;
	}
	/**
	 * 构造方法
	 * @param
	 */
	public TestService2(){this.testInterface =null;}



	public void get(){
		if  (this.testInterface== null) return;
		this.testInterface.get();
	}


}
