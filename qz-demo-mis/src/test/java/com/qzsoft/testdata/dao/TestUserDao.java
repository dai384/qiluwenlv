package com.qzsoft.testdata.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.qzsoft.demomis.repository.sys.entity.SysUserEntity;
import com.qzsoft.jeemis.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 测试
 *
 * @author sdmq
 * @since 2019-11-26 11:29:48
 */
@Mapper
public interface TestUserDao  extends BaseDao<SysUserEntity> {

	/**
	 *
	 * @return
	 */
 	List<Map<String,Object>> getRoleUser(@Param(Constants.WRAPPER) Wrapper wrapper);

 	List<Map<String,Object>> getUser(@Param(Constants.WRAPPER) Wrapper wrapper);
}