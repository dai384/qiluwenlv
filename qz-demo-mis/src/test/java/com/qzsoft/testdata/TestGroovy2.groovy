package com.qzsoft.testdata

import com.qzsoft.TestService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

class TestGroovy2 {
    @Autowired
    TestService testService;
    def String sayHello() {
        def greet = "Hello, world!"
        testService.test1(1);
        System.out.println(greet);
    }
}
