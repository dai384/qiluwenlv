package com.qzsoft;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.crypto.KeyUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.json.JSONUtil;
import com.baomidou.dynamic.datasource.toolkit.CryptoUtils;
import com.qzsoft.jeemis.common.utils.KeyStoreUtil;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;

/**
 * @author sdmq
 * @date 2019/12/23 13:56
 */
public class testmain {

	public static void main(String[] args) throws Exception {
		// 一个加解密例子
		String password = "111111111";
		String encodePassword = CryptoUtils.encrypt(password);
		System.out.println("---------加密后的密码----------");
		System.out.println(encodePassword);

		ClassPathResource resource = new ClassPathResource("qzsoft.jks");
		KeyStore keyStore = SecureUtil.readJKSKeyStore(resource.getStream(), "123456".toCharArray());
		Certificate crt = SecureUtil.readCertificate("PKCS12", resource.getStream(), "123456".toCharArray(), "sdmq");
		byte[] publicKeyString = Base64.encode(crt.getPublicKey().getEncoded(), true);
		String s3 = new String(publicKeyString);
		System.out.println("-------publicKeyString公钥--------");
		System.out.println(s3);
		// 公钥加密
		PrivateKey privateKey = (PrivateKey) keyStore.getKey("sdmq", "123456".toCharArray());
		String w1 = SecureUtil.rsa().setPublicKey(keyStore.getCertificate("sdmq").getPublicKey())
				.encryptBase64("123456", KeyType.PublicKey);
		System.out.println(w1);
		// 私钥解密
		String w2 = SecureUtil.rsa().setPrivateKey(privateKey).decryptStr(w1, KeyType.PrivateKey);
		System.out.println(w2);


	}
	public static void main1(String[] args) throws Exception {
		String password = "Qljt20191029@";

		// String encodePassword = CryptoUtils.encrypt(password);
		//System.out.println(encodePassword);
		String[] arr = CryptoUtils.genKeyPair(512);
		/*System.out.println("privateKey:" + arr[0]);
        System.out.println("publicKey:" + arr[1]);*/
       /* String s= CryptoUtils.encrypt(arr[1], "123456");
        System.out.println("password:" + s);
        System.out.println("password:" + CryptoUtils.decrypt(arr[0], s));*/
		//String s1=SecureUtil.rsa(arr[0],arr[1]).encryptBase64("123456".getBytes("UTF-8"), KeyType.PublicKey);
		//System.out.println(s1);
		String s1 = "gKtVZCAkvdZiwcgb/KbS1g8rjOAXdq5CI1x5yhREhyCSf4e2hYbQPZXRIzzJUxKJ2ZudaZ6GU7pGTW2xC48BeQmvgLOQbNLRw800uwEhuu8/NYIKVKqdNPwF6C6xMAx6pmhRfWUkUPo9NAEVN5TLjPBQScT3nRwt6u+mk7ALNHA=";
		String s2 = SecureUtil
				.rsa("MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKqI66WlSBeHmHu1Rh8fkszXHJe2srRc97QAkkd1BXY3T16gZ8zs2RyZ+0CG9OqJnA4npo4EqJ1TYfi4t0v+3LHUMTeejR+K4IkJgJvLIPK5FfPwgmx0q/pOSI+9QwDT5/WHHpg/GVGc2L/Akt7lgwlcZQPKdNljUgrRLZsUxwddAgMBAAECgYAWlhvfzIxH0YW4nMG0zZG736HB1kjE6qtEA+9b13vptFmmxsf4tEXtK9fDLBy0E5W/qjC9DpDmhWysbIqaXh8Mlv4KLEG0lornhCxO8gN7xZ353/SVLxoZerh+m5PMm0Z3R1G78DRxmAuM3uOUeU+f6dE3k+k8MB4ujlC52W72AQJBAN9BHv45tkXI4CQ+nNrGQyDHmhGcZudbweKhV1xaQzOsjWrkDM7V6y82OvFzzt3+nePeehJuu0nP/YhqL75UINECQQDDjEHZT2APjEYP2douVghHFutGDUXbMjz/S97V3eHAs6nEqdCDXRCl37gsTmNo12+s3zZHdE0DQmLMrXi5DMDNAkBI83eULPmRjhUn4snQdy6iPHbswEWj3gjZ7EymodwJu52uSvygbahfEgJquaPkSHoBZsZO1Z4ffXZkKAaX09exAkEAn+OEy/yc3NmjR76nKQ/rQMU6plKf9bREL7KHh3Me7YN5onX/KgnkXNxqKMAHhGfyxfTU2uP7PFkBk72bYiNNWQJBAMQ9jDTzRhyA9HwUmZDY7ylYZKcujIiIl6vNxkurmoDHgQ+nIIlqV1zEnVC4hV03ZeIPILIeJ4juxAQiRa+OmHU=",
						arr[1]).decryptStr(s1, KeyType.PrivateKey);
		System.out.println(s2);
		//String s2=SecureUtil.rsa(arr[0],arr[1]).decryptStr(s1, KeyType.PrivateKey);
		ClassPathResource resource = new ClassPathResource("qzsoft.jks");
		KeyStore keyStore = SecureUtil.readJKSKeyStore(resource.getStream(), "123456".toCharArray());
		Certificate crt = SecureUtil.readCertificate("PKCS12", resource.getStream(), "123456".toCharArray(), "sdmq");
		byte[] publicKeyString = Base64.encode(crt.getPublicKey().getEncoded(), true);
		String s3 = new String(publicKeyString);
		System.out.println("-------publicKeyString--------");
		System.out.println(s3);
		PrivateKey privateKey = (PrivateKey) keyStore.getKey("sdmq", "123456".toCharArray());
		String w1 = SecureUtil.rsa().setPublicKey(keyStore.getCertificate("sdmq").getPublicKey())
				.encryptBase64("123456", KeyType.PublicKey);
		System.out.println(w1);
		w1 = "BQcWhwYz4c2T235KrV96t6mmnJ07SPcVGvIVrTwx+OYwDV+RyAp2+iS8G5XMQjdpVt5IsBYt9Wbg5WQKfUO6KQ==";
		String w2 = SecureUtil.rsa().setPrivateKey(privateKey).decryptStr(w1, KeyType.PrivateKey);
		System.out.println(w2);


	}
}
