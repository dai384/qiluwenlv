package com.qzsoft;

/**
 * @author sdmq
 * @date 2019/12/20 14:10
 */

import com.qzsoft.jeemis.im.controller.JimMessageController;
import com.qzsoft.jeemis.platform.im.JeeImMessageHelper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
public class TestMvcMq {

	private MockMvc mockMvc;        //SpringMVC提供的Controller测试类
	@Autowired
	JimMessageController jimMessageController;

	@Before
	public void setUp() {
		//MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(jimMessageController).build();
	}

	@Test
	public void testGetUser() throws Exception {

		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/chat/sendtouser")
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.header("token", "97b77e25d0f7991bd441a4ba57416306").param("content", "111111111111111111111")
				.param("to", "admin");

		ResultActions perform = mockMvc.perform(request);
		MvcResult mvcResult = perform.andReturn();
 		perform.andExpect(MockMvcResultMatchers.status().isOk());
		MockHttpServletResponse response = mvcResult.getResponse();
		System.out.println(response.getContentAsString());


	}
}